import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class LogsFile {

	FileWriter archivo;
	//nuestro archivo log
	
	// Array con los mensajes a escribir.
	// Ruta donde escribir el log.
	public void InfoLog(String strLog, String strNombreLog) throws IOException{
		
//		System.out.println("------------------- LogsFile ----- InfoLog ----strLog: (" + strLog + ") strNombreLog: (" + strNombreLog + ")");
	    //A la ruta que se ha recibido,  se concatena la carpeta: Concur_Log
	    //	si no existe la crea.

		String strDate = "";
//		strDate = Ventana.getStrDateFileName();
		strDate = Ventana.obtenerFechayyyyMMdd_HHmmss();
//		System.out.println("3fichero: Ventana.getStrDateFileName:(" + Ventana.getStrDateFileName() + ")");
		String strRuta = "";
		strRuta = Ventana.getStrRutaDestinoArchivos();		
//		System.out.println("3fichero: Ventana.getStrRutaLayOut:(" + Ventana.getStrRutaDestinoArchivos() + ")");
	    strRuta = strRuta + Ventana.getStrCarpetaArchivosLog() + "\\";
//		strRuta = "C:\\Saldos\\";
	    
		//Pregunta el archivo existe, caso contrario crea uno con el nombre log.txt
		String FileName = strRuta + "log_" + strNombreLog + "_" + strDate + ".txt";

		if (new File(FileName).exists()==false){
			File file = new File(strRuta + "log_" + strNombreLog + "_" + strDate + ".txt");
			file.getParentFile().mkdirs();
			archivo = new FileWriter(file, true);
		}
		archivo = new FileWriter(new File(FileName), true);

//		Calendar fechaActual = Calendar.getInstance(); //Para poder utilizar el paquete calendar
		//Empieza a escribir en el archivo

//		archivo.write("["+(String.valueOf(fechaActual.get(Calendar.YEAR))
//		+"/"+String.valueOf(fechaActual.get(Calendar.MONTH)+1)
//		+"/"+String.valueOf(fechaActual.get(Calendar.DAY_OF_MONTH))
//		+" "+String.valueOf(fechaActual.get(Calendar.HOUR_OF_DAY))
//		+":"+String.valueOf(fechaActual.get(Calendar.MINUTE))
//		+":"+String.valueOf(fechaActual.get(Calendar.SECOND)))+"]"+"[INFO]"+strLog+"\r\n");
		Calendar cal = Calendar.getInstance();
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
	    String strDateInfo = sdf.format(cal.getTime());
	    
		archivo.write(Ventana.getStrVersion() + "["+strDateInfo+"]"+"[INFO]"+strLog+"\r\n");
		
		archivo.close(); //Se cierra el archivo
	}//Fin del metodo InfoLog
}	
