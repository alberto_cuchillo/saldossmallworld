import java.lang.Runtime;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
 
import java.io.IOException;

import javax.swing.ComboBoxEditor;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.MessageDigestAlgorithms;

public class Ventana extends javax.swing.JFrame {
  
    private static javax.swing.JButton btnInicio;
    //Consultas---------------------------------------------------------------
    private static javax.swing.JComboBox<String> cbTipoUsuario;
    private static javax.swing.JTextField txtFieldUser;
    private static javax.swing.JTextField txtFieldPassword;
    private static javax.swing.JButton btnIdentificarUsuario;
    private static javax.swing.JTextArea txtAreaResultadoComprobaciones;
    private static javax.swing.JLabel jLabelUser = new javax.swing.JLabel();
    private static javax.swing.JLabel jLabelPassword = new javax.swing.JLabel();

//  private javax.swing.JComboBox<String> cbOpcionesLayout;
    private static javax.swing.JComboBox<String> cbOpcionesLayout;

    private static String strVersion = "v1.0.0";
                                 
    private static javax.swing.JLabel jLabelMensaje = new javax.swing.JLabel();

//    private static javax.swing.JLabel jLabelCabecera;

	private static String strCarpetaArchivosLog = "Saldos_Log";
	
	private static String strOpcionBancoNoSelected = "Seleccionar Banco...";
	private static String strOpcionBancoSelected = "Seleccionar Banco...";

	private static String strOpcionCuentaSelected = "";
	private static String strOpcionDescripcionSelected = "";
	private static String strOpcionDatosIdAgenteSelected = "";
	private static String strOpcionImporteSelected  = "";        	
	private static String strOpcionIdentificadorSelected = "";
	private static String strOpcionFilaPrimerMovimientoSelected = "";
	private static String strOpcionPosicionSaldoSelected = "";
	private static String strOpcionDistanciaSaldoAdatosSelected = "";
	private static String strOpcionOrdenSelected = "";	
	private static String strOpcionPosicionMovimientoSelected = "";	
	private static String strOpcionPosicionFechaSelected = "";

	private static String strRutaDestinoArchivos = "";
	private static String strRutaJAR = "";

	private static SaldosProperties datosSaldosProperties = new SaldosProperties();
	
	private static ArrayList<ArrayList<String>> arrayListBidimSaldosProperties = new ArrayList<ArrayList<String>>();
	private static ArrayList<ArrayList<String>> arrayListBidimTiposUsuario = new ArrayList<ArrayList<String>>();

	//Array para escribir en el HistorialOmnex
	private static ArrayList<String> arrayCortePreparadoParaHistorialOmnex = new ArrayList<String>();
	
	private static LogsFile ficheroLog = new LogsFile();

	private static ArrayList<String> arrayMovimientoSaldo = new ArrayList<String>();    
	
	private static String strNuevoSaldoCuenta = "";
	
	private static int intIndiceCB = 0;

	private static double swDoubleSumatorioImportes = 0;

//	private static String strTextoResultadoComprobaciones = "\n" + "Resultado de las comprobaciones:" + "\n";
	private static String strTextoResultadoComprobaciones = "";

	private static boolean booleanComprobacionCorrecta = true;
	
	private static String strNombreDelArchivoXLS = "";

	private static boolean booleanBtnComprobacionesEnabled = true;
	private static boolean booleanBtnCrearExcelOmnexEnabled = true;

//	private static String strFileExcelAgentes = "C:\\Work\\20170406_Saldos_LaCaixa_BBVA_etc\\agentes.xls";
	private static String strFileExcelAgentes = "";

	private static String strSWMensajesErrorVentana = "";
	private static String strPruebaCodigo = "";
	
	private static ArrayList<String> arrayListSWAgentesValidos = new ArrayList<String>();
	private static ArrayList<ArrayList<String>> arrayListSWBidimSaldosPropertiesCuentas = new ArrayList<ArrayList<String>>();
	private static String strSWrutaDestinoFicherosGenerados = "";
	private static String strSWrutaAgentesActivos = "";
	private static String strTipoConfigInicioErrorSaldosPropertiesFormato = "TipoConfigInicioErrorSaldosPropertiesFormato";
	private static String strTipoConfigComprobacionesCorrectas = "TipoConfigComprobacionesCorrectas";
	private static String strTipoConfigCrearExcelCorrecto = "TipoConfigCrearExcelCorrecto";
	private static String strTipoConfigBotonInicio = "TipoConfigBotonInicio";
	
	private static String strSWletras = "abcdefghijklmn";
	private static String strMsgTxtFieldSaldoSinValor = "No se ha introducido el valor del nuevo saldo final.";
	
	//ArrayList  desde lectura del excel seleccionado.
	private static ArrayList<ArrayList<String>> swArrayBiDimensDelExcel = new ArrayList<ArrayList<String>>();

	private static ArrayList<ArrayList<String>> swArrayDatosParaOmnex = new ArrayList<ArrayList<String>>();

	private static int intSwMovimientos = 0;

	private static Double swDoubleSaldoHistorico = 0.0;

	private static int intSwMovimientosSobran = 0;
	private static int intSwMovimientosNoSobran = 0;
    
	private static String strTextoAreaRutasArchivos = "";

	private static ArrayList<String> arrayStrRutasFicherosGenerados = new ArrayList<>();

	//ArrayList con los movimientos sobrantes del excel seleccionado.
	private static ArrayList<ArrayList<String>> arrayListSWMovimientosSobrantes = new ArrayList<ArrayList<String>>();

	//ArrayList con los movimientos sobrantes del excel seleccionado.
	private static String strRutaFileNameExcelGenerado = "";
    private static String strDate = "";
    private static Calendar cal;
    private static String strRespuestaComprobacionFormatoSaldosProperties = "";
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
    private static String strDateExcelOmnex = "";

    private static String strAjusteSW = "ajusteSW";

    private static ArrayList<ArrayList<String>> arrayListStr10historicos = new ArrayList<ArrayList<String>>();
    private static String strBancoCuenta_Caixa_2200076885 = "La Caixa - Uno Agentes2200076885";
    private static String strBancoCuenta_BBVA_1111111111 = "BBVA1111111111";
    private static String strUsuarioActivado = "";

//    private static String strBancoEnSaldosProperties = "";

    @SuppressWarnings("deprecation")

	public static void main(String args[]) throws IOException{
  
    	//mostrarPantallaIdentificacion();
		try {ficheroLog.InfoLog("arrayListBidimSaldosProperties size: " + arrayListBidimSaldosProperties.size(), "Ventana_03");} catch (IOException e) {e.printStackTrace();}        

		System.out.println("***** Main.");
		strDate = obtenerFechayyyyMMdd_HHmmss();		
    	strRutaJAR = Ventana.class.getProtectionDomain().getCodeSource().getLocation().toString();
   	
    	//*************************************** Cargamos datos en arrayListBidimSaldosProperties ******************************************
	   	if (strSWMensajesErrorVentana.equals("")){	    	
    		System.out.println("strRutaJAR--->: " + strRutaJAR);
	   		try {
	    		arrayListBidimSaldosProperties.clear();
				arrayListBidimSaldosProperties = datosSaldosProperties.getBancos(strRutaJAR);
			} catch (IOException e) {
				// TODO Auto-generated catch block
    			strSWMensajesErrorVentana = "ERROR: * No se pudo cargar datos de cuentas configuradas en saldos_properties";
    			Ventana.acumularStrTextoResultadoComprobaciones("\n" + " (i) ERROR: * No se pudo cargar datos de cuentas configuradas en saldos_properties");
				e.printStackTrace();
			}
//	    	Ventana.acumularStrTextoResultadoComprobaciones("\n" + " SiZE: arrayListBidimSaldosProperties.size: " + arrayListBidimSaldosProperties.size());    		
	   	}

		System.out.println("nov6____" + "arrayListBidimSaldosProperties size: " + arrayListBidimSaldosProperties.size());        
		try {ficheroLog.InfoLog("arrayListBidimSaldosProperties size: " + arrayListBidimSaldosProperties.size(), "nov6____");} catch (IOException e) {e.printStackTrace();}        
	   	
	   	//ENVIADO A PROCESOS INICIALES ----------------

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
 
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Ventana().setVisible(true);
            }
        });
    }

    public Ventana() {
    	initComponents();
    }
 
    @SuppressWarnings("unchecked")
    private void initComponents() {
        
    	btnIdentificarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	btnIdentificarUsuarioActionPerformed(evt);            
            }
        });
        
        btnInicio = new javax.swing.JButton();
        btnIdentificarUsuario = new javax.swing.JButton();
        txtFieldUser = new javax.swing.JTextField();
        txtFieldPassword = new javax.swing.JTextField();
        
        txtAreaResultadoComprobaciones = new javax.swing.JTextArea();

    	try {
   			//cotrebla111
			cbOpcionesLayout = new javax.swing.JComboBox(cargarDatosEncb(arrayListBidimSaldosProperties));
			cbTipoUsuario = new javax.swing.JComboBox(cargarDatosEncbTiposUsuario());
//			cbFormAjustarSaldoCuentaOpcionesCuenta = new javax.swing.JComboBox(cargarDatosEncb(arrayListBidimSaldosProperties));
		} catch (IOException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
//      jLabelMensaje = new javax.swing.JLabel();
        jLabelUser = new javax.swing.JLabel();
        jLabelPassword = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("PRUEBAS " + strVersion + " - SaldosSWConsultas.");

        btnInicio.setText("Inicio");
        btnIdentificarUsuario.setText("Identificar usuario");
        
        txtAreaResultadoComprobaciones.setEditable(false);
        txtAreaResultadoComprobaciones.setVisible(false);

        cbOpcionesLayout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	cbOpcionesLayoutActionPerformed(evt);
            }
        });
     
        jLabelMensaje.setText(" ");
        jLabelUser.setText("Usuario: ");
        jLabelPassword.setText("Password: ");
        
        Font fontjLabelVerdanaBold12 = new Font("Verdana", Font.BOLD,12);
        Font fontjLabelVerdanaBold14 = new Font("Verdana", Font.BOLD,14);
        Font fontjLabelVerdanaBold15 = new Font("Verdana", Font.BOLD,15);
        
        Font fontjLabelCabecera = new Font("Verdana", Font.BOLD,12);
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(43, 43, 43)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnInicio)
                    .addComponent(cbTipoUsuario)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jLabelUser)
                        .addComponent(txtFieldUser))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jLabelPassword)
                        .addComponent(txtFieldPassword))
//                    .addComponent(txtFieldUser)
//                    .addComponent(txtFieldPassword)
                    .addComponent(btnIdentificarUsuario)
					.addGap(18, 18, 18)
                    .addComponent(jLabelMensaje)
					.addGap(18, 18, 18)
                    .addComponent(cbOpcionesLayout)
                    .addGap(18, 18, 18)
                    .addGap(18, 18, 18)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18))                        
//                        .addComponent(cbFormAjustarSaldoCuentaOpcionesCuenta))                        
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18))
//                        .addComponent(btnAbrirExcelGenerado)
//                        .addComponent(btnAceptarCorteOmnex))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(txtAreaResultadoComprobaciones))                        
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)))                        
                .addContainerGap(31, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(btnInicio)
                .addComponent(cbTipoUsuario)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelUser)                		
                    .addComponent(txtFieldUser))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelPassword)                		
                    .addComponent(txtFieldPassword))                    
//                .addComponent(txtFieldUser)
//                .addComponent(txtFieldPassword)                
                .addComponent(btnIdentificarUsuario)
                .addGap(18, 18, 18)
                .addComponent(jLabelMensaje)
                .addGap(18, 18, 18)
                .addComponent(cbOpcionesLayout)
                .addGap(18, 18, 18)             
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE))
//                    .addComponent(cbFormAjustarSaldoCuentaOpcionesCuenta))                      
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE))
//                    .addComponent(btnAbrirExcelGenerado)
//                    .addComponent(btnAceptarCorteOmnex))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtAreaResultadoComprobaciones))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE))
                .addContainerGap(44, Short.MAX_VALUE))
        );        
        //Para mostrar siempre los mensajes,     sin condicionarlo a  que la comprobaci�n sea correcta o no:
//        if (!(getBooleanComprobacionCorrecta())){
        	txtAreaResultadoComprobaciones.setVisible(true);
        	txtAreaResultadoComprobaciones.setText("contenido de prueba");
//        }

    	System.out.println("---FinDeInicio---");
 	
    	mostrarPantallaIdentificacion();
    	    	
//    	swProcesosIniciales();
    	
        pack();
    }// </editor-fold>

    private void btnIdentificarUsuarioActionPerformed(java.awt.event.ActionEvent evt) {

    	String strValorTipoUser = "";
    	String strValorUser = "";
    	String strValorClave = "";
    	String strValorClaveEncriptada = "";
    	
    	comprobarFormatoArchivoClaves();
    	
    	strValorClaveEncriptada = obtenerValorEncriptado(strValorUser);
//    	consultarValidezDeIdentificacion(strTipoUsuario, strUser, strPasswordEncriptada);
    	consultarValidezDeIdentificacion(strValorTipoUser, strValorClave, strValorClaveEncriptada);
    	
    	cotrebla
    	Hay que terminar de preparar y probar este m�todo.
    	
    }
    

	public static void inicializarTxtAreaResultadoComprobaciones(){
//    	strTextoResultadoComprobaciones = "\n" + "Resultado de las comprobaciones:" + "\n";
    	strTextoResultadoComprobaciones = "";
    	txtAreaResultadoComprobaciones.setText("");
	}
    public static void swProcesosIniciales(){
    	
    	inicializarTxtAreaResultadoComprobaciones();
    	txtAreaResultadoComprobaciones.setVisible(true);

		Ventana.acumularStrTextoResultadoComprobaciones("\n Procesos Iniciales.");

    	String strInfoProcesosIniciales = "";
    	
		System.out.println("swProcesosIniciales: ");
//    	strSWMensajesErrorVentana = "";
    	//*****************************************************************************************************************************
		strInfoProcesosIniciales = strInfoProcesosIniciales + "\n" + " " + "Informaci�n - Procesos iniciales: ";
//		Ventana.acumularStrTextoResultadoComprobaciones("\n" + " " + "Informaci�n:");
		
		//***************************************  Si el formato es incorrecto  *******************************************************
    	//**********************************  ---> no se contin�a con los procesos y mostramos en Ventana un mensaje con la incidencia ocurrida
		strInfoProcesosIniciales = strInfoProcesosIniciales + "\n" + " " + ".....Formato Saldos Properties.";
//    	Ventana.acumularStrTextoResultadoComprobaciones("\n" + " " + ".....Formato Saldos Properties.");
    	
		if (strSWMensajesErrorVentana.equals("")){
			try {
				strRespuestaComprobacionFormatoSaldosProperties = datosSaldosProperties.comprobarFormatoValido(strRutaJAR);
			} catch (IOException e2) {
    			strSWMensajesErrorVentana = "ERROR: * No se pudo comprobar el formato del archivo: " + strRutaJAR;
        		acumularStrTextoResultadoComprobaciones("\n" + " (i) ERROR: * No se pudo comprobar el formato del archivo: " + strRutaJAR);				
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
	    	//Si el formato es incorrecto ---> no se contin�a con los procesos y mostramos en Ventana un mensaje con la incidencia ocurrida.
	    	if (!(strRespuestaComprobacionFormatoSaldosProperties.equals(""))){
	    		System.out.println("***** Ventana formato incorrecto");
	    		Ventana.acumularStrTextoResultadoComprobaciones("\n" + "Error **** : " + strRespuestaComprobacionFormatoSaldosProperties);  		
	    		
	    		strSWMensajesErrorVentana = strRespuestaComprobacionFormatoSaldosProperties;
	    	}
	    	
			if (strSWMensajesErrorVentana.equals("")){
				Ventana.acumularStrTextoResultadoComprobaciones("\n (i) SaldosProperties: Formato correcto.");				
			}
	   	}
    	//*****************************************************************************************************************************
    }

	public static ArrayList<ArrayList<String>> generarArrayDesdeArchivoHistorialDeMovimientos(String strArchivo) throws IOException{
		
		ArrayList<ArrayList<String>> arrayListBidimenMovimientos = new ArrayList<ArrayList<String>>();
		ArrayList<String> arrayListMovimiento = new ArrayList<String>();
		
		arrayListMovimiento.add(0, "");
		arrayListMovimiento.add(1, "");
		arrayListMovimiento.add(2, "");
		arrayListMovimiento.add(3, "");

		String strIdAgente = "";
		String strDatos = "";
		String strImporte = "";
		String strSaldo = "";

		int intPosicionInicioIdAgente = 0;
		int intPosicionFinalIdAgente =  0;
		int intPosicionInicioDatos = 0;
		int intPosicionFinalDatos =  0;
		int intPosicionInicioImporte = 0;
		int intPosicionFinalImporte =  0;
		int intPosicionInicioSaldo = 0;
		int intPosicionFinalSaldo =  0;
		
        BufferedReader br = null;

        String line = "";
		System.out.println("11properties-***1 --dato-strRutaProperties (" + strArchivo + ")");
        try {
            br = new BufferedReader(new FileReader(strArchivo));
            int intContMovimientos = 0;
            int contArrayBancoCuentaDesc = 0;
            
            while((line = br.readLine()) != null){           	

        		intPosicionInicioIdAgente = line.indexOf("celda3(") + 7;
        		intPosicionFinalIdAgente =  line.indexOf(")_celda4") - 1;
        		intPosicionInicioDatos = line.indexOf("celda4(") + 7;
        		intPosicionFinalDatos =  line.indexOf(")_celda5") - 1;
        		intPosicionInicioImporte = line.indexOf("celda6(") + 7;
        		intPosicionFinalImporte =  line.indexOf(")_celda7") - 1;
        		intPosicionInicioSaldo = line.indexOf("saldo:") + 6;
        		intPosicionFinalSaldo =  line.length() - 1;

        		strIdAgente = line.substring(intPosicionInicioIdAgente, intPosicionFinalIdAgente + 1);
        		strDatos = line.substring(intPosicionInicioDatos, intPosicionFinalDatos + 1);
        		strImporte = line.substring(intPosicionInicioImporte, intPosicionFinalImporte + 1);
        		strSaldo = line.substring(intPosicionInicioSaldo, intPosicionFinalSaldo + 1);
        		
            	arrayListMovimiento.set(0, strIdAgente);
            	arrayListMovimiento.set(1, strDatos);
            	arrayListMovimiento.set(2, strImporte);
            	arrayListMovimiento.set(3, strSaldo);
	    		arrayListBidimenMovimientos.add(new ArrayList<>(arrayListMovimiento));
            }
        } catch (StringIndexOutOfBoundsException e) {
    		System.out.println("StringIndexOutOfBoundsException *** catch.");
//    		Ventana.setStrSWMensajesErrorVentana("ERROR ***** : Archivo historial: formato incorrecto.");
    		acumularStrTextoResultadoComprobaciones("ERROR ***** : Archivo historial: formato incorrecto.");
        } catch (FileNotFoundException e) {
    		System.out.println("properties-*** catch.");
    		acumularStrTextoResultadoComprobaciones("ERROR ***** : No se ha podido encontrar el archivo historial.");
        } catch (IOException e) {
    		acumularStrTextoResultadoComprobaciones("ERROR ***** : Archivo historial.   Se ha producico un error IO.");
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                	br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

       return arrayListBidimenMovimientos;
	}
	
	private static String[] cargarDatosEncb(ArrayList<ArrayList<String>> arrayListValores) throws IOException{
    	String[] arrayCombo = new String[arrayListValores.size() + 1];
		arrayCombo[0] =	"Seleccione una cuenta:";
		
		String strAbreParenteris = " ";
		String strCierraParenteris = " ";

		System.out.println("-----> " + arrayListValores.size());
		for(int contEle=0;contEle<arrayListValores.size();contEle++){
	    	System.out.println("-----> " + arrayListValores.get(contEle));

			strAbreParenteris = " ";
			strCierraParenteris = " ";
			if (arrayListValores.get(contEle).get(2).length()>0){
				strAbreParenteris = " (";
				strCierraParenteris = ") ";
			}
    		arrayCombo[contEle + 1] =	arrayListValores.get(contEle).get(0) + strAbreParenteris +
									arrayListValores.get(contEle).get(2) + strCierraParenteris +
    								arrayListValores.get(contEle).get(1);
    	}
		return arrayCombo;
	}
	
	private static String[] cargarDatosEncbTiposUsuario() throws IOException{
    	String[] arrayCombo = new String[3];
		arrayCombo[0] =	"Seleccione tipo de usuario:";
		arrayCombo[1] =	"Administrador";
		arrayCombo[2] =	"Operador";
		
		return arrayCombo;
	}

	public static String obtenerFechayyyyMMdd_HHmmss(){
		String strDate = "";
		cal = Calendar.getInstance();
		sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
	    strDate = sdf.format(cal.getTime());
	    return strDate;	
	}
	
	public static void acumularStrTextoResultadoComprobaciones(String msg){
		strTextoResultadoComprobaciones = strTextoResultadoComprobaciones + msg;
//		System.out.println("->->: " + strTextoResultadoComprobaciones);
		txtAreaResultadoComprobaciones.setText(strTextoResultadoComprobaciones);
	}	

	private void cbOpcionesLayoutActionPerformed(java.awt.event.ActionEvent evt) {
    	int indiceCB = cbOpcionesLayout.getSelectedIndex();
    	strOpcionBancoSelected = "";
    	strOpcionCuentaSelected = "";
    	strOpcionDescripcionSelected = "";
    	intIndiceCB = indiceCB;

    	System.out.println("----------------click en el combo.   intIndiceCB = " + intIndiceCB);
    	System.out.println("-- strOpcionBancoSelected: " + strOpcionBancoSelected);
    	System.out.println("-- strOpcionCuentaSelected: " + strOpcionCuentaSelected);
    	System.out.println("-- strOpcionDescripcionSelected: " + strOpcionDescripcionSelected);
    	System.out.println("---------fin----click en el combo.   intIndiceCB = " + intIndiceCB);

    	//indiceCB = 0     es la opci�n 'Seleccione una cuenta'.
    	if (indiceCB>0){
        	//Obtenemos el texto de la opci�n elegida del ComboBox.
        	strOpcionBancoSelected = arrayListBidimSaldosProperties.get(indiceCB-1).get(0).toString();
        	strOpcionBancoSelected = strOpcionBancoSelected.trim();
        	strOpcionCuentaSelected = arrayListBidimSaldosProperties.get(indiceCB-1).get(1).toString();
        	strOpcionCuentaSelected = strOpcionCuentaSelected.trim();        	
        	strOpcionDescripcionSelected = arrayListBidimSaldosProperties.get(indiceCB-1).get(2).toString();
        	strOpcionDescripcionSelected = strOpcionDescripcionSelected.trim();      
        	strOpcionDatosIdAgenteSelected = arrayListBidimSaldosProperties.get(indiceCB-1).get(3).toString();
        	strOpcionDatosIdAgenteSelected = strOpcionDatosIdAgenteSelected.trim();      
        	strOpcionImporteSelected = arrayListBidimSaldosProperties.get(indiceCB-1).get(4).toString();
        	strOpcionImporteSelected = strOpcionImporteSelected.trim();        	
        	strOpcionIdentificadorSelected = arrayListBidimSaldosProperties.get(indiceCB-1).get(5).toString();
        	strOpcionIdentificadorSelected = strOpcionIdentificadorSelected.trim();
        	strOpcionFilaPrimerMovimientoSelected = arrayListBidimSaldosProperties.get(indiceCB-1).get(6).toString();
        	strOpcionFilaPrimerMovimientoSelected = strOpcionFilaPrimerMovimientoSelected.trim();
        	strOpcionPosicionSaldoSelected = arrayListBidimSaldosProperties.get(indiceCB-1).get(7).toString();
        	strOpcionPosicionSaldoSelected = strOpcionPosicionSaldoSelected.trim();
        	strOpcionDistanciaSaldoAdatosSelected = arrayListBidimSaldosProperties.get(indiceCB-1).get(8).toString();
        	strOpcionDistanciaSaldoAdatosSelected = strOpcionDistanciaSaldoAdatosSelected.trim();
        	strOpcionOrdenSelected = arrayListBidimSaldosProperties.get(indiceCB-1).get(9).toString();
        	strOpcionOrdenSelected = strOpcionOrdenSelected.trim();
        	strOpcionPosicionMovimientoSelected = arrayListBidimSaldosProperties.get(indiceCB-1).get(10).toString();
        	strOpcionPosicionMovimientoSelected = strOpcionPosicionMovimientoSelected.trim();
        	strOpcionPosicionFechaSelected = arrayListBidimSaldosProperties.get(indiceCB-1).get(11).toString();
        	strOpcionPosicionFechaSelected = strOpcionPosicionFechaSelected.trim();

        	System.out.println("778899 arrayListBidimSaldosProperties: " + arrayListBidimSaldosProperties.get(indiceCB-1));
        	System.out.println("778899 cabecera(" + arrayListBidimSaldosProperties.get(indiceCB-1).get(7).toString() + ")");
    	}
    }

	public static String getStrVersion() {
		return strVersion;
	}	
	
    public static String getStrCarpetaArchivosLog() {
		return strCarpetaArchivosLog;
	}

	public static String getStrRutaDestinoArchivos() {
		return strRutaDestinoArchivos;
	}	

    public static void comprobarFormatoArchivoClaves(){
    	//**Asignar nuevo valor a la fecha hora	******************************
	    strDate = obtenerFechayyyyMMdd_HHmmss();
	    //********************************************************************
	    strSWMensajesErrorVentana = "";

    	swProcesosIniciales();
    	
//    	desactivar usuarios
    	strUsuarioActivado = "";
    	

    	
    	inicializarTxtAreaResultadoComprobaciones();
    	txtAreaResultadoComprobaciones.setVisible(true);

		strSWMensajesErrorVentana = "";
		if (strSWMensajesErrorVentana.equals("")){
			try {
				strRespuestaComprobacionFormatoSaldosProperties = datosSaldosProperties.comprobarFormatoValido(strRutaJAR);
			} catch (IOException e2) {
    			strSWMensajesErrorVentana = "ERROR: * No se pudo comprobar el formato del archivo: " + strRutaJAR;
        		acumularStrTextoResultadoComprobaciones("\n" + " (i) ERROR: * No se pudo comprobar el formato del archivo: " + strRutaJAR);				
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
	    	//Si el formato es incorrecto ---> no se contin�a con los procesos y mostramos en Ventana un mensaje con la incidencia ocurrida.
	    	if (!(strRespuestaComprobacionFormatoSaldosProperties.equals(""))){
	    		System.out.println("***** Ventana formato incorrecto");
	    		Ventana.acumularStrTextoResultadoComprobaciones("\n" + "Error **** : " + strRespuestaComprobacionFormatoSaldosProperties);  		
	    		
	    		strSWMensajesErrorVentana = strRespuestaComprobacionFormatoSaldosProperties;
	    	}
	    	
			if (strSWMensajesErrorVentana.equals("")){
				Ventana.acumularStrTextoResultadoComprobaciones("\n (i) SaldosProperties: Formato correcto.");				
			}
	   	}
    	//*****************************************************************************************************************************
    }

    public static void mostrarPantallaIdentificacion(){
    	btnInicio.setEnabled(true);
    	btnInicio.setVisible(true);
    	cbTipoUsuario.setEnabled(true);
    	cbTipoUsuario.setVisible(true);
    	txtFieldUser.setEnabled(true);
    	txtFieldUser.setVisible(true);
    	txtFieldPassword.setEnabled(true);
    	txtFieldPassword.setVisible(true);
    	
    	cbOpcionesLayout.setEnabled(true);
    	cbOpcionesLayout.setVisible(false);
    	txtAreaResultadoComprobaciones.setEnabled(false);
    	txtAreaResultadoComprobaciones.setVisible(false);
    	
    	btnIdentificarUsuario.setEnabled(true);
    	btnIdentificarUsuario.setVisible(true);
    }
    
    public static String obtenerValorEncriptado(String strValor){
    	String strRetorno = "";

    	MessageDigest md;
    	try {
    		md = MessageDigest.getInstance(MessageDigestAlgorithms.MD5);
        
    		//Aqu� indicamos la clave que se ha introducido
//    		md.update("texto a cifrar".getBytes());
    		md.update(strValor.getBytes());
            byte[] digest = md.digest();
            // Se escribe byte a byte en hexadecimal
            for (byte b : digest) {
               System.out.print(Integer.toHexString(0xFF & b));
            }
            System.out.println();

            // Se escribe codificado base 64. Se necesita la librer�a
            // commons-codec-x.x.x.jar de Apache
            byte[] encoded = Base64.encodeBase64(digest);
            System.out.println(new String(encoded));
            strRetorno = String.valueOf(encoded);
    	} catch (NoSuchAlgorithmException e1) {
    		// TODO Auto-generated catch block
    		strRetorno = strValor;
    		e1.printStackTrace();
    	}
    	
    	return strRetorno;
    }
    public static boolean consultarValidezDeIdentificacion(String strTipoUsuario, String strUser, String strPasswordEncriptada){
       	boolean booleanRetorno = false;
    	
        System.out.print("1111**********************************************************************************");

        MessageDigest md;
    	try {
    		md = MessageDigest.getInstance(MessageDigestAlgorithms.MD5);
            md.update("texto a cifrar".getBytes());
            byte[] digest = md.digest();
            // Se escribe byte a byte en hexadecimal
            for (byte b : digest) {
               System.out.print(Integer.toHexString(0xFF & b));
            }
            System.out.println();

            // Se escribe codificado base 64. Se necesita la librer�a
            // commons-codec-x.x.x.jar de Apache
            byte[] encoded = Base64.encodeBase64(digest);
            System.out.println(new String(encoded));
      			
    	} catch (NoSuchAlgorithmException e1) {
    		// TODO Auto-generated catch block
    		e1.printStackTrace();
    	}

        System.out.print("2222**********************************************************************************");
    	return booleanRetorno;
    }    
}