import java.awt.peer.SystemTrayPeer;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormat;


public class ExcelAcumuladoMensual {

	ArrayList<String> arrayListMovimientosDelCorte = new ArrayList<>();
	private static String strBancoDelCorte = "";
	private static String strCuentaDelCorte = "";
	String strNombrePrimerDeposito = "";
	String strNombreUltimoDeposito = "";

	int intDepositosEscritos = 0;
	String strRuta = "";
	
	public ExcelAcumuladoMensual(ArrayList<String> arrayListDatos,   String strBanco,   String strCuenta) {
		arrayListMovimientosDelCorte = arrayListDatos;
		strBancoDelCorte = strBanco;
		strCuentaDelCorte = strCuenta; 
		int intUltimaPosicion = -1;
		
		if (arrayListMovimientosDelCorte.size()>0){
			intUltimaPosicion = arrayListMovimientosDelCorte.size() - 1;

			strNombrePrimerDeposito = obtenerNombreArchivoAcumulado(arrayListMovimientosDelCorte.get(0));
			strNombreUltimoDeposito = obtenerNombreArchivoAcumulado(arrayListMovimientosDelCorte.get(intUltimaPosicion));
		}
		
		String strNombreArchivoExcelParaAcumular = "";
/*
		leer el primer deposito del array

		if (	si la fecha del primer deposito tiene fecha de un mes que ya tiene archivo excel ----> entonces acumulamos en el archivo existente){
			strNombreArchivoExcelParaAcumular = (consultamos el nombre del archivo donde haya que acumular.  Ordenado decreciente: el primero que aparezca.)
		}else
		
		//el resto de depositos tambien se acumulan en el mismo archivo donde se haya acumulado el primer deposito){
		for(int contEle = 1;contEle<arrayListDatos.size();contEle++){

		}

		if (el ultimo deposito tiene fecha de un mes distinto  al primer deposito del array -------> entonces  creamos otro excel y acumulamo todo el 
			array en �l tambi�n)	{
			
			for(  acumular todo el array en el nuevo excel)
			
			
		}
*/		
	}

	public static String ConsultarSiExisteArchivoParaDeposito(String strNombreArchivoAcumuladoMes) {
		//*********** Se obtiene en un array todos los nombres de los archivos de la carpeta de acumulados mensuales
		//*********** 						de ese banco y cuenta,    para ese mes y a�o.
		String strRuta = "";
		strRuta = Ventana.getStrRutaDestinoArchivos();

		String strFileEncontrado = "";
		ArrayList<String> arrayFiles = new ArrayList<String>();
		
		// Aqu� la carpeta donde queremos buscar
		String path = strRuta;
		path = strRuta + "Historial_Omnex/Archivos_Mensuales" + "/";

		String files;
		File folder = new File(path);
		File[] listOfFiles = folder.listFiles();
				
		for (int i = 0; i < listOfFiles.length; i++){
				if (listOfFiles[i].isFile())         
				{
//					files = listOfFiles[i].getName();
//					if (files.startsWith("mensual_")){
//						arrayFiles.add(strRuta + "/" + files);
//					}
					files = listOfFiles[i].getName();
					
					if (files.equals(strNombreArchivoAcumuladoMes)){
						strFileEncontrado = strRuta + "/" + files + ".xls";
					}
				}				
		}

		return strFileEncontrado;
	}	

	public static String obtenerNombreArchivoAcumulado(String strLineaDeposito) {
		boolean booleanRetorno = false;
		int intPosicion_Celda5 = -1;
		int intPosicion_Celda6 = -1;

		String str1_banco = strBancoDelCorte;
		String str2_cuenta = strCuentaDelCorte;
		String str3_mouth = "";
		String str4_year = "";
		String strNombreArchivo = "";
		String strFileEncontradoEnRuta = "";

		String strDateExcelOmnex = "";
		ArrayList<String> arrayListFilesRuta = new ArrayList<>();

		intPosicion_Celda5 = strLineaDeposito.lastIndexOf("_celda5");
		intPosicion_Celda6 = strLineaDeposito.lastIndexOf("_celda6");

		str1_banco = str1_banco.trim();
		str1_banco = str1_banco.replace(" ", "");
		
		str2_cuenta = str2_cuenta.trim();
		str2_cuenta = str2_cuenta.replace(" ", "");

		
		if(intPosicion_Celda5 < 0 || intPosicion_Celda6 < 0){
			strNombreArchivo = "";
		}else{
			str3_mouth = strLineaDeposito.substring(intPosicion_Celda5 + 8, intPosicion_Celda6 - 9);
			str4_year = strLineaDeposito.substring(intPosicion_Celda5 + 14, intPosicion_Celda6 - 1);
			strNombreArchivo = "mensual_" + str1_banco.trim() + "-" + str2_cuenta + "-" + str3_mouth + str4_year + ".xls";
		}

		System.out.println("str1_banco: " + str1_banco);
		System.out.println("str2_cuenta: " + str2_cuenta);
		System.out.println("str3_mouth: " + str3_mouth);
		System.out.println("str4_year: " + str4_year);

		System.out.println(strNombreArchivo);
		System.out.println("-----------------------------------------------------------------------");

		return strNombreArchivo;
	}
	
	public static boolean crearArchivoExcelMensual(String strNombreArchivo) {
		String strRutaCompletaNuevoArchivo = "";
		
		try {
        	// Se crea el libro
        	HSSFWorkbook libro = new HSSFWorkbook();
        	// Se crea una hoja dentro del libro
        	HSSFSheet hoja = libro.createSheet();

        	//Escribimos la cabecera.
        	HSSFRow fila0 = hoja.createRow(0);
        	HSSFCell celda1Cabecera = fila0.createCell(0);
        	HSSFCell celda2Cabecera = fila0.createCell(1);
        	HSSFCell celda3Cabecera = fila0.createCell(2);
        	HSSFCell celda4Cabecera = fila0.createCell(3);
        	HSSFCell celda5Cabecera = fila0.createCell(4);
        	HSSFCell celda6Cabecera = fila0.createCell(5);
        	HSSFCell celda7Cabecera = fila0.createCell(6);
        	HSSFCell celda8Cabecera = fila0.createCell(7);
        	HSSFCell celda9Cabecera = fila0.createCell(8);
        	celda1Cabecera.setCellValue("BankNumber");
        	celda2Cabecera.setCellValue("Bank Name");
        	celda3Cabecera.setCellValue("Bank Account");
        	celda4Cabecera.setCellValue("Agent");
        	celda5Cabecera.setCellValue("Notes");
        	celda6Cabecera.setCellValue("Date");
        	celda7Cabecera.setCellValue("Amount");
        	celda8Cabecera.setCellValue("Currency");
        	celda9Cabecera.setCellValue("Type Code");

        	String stringMensajesExcepcionesAgentes = "";
        	Ventana.setStrRutaFileNameExcelGenerado(Ventana.getStrRutaDestinoArchivos() + "Omnex_" + Ventana.getStrOpcionBancoSelected() + "_" + Ventana.getStrOpcionDescripcionSelected() + "_" + Ventana.getStrDateFileName() + ".xls");
            
        	strRutaCompletaNuevoArchivo = Ventana.getStrRutaDestinoArchivos() + "Historial_Omnex" + "/" + "Archivos_Mensuales" + "/" + strNombreArchivo;

        	System.out.println("11 a --->--> getStrRutaDestinoArchivos: " + Ventana.getStrRutaDestinoArchivos());
        	System.out.println("11 b --->--> strRutaCompletaNuevoArchivo: " + strRutaCompletaNuevoArchivo);
        	
        	FileOutputStream elFichero = new FileOutputStream(strRutaCompletaNuevoArchivo);
        	System.out.println("1111---->--> getStrRutaDestinoArchivos: " + Ventana.getStrRutaDestinoArchivos());

        	libro.write(elFichero);

        	System.out.println("3---->--> getStrRutaDestinoArchivos: " + Ventana.getStrRutaDestinoArchivos());
        	System.out.println("4---->--> strRutaCompletaNuevoArchivo: " + strRutaCompletaNuevoArchivo);

            elFichero.close();
        } catch (Exception e) {
            //e.printStackTrace();
            return false;
        }
		return true;
    }

	public static boolean acumularDepositosDelArrayEnArchivoXls(ArrayList<String> arrayListDepositos,String strNombreArchivo) {
		boolean booleanRetorno = false;
		boolean booleanFilaNula = false;

    	String strRutaCompletaNuevoArchivo = "";

    	System.out.println("11 into---->--> strNombreArchivo: " +strNombreArchivo);
    	System.out.println("22 into---->--> arrayListDepositos size: " + arrayListDepositos.size());
    	
		try {
	    	strRutaCompletaNuevoArchivo = Ventana.getStrRutaDestinoArchivos() + "Historial_Omnex" + "/" + "Archivos_Mensuales" + "/" + strNombreArchivo;
	    	System.out.println("33 into---->--> strRutaCompletaNuevoArchivo: " + strRutaCompletaNuevoArchivo);
			FileInputStream file = new FileInputStream(strRutaCompletaNuevoArchivo);
	    	HSSFWorkbook libro = new HSSFWorkbook(file);
	    	HSSFSheet sheet = libro.getSheetAt(0);
	    	Cell cell = null;
	    	HSSFRow sheetRow = sheet.getRow(20);
	    	HSSFRow sheetRowNula = sheet.getRow(0);
	    	//Retrieve the row and check for null
            if(sheetRow == null){
                sheetRow = sheet.createRow(20);
            }

	    	System.out.println("44 into---->--> sheetRow.getRowNum(): " + sheetRow.getRowNum());            
            
//            //Update the value of cell
//            cell = sheetRow.getCell(4);
//            if(cell == null){
//                cell = sheetRow.createCell(4);
//            }
//            cell.setCellValue("Pass1");
            
            int intContFilas = 0;
            int intFilaNula = 0;
            while(!booleanFilaNula){

            	System.out.println("*** while into---->--> sheetRow.getRowNum(): " + sheetRow.getRowNum());            
    	    	System.out.println("-------------------------- " + strNombreArchivo);
            	sheetRowNula = sheet.getRow(intContFilas);    	    		

            	
            	
    	    	sheetRowNula = sheet.getRow(intContFilas);
    	    	//Retrieve the row and check for null
                if(sheetRowNula == null){
    	    		intFilaNula = intContFilas;

    	    		sheetRowNula = sheet.createRow(intFilaNula);
    	    		//Update the value of cell
    	            cell = sheetRowNula.getCell(0);
    	            if(cell == null){
    	                cell = sheetRowNula.createCell(0);
    	            }
    	            cell.setCellValue("Pass1" + intContFilas);
    	    		booleanFilaNula = true;
                }
            	intContFilas++;
            }

            System.out.println("55 into---->--> intContFilas: " + intContFilas);            
            System.out.println("55 into---->--> intFilaNula: " + intFilaNula);            

            file.close();

//          try {
//				Thread.sleep(1500);
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
            
            FileOutputStream outFile =new FileOutputStream(new File(strRutaCompletaNuevoArchivo));
            libro.write(outFile);
            outFile.close();
            
        } catch (FileNotFoundException e) {
           e.printStackTrace();
        } catch (IOException e) {
           e.printStackTrace();
        }
		return booleanRetorno;
	}		
}
