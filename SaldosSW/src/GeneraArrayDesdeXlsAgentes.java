
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream.GetField;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.record.ArrayRecord;
import org.apache.poi.ss.usermodel.Cell;

public class GeneraArrayDesdeXlsAgentes {
	//Ruta Archivo:		Desde Ventana consultaremos la ruta que seleccion�, donde se encuentra el archivo excel del banco
	//Banco:			Desde Ventana consultaremos el banco desde el que se descarg� el archivo excel.
	//						dependiendo del banco, las posiciones de las celdas variar�.
	
	private static LogsFile ficheroLog = new LogsFile();
	private static int intUltimoRow = 0;
	private static ArrayList<ArrayList<String>> arrayListAgentesActivosEUR = new ArrayList<ArrayList<String>>();
	
	public GeneraArrayDesdeXlsAgentes() throws IOException{
    }

    @SuppressWarnings("resource")
	public ArrayList<String> generar() throws IOException{

//		Ventana.acumularStrTextoResultadoComprobaciones("\n Aviso 1 generar   GeneraArrayDesdeXlsAgentes");

		ArrayList<String> arrayRowsValidas = new ArrayList<String>();
		
//		String sRuta = "C:\\Work\\20170406_Saldos_LaCaixa_BBVA_etc\\agentes.xls";
//		String sRuta = "C:\\SW\\agentes.xls";
//		sRuta = Ventana.getStrRutaArchivoXlsBanco();

//		String sRuta = Ventana.getStrFileExcelAgentes();
		
//		String sRuta = "C:\\Work\\agentes.xls";
//		String sRuta = Ventana.getStrRutaDestinoArchivos() + "Historial_Omnex\\" + "agentes.xls";
//		String sRuta = "C:\Saldos_SW_Archivos\Historial_Omnex\agentes.xls";

//		String sRuta = "C:/Saldos_SW_Archivos/Historial_Omnex/agentes.xls";
//		String sRuta = "//md-filesrv/md-LaFinca/Operaciones/SALDOS/PRUEBACAIXA/Historial_Omnex/agentes.xls";
		String sRuta = Ventana.getStrRutaDestinoArchivos() + "Historial_Omnex/" + "agentes.xls";
		
//		sRuta = "C:/Work/agentes.xls";
//		sRuta = "C:/Saldos_SW_Archivos/agentes.xls";

//ok	sRuta = "C:/Saldos_SW_Archivos/Historial_Omnex/agentes.xls";

//		sRuta = Ventana.getStrRutaJAR() + "agentes.xls";
		sRuta = Ventana.getStrSWrutaAgentesActivos() + "agentes.xls";

		
		Ventana.setStrPruebaCodigo(sRuta + "+++");
		
		//cotrebla
//		sRuta = "//md-filesrv/md-LaFinca/Operaciones/SALDOS/PRUEBACAIXA/Historial_Omnex/agentes.xls";
//		sRuta = "//md-filesrv/md-LaFinca/Operaciones/SALDOS/PRUEBACAIXA/Historial_Omnex/agentes.xls";
//		sRuta = "//md-filesrv/md-LaFinca/Operaciones/SALDOS/PRUEBACAIXA/ArchivosGenerados/agentes.xls";

		FileInputStream file = new FileInputStream(new File(sRuta));
		// Crear el objeto que tendra el libro de Excel
		HSSFWorkbook workbook = new HSSFWorkbook(file);
		try {
			workbook = new HSSFWorkbook(file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
//			Ventana.acumularStrTextoResultadoComprobaciones("\n ERROR **** : Agentes: Catch. Error IO.");
			e.printStackTrace();
		}
//		try {workbook = new HSSFWorkbook(file);} catch (Exception e) {Ventana.acumularStrTextoResultadoComprobaciones("\n *A* Catch");}

		HSSFSheet sheet = workbook.getSheetAt(0);	
    	System.out.println("Agentes1: " + workbook.getSheetAt(0).getSheetName());
    	System.out.println("Agentes2: " + workbook.getSheetAt(0).getFirstRowNum());
    	System.out.println("Agentes3: " + workbook.getSheetAt(0).getLastRowNum());
    	System.out.println("Agentes4: " + sheet.getRow(3).getCell(1).getStringCellValue());
    	intUltimoRow = workbook.getSheetAt(0).getLastRowNum();
		arrayRowsValidas = procesarHoja(sheet);
	
		workbook.close();
		return	arrayRowsValidas;
	}
    
    @SuppressWarnings("deprecation")
	public static ArrayList<String> procesarHoja(HSSFSheet sheet) throws IOException{
		
    	ArrayList<String> arrayListSWResultado = new ArrayList<String>();

    	int intNumeroFilaPrimerMovimientoDesdeCero = 11;
    	int intNumeroFilaUltimoMovimientoDesdeCero = intUltimoRow;
    	
		//--------------- Inicio del for	-   para evaluar todos los rows del Excel ------------------
		ArrayList<String> arrayRow = new ArrayList<>();
		arrayRow.add("");
		arrayRow.add("");
		arrayRow.add("");
		//Ventana.acumularStrTextoResultadoComprobaciones("\n" + " Info: N�mero de Filas de movimientos en el archivo: " + intMovimientos);

		for (int contInicio = intNumeroFilaPrimerMovimientoDesdeCero ;contInicio<=intNumeroFilaUltimoMovimientoDesdeCero ;contInicio++){
			
			//-----------------------------------------------------------------------------------------------------------------------------------------
   	    	if(sheet.getRow(contInicio).getCell(0).getStringCellValue().toString().equals("Active")
   	    			&& 
   	    			sheet.getRow(contInicio).getCell(1).getStringCellValue().toString().equals("EUR")){
   	   	    	arrayListSWResultado.add(sheet.getRow(contInicio).getCell(3).getStringCellValue().toString());   	    		
   	    	}
		}

		//--mostrar---Escribir en Logs
		for(int contEle = 0; contEle<arrayListSWResultado.size();contEle++){
//			System.out.println("q1arrayListSWResultado contEle: " + contEle + " valor: " + arrayListSWResultado.get(contEle));
		}
		return arrayListSWResultado;
    }
}
