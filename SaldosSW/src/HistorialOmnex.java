import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class HistorialOmnex {

	FileWriter archivo;
	
	public int grabarCorte(String strMovimiento, String strFechaArchivo) throws IOException{

		int intDepositosEscritos = 0;
		
		Calendar cal = Calendar.getInstance();
		
		String strRuta = "";
		strRuta = Ventana.getStrRutaDestinoArchivos();		
	    strRuta = strRuta + Ventana.getStrCarpetaHistorialOmnex() + "\\";
	    
	    //Preparar la fecha
//	    SimpleDateFormat sdFormatFileName = new SimpleDateFormat("yyyyMMdd");
//	    String strDateInfoFileName = sdFormatFileName.format(cal.getTime());

//		String FileName = "C:/Saldos_SW_Archivos/Historial_Omnex/Cuenta" + Ventana.getStrOpcionCuentaSelected() + "/HistorialOmnex.txt";
//		String FileName = strRuta + "Cuenta" + Ventana.getStrOpcionCuentaSelected() + "/HistorialOmnex_" + strDateInfoFileName + ".txt";
		String FileName = strRuta + "Cuenta" + Ventana.getStrOpcionCuentaSelected() + "/HistorialOmnex_" + strFechaArchivo + ".txt";

//	    System.out.println("*info hoho FileName:" + FileName);
//	    System.out.println("*info hoho Cuenta:" + Ventana.getStrOpcionCuentaSelected());
	
		if (new File(FileName).exists()==false){
		    System.out.println("*No existe:" + FileName);
//			File file = new File(strRuta + ".txt");
			File file = new File(FileName);
		    System.out.println("*No existe------- strRuta:" + strRuta);
		    
			file.getParentFile().mkdirs();
		    System.out.println("*No existe      file:" + file);
		    
			archivo = new FileWriter(file, true);
		    System.out.println("*No existe      archivo:" + archivo);
		}
		archivo = new FileWriter(new File(FileName), true);

		SimpleDateFormat sdformatoDate = new SimpleDateFormat("yyyyMMdd_HHmmss");
	    String strDateInfo = sdformatoDate.format(cal.getTime());
	    
		archivo.write("["+strDateInfo+"]"+"[INFO]"+strMovimiento+"\r\n");
		intDepositosEscritos++;
		
		archivo.close(); //Se cierra el archivo
		
		return intDepositosEscritos;
	}//Fin del metodo InfoLog

	public void escribirSaldo(String strMovimiento) throws IOException{

    	System.out.println("escribirSaldo: " + strMovimiento);

		String strRuta = "";
		strRuta = Ventana.getStrRutaDestinoArchivos();		
	    strRuta = strRuta + Ventana.getStrCarpetaHistorialOmnex() + "\\";
//		strRuta = "C:\\Saldos\\";
	    
		//Pregunta el archivo existe, caso contrario crea uno con el nombre log.txt
		String FileName = strRuta + "Saldo_Historico" + ".txt";

//	    System.out.println("info hoho FileName:" + FileName);
	
		if (new File(FileName).exists()==false){
			File file = new File(strRuta + ".txt");
			file.getParentFile().mkdirs();
			archivo = new FileWriter(file, true);
		}
		archivo = new FileWriter(new File(FileName), true);

//		Calendar fechaActual = Calendar.getInstance();

		Calendar cal = Calendar.getInstance();
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
	    String strDateInfo = sdf.format(cal.getTime());
	    
		archivo.write("["+strDateInfo+"]"+"[INFO]"+strMovimiento+"\r\n");
		archivo.close(); //Se cierra el archivo
	}

	public String obtenerUltimoSaldoDeCuenta(String strCuenta) throws IOException{

		String strUltimoMovimientoDeLaCuenta = "";
		int intPosicionSaldo = 0;
    	int intLongitudMovimiento = 0;
    	
		String strultimoSaldo = "";
		//Obtendremos   la ruta que indica     el archivo   Saldo_Historico.txt
		
		String strRutaProperties = "\\" + Ventana.getStrRutaDestinoArchivos() + "Historial_Omnex/" + "Saldo_Historico.txt";
		
    	System.out.println("line line line obtenerUltimoSaldoDeCuenta 6");

        BufferedReader br = null;
        String line = "";
//    	strRutaProperties = "//md-filesrv/md-LaFinca/Operaciones/SALDOS/PRUEBACAIXA/Historial_Omnex/Saldo_Historico.txt";

    	try {
            br = new BufferedReader(new FileReader(strRutaProperties));
            while((line = br.readLine()) != null){           	

               	if (!(line.indexOf(")_celda2(" + strCuenta + ")_celda3(")<0)) {
                		strUltimoMovimientoDeLaCuenta = line;
               	}
            }
        	
			intPosicionSaldo = strUltimoMovimientoDeLaCuenta.lastIndexOf("saldo:");
			intLongitudMovimiento = strUltimoMovimientoDeLaCuenta.length();
        	
       		if (intPosicionSaldo>=0){
				strultimoSaldo = strUltimoMovimientoDeLaCuenta.substring(intPosicionSaldo, intLongitudMovimiento);           			
       		}

        } catch (FileNotFoundException e) {
        	Ventana.acumularStrTextoResultadoComprobaciones("\n" + " Error **** : FileNotFoundException");        	
//          e.printStackTrace();
        } catch (IOException e) {
        	Ventana.acumularStrTextoResultadoComprobaciones("\n" + " Error **** : IOException");        	
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                	br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
		return strultimoSaldo;
	}	
}	
