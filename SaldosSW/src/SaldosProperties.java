
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class SaldosProperties {
	//Descripcion:	Se consulta el archivo saldos_properties.txt y desde los datos que lea
	//				se asignan valores a las variables de Ventana:
	//				

	private static LogsFile ficheroLog = new LogsFile();
	private static String strConfiguracionCuenta = "";
	
//	public String getNombresBancos() throws IOException{
/*	public String getStrConfiguracionCuenta(){
		return strConfiguracionCuenta;
	}
*/	
	public ArrayList<String> getNombresBancos(){
		ArrayList<String> arrayListStrBancos = new ArrayList<String>();
		
		System.out.println("Clase SaldosProperties. Metodo getNombresBancos. arrayListStrBancos:(" + arrayListStrBancos+")");
		return arrayListStrBancos;
	}
	
	public String getCuentaBanco(String strBanco){
	
		System.out.println("Clase SaldosProperties. Metodo getCuentaBanco. strBanco:(" + strBanco+")");
		return strBanco;
	}
	
	public ArrayList<ArrayList<String>> getBancos(String strRutaJAR) throws IOException{

		ArrayList<ArrayList<String>> arrayBidimenStrBancosCuentaDesc = new ArrayList<ArrayList<String>>();
		ArrayList<String> arrayStrBancosCuentaDesc = new ArrayList<String>();
		
		arrayStrBancosCuentaDesc.add(0, "");
		arrayStrBancosCuentaDesc.add(1, "");
		arrayStrBancosCuentaDesc.add(2, "");
		arrayStrBancosCuentaDesc.add(3, "");
		arrayStrBancosCuentaDesc.add(4, "");
		arrayStrBancosCuentaDesc.add(5, "");
		arrayStrBancosCuentaDesc.add(6, "");
		arrayStrBancosCuentaDesc.add(7, "");
		arrayStrBancosCuentaDesc.add(8, "");
		arrayStrBancosCuentaDesc.add(9, "");
		arrayStrBancosCuentaDesc.add(10, "");
		arrayStrBancosCuentaDesc.add(11, "");
		
		String strBancoNombre = "";
		String strBancoCuenta = "";
		String strDescripcionDeCuenta = "";
		String strIdAgente = "";
		String strImporte = "";
		String strIdentificador = "";
		String strFilaPrimerMovimiento = "";
		String strPosicionSaldo = "";
		String strDistanciaSaldoAdatos = "";
		String strOrden = "";
		String strPosicionMovimiento = "";
		String strPosicionFecha = "";		
		int intPosicionInicioBancoNombre = 0;
		int intPosicionFinalBancoNombre = 0;
		int intPosicionInicioBancoCuenta = 0;
		int intPosicionFinalBancoCuenta = 0;
		int intPosicionInicioBancoDescripcion = 0;
		int intPosicionFinalBancoDescripcion = 0;
		int intPosicionInicioCeldaDelIdAgente = 0;
		int intPosicionFinalCeldaDelIdAgente = 0;
		int intPosicionInicioCeldaDelImporte = 0;
		int intPosicionFinalCeldaDelImporte =  0;
		int intPosicionInicioIdentificador = 0;
		int intPosicionFinalIdentificador =  0;
		int intPosicionInicioFilaPrimerMovimiento = 0;
		int intPosicionFinalFilaPrimerMovimiento =  0;
		int intPosicionInicioPosicionSaldo = 0;
		int intPosicionFinalPosicionSaldo =  0;
		int intPosicionInicioDistanciaSaldoAdatos = 0;
		int intPosicionFinalDistanciaSaldoAdatos =  0;
		int intPosicionInicioOrden = 0;
		int intPosicionFinalOrden =  0;
		int intPosicionInicioMovimiento = 0;
		int intPosicionFinalMovimiento =  0;
		int intPosicionInicioFecha = 0;
		int intPosicionFinalFecha =  0;
		

		//Obtendremos   la ruta que indica     el archivo   saldos_properties.txt
		String strLocalizacionJAR = "";

		//El usuario debe situar el     archivo    saldos_properties.txt     donde haya ubicado		el archivo	.jar
		//Localizamos la ruta donde se encuentra    												el archivo	.jar

//		String strFileNameRutaTotal = "";
//    	strFileNameRutaTotal = Ventana.class.getProtectionDomain().getCodeSource().getLocation().toString();
    	strRutaJAR = Ventana.class.getProtectionDomain().getCodeSource().getLocation().toString();
	
		System.out.println("properties-***0 --dato-strRutaJAR (" + strRutaJAR + ")");
    	
    	String[] elemsFileNameRutaTotal = strRutaJAR.split("file:/");

		String strRutaTotal = elemsFileNameRutaTotal[1].toString();
		int lengthCadenaRuta = strRutaTotal.length();

		int intPosicionFileNameJar = strRutaTotal.toString().lastIndexOf("/");
		strLocalizacionJAR = strRutaTotal.substring(0, intPosicionFileNameJar+1);
		
		String strRutaProperties = strLocalizacionJAR + "saldos_properties.txt";
		strRutaProperties = "\\" + strRutaProperties;

		System.out.println("Separador:"+File.separator);
		System.out.println("Separador strRutaProperties:"+strRutaProperties);
		
        BufferedReader br = null;

        String line = "";
		System.out.println("properties-***1 --dato-strRutaProperties (" + strRutaProperties + ")");
		
        try {
    		System.out.println("-***2 --dato-strRutaProperties (" + strRutaProperties + ")");
            br = new BufferedReader(new FileReader(strRutaProperties));
            int intContBancos = 0;
            boolean booleanLineaComentario = false;
            int contArrayBancoCuentaDesc = 0;
            
			while((line = br.readLine()) != null){
            	booleanLineaComentario = false;
  
        		//*** Si el archivo tenia las l�neas de datos con el formato correcto:
               	if (line.charAt(0)=='*'){
               		booleanLineaComentario = true;
               	}
               	
               	if (!booleanLineaComentario){
	        		//*** Si el archivo tenia las l�neas de datos con el formato correcto:
	               	if (!(line.indexOf("banco:(")<0) && !(line.indexOf(")banco*")<0) && !(line.indexOf("cuenta:(")<0) && 
	               			!(line.indexOf(")cuenta*")<0) && !(line.indexOf("descripcion:(")<0) && !(line.indexOf(")descripcion*")<0)){
	                	System.out.println("-*-*-*-*-*-*-*-*-*-* FORMATO CORRECTO");        	
	               		intPosicionInicioBancoNombre = line.indexOf("banco:(") + 7;
	            		intPosicionFinalBancoNombre = line.indexOf(")banco*") - 1;
	            		intPosicionInicioBancoCuenta = line.indexOf("cuenta:(") + 8;
	            		intPosicionFinalBancoCuenta = line.indexOf(")cuenta*") - 1;
	            		intPosicionInicioBancoDescripcion = line.indexOf("descripcion:(") + 13;
	            		intPosicionFinalBancoDescripcion =  line.indexOf(")descripcion*") - 1;
	            		intPosicionInicioCeldaDelIdAgente = line.indexOf("datosIdAgente:(") + 15;
	            		intPosicionFinalCeldaDelIdAgente =  line.indexOf(")datosIdAgente*") - 1;
	            		intPosicionInicioCeldaDelImporte = line.indexOf("importe:(") + 9;
	            		intPosicionFinalCeldaDelImporte =  line.indexOf(")importe*") - 1;
	            		intPosicionInicioIdentificador = line.indexOf("identificador:(") + 15;
	            		intPosicionFinalIdentificador =  line.indexOf(")identificador*") - 1;
	            		intPosicionInicioFilaPrimerMovimiento = line.indexOf("filaPrimerMovimiento:(") + 22;
	            		intPosicionFinalFilaPrimerMovimiento =  line.indexOf(")filaPrimerMovimiento*") - 1;
	            		intPosicionInicioPosicionSaldo = line.indexOf("posicionSaldo:(") + 15;
	            		intPosicionFinalPosicionSaldo =  line.indexOf(")posicionSaldo*") - 1;
	            		intPosicionInicioDistanciaSaldoAdatos = line.indexOf("distanciaSaldoAdatos:(") + 22;
	            		intPosicionFinalDistanciaSaldoAdatos =  line.indexOf(")distanciaSaldoAdatos*") - 1;
	            		intPosicionInicioOrden = line.indexOf("orden:(") + 7;
	            		intPosicionFinalOrden =  line.indexOf(")orden*") - 1;
	            		intPosicionInicioMovimiento = line.indexOf("posicionMovimiento:(") + 20;
	            		intPosicionFinalMovimiento =  line.indexOf(")posicionMovimiento*") - 1;
	            		intPosicionInicioFecha = line.indexOf("posicionFecha:(") + 15;
	            		intPosicionFinalFecha =  line.indexOf(")posicionFecha*") - 1;

	            		strBancoNombre = line.substring(intPosicionInicioBancoNombre, intPosicionFinalBancoNombre + 1);
	            		strBancoCuenta = line.substring(intPosicionInicioBancoCuenta, intPosicionFinalBancoCuenta + 1);
	            		strDescripcionDeCuenta = line.substring(intPosicionInicioBancoDescripcion, intPosicionFinalBancoDescripcion + 1);
	            		strIdAgente = line.substring(intPosicionInicioCeldaDelIdAgente, intPosicionFinalCeldaDelIdAgente + 1);
	            		strImporte = line.substring(intPosicionInicioCeldaDelImporte, intPosicionFinalCeldaDelImporte + 1);
	            		strIdentificador = line.substring(intPosicionInicioIdentificador, intPosicionFinalIdentificador + 1);
	            		strFilaPrimerMovimiento = line.substring(intPosicionInicioFilaPrimerMovimiento, intPosicionFinalFilaPrimerMovimiento + 1);
	            		strPosicionSaldo = line.substring(intPosicionInicioPosicionSaldo, intPosicionFinalPosicionSaldo + 1);
	            		strDistanciaSaldoAdatos = line.substring(intPosicionInicioDistanciaSaldoAdatos, intPosicionFinalDistanciaSaldoAdatos + 1);
	            		strOrden = line.substring(intPosicionInicioOrden, intPosicionFinalOrden + 1);
	            		strPosicionMovimiento = line.substring(intPosicionInicioMovimiento, intPosicionFinalMovimiento + 1);
	            		strPosicionFecha = line.substring(intPosicionInicioFecha, intPosicionFinalFecha + 1);
	            		
//	            		System.out.println("-------------------------valores:");        	
//	            		System.out.println("strBancoNombre: " + strBancoNombre);        	
//	            		System.out.println("strBancoCuenta: " + strBancoCuenta);        	
//	            		System.out.println("strBancoDescripcion: " + strDescripcionDeCuenta);
//	            		System.out.println("---------------------------------------------------------");            		
	
//	                	System.out.println("-*-*-*-*-*-*-*-*-*-* NOMBRE longitud: " + strBancoNombre.length());
//	                	System.out.println("-*-*-*-*-*-*-*-*-*-* CUENTA longitud: " + strBancoCuenta.length());
//	                	System.out.println("-*-*-*-*-*-*-*-*-*-* line chartAt(0): " + line.charAt(0));
	
	                	//*** Si nombre y la cuenta del banco tienen valor:	Escribimos en el array **********************
	            		if(strBancoNombre.length()>0 && strBancoCuenta.length()>0){
//	                    	System.out.println("-*-*-*-*-*-*-*-*-*-* NOMBRE Y CUENTA CON DATOS----ok");        	
//                        	System.out.println("-*-*-*-*-*-*-*-*-*-* linea valida,   no empieza por *");        	
                			arrayStrBancosCuentaDesc.set(0, strBancoNombre);
                			arrayStrBancosCuentaDesc.set(1, strBancoCuenta);
                			arrayStrBancosCuentaDesc.set(2, strDescripcionDeCuenta);          		
                			arrayStrBancosCuentaDesc.set(3, strIdAgente);
                			arrayStrBancosCuentaDesc.set(4, strImporte);
                			arrayStrBancosCuentaDesc.set(5, strIdentificador);          		
                			arrayStrBancosCuentaDesc.set(6, strFilaPrimerMovimiento);
                			arrayStrBancosCuentaDesc.set(7, strPosicionSaldo);
                			arrayStrBancosCuentaDesc.set(8, strDistanciaSaldoAdatos);
                			arrayStrBancosCuentaDesc.set(9, strOrden);
                			arrayStrBancosCuentaDesc.set(10, strPosicionMovimiento);
                			arrayStrBancosCuentaDesc.set(11, strPosicionFecha);
                			arrayBidimenStrBancosCuentaDesc.add(new ArrayList<>(arrayStrBancosCuentaDesc));
//                			arrayBidimenStrBancosCuentaDesc[contArrayBancoCuentaDesc] = strBancoNombre + " (" + strBancoDescripcion + ") " + strBancoCuenta;
//                			contArrayBancoCuentaDesc++;
                		}
	            	}
               	}
//            	System.out.println("intContBancos: " + intContBancos);            	
                intContBancos++;
            }
        } catch (FileNotFoundException e) {
    		System.out.println("properties-*** catch.");        	
        	//Escribir FileLogs "Error en la lectura del fichero saldos_properties.txt".
    		//LogsFile fileLogs = new...
    		//e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                	br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        //*************************** mostrar *****************************
    	System.out.println("validos bidimensional arrayBidimenStrBancosCuentaDesc.size: " + arrayBidimenStrBancosCuentaDesc.size());       	
        for(int contEle = 0;contEle<arrayBidimenStrBancosCuentaDesc.size();contEle++){
        	System.out.println("9876validos bidimensional arrayBidimenStrBancosCuentaDesc: " + arrayBidimenStrBancosCuentaDesc.get(contEle));        	
        }
        //*************************** mostrado ****************************

        
        //cotrebla
//      Ventana vvv = new Ventana();
        
        
		System.out.println("Clase SaldosProperties. Metodo getBancos. arrayBidimenStrBancosCuentaDesc:(" + arrayBidimenStrBancosCuentaDesc+")");
        return arrayBidimenStrBancosCuentaDesc;
	}
	
	public String getDirRutaOutput() throws IOException{

		//Obtendremos   la ruta que indica     el archivo   saldos_properties.txt
		String strLocalizacionJAR = "";
		String strRutaJAR = "";
		
		//El usuario debe situar el     archivo    saldos_properties.txt     donde haya ubicado		el archivo	.jar
		//Localizamos la ruta donde se encuentra    												el archivo	.jar

//		String strFileNameRutaTotal = "";
//    	strFileNameRutaTotal = Ventana.class.getProtectionDomain().getCodeSource().getLocation().toString();
    	strRutaJAR = Ventana.class.getProtectionDomain().getCodeSource().getLocation().toString();

		System.out.println("properties-***0 --dato-strRutaJAR (" + strRutaJAR + ")");
    	
    	String[] elemsFileNameRutaTotal = strRutaJAR.split("file:/");

		String strRutaTotal = elemsFileNameRutaTotal[1].toString();
		int lengthCadenaRuta = strRutaTotal.length();

		int intPosicionFileNameJar = strRutaTotal.toString().lastIndexOf("/");
	    int numCaracteres = lengthCadenaRuta - intPosicionFileNameJar;
		strLocalizacionJAR = strRutaTotal.substring(0, intPosicionFileNameJar+1);

		String strRutaProperties = strLocalizacionJAR + "saldos_properties.txt";
//		String strRutaProperties = "C:/Work/Alberto/a/concur_properties.txt";
		strRutaProperties = "\\" + strRutaProperties;

        BufferedReader br = null;
        String strRutaOutputProperties = "";
        String line = "";
//        String cvsSplitBy = ",";
        String strSplitBy = "output:";
		System.out.println("properties-***1 --dato-strRutaProperties (" + strRutaProperties + ")");

//    	Ventana.acumularStrTextoResultadoComprobaciones("\n" + " (d19) 111 strRutaProperties: " + strRutaProperties);

        try {
    		System.out.println("-***2 --dato-strRutaProperties (" + strRutaProperties + ")");
            br = new BufferedReader(new FileReader(strRutaProperties));
    		
            while((line = br.readLine()) != null){
               	if (!(line.indexOf("rutaDestinoArchivos:(")<0)) {            
               		
            		int intInicioRuta = line.indexOf("rutaDestinoArchivos:(");            		
            		if (!(line.indexOf(")rutaDestinoArchivos*")<0)) {
                    		int intFinRuta = line.indexOf(")rutaDestinoArchivos*");
                    		
                    		strRutaOutputProperties = line.substring(intInicioRuta + 21, intFinRuta);
//                    		ficheroLog.InfoLog("strRutaOutputProperties: " + strRutaOutputProperties,"destino");
                    		
            		}else{
//                    	strRutaOutputProperties = "C:/Saldos_Ruta_Default/";
                    	strRutaOutputProperties = "";            			
            		}
                }
            }
        } catch (FileNotFoundException e) {
    		System.out.println("properties-*** catch.");        	
        	//Mostrar una ventana informando del error en la lectura del fichero concur_properties.txt
//			strExcepcion = "Exception-ConcurProperties_catch FileNotFoundException";
//			excepciones.InfoLog(strExcepcion, GenerarFicheros.getStrRutaLayOut(), GenerarFicheros.getStrDateFileName());        	
//          e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                	strRutaOutputProperties = strRutaOutputProperties.trim();
                	br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }	
        
		System.out.println("properties----- saldos properties - strRutaOutputProperties: " + strRutaOutputProperties);

		System.out.println("Clase SaldosProperties. Metodo getDirRutaOutput. strRutaOutputProperties:(" + strRutaOutputProperties+")");
		
		return strRutaOutputProperties;
	}
	
	public String getDirRutaAgentesActivos() throws IOException{

		String strLocalizacionJAR = "";
		String strRutaJAR = "";
    	strRutaJAR = Ventana.class.getProtectionDomain().getCodeSource().getLocation().toString();
    	
    	String[] elemsFileNameRutaTotal = strRutaJAR.split("file:/");

		String strRutaTotal = elemsFileNameRutaTotal[1].toString();
		int lengthCadenaRuta = strRutaTotal.length();

		int intPosicionFileNameJar = strRutaTotal.toString().lastIndexOf("/");
	    int numCaracteres = lengthCadenaRuta - intPosicionFileNameJar;
		strLocalizacionJAR = strRutaTotal.substring(0, intPosicionFileNameJar+1);

		String strRutaProperties = strLocalizacionJAR + "saldos_properties.txt";
		strRutaProperties = "\\" + strRutaProperties;

        BufferedReader br = null;
        String strRutaOutputProperties = "";
        String line = "";
        String strSplitBy = "output:";
        try {
            br = new BufferedReader(new FileReader(strRutaProperties));

//    		Ventana.acumularStrTextoResultadoComprobaciones("\n (c)Agente(strRutaProperties): " + strRutaProperties);

            while((line = br.readLine()) != null){
//        		Ventana.acumularStrTextoResultadoComprobaciones("\n (c)Agente(strRutaProperties) line");

            	if (!(line.indexOf("rutaAgentesActivos:(")<0)) {            
               		
            		int intInicioRuta = line.indexOf("rutaAgentesActivos:(");            		
            		if (!(line.indexOf(")rutaAgentesActivos*")<0)) {
                    		int intFinRuta = line.indexOf(")rutaAgentesActivos*");
                    		
                    		strRutaOutputProperties = line.substring(intInicioRuta + 20, intFinRuta);
//                    		ficheroLog.InfoLog("Agentes____: " + strRutaOutputProperties,"Agentes_destino");
            		}else{
//                    	strRutaOutputProperties = "C:/Saldos_Ruta_Default/";
                    	strRutaOutputProperties = "";            			
//        				ficheroLog.InfoLog("La ruta para los archivos generados es: " + strRutaOutputProperties + ". El archivo saldos_properties.txt no indica correctamente la carpeta donde se encuentra agentes activos.", "Agentes-");
            		}
                }else{
//                	strRutaOutputProperties = "C:/Saldos_Ruta_Default/";
                	strRutaOutputProperties = "";
//    				ficheroLog.InfoLog("La ruta para los archivos generados es: " + strRutaOutputProperties + ". El archivo saldos_properties.txt no indica correctamente la carpeta donde se encuentra agentes activos.", "Agentes2-");
                	//Mostrar una ventana informando del error en la lectura del fichero concur_properties.txt
                }
            }
        } catch (FileNotFoundException e) {
    		System.out.println("properties-*** catch.");        	
        	//Mostrar una ventana informando del error en la lectura del fichero concur_properties.txt
//			strExcepcion = "Exception-ConcurProperties_catch FileNotFoundException";
//			excepciones.InfoLog(strExcepcion, GenerarFicheros.getStrRutaLayOut(), GenerarFicheros.getStrDateFileName());        	
//          e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                	strRutaOutputProperties = strRutaOutputProperties.trim();
                	br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

		return strRutaOutputProperties;
	}
		
	public String comprobarFormatoValido(String strRutaJAR) throws IOException{

		String mensajeFormatoIncorrecto = "";
        		
		ArrayList<ArrayList<String>> arrayBidimenStrBancosCuentaDesc = new ArrayList<ArrayList<String>>();
		ArrayList<String> arrayStrBancosCuentaDesc = new ArrayList<String>();
/*		
		arrayStrBancosCuentaDesc.add(0, "");
		arrayStrBancosCuentaDesc.add(1, "");
		arrayStrBancosCuentaDesc.add(2, "");
		arrayStrBancosCuentaDesc.add(3, "");
		arrayStrBancosCuentaDesc.add(4, "");
		arrayStrBancosCuentaDesc.add(5, "");
		arrayStrBancosCuentaDesc.add(6, "");
		arrayStrBancosCuentaDesc.add(7, "");
		arrayStrBancosCuentaDesc.add(8, "");
		arrayStrBancosCuentaDesc.add(9, "");
		arrayStrBancosCuentaDesc.add(10, "");
		arrayStrBancosCuentaDesc.add(11, "");
*/		

		//Obtendremos   la ruta que indica     el archivo   saldos_properties.txt
		String strLocalizacionJAR = "";

		//El usuario debe situar el     archivo    saldos_properties.txt     donde haya ubicado		el archivo	.jar
		//Localizamos la ruta donde se encuentra    												el archivo	.jar

//		String strFileNameRutaTotal = "";
//    	strFileNameRutaTotal = Ventana.class.getProtectionDomain().getCodeSource().getLocation().toString();
    	strRutaJAR = Ventana.class.getProtectionDomain().getCodeSource().getLocation().toString();

		System.out.println("properties-***0 --dato-strRutaJAR (" + strRutaJAR + ")");
    	
    	String[] elemsFileNameRutaTotal = strRutaJAR.split("file:/");

		String strRutaTotal = elemsFileNameRutaTotal[1].toString();
		int lengthCadenaRuta = strRutaTotal.length();

		int intPosicionFileNameJar = strRutaTotal.toString().lastIndexOf("/");
//	    int numCaracteres = lengthCadenaRuta - intPosicionFileNameJar;
		strLocalizacionJAR = strRutaTotal.substring(0, intPosicionFileNameJar+1);
		
		String strRutaProperties = strLocalizacionJAR + "saldos_properties.txt";

		strRutaProperties = "\\" + strRutaProperties;

		System.out.println("Separador:"+File.separator);
		System.out.println("Separador strRutaProperties:"+strRutaProperties);
		
		//strRutaProperties = strRutaProperties.replace("/", File.separator);
		
//		String strRutaProperties = "C:/Work/Alberto/a/concur_properties.txt";

        BufferedReader br = null;

        String line = "";
//        String cvsSplitBy = ",";
//        String strSplitBy = "output:";
		System.out.println("properties-***1 --dato-strRutaProperties (" + strRutaProperties + ")");
        try {
    		System.out.println("-***2 --dato-strRutaProperties (" + strRutaProperties + ")");
            br = new BufferedReader(new FileReader(strRutaProperties));
            int intContBancos = 0;
            boolean booleanLineaComentario = false;
            int contArrayBancoCuentaDesc = 0;
            
            int contLineas = 0;
            while((line = br.readLine()) != null){
            	contLineas++;
            	
            	booleanLineaComentario = false;

//             	System.out.println("***cont linea: " + contLineas + " - linea:" + line);
               	//*** Si el archivo tenia las l�neas de datos con el formato correcto:
               	if (line.charAt(0)=='*'){
               		booleanLineaComentario = true;
               	}
               	if (!booleanLineaComentario){
//                 	System.out.println("***cont linea: " + contLineas + " no es comentario");
               		if (mensajeFormatoIncorrecto==""){
    	        		//*** Si el archivo comienza por:	banco:(		entonces la linea debe tener formato de configuracion de cuenta.
               			if ((line.indexOf("banco:(")==0)){
//                           	System.out.println("***cont linea: " + contLineas + " la linea es para banco cuenta");
        	            	if (!(line.indexOf(")banco*")<0) && !(line.indexOf("cuenta:(")<0) && !(line.indexOf(")cuenta*")<0) && !(line.indexOf("descripcion:(")<0)	&& !(line.indexOf(")descripcion*")<0) && !(line.indexOf("datosIdAgente:(")<0)	&& !(line.indexOf(")datosIdAgente*")<0) && !(line.indexOf("importe:(")<0)	&& !(line.indexOf(")importe*")<0) && !(line.indexOf("identificador:(")<0)	&& !(line.indexOf(")identificador*")<0) && !(line.indexOf("filaPrimerMovimiento:(")<0)	&& !(line.indexOf(")filaPrimerMovimiento*")<0) && !(line.indexOf("posicionSaldo:(")<0)	&& !(line.indexOf(")posicionSaldo*")<0) && !(line.indexOf("distanciaSaldoAdatos:(")<0)	&& !(line.indexOf(")distanciaSaldoAdatos*")<0)){
        	            		String strRespuestaCuentaConfigurada = "";
        	            		strRespuestaCuentaConfigurada = validarDatosCuentaConfigurada(line);
        	            		if (!(strRespuestaCuentaConfigurada.equals(""))){
    	                            mensajeFormatoIncorrecto = mensajeFormatoIncorrecto + strRespuestaCuentaConfigurada;
        	            		}
        	            	}else{
	                            System.out.println("Formato de configuracion de cuenta incorrecto en la l�nea: " + contLineas);            				
	                            mensajeFormatoIncorrecto = mensajeFormatoIncorrecto + "ERROR ***** Archivo saldos_properties.txt:  Formato de configuracion de cuenta incorrecto. En la l�nea " + contLineas + " del archivo. *****" + "\n Se debe corregir el archivo con un formato correcto.";
	            			} 
	            		}
    	        		//*** Si el archivo comienza por:	rutaDestinoArchivos:(		entonces la linea debe tener formato de configuracion de ruta destino de archivos.
               			if ((line.indexOf("rutaDestinoArchivos:(")==0)){
//                         	System.out.println("***cont linea: " + contLineas + " la linea es para RUTA");               				
               				if ((line.indexOf(")rutaDestinoArchivos*")<0)){
	                            mensajeFormatoIncorrecto = mensajeFormatoIncorrecto + "ERROR ***** Archivo saldos_properties.txt:  Formato de configuracion de cuenta incorrecto. En la l�nea " + contLineas + " del archivo *****" + "\n Se debe corregir el archivo con un formato correcto.";
               				}
               			}
    	        		//*** -------Pendiente de incluir la comprobacion del formato de excepciones: ---------------------------------
    	        		//*** Si el archivo comienza por:	excepcion:(    ------>     habr� que validar la l�nea tenga el formato de una excepci�n
               			if ((line.indexOf("excepcion:(")==0)){
//                         	System.out.println("***cont linea: " + contLineas + " la linea es para Excepcion");
                           	if (!(line.indexOf(")excepcion*")<0) && !(line.indexOf("banco:(")<0) && !(line.indexOf(")banco*")<0) && !(line.indexOf("cuenta:(")<0) && !(line.indexOf(")cuenta*")<0) &&	!(line.indexOf("agente:(")<0) && !(line.indexOf(")agente*")<0) && !(line.indexOf("nuevocodigo:(")<0) && !(line.indexOf(")nuevocodigo*")<0)){
        	            		String strRespuestaExcepcionConfigurada = "";
        	            		strRespuestaExcepcionConfigurada = validarDatosExcepcionConfigurada(line, contLineas);
        	            		if (!(strRespuestaExcepcionConfigurada.equals(""))){
    	                            mensajeFormatoIncorrecto = mensajeFormatoIncorrecto + strRespuestaExcepcionConfigurada;
        	            		}
        	            	}else{
	                            System.out.println("Formato de configuracion de cuenta incorrecto en la l�nea: " + contLineas);            				
	                            mensajeFormatoIncorrecto = mensajeFormatoIncorrecto + "ERROR ***** Archivo saldos_properties.txt:  Formato de configuracion de excepci�n incorrecto. En la l�nea " + contLineas + " del archivo. *****" + "\n Se debe corregir el archivo con un formato correcto.";
	            			}   	
               			}
    	        		//*** ---------------------------------------------------------------------------------------------------------------------
               		}
               	}
            }
        } catch (FileNotFoundException e) {
    		System.out.println("properties-*** catch.");        	
    		//e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                	br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
       System.out.println("mensajeFormatoIncorrecto--------------------:" + mensajeFormatoIncorrecto);
       return mensajeFormatoIncorrecto;
	}

	public String validarDatosCuentaConfigurada(String strLine) throws IOException{

		ArrayList<String> arrayConversionLetraColumna = new ArrayList<>();
		String strLetras = "abcdefghijklmn";
    		for(int contEle=0;contEle<14;contEle++){
   			arrayConversionLetraColumna.add(strLetras.substring(contEle, contEle+1));
   		}
		String strMensajeError = "";

		//si alg�n error sucede, el valor de esa variable comenzar� por 'Error'.
		//strMensajeError = "Error":
	
       	int intPosicionInicioBancoNombre = strLine.indexOf("banco:(") + 7;
   		int intPosicionFinalBancoNombre = strLine.indexOf(")banco*") - 1;
   		int intPosicionInicioBancoCuenta = strLine.indexOf("cuenta:(") + 8;
   		int intPosicionFinalBancoCuenta = strLine.indexOf(")cuenta*") - 1;
   		int intPosicionInicioBancoDescripcion = strLine.indexOf("descripcion:(") + 13;
   		int intPosicionFinalBancoDescripcion =  strLine.indexOf(")descripcion*") - 1;
   		int intPosicionInicioCeldaDelIdAgente = strLine.indexOf("datosIdAgente:(") + 15;
   		int intPosicionFinalCeldaDelIdAgente =  strLine.indexOf(")datosIdAgente*") - 1;
   		int intPosicionInicioCeldaDelImporte = strLine.indexOf("importe:(") + 9;
   		int intPosicionFinalCeldaDelImporte =  strLine.indexOf(")importe*") - 1;
   		int intPosicionInicioIdentificador = strLine.indexOf("identificador:(") + 15;
   		int intPosicionFinalIdentificador =  strLine.indexOf(")identificador*") - 1;
   		int intPosicionInicioFilaPrimerMovimiento = strLine.indexOf("filaPrimerMovimiento:(") + 22;
   		int intPosicionFinalFilaPrimerMovimiento =  strLine.indexOf(")filaPrimerMovimiento*") - 1;
   		int intPosicionInicioPosicionSaldo = strLine.indexOf("posicionSaldo:(") + 15;
   		int intPosicionFinalPosicionSaldo =  strLine.indexOf(")posicionSaldo*") - 1;
   		int intPosicionInicioDistanciaSaldoAdatos = strLine.indexOf("distanciaSaldoAdatos:(") + 22;
   		int intPosicionFinalDistanciaSaldoAdatos =  strLine.indexOf(")distanciaSaldoAdatos*") - 1;
   		int intPosicionInicioOrden = strLine.indexOf("orden:(") + 7;
   		int intPosicionFinalOrden =  strLine.indexOf(")orden*") - 1;
   		int intPosicionInicioCeldaDelMovimiento = strLine.indexOf("posicionMovimiento:(") + 20;
   		int intPosicionFinalCeldaDelMovimiento =  strLine.indexOf(")posicionMovimiento*") - 1;
   		int intPosicionInicioCeldaFecha = strLine.indexOf("posicionFecha:(") + 16;
   		int intPosicionFinalCeldaFecha =  strLine.indexOf(")posicionFecha*") - 1;
    		
//   		System.out.println("*1*1*1 : " + intPosicionInicioBancoNombre);
//   		System.out.println("*1*1*1 : " + intPosicionFinalBancoNombre);
//   		System.out.println("*1*1*1 : " + intPosicionInicioBancoCuenta);
//   		System.out.println("*1*1*1 : " + intPosicionFinalBancoCuenta);
//   		System.out.println("*1*1*1 : " + intPosicionInicioBancoDescripcion);
//   		System.out.println("*1*1*1 : " + intPosicionFinalBancoDescripcion);
//   		System.out.println("*1*1*1 : " + intPosicionInicioCeldaDelIdAgente);
//   		System.out.println("*1*1*1 : " + intPosicionFinalCeldaDelIdAgente);
//   		System.out.println("*1*1*1 : " + intPosicionInicioCeldaDelImporte);
//   		System.out.println("*1*1*1 : " + intPosicionFinalCeldaDelImporte);
//   		System.out.println("*1*1*1 : " + intPosicionInicioIdentificador);
//   		System.out.println("*1*1*1 : " + intPosicionFinalIdentificador);
//   		System.out.println("*1*1*1 : " + intPosicionInicioFilaPrimerMovimiento);
//   		System.out.println("*1*1*1 : " + intPosicionFinalFilaPrimerMovimiento);
//   		System.out.println("*1*1*1 : " + intPosicionInicioPosicionSaldo);
//   		System.out.println("*1*1*1 : " + intPosicionFinalPosicionSaldo);
//   		System.out.println("*1*1*1 : " + intPosicionInicioDistanciaSaldoAdatos);
//   		System.out.println("*1*1*1 : " + intPosicionFinalDistanciaSaldoAdatos);
//   		System.out.println("*1*1*1 : " + intPosicionInicioOrden);
//   		System.out.println("*1*1*1 : " + intPosicionFinalOrden);
//   		System.out.println("*1*1*1 : " + intPosicionInicioCeldaDelMovimiento);
//   		System.out.println("*1*1*1 : " + intPosicionFinalCeldaDelMovimiento);
//   		System.out.println("*1*1*1 : " + intPosicionInicioCeldaFecha);
//   		System.out.println("*1*1*1 : " + intPosicionFinalCeldaFecha);
   		
   		String strBancoNombre = strLine.substring(intPosicionInicioBancoNombre, intPosicionFinalBancoNombre + 1);
   		String strBancoCuenta = strLine.substring(intPosicionInicioBancoCuenta, intPosicionFinalBancoCuenta + 1);
   		String strDescripcionDeCuenta = strLine.substring(intPosicionInicioBancoDescripcion, intPosicionFinalBancoDescripcion + 1);
   		String strPosicionDatosIdAgente = strLine.substring(intPosicionInicioCeldaDelIdAgente, intPosicionFinalCeldaDelIdAgente + 1);
   		String strImporte = strLine.substring(intPosicionInicioCeldaDelImporte, intPosicionFinalCeldaDelImporte + 1);
   		String strIdentificador = strLine.substring(intPosicionInicioIdentificador, intPosicionFinalIdentificador + 1);
   		String strFilaPrimerMovimiento = strLine.substring(intPosicionInicioFilaPrimerMovimiento, intPosicionFinalFilaPrimerMovimiento + 1);
   		String strPosicionSaldo = strLine.substring(intPosicionInicioPosicionSaldo, intPosicionFinalPosicionSaldo + 1);
   		String strDistanciaSaldoAdatos = strLine.substring(intPosicionInicioDistanciaSaldoAdatos, intPosicionFinalDistanciaSaldoAdatos + 1);
   		String strOrden = strLine.substring(intPosicionInicioOrden, intPosicionFinalOrden + 1);
   		String strMovimiento = strLine.substring(intPosicionInicioCeldaDelMovimiento, intPosicionFinalCeldaDelMovimiento + 1);
   		String strFecha = strLine.substring(intPosicionInicioCeldaFecha, intPosicionFinalCeldaFecha + 1);

   		//************************************Comprobaciones para Posicion de Agente
   		String strPrimerCaracterPosicionDatosIdAgente = "";
    		
   		if(strPosicionDatosIdAgente.length()>0){
   	   		strPrimerCaracterPosicionDatosIdAgente = strPosicionDatosIdAgente.substring(0, 1);
   			boolean booleanEncuentraLetra = false;
   			for(int contEle=0;contEle<14;contEle++){
   				if (arrayConversionLetraColumna.get(contEle).toString().equals(strPrimerCaracterPosicionDatosIdAgente.toLowerCase())){
   	    			booleanEncuentraLetra = true; 
   				}
   			}
   			if (!booleanEncuentraLetra){
       			strMensajeError = strMensajeError + "Error ***** : No se ha especificado en la Posicion de Agente una columna entre A y H.";    				
//               	System.out.println("***prueba     cont linea: " + strLine);   				
//               	System.out.println("***prueba: " + strMensajeError);   				
   			}
   		}else{
   			strMensajeError = strMensajeError + "Error ***** : No se ha especificado un valor para la Posicion de Agente.";
   		}   		
   		//************************************Comprobaciones para  Posicion de Importe
   		String strPrimerCaracterPosicionImporte = "";
    		
   		if(strImporte.length()>0){
   	   		strPrimerCaracterPosicionImporte = strImporte.substring(0, 1);
   			boolean booleanEncuentraLetra = false;
   			for(int contEle=0;contEle<14;contEle++){
   				if (arrayConversionLetraColumna.get(contEle).toString().equals(strPrimerCaracterPosicionImporte.toLowerCase())){
   	    			booleanEncuentraLetra = true; 
   				}
   			}
   			if (!booleanEncuentraLetra){
       			strMensajeError = strMensajeError + "Error ***** : No se ha especificado en la Posicion de Importe una columna entre A y H.";
//               	System.out.println("***importe prueba     cont linea: " + strLine);   				
//               	System.out.println("***importe prueba: " + strMensajeError);   				       			
   			}
   		}else{
   			strMensajeError = strMensajeError + "Error ***** : No se ha especificado un valor para la Posicion de Importe.";
   		}
   		//************************************Comprobaciones Identificador
   		if(!(strIdentificador.length()>0)){
   			strMensajeError = strMensajeError + "Error ***** : No se ha especificado un valor para el Identificador.";
   		}
   		//************************************Comprobaciones para Fila primer movimiento
   		if(strFilaPrimerMovimiento.length()>0){
   			try {
				if (!(Integer.valueOf(strFilaPrimerMovimiento)>0 && Integer.valueOf(strFilaPrimerMovimiento)<21)){
					strMensajeError = strMensajeError + "Error ***** : La primera fila con datos del archivo debe ser un n�mero entre 1 y 20.";
				}
			} catch (NumberFormatException e) {
				strMensajeError = strMensajeError + "Error ***** : La 'filaPrimerMovimiento' del archivo debe ser un n�mero entre 1 y 20.";
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
   		}else{
   			strMensajeError = strMensajeError + "Error ***** : No se ha especificado un valor para la Fila con primer movimiento.";
   		}
   		//************************************Comprobaciones posicion saldo.
   		String strCoordenadaPosicionSaldo = "";
   		if(strPosicionSaldo.length()>0){
   			if(strPosicionSaldo.startsWith("todas:") || strPosicionSaldo.startsWith("cabecera:") || strPosicionSaldo.startsWith("final:")){
   	   			//---------------------------------------------------------todas-----------------
   				if(strPosicionSaldo.startsWith("todas:")){
//   					System.out.println("Error-Todas:" + strLine);
   					
   	   	   			if(strPosicionSaldo.length()>6){
   	   	   				strCoordenadaPosicionSaldo = strPosicionSaldo.substring(6);
//	   	   				System.out.println("Error-Todas- strCoordenadaPosicionSaldo:" + strCoordenadaPosicionSaldo );   	   	   				
   	   	   				boolean booleanEncuentraLetra = false;
   	   	   				for(int contEle=0;contEle<14;contEle++){
   	   	   					if (arrayConversionLetraColumna.get(contEle).toString().equals(strCoordenadaPosicionSaldo.toLowerCase())){
   	   	   						booleanEncuentraLetra = true; 
   	   	   					}
   	   	   				}
   	   	   				if (!booleanEncuentraLetra){
   	   	   					strMensajeError = strMensajeError + "Error *****Posicion saldo (todas) : No se ha especificado en la Posicion de Saldo una columna entre A y H.";
   	   	   				}	
   	   	   			}else{
   	        			strMensajeError = strMensajeError + "Error ***** : No se ha especificado la Posicion de Saldo (una columna entre A y H).";
   	   	   			}
 	   	   		}
   	   			//---------------------------------------------------------cabecera-----------------
   				if(strPosicionSaldo.startsWith("cabecera:")){
   	   	   			if(strPosicionSaldo.length()>9){
   	        			strMensajeError = strMensajeError + "Error ***** : Cuando el importe del saldo actual se introduce por pantalla,  en 'posicion del saldo' s�lo se debe indicar: cabecera:";
   	   	   			}
   				}   				
   	   			//---------------------------------------------------------final--------------------
   				if(strPosicionSaldo.startsWith("final:")){   					
   	   	   			if(strPosicionSaldo.length()>6){
   	   	   				strCoordenadaPosicionSaldo = strPosicionSaldo.substring(6);
   	   	   				boolean booleanEncuentraLetra = false;
   	   	   				for(int contEle=0;contEle<14;contEle++){
   	   	   					if (arrayConversionLetraColumna.get(contEle).toString().equals(strCoordenadaPosicionSaldo.toLowerCase())){
   	   	   						booleanEncuentraLetra = true; 
   	   	   					}
   	   	   				}
   	   	   				if (!booleanEncuentraLetra){
   	   	   					strMensajeError = strMensajeError + "Error *****Posicion saldo (final) : No se ha especificado en la Posicion de Saldo una columna entre A y H.";
   	   	   				}	
   	   	   			}else{
   	        			strMensajeError = strMensajeError + "Error ***** : No se ha especificado la Posicion de Saldo (una columna entre A y H).";
   	   	   			}
   				}
   				//----------------------------------------------------------------------------------
   			}else{
       			strMensajeError = strMensajeError + "Error ***** : La posicion de saldo se debe especificar con alguno de tres prefijos (todas:,  cabecera:  o  final:)";
   			}
   		}else{
   			strMensajeError = strMensajeError + "Error ***** : No se ha especificado un valor para la posicion del saldo.";
   		}
   		//*********************************** Comprobaciones distanciaSaldoAdatos
   		if(strDistanciaSaldoAdatos.length()>0){
   			try {
				int intDistanciaSaldoaDatos = Integer.valueOf(strDistanciaSaldoAdatos);
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
	   			strMensajeError = strMensajeError + "Error ***** : No se ha especificado un valor correcto para la distancia entre los datos y el saldo.";   			
				e.printStackTrace();
			}    		
   		}else{
   			strMensajeError = strMensajeError + "Error ***** : No se ha especificado un valor para la distancia entre los datos y el saldo.";   			
   		}
   		//*******************************************************************************
   		return strMensajeError;
	}

	public String validarDatosExcepcionConfigurada(String strLine, int contLineas) throws IOException{
		String strMensajeError = "";

		//si alg�n error sucede, el valor de esa variable comenzar� por 'Error'.
		//strMensajeError = "Error":
	
       	int intPosicionInicioExcepcion = strLine.indexOf("excepcion:(") + 11;
   		int intPosicionFinalExcepcion = strLine.indexOf(")excepcion*") - 1;
   		int intPosicionInicioBancoNombre = strLine.indexOf("banco:(") + 7;
   		int intPosicionFinalBancoNombre = strLine.indexOf(")banco*") - 1;
   		int intPosicionInicioBancoCuenta = strLine.indexOf("cuenta:(") + 8;
   		int intPosicionFinalBancoCuenta = strLine.indexOf(")cuenta*") - 1;
   		int intPosicionInicioAgente = strLine.indexOf("agente:(") + 8;
   		int intPosicionFinalAgente =  strLine.indexOf(")agente*") - 1;
   		int intPosicionInicioNuevoCodigo = strLine.indexOf("nuevocodigo:(") + 13;
   		int intPosicionFinalNuevoCodigo =  strLine.indexOf(")nuevocodigo*") - 1;
 
		System.out.println("1      :" + intPosicionInicioExcepcion);   	   	   				
		System.out.println("2      :" + intPosicionFinalExcepcion);   	   	   				
		System.out.println("3      :" + intPosicionInicioBancoNombre);   	   	   				
		System.out.println("4      :" + intPosicionFinalBancoNombre);
		System.out.println("5      :" + intPosicionInicioBancoCuenta);
		System.out.println("6      :" + intPosicionFinalBancoCuenta);
		System.out.println("7      :" + intPosicionInicioAgente);
		System.out.println("8      :" + intPosicionFinalAgente);
		System.out.println("9      :" + intPosicionInicioNuevoCodigo);
		System.out.println("10     :" + intPosicionFinalNuevoCodigo);
				
   		String strExcepcion = strLine.substring(intPosicionInicioExcepcion, intPosicionFinalExcepcion + 1);
   		String strBancoNombre = strLine.substring(intPosicionInicioBancoNombre, intPosicionFinalBancoNombre + 1);
   		String strBancoCuenta = strLine.substring(intPosicionInicioBancoCuenta, intPosicionFinalBancoCuenta + 1);
   		String strAgente = strLine.substring(intPosicionInicioAgente, intPosicionFinalAgente + 1);
   		String strNuevoCodigo = strLine.substring(intPosicionInicioNuevoCodigo, intPosicionFinalNuevoCodigo + 1);

		System.out.println("1     :" + strExcepcion);
		System.out.println("2     :" + strBancoNombre);
		System.out.println("3     :" + strBancoCuenta);
		System.out.println("4     :" + strAgente);
		System.out.println("5     :" + strNuevoCodigo);

   		//************************************Comprobaciones Excepcion
   		if(!(strExcepcion.length()>0)){
   			strMensajeError = strMensajeError + "\n" + "***** Error ***** : No se ha especificado el estado de la excepci�n (ACTIVADA/DESACTIVADA).";
   		}
   		//************************************Comprobaciones para Banco
   		if(!(strBancoNombre.length()>0)){
   	   		strMensajeError = strMensajeError + "\n" + "***** Error ***** : No se ha especificado el banco.";
   	   	}
   		//************************************Comprobaciones para la cuenta
   		if(!(strBancoCuenta.length()>0)){
   			strMensajeError = strMensajeError + "\n" + "***** Error ***** : No se ha especificado la cuenta.";
   		}
   		//************************************Comprobaciones para el agente
   		if(!(strAgente.length()>0)){
   			strMensajeError = strMensajeError + "\n" + "***** Error ***** : No se ha especificado el agente.";
   		}
   		//************************************Comprobaciones para el nuevo c�digo de agente
   		if(!(strNuevoCodigo.length()>0)){
   			strMensajeError = strMensajeError + "\n" + "***** Error ***** : No se ha especificado el nuevo c�digo de agente.";
   		}
   		//*******************************************************************************
   		if (strMensajeError.length()>0){
   			strMensajeError = "Error ***** en la l�nea: " + contLineas + " del archivo saldos_properties.txt: " + strMensajeError;
   		}
   		return strMensajeError;
	}

	public ArrayList<ArrayList<String>> getExcepcionesActivadas(String strRutaJAR) throws IOException{

		ArrayList<ArrayList<String>> arrayListBidimenExcepcionesActivadas = new ArrayList<ArrayList<String>>();
		ArrayList<String> arrayListExcepcionActivada = new ArrayList<String>();
		
		arrayListExcepcionActivada.add(0, "");
		arrayListExcepcionActivada.add(1, "");
		arrayListExcepcionActivada.add(2, "");
		arrayListExcepcionActivada.add(3, "");
		arrayListExcepcionActivada.add(4, "");
		
		String strExcepcion = "";
		String strBancoNombre = "";
		String strBancoCuenta = "";
		String strIdAgente = "";
		String strNuevoCodigo = "";
		int intPosicionInicioExcepcion = 0;
		int intPosicionFinalExcepcion = 0;
		int intPosicionInicioBancoNombre = 0;
		int intPosicionFinalBancoNombre = 0;
		int intPosicionInicioBancoCuenta = 0;
		int intPosicionFinalBancoCuenta = 0;
		int intPosicionInicioIdAgente = 0;
		int intPosicionFinalIdAgente = 0;
		int intPosicionInicioNuevoCodigo = 0;
		int intPosicionFinalNuevoCodigo =  0;

		//Obtendremos   la ruta que indica     el archivo   saldos_properties.txt
		String strLocalizacionJAR = "";

		//El usuario debe situar el     archivo    saldos_properties.txt     donde haya ubicado		el archivo	.jar
		//Localizamos la ruta donde se encuentra    												el archivo	.jar

//		String strFileNameRutaTotal = "";
//    	strFileNameRutaTotal = Ventana.class.getProtectionDomain().getCodeSource().getLocation().toString();
    	strRutaJAR = Ventana.class.getProtectionDomain().getCodeSource().getLocation().toString();

		System.out.println("properties-***0 --dato-strRutaJAR (" + strRutaJAR + ")");
    	
    	String[] elemsFileNameRutaTotal = strRutaJAR.split("file:/");

		String strRutaTotal = elemsFileNameRutaTotal[1].toString();

		int intPosicionFileNameJar = strRutaTotal.toString().lastIndexOf("/");
		strLocalizacionJAR = strRutaTotal.substring(0, intPosicionFileNameJar+1);
		
		String strRutaProperties = strLocalizacionJAR + "saldos_properties.txt";

		//strRutaProperties.replace("/", "\\");
		strRutaProperties = "\\" + strRutaProperties;
			
		BufferedReader br = null;

        String line = "";
		System.out.println("properties-***1 --dato-strRutaProperties (" + strRutaProperties + ")");
        try {
            br = new BufferedReader(new FileReader(strRutaProperties));
            int intContBancos = 0;
            boolean booleanLineaComentario = false;
            int contArrayBancoCuentaDesc = 0;
            while((line = br.readLine()) != null){
            	
            	booleanLineaComentario = false;

        		//*** Si la linea empieza por asterisco:	es linea de comentario.
               	if (line.charAt(0)=='*'){
               		booleanLineaComentario = true;
               	}
               	
               	if (!booleanLineaComentario){
	        		//*** Si el archivo lee una l�nea de excepci�n con el formato correcto:
//               		excepcion:()excepcion***banco:()banco***cuenta:(a)cuenta***agente:()agente***nuevocodigo:()nuevocodigo***findatos
	               	if (!(line.indexOf("excepcion:(")<0) && !(line.indexOf(")excepcion*")<0) && !(line.indexOf("banco:(")<0) && !(line.indexOf(")banco*")<0) 
	               			&& !(line.indexOf("cuenta:(")<0) && !(line.indexOf(")cuenta*")<0) && !(line.indexOf("agente:(")<0) && !(line.indexOf(")agente*")<0) 
	    	               	&& !(line.indexOf("nuevocodigo:(")<0) && !(line.indexOf(")nuevocodigo*")<0)	){
	               		intPosicionInicioExcepcion = line.indexOf("excepcion:(") + 11;
	            		intPosicionFinalExcepcion = line.indexOf(")excepcion*") - 1;
	               		intPosicionInicioBancoNombre = line.indexOf("banco:(") + 7;
	            		intPosicionFinalBancoNombre = line.indexOf(")banco*") - 1;
	            		intPosicionInicioBancoCuenta = line.indexOf("cuenta:(") + 8;
	            		intPosicionFinalBancoCuenta = line.indexOf(")cuenta*") - 1;
	            		intPosicionInicioIdAgente = line.indexOf("agente:(") + 8;
	            		intPosicionFinalIdAgente =  line.indexOf(")agente*") - 1;
	            		intPosicionInicioNuevoCodigo = line.indexOf("nuevocodigo:(") + 13;
	            		intPosicionFinalNuevoCodigo =  line.indexOf(")nuevocodigo*") - 1;
	            		
	            		strExcepcion = line.substring(intPosicionInicioExcepcion, intPosicionFinalExcepcion + 1);
	            		strBancoNombre = line.substring(intPosicionInicioBancoNombre, intPosicionFinalBancoNombre + 1);
	            		strBancoCuenta = line.substring(intPosicionInicioBancoCuenta, intPosicionFinalBancoCuenta + 1);
	            		strIdAgente = line.substring(intPosicionInicioIdAgente, intPosicionFinalIdAgente + 1);
	            		strNuevoCodigo = line.substring(intPosicionInicioNuevoCodigo, intPosicionFinalNuevoCodigo + 1);

	                	//*** Si nombre y la cuenta del banco tienen valor:	Escribimos en el array **********************
//	            		excepcion	banco	cuenta	agente	nuevocodigo
	            		if(strExcepcion.length()>0 && strBancoNombre.length()>0 && strBancoCuenta.length()>0 && strIdAgente.length()>0 && strNuevoCodigo.length()>0){
	            			arrayListExcepcionActivada.set(0, strExcepcion);
	            			arrayListExcepcionActivada.set(1, strBancoNombre);
	            			arrayListExcepcionActivada.set(2, strBancoCuenta);
	            			arrayListExcepcionActivada.set(3, strIdAgente);
	            			arrayListExcepcionActivada.set(4, strNuevoCodigo);
                			arrayListBidimenExcepcionesActivadas.add(new ArrayList<>(arrayListExcepcionActivada));
                		}
	            	}
               	}
            }
        } catch (FileNotFoundException e) {
    		System.out.println("properties-*** catch.");
    		Ventana.setStrSWMensajesErrorVentana("ERROR ***** : No se ha podido encontrar el fichero saldos_properties.txt.");
        	//Escribir FileLogs "Error en la lectura del fichero saldos_properties.txt".
    		//LogsFile fileLogs = new...
    		//e.printStackTrace();
        } catch (IOException e) {
    		Ventana.setStrSWMensajesErrorVentana("ERROR ***** : saldos_properties.txt.   Se ha producico un error IO.");
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                	br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        //*************************** mostrar *****************************
        for(int contEle = 0;contEle<arrayListBidimenExcepcionesActivadas.size();contEle++){
        	System.out.println("9876validos bidimensional arrayListBidimenExcepcionesActivadas: " + arrayListBidimenExcepcionesActivadas.get(contEle));        	
        }
        //*************************** mostrado ****************************

        //*****************escribir logs   	
//        for(int contEle = 0;contEle<arrayBidimenStrBancosCuentaDesc.size();contEle++){
//           ficheroLog.InfoLog(arrayBidimenStrBancosCuentaDesc.get(contEle).toString(), "123SaldosProperties_getBancos_arrayBidimenStrBancosCuentaDesc");        	
//        }
        //**************************
        
       return arrayListBidimenExcepcionesActivadas;
	}	
}