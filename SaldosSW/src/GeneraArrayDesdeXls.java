
import java.io.File; 
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream.GetField;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.record.ArrayRecord;
import org.apache.poi.sl.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Cell;

public class GeneraArrayDesdeXls {
	//Ruta Archivo:		Desde Ventana consultaremos la ruta que seleccionó, donde se encuentra el archivo excel del banco
	//Banco:			Desde Ventana consultaremos el banco desde el que se descargó el archivo excel.
	//						dependiendo del banco, las posiciones de las celdas variará.
	
	private static String textoInfoEncabezamiento = "LOG - DetalleCC - Info:";
	private static boolean 	bHojaCorrecta = false;

	private static int numberOfTheSheetSelected = 0;

	private static String strBancoOrigen = "";

	private ArrayList<String> arrayRows = new ArrayList<String>();
	private static ArrayList<String> arrayTextoLog = new ArrayList<String>();
	private static LogsFile ficheroLog = new LogsFile();
	//private static String strMensaje = "";
	private static String strNombreLog = "";
	private static int intUltimaFila = 0;

	//Sumatorio de todos los importes del corte.
	private static double doubleTotalImportes = 0;
	
	private static ArrayList<String> arrayConversionLetraColumna = new ArrayList<>();
	private static String strLetras = "abcdefghijklmn";

	private static String strCreciente = "creciente";
	private static String strDecreciente = "decreciente";
	
	private static ArrayList<ArrayList<String>> arrayListSWResultadoBiDimensTODOS = new ArrayList<ArrayList<String>>();
	private static ArrayList<ArrayList<String>> arrayListSWResultadoBiDimens = new ArrayList<ArrayList<String>>();
	
	//Aquí asignamos el valor del banco concatenado al valor de la cuenta tal cual lo tengamos en el archivo saldos_properties.txt
	//Aquí creamos los arrayList,   uno por cada banco/cuenta   al que queramos indicar qué literales son los que condicionarán que se asigne el valor 1111 al código de agente.
	private static ArrayList<String> arrayList_Caixa_2200076885_cambianA_1111 = new ArrayList();
	private static ArrayList<String> arrayList_BBVA_1111111111_cambianA_1111 = new ArrayList();
	
	public GeneraArrayDesdeXls() throws IOException{
		arrayList_Caixa_2200076885_cambianA_1111.add(0, "SCF-TRASPASO FONDOS");arrayList_Caixa_2200076885_cambianA_1111.add(1, "MANTENIMIENTO");arrayList_Caixa_2200076885_cambianA_1111.add(2, "EXTRACTO");arrayList_Caixa_2200076885_cambianA_1111.add(3, "PRECIO ED. EXTRACTO");arrayList_Caixa_2200076885_cambianA_1111.add(4, "CUOTA T. PRIVADA");arrayList_Caixa_2200076885_cambianA_1111.add(5, "COND.CUOTA TARJETA");arrayList_Caixa_2200076885_cambianA_1111.add(6, "MANT. CERG FINANCE");
		arrayList_BBVA_1111111111_cambianA_1111.add(0, "SCF-TRASPASO FONDOS");arrayList_BBVA_1111111111_cambianA_1111.add(1, "MANTENIMIENTO");arrayList_BBVA_1111111111_cambianA_1111.add(2, "EXTRACTO");arrayList_BBVA_1111111111_cambianA_1111.add(3, "PRECIO ED. EXTRACTO");arrayList_BBVA_1111111111_cambianA_1111.add(4, "CUOTA T. PRIVADA");arrayList_BBVA_1111111111_cambianA_1111.add(5, "COND.CUOTA TARJETA");arrayList_BBVA_1111111111_cambianA_1111.add(6, "MANT. CERG FINANCE");
		
		System.out.println("6 >>>>>>> >>>>>>>>> >>>>>>> >>>>>>>>> >>>>>>> >>>>>>>>> >>>>>>> >>>>>>>>> Ventana.getStrRutaArchivoXlsBanco : " + Ventana.getStrRutaArchivoXlsBanco());
		System.out.println("6 >>>>>>> >>>>>>>>> >>>>>>> >>>>>>>>> >>>>>>> >>>>>>>>> >>>>>>> >>>>>>>>> Ventana.getStrRutaLayOut : " + Ventana.getStrRutaDestinoArchivos());
		System.out.println("6 >>>>>>> >>>>>>>>> >>>>>>> >>>>>>>>> >>>>>>> >>>>>>>>> >>>>>>> >>>>>>>>> Ventana.getStrOpcionBancoSelected : " + Ventana.getStrOpcionBancoSelected());
		System.out.println("6 >>>>>>> >>>>>>>>> >>>>>>> >>>>>>>>> >>>>>>> >>>>>>>>> >>>>>>> >>>>>>>>> Ventana.getStrDateFileName : " + Ventana.getStrDateFileName());
		
		for(int contEle=0;contEle<14;contEle++){
			arrayConversionLetraColumna.add(strLetras.substring(contEle, contEle+1));
		}
    }

    @SuppressWarnings("resource")
	public ArrayList<ArrayList<String>> generar() throws IOException{
		//ficheroLog.InfoLog("generar" ,"__3333333333___Ajuste");
//		ficheroLog.InfoLog("datos arrayListSWResultadoBiDimens.size(" + arrayListSWResultadoBiDimens.size() + ")" ,"Compartido_" + "generar01");			

		arrayListSWResultadoBiDimens.clear();
		ArrayList<ArrayList<String>> arrayRowsValidas = new ArrayList<ArrayList<String>>();
		String sRuta = "";
//		sRuta = "C:\\Work\\DetalleCC_EUR.xlsx";
		sRuta = Ventana.getStrRutaArchivoXlsBanco();
		FileInputStream file = new FileInputStream(new File(sRuta));

    	System.out.println("Antes de io io io. SRuta: " + sRuta);       	

		// Crear el objeto que tendra el libro de Excel
		HSSFWorkbook workbook = new HSSFWorkbook(file);
		
		try {
			workbook = new HSSFWorkbook(file);
		} catch (IOException e) {
	    	System.out.println("io io io");       	
			// TODO Auto-generated catch block
			//e.printStackTrace();();
		}

		HSSFSheet sheet = workbook.getSheetAt(0);
		
    	System.out.println("procesarHoja sheet.getLastRowNum(): " + sheet.getLastRowNum());       	
		
		// Se llama a procesarHoja(sheet).--> enviamos la hoja
		// Recibimos el arrayBidimensional.
//		Ventana.setStrSWMensajesErrorVentana(Ventana.getStrSWMensajesErrorVentana() + "7");
		arrayRowsValidas = procesarHoja(sheet);
//		Ventana.setStrSWMensajesErrorVentana(Ventana.getStrSWMensajesErrorVentana() + "8");
        //*************************** mostrar *****************************
    	System.out.println("procesarHoja " + arrayRowsValidas.size());       	
    	System.out.println("procesarHoja sRuta: " + sRuta);       	
        for(int contEle = 0;contEle<arrayRowsValidas.size();contEle++){
//        	System.out.println("20171005 procesarHoja : " + arrayRowsValidas.get(contEle));
    		//ficheroLog.InfoLog("20171005 fechas: " + arrayRowsValidas.get(contEle).toString(), "__1212121212___fechas");        	
        }
        //*************************** mostrado ****************************
		
//		Ventana.setStrSWMensajesErrorVentana(Ventana.getStrSWMensajesErrorVentana() + "4");
		
		//ficheroLog.InfoLog("size: " + String.valueOf(arrayRowsValidas.size()), "__3333333333___Ajuste");
//		ficheroLog.InfoLog("sRuta: " + sRuta, "_24_");
		
		workbook.close();

		System.out.println("arrayRowsValidas size: " + arrayRowsValidas.size());
		

		
/*        for(int contEle = 0;contEle<arrayRowsValidas.size();contEle++){
    		ficheroLog.InfoLog(arrayRowsValidas.get(contEle).toString(), "inicial");
        }*/
/*
    	ficheroLog.InfoLog("20171025 m(generar) arrayRowsValidas.size(): " + arrayRowsValidas.size(), "20171025_GeneraArrayDesdeXLS");        	
        for(int contEle = 0;contEle<arrayRowsValidas.size();contEle++){
        	ficheroLog.InfoLog("20171025 m(generar) arrayRowsValidas.get(contEle).toString(): " + arrayRowsValidas.get(contEle).toString(), "20171025_GeneraArrayDesdeXLS");        	
        }
*/		
		return	arrayRowsValidas;
	}
    
    @SuppressWarnings("deprecation")
	public static ArrayList<ArrayList<String>> procesarHoja(HSSFSheet sheet) throws IOException{
//		Ventana.setStrSWMensajesErrorVentana(Ventana.getStrSWMensajesErrorVentana() + "a");

    	ArrayList<ArrayList<String>> arrayResultadoBiDimens = new ArrayList<ArrayList<String>>();
//    	ArrayList<ArrayList<String>> arrayListSWResultadoBiDimens = new ArrayList<ArrayList<String>>();
    	
    	System.out.println("8 -size: procesar hoja: " + arrayListSWResultadoBiDimens.size());

    	String  strCeldaMovimiento = "";
    	String  strCeldaObservaciones = "";
    	String  strCeldaImporte = "";
    	String  strCeldaSaldo = "";
    	String  strCeldaFecha = "";
    	
    	double doubleCeldaImporte = 0;
    	System.out.println("8 -size: a " + arrayListSWResultadoBiDimens.size());

    	int intNumeroDiferenciaDatosAsaldo = 0;
    	int intNumeroFilaPrimerMovimiento = 0;
    	int intNumeroFilaPrimerMovimientoDesdeCero = 0;
    	int intNumeroFilaUltimoMovimientoDesdeCero = 0;

    	intNumeroDiferenciaDatosAsaldo = Integer.valueOf(Ventana.getStrOpcionDistanciaSaldoAdatosSelected().toString());   	
    	intNumeroFilaPrimerMovimiento = Integer.valueOf(Ventana.getStrOpcionFilaPrimerMovimientoSelected().toString());
    	System.out.println("8 -size: a " + arrayListSWResultadoBiDimens.size());
    	
    	intNumeroFilaPrimerMovimientoDesdeCero = intNumeroFilaPrimerMovimiento - 1;
    	int contRowsConInfo = sheet.getLastRowNum();
    	System.out.println("<<<antes contRowsConInfo: " + contRowsConInfo);
    	System.out.println("<<<antes intNumeroDiferenciaDatosAsaldo: " + intNumeroDiferenciaDatosAsaldo);
//		Ventana.setStrSWMensajesErrorVentana(Ventana.getStrSWMensajesErrorVentana() + "b");

    	intNumeroFilaUltimoMovimientoDesdeCero = contRowsConInfo - intNumeroDiferenciaDatosAsaldo;
    	System.out.println("<<<antes intNumeroFilaUltimoMovimientoDesdeCero: " + intNumeroFilaUltimoMovimientoDesdeCero);
    	System.out.println("8 -size: a " + arrayListSWResultadoBiDimens.size());

		//--------------- Inicio del for	-   para evaluar todos los rows del Excel ------------------
		String textoInfo = "";
		boolean incidenciasLog = false;
		
		int intPosicionInicioRowMovimiento = Ventana.getIntRowPosicionInicialMovimiento();
		int intPosicionInicioCellMovimiento = Ventana.getIntCellPosicionInicialMovimiento();
		int intPosicionInicioRowObservaciones = Ventana.getIntRowPosicionInicialObservaciones();
		int intPosicionInicioCellObservaciones = Ventana.getIntCellPosicionInicialObservaciones();
		int intPosicionInicioRowImporte = Ventana.getIntRowPosicionInicialImporte();
		int intPosicionInicioCellImporte = Ventana.getIntCellPosicionInicialImporte();
		int intPosicionInicioRowSaldo = Ventana.getIntRowPosicionInicialSaldo();
		int intPosicionInicioCellSaldo = Ventana.getIntCellPosicionInicialSaldo();
		int intPosicionInicioRowFecha = Ventana.getIntRowPosicionInicialFecha();
		int intPosicionInicioCellFecha = Ventana.getIntCellPosicionInicialFecha();
		ArrayList<String> arrayRow = new ArrayList<>();
		arrayRow.add("");
		arrayRow.add("");
		arrayRow.add("");
    	System.out.println("8 -size: a " + arrayListSWResultadoBiDimens.size());

		ArrayList<String> arraySWrow = new ArrayList<>();
		arraySWrow.add("");
		arraySWrow.add("");
		arraySWrow.add("");
		arraySWrow.add("");
		arraySWrow.add("");
//El elemento 4 es para guardar el valor de la celda: <Movimiento>

		arraySWrow.add("");
//El elemento 5 es para guardar el valor de la celda: fecha

		System.out.println("8 -size: a " + arrayListSWResultadoBiDimens.size());

    	System.out.println("<<<2 antes intNumeroFilaUltimoMovimientoDesdeCero: " + intNumeroFilaUltimoMovimientoDesdeCero);
    	System.out.println("<<<1 antes intNumeroFilaPrimerMovimientoDesdeCero: " + intNumeroFilaPrimerMovimientoDesdeCero);

    	System.out.println("8 -size: a " + arrayListSWResultadoBiDimens.size());

//		Ventana.setIntSwMovimientos(intNumeroFilaUltimoMovimientoDesdeCero - intNumeroFilaPrimerMovimientoDesdeCero + 1);
//		Ventana.setStrSWMensajesErrorVentana(Ventana.getStrSWMensajesErrorVentana() + "c");

		boolean booleanCeldaFechaAceptada = false;
		boolean booleanCeldaMovimientoAceptada = false;
		boolean booleanCeldaObservacionesAceptada = false;
		boolean booleanCeldaImporteAceptada = false;
		boolean booleanCeldaSaldoAceptada = false;
		boolean booleanEscribirRow = true;
				
    	System.out.println("8 -size: a VALOR " + arrayListSWResultadoBiDimens.size());

    	System.out.println("313: Movimientos: " + Ventana.getIntSwMovimientos()); 
    	System.out.println("313: Ventana getStrOpcionDistanciaSaldoAdatosSelected: " + Ventana.getStrOpcionDistanciaSaldoAdatosSelected()); 
    	System.out.println("313: Ventana getStrOpcionPosicionSaldoSelected: " + Ventana.getStrOpcionPosicionSaldoSelected()); 
    	System.out.println("313: Ventana getStrOpcionFilaPrimerMovimientoSelected: " + Ventana.getStrOpcionFilaPrimerMovimientoSelected()); 
			
    	doubleTotalImportes=0;
    	
    	System.out.println("datos 10 <<<<<<<<<<<<<<<<< intNumeroFilaPrimerMovimientoDesdeCero: "+ intNumeroFilaPrimerMovimientoDesdeCero);
    	System.out.println("datos 10 <<<<<<<<<<<<<<<<< intNumeroFilaUltimoMovimientoDesdeCero: "+ intNumeroFilaUltimoMovimientoDesdeCero);
    	
    	System.out.println("5 antes -size: " + arrayListSWResultadoBiDimens.size());

//    	System.out.println("8 -size for:  VALOR 3 : " + arrayListSWResultadoBiDimens.size());

		
    	
		//Ventana.setStrSWMensajesErrorVentana("");
		
//		Ventana.setStrSWMensajesErrorVentana(Ventana.getStrSWMensajesErrorVentana() + "-intNumeroFilaPrimerMovimientoDesdeCero(" + intNumeroFilaPrimerMovimientoDesdeCero + ")");
//		Ventana.setStrSWMensajesErrorVentana(Ventana.getStrSWMensajesErrorVentana() + "-intNumeroFilaUltimoMovimientoDesdeCero(" + intNumeroFilaUltimoMovimientoDesdeCero + ")");
		
		for (int contInicio= intNumeroFilaPrimerMovimientoDesdeCero ;contInicio<=intNumeroFilaUltimoMovimientoDesdeCero;contInicio++){
//	    	System.out.println("nov6 -size for:  VALOR 33 : " + arrayListSWResultadoBiDimens.size());

			//	    	System.out.println("4 antes -size: " + arrayListSWResultadoBiDimens.size());

			//	    	System.out.println("<1111 procesarHoja contInicio(" + contInicio + ")");
			//System.out.println("<2222 procesarHoja intNumeroFilaUltimoMovimientoDesdeCero(" + intNumeroFilaUltimoMovimientoDesdeCero + ")");
			//System.out.println("<3333 procesarHoja contInicio(" + contInicio + ") intPosicionInicioCellObservaciones(" + intPosicionInicioCellObservaciones + ")*************** sheet.getRow(contInicio).getCell(intPosicionInicioCellObservaciones).getCellType()" + sheet.getRow(contInicio).getCell(intPosicionInicioCellObservaciones).getCellType());
			//System.out.println("<4444 procesarHoja contInicio(" + contInicio + ") intPosicionInicioCellImporte(" + intPosicionInicioCellImporte + ")************** sheet.getRow(contInicio).getCell(intPosicionInicioCellImporte).getCellType()" + sheet.getRow(contInicio).getCell(intPosicionInicioCellImporte).getCellType());

			booleanEscribirRow = false;
			//Estas dos variables booleanas para saber si sus celdas son aptas para escribirlas en el array.
			booleanCeldaFechaAceptada = false;
			booleanCeldaMovimientoAceptada = false;
			booleanCeldaObservacionesAceptada = false;
			booleanCeldaImporteAceptada = false;
			booleanCeldaSaldoAceptada = false;

//			Ventana.setStrSWMensajesErrorVentana(Ventana.getStrSWMensajesErrorVentana() + "a");			
			strCeldaMovimiento = "";
			strCeldaObservaciones = "";
			strCeldaImporte = "";
			strCeldaSaldo = "";
			strCeldaFecha = "";
			
			
			//cotrebla:
			//	Se está asumiento que la celda 'Movimiento' se encuentra en la posicion CERO de la hoja (en el caso de la Caixa)
			//		pero habría que leer la posicición desde saldos_properties.txt
			//cotrebla:
			//	Se está asumiento que la celda 'Fecha' se encuentra en la posicion UNO de la hoja (en el caso de la Caixa)
			//		pero habría que leer la posicición desde saldos_properties.txt
			
			//********** Consultar si las celdas Fecha, Movimiento, Observaciones e Importe son de tipo String o Numeric **************************************************
			//----- Fecha: ----------------------
			if(!booleanCeldaFechaAceptada){
				try {
//					strCeldaFecha = String.valueOf(sheet.getRow(contInicio).getCell(1).getStringCellValue());
					strCeldaFecha = String.valueOf(sheet.getRow(contInicio).getCell(intPosicionInicioCellFecha).getStringCellValue());
					booleanCeldaFechaAceptada = true;					
				} catch (Exception e) {
					strCeldaFecha = "";
					booleanCeldaFechaAceptada = false;
					System.out.println("Mensaje 262 :  ************ java.lang.IllegalStateException: Cannot get a STRING value from a NUMERIC cell");
					// TODO Auto-generated catch block
					e.printStackTrace();
				}				
			}
			if(!booleanCeldaFechaAceptada){
				try {
//					strCeldaFecha = String.valueOf(sheet.getRow(contInicio).getCell(1).getDateCellValue());
					strCeldaFecha = String.valueOf(sheet.getRow(contInicio).getCell(intPosicionInicioCellFecha).getDateCellValue());
					booleanCeldaFechaAceptada = true;					
				} catch (Exception e) {
					strCeldaFecha = "";
					booleanCeldaFechaAceptada = false;
					System.out.println("Mensaje 274 *****************************************************************************************");
					// TODO Auto-generated catch block
					e.printStackTrace();
				}				
			}
			if(!booleanCeldaFechaAceptada){
				try {
//					strCeldaFecha = String.valueOf(sheet.getRow(contInicio).getCell(1).getNumericCellValue());
					strCeldaFecha = String.valueOf(sheet.getRow(contInicio).getCell(intPosicionInicioCellFecha).getNumericCellValue());
					booleanCeldaFechaAceptada = true;					
				} catch (Exception e) {
					strCeldaFecha = "";
					booleanCeldaFechaAceptada = false;					
					System.out.println("Mensaje 287 *****************************************************************************************");
					// TODO Auto-generated catch block
					e.printStackTrace();
				}	
			}			
//			ficheroLog.InfoLog(strCeldaFecha, "______celda_Fecha");

			//---------funcion para obtener fecha co formato para Omnex:			
			String strFechaParaOmnex = "";
			strFechaParaOmnex = obtenerFormatoDeFechaParaOmnex(strCeldaFecha);
			
			
			
			//-------------------------------------------		
			//----- Movimiento: ----------------------
			if (sheet.getRow(contInicio).getCell(intPosicionInicioCellMovimiento).getCellType() == Cell.CELL_TYPE_NUMERIC){
				strCeldaMovimiento = String.valueOf(sheet.getRow(contInicio).getCell(intPosicionInicioCellMovimiento).getNumericCellValue());
				booleanCeldaMovimientoAceptada = true;
			}
			
			if (sheet.getRow(contInicio).getCell(intPosicionInicioCellMovimiento).getCellType() == Cell.CELL_TYPE_STRING){
				strCeldaMovimiento = sheet.getRow(contInicio).getCell(intPosicionInicioCellMovimiento).getStringCellValue();
				booleanCeldaMovimientoAceptada = true;
			}
			//-------------------------------------------		
			//----- Observaciones: ----------------------
//			ficheroLog.InfoLog("strCeldaObservaciones 1: " + strCeldaObservaciones, "0006");			
//			ficheroLog.InfoLog("-------------------> sheet.getRow(contInicio).getCell(intPosicionInicioCellObservaciones).getCellType(): " + sheet.getRow(contInicio).getCell(intPosicionInicioCellObservaciones).getCellType(), "0006");			

			if (sheet.getRow(contInicio).getCell(intPosicionInicioCellObservaciones).getCellType() == Cell.CELL_TYPE_NUMERIC){
//				ficheroLog.InfoLog("-------------------> TIPO --------------------> 11111111111111111111111111 numeric", "0006");			
				strCeldaObservaciones = String.valueOf(sheet.getRow(contInicio).getCell(intPosicionInicioCellObservaciones).getNumericCellValue());
				booleanCeldaObservacionesAceptada = true;
			}
			
			if (sheet.getRow(contInicio).getCell(intPosicionInicioCellObservaciones).getCellType() == Cell.CELL_TYPE_STRING){
//				ficheroLog.InfoLog("-------------------> TIPO --------------------> aaaaaaaaaaaaaaaaaaaaaaaaaaaaa   String", "0006");							
				strCeldaObservaciones = sheet.getRow(contInicio).getCell(intPosicionInicioCellObservaciones).getStringCellValue();
				booleanCeldaObservacionesAceptada = true;
			}
			
//			ficheroLog.InfoLog("strCeldaObservaciones 2: " + strCeldaObservaciones, "0006");			
			//-------------------------------------------		
			//----- Importe: ----------------------------
			if (sheet.getRow(contInicio).getCell(intPosicionInicioCellImporte).getCellType() == Cell.CELL_TYPE_NUMERIC){
				strCeldaImporte = String.valueOf(sheet.getRow(contInicio).getCell(intPosicionInicioCellImporte).getNumericCellValue());
				booleanCeldaImporteAceptada = true;
			}

			if (sheet.getRow(contInicio).getCell(intPosicionInicioCellImporte).getCellType() == Cell.CELL_TYPE_STRING){
				strCeldaImporte = sheet.getRow(contInicio).getCell(intPosicionInicioCellImporte).getStringCellValue();
				booleanCeldaImporteAceptada = true;
			}
			//-------------------------------------------		
			
			
			//cotrebla:
			//Las siguientes lineas de código se deberán condicionar a si el formato del corte es con lineas de movimientos cada una con saldo.
			//como por ejemplo sucede con: La Caixa, BBVA,...
			
			//----- Saldo: ----------------------------
			if (sheet.getRow(contInicio).getCell(intPosicionInicioCellSaldo).getCellType() == Cell.CELL_TYPE_NUMERIC){
				strCeldaSaldo = String.valueOf(sheet.getRow(contInicio).getCell(intPosicionInicioCellSaldo).getNumericCellValue());
				booleanCeldaSaldoAceptada = true;
			}

			if (sheet.getRow(contInicio).getCell(intPosicionInicioCellSaldo).getCellType() == Cell.CELL_TYPE_STRING){
				strCeldaSaldo = sheet.getRow(contInicio).getCell(intPosicionInicioCellSaldo).getStringCellValue();
				booleanCeldaSaldoAceptada = true;
			}
			
			
			
			
			//*****************************************************************************************************************************************
			//			System.out.println("procesarHoja booleanos booleanCeldaImporteAceptada" + booleanCeldaImporteAceptada);
			//System.out.println("procesarHoja booleanos booleanCeldaObservacionesAceptada" + booleanCeldaObservacionesAceptada);
//noviembrecotrebla
//			System.out.println("nov6 boleanos: (" + booleanCeldaFechaAceptada + ")(" +booleanCeldaMovimientoAceptada + ")(" +booleanCeldaImporteAceptada + ")(" +booleanCeldaObservacionesAceptada + ")(" + booleanCeldaSaldoAceptada + ")");
//			System.out.println("strings: (" + strCeldaFecha + ")(" + strCeldaMovimiento + ")(" + strCeldaImporte + ")(" + strCeldaObservaciones + ")(" + strCeldaSaldo + ")");

			
			//la caixa bbva.	Cuando haya que preparar cortes que no contengan el saldo de los movimientos en cada línea, no habrá que preguntar por strCeldaSaldo
			//la caixa bbva.	Cuando haya que preparar cortes que no contengan el saldo de los movimientos en cada línea, no habrá que preguntar por strCeldaSaldo
			
//			if (booleanCeldaFechaAceptada && booleanCeldaMovimientoAceptada && booleanCeldaImporteAceptada && booleanCeldaObservacionesAceptada && booleanCeldaSaldoAceptada){
			
//			Ahora quitamos comprobación de booleanCeldaObservacionesAceptada 		para admitir cuando llegue en blanco.	
			if (booleanCeldaFechaAceptada && booleanCeldaMovimientoAceptada && booleanCeldaImporteAceptada && booleanCeldaSaldoAceptada){
//				if (!(strCeldaFecha.equals("") || strCeldaMovimiento.equals("") || strCeldaObservaciones.equals("") || strCeldaImporte.equals("") || strCeldaSaldo.equals(""))){
//				Permitimos que la celda Observaciones llegue sin contenido:		strCeldaObservaciones.equals("")
//																									 y tampoco preguntamos por el saldo.

//				if (!(strCeldaFecha.equals("") || strCeldaMovimiento.equals("") || strCeldaImporte.equals(""))){
				if (!(strCeldaFecha.equals("") || strCeldaImporte.equals(""))){
//				Ahora permitimos que strCeldaMovimiento.equals("") 
					booleanEscribirRow = true;
				}else{
					ficheroLog.InfoLog("boleanos: (" + booleanCeldaFechaAceptada + ")(" +booleanCeldaMovimientoAceptada + ")(" +booleanCeldaImporteAceptada + ")(" +booleanCeldaObservacionesAceptada + ")(" + booleanCeldaSaldoAceptada + ")","__________booleanEscribirRow");
					ficheroLog.InfoLog("strings: strCeldaFecha(" + strCeldaFecha + ")strCeldaMovimiento(" + strCeldaMovimiento + ")strCeldaImporte(" + strCeldaImporte + ")strCeldaObservaciones(" + strCeldaObservaciones + ")strCeldaSaldo(" + strCeldaSaldo + ")","__________booleanEscribirRow");					
				}
			}
			//	    	System.out.println("3 antes -size: " + arrayListSWResultadoBiDimens.size());

			if(!booleanEscribirRow){
				System.out.println("111- booleanCeldaFechaAceptada && booleanCeldaMovimientoAceptada && booleanCeldaImporteAceptada && booleanCeldaObservacionesAceptada && booleanCeldaSaldoAceptada");
				System.out.println("111- (" + booleanCeldaFechaAceptada + ")(" + booleanCeldaMovimientoAceptada + ")(" + booleanCeldaImporteAceptada + ")(" + booleanCeldaObservacionesAceptada + ")(" + booleanCeldaSaldoAceptada + ")");

				System.out.println("222-    				if (!(strCeldaFecha.equals() || strCeldaMovimiento.equals() || strCeldaImporte.equals())){");
				System.out.println("222- (" + strCeldaFecha + ")(" + strCeldaMovimiento + ")(" + strCeldaImporte + ")");
				System.out.println("333-booleanEscribirRow: " + booleanEscribirRow);				
			}
			
			//-----------------------------------------------------------------------------------------------------------------------------------------
			if (booleanEscribirRow){
//				Ventana.setStrSWMensajesErrorVentana(Ventana.getStrSWMensajesErrorVentana() + "b");
				//	System.out.println("procesarHoja ESCRIBIR row");
				
				//****** Comprobar el valor de la celda MOVIMIENTO
				//******  si su valor es algunos de los siguientes:
				//******  (SCF-TRASPASO FONDOS, MANTENIMIENTO, EXTRACTO, PRECIO ED. EXTRACTO, CUOTA T. PRIVADA, COND.CUOTA TARJETA, MANT. CERG FINANCE),
				//******  entonces se asigna valor 1111 a observaciones.
				String strCeldaMovimientoTexto = "";
				
				
				
				//cotrebla:
				//	Se está asumiento que la celda 'Movimiento' se encuentra en la posicion CERO de la hoja (en el caso de la Caixa)
				//		pero habría que leer la posicición desde saldos_properties.txt
				try {
					strCeldaMovimientoTexto = sheet.getRow(contInicio).getCell(0).getStringCellValue();
				} catch (Exception e) {
					strCeldaMovimientoTexto = "";
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				String strCodigoDeAgente = strCeldaObservaciones;
				
//				ficheroLog.InfoLog("strCodigoDeAgente: " + strCodigoDeAgente, "0005");			
				
//				Ventana.setStrSWMensajesErrorVentana(Ventana.getStrSWMensajesErrorVentana() + "u");
				
				//	La Caixa, BBVA, Posta, ...
				//	Para una cuenta hay ciertos literales en la columna <movimiento>  que significan que esa fila corresponde a un movimiento interno de SmallWorld
				//		y se debe  asignar el valor: 1111 al codigo de agente.
            	//*******************************************************************************************************************************************************
            	//
            	//	Código condicionado por banco/cuenta.
            	//	
            	//	Al código de agente: arrayListBid.get(contEle).set(2, ...		se asignará el valor: 1111		cuando el movimiento sea una operación interna de SmallWorld,
            	//	para ello se consultará el valor de la columna excel  (movimiento) , 	que en el array getSwArrayBiDimensDelExcel	lo tenemos en el elemento 4.
            	//
            	// La Caixa:
            	//			"SCF-TRASPASO FONDOS"	|| "MANTENIMIENTO"		|| "EXTRACTO") || "PRECIO ED. EXTRACTO" || 
            	//			"CUOTA T. PRIVADA"		|| "COND.CUOTA TARJETA"	|| "MANT. CERG FINANCE"
            	//
            	// BBVA ...
            	// Posta ...
            	// etc...
            	//
            	//*******************************************************************************************************************************************************
/*
				if (strCeldaMovimientoTexto.equals("SCF-TRASPASO FONDOS") || strCeldaMovimientoTexto.equals("MANTENIMIENTO") ||
					strCeldaMovimientoTexto.equals("EXTRACTO") || strCeldaMovimientoTexto.equals("PRECIO ED. EXTRACTO") ||
					strCeldaMovimientoTexto.equals("CUOTA T. PRIVADA") || strCeldaMovimientoTexto.equals("COND.CUOTA TARJETA") || 
					strCeldaMovimientoTexto.equals("MANT. CERG FINANCE")){
					strCeldaObservaciones = "1111";		
				}
*/
            	//*******************************************************************************************************************************************************
            	ArrayList<String> arrayListTemp = new ArrayList<String>();
			
            	String stringBancoCuenta = "";
            	stringBancoCuenta = Ventana.getStrOpcionBancoSelected() + Ventana.getStrOpcionCuentaSelected();

            	//los case     son valores del banco concatenado al valor de la cuenta tal cual lo tengamos en el archivo saldos_properties.txt
            	switch(stringBancoCuenta){
            		case "La Caixa - Uno Agentes2200076885":
            										arrayListTemp.addAll(arrayList_Caixa_2200076885_cambianA_1111);
            										break;
            		case "BBVA1111111111":
            										arrayListTemp.addAll(arrayList_BBVA_1111111111_cambianA_1111);
            										break;
            		default:						break;            	
            	}
           		//for para recorrer todos los literales,  para esa cuenta, 	que hacer que se asigne el valor 1111     al código de agente.
           		for (int i1111 = 0; i1111 < arrayListTemp.size(); i1111++) {
               		if (strCeldaMovimientoTexto.equals(arrayListTemp.get(i1111))){
       					//Asignamos 1111 al código de agente.
               			strCeldaObservaciones = "1111";
               		}
				}
            	//*******************************************************************************************************************************************************
            	
            	
				//********************************************************************************************************************************************************
//				Ventana.setStrSWMensajesErrorVentana(Ventana.getStrSWMensajesErrorVentana() + "v");
				
				//****** end Validaciones ******

				arrayRow.set(0, strCeldaObservaciones);
				arrayRow.set(1, strCeldaImporte);
				
				//		CodAgente   Importe		Saldo	Concepto	Observaciones.
//				Ventana.setStrSWMensajesErrorVentana(Ventana.getStrSWMensajesErrorVentana() + "w");

				arraySWrow.set(0, strCodigoDeAgente);
				arraySWrow.set(1, strCeldaImporte);
				arraySWrow.set(3, strCeldaObservaciones);
				arraySWrow.set(4, strCeldaMovimiento);
//				arraySWrow.set(5, strCeldaFecha);
				arraySWrow.set(5, strFechaParaOmnex);
				
				//Acumulamos el importe del deposito leido al total del corte que tenemos en el archivo excel del banco
				//convertir a numerico.
						
				doubleCeldaImporte = (Double)Double.valueOf(strCeldaImporte);
				doubleTotalImportes = doubleTotalImportes + doubleCeldaImporte;
//				Ventana.setStrSWMensajesErrorVentana(Ventana.getStrSWMensajesErrorVentana() + "5");

				//strCeldaImporte = strCeldaImporte.replace(".", ",");

				//saldo
				arrayRow.set(2, "");
//				arraySWrow.set(2, "");
				arraySWrow.set(2, strCeldaSaldo);
				arrayResultadoBiDimens.add(new ArrayList<>(arrayRow));
				
				//cotrebla:  Aquí hay que tener bien preparado arraySWrow,  con sus 5 valores:  importe, concepto, codAgente, observaciones, saldo.
				arrayListSWResultadoBiDimens.add(new ArrayList<>(arraySWrow));
			}else{
//				Ventana.setStrSWMensajesErrorVentana(Ventana.getStrSWMensajesErrorVentana() + "i");			
				
				Ventana.setBooleanComprobacionCorrecta(false);
				//Cuando las celdas Importe u Observaciones no han sido aceptadas generamos mensaje.
				Ventana.acumularStrTextoResultadoComprobaciones("\n" + " *** ERROR: Celdas no correctas.");
				Ventana.acumularStrTextoResultadoComprobaciones(" Valor Movimiento aceptado: " + booleanCeldaMovimientoAceptada + ".");
				Ventana.acumularStrTextoResultadoComprobaciones(" Valor Fecha aceptado: " + booleanCeldaFechaAceptada + ".");
				Ventana.acumularStrTextoResultadoComprobaciones(" Valor Observaciones aceptado: " + booleanCeldaObservacionesAceptada + ".");
				Ventana.acumularStrTextoResultadoComprobaciones(" Valor Importe aceptado: " + booleanCeldaImporteAceptada + ".");
				Ventana.acumularStrTextoResultadoComprobaciones(" (" + strCeldaMovimiento + ").");
				Ventana.acumularStrTextoResultadoComprobaciones(" (" + strCeldaFecha + ").");
				Ventana.acumularStrTextoResultadoComprobaciones(" (" + strCeldaObservaciones + ").");
				Ventana.acumularStrTextoResultadoComprobaciones(" (" + strCeldaImporte + ").");
				Ventana.acumularStrTextoResultadoComprobaciones(" ***");
			}
			//	    	System.out.println("8 -size for end1212:  continicio" + contInicio + " size: " + arrayListSWResultadoBiDimens.size());
		}
//		Ventana.setStrSWMensajesErrorVentana(Ventana.getStrSWMensajesErrorVentana() + "!");

		//    	System.out.println("8 -size for end2: " + arrayListSWResultadoBiDimens.size());

		//*********** Si el corte del archivo Excel contenía sus movimientos orden decreciente:  se modifica las posición en que se 
		//				encuentran los elementos del arrayListSWResultadoBiDimens para que se posicionen en orden decreciente.
		//				strCreciente	ctrDecreciente
		if (Ventana.getStrOpcionOrdenSelected().equals(strCreciente)){
			System.out.println("posicion orden: " + Ventana.getStrOpcionOrdenSelected());
			
			int intSizeArray = arrayListSWResultadoBiDimens.size();
			int contMover = 0; 
			ArrayList<ArrayList<String>> arrayListSWResultadoBiDimensMover = new ArrayList<>();

			System.out.println("intSizeArray: " + intSizeArray);
			for(int contEle = 0; contEle<intSizeArray; contEle++ ){
				
				contMover = intSizeArray - 1 - contEle;
				arrayListSWResultadoBiDimensMover.add(arrayListSWResultadoBiDimens.get(contMover));
			}
			System.out.println("04_setIn 111 CLEAR Antes de clear y antes de  addAll  SIZE: " + arrayListSWResultadoBiDimens.size());
			arrayListSWResultadoBiDimens.clear();
			System.out.println("04_setIn 111 CLEAR Despues de clear y antes de  addAll  SIZE: " + arrayListSWResultadoBiDimens.size());
			arrayListSWResultadoBiDimens.addAll(arrayListSWResultadoBiDimensMover);
			System.out.println("04_setIn 111 CLEAR despues de clear y despues de  addAll  SIZE: " + arrayListSWResultadoBiDimens.size());

			System.out.println("intSizeArray: " + arrayListSWResultadoBiDimens.size());
		}	
		//****************************************************************************************************************************
		System.out.println("2antesDespuesFor -size: " + arrayListSWResultadoBiDimens.size());

    	System.out.println("8 -size for end3: " + arrayListSWResultadoBiDimens.size());

		//******* Capturar el saldo y asignar su valor en el array **********************************************************
		//****** Si en saldo_properties.txt  se ha especificado    todas:		entonces se leerá desde el ultimo movimiento.
		//****** Si en saldo_properties.txt  se ha especificado    final:		entonces se leerá desde la celda que especifique.
		//****** Si en saldo_properties.txt  se ha especificado    cabecera:	entonces se leerá el importe que el operador introdujo desde la Ventana.
		//Asignar, en el array, el nuevo saldo que le corresponda.
    	Ventana.setStrNuevoSaldoCuenta(String.valueOf(0));
    	if (arrayListSWResultadoBiDimens.size()>0){
    		Ventana.setStrNuevoSaldoCuenta(swObtenerImporteSaldo(sheet));
    		
    		System.out.println("1valor de SWsaldo: " + arrayListSWResultadoBiDimens.get(0));
    		arrayListSWResultadoBiDimens.get(0).set(2, Ventana.getStrNuevoSaldoCuenta());
    		System.out.println("2valor de SWsaldo: " + Ventana.getStrNuevoSaldoCuenta());
    		System.out.println("3valor de SWsaldo: " + arrayListSWResultadoBiDimens.get(0));
    	}

    	//******************************************************************************************************************************
		System.out.println("5antesDespuesFor -size: " + arrayListSWResultadoBiDimens.size());

		System.out.println("impFinal: " + doubleTotalImportes);
		setDoubleTotalImportes(doubleTotalImportes);
		
//		Ventana.acumularStrTextoResultadoComprobaciones("\n" + " Info: Valor del sumatorio de los movimientos: " + Ventana.getDoubleSumatorioImportes());

//		Ventana.acumularStrTextoResultadoComprobaciones("\n" + " Info: Valor del sumatorio de los movimientos: " + redondearDecimales(Ventana.getDoubleSumatorioImportes(), 2));
		
		HistorialOmnex ficheroHistorialOmnex = new HistorialOmnex();
		
		
//		Ventana.setStrSWMensajesErrorVentana(Ventana.getStrSWMensajesErrorVentana() + "((((" + Ventana.getStrOpcionCuentaSelected() + "))))");
		
    	String strUltimoParametroSaldoDeLaCuenta = ficheroHistorialOmnex.obtenerUltimoSaldoDeCuenta(Ventana.getStrOpcionCuentaSelected());
//    	Ventana.acumularStrTextoResultadoComprobaciones("\n" + " (d19) Ventana.getStrOpcionCuentaSelected(): " + Ventana.getStrOpcionCuentaSelected());
//    	Ventana.acumularStrTextoResultadoComprobaciones("\n" + " (d19) strUltimoParametroSaldoDeLaCuenta: " + strUltimoParametroSaldoDeLaCuenta);
    	
//    	Ventana.setStrSWMensajesErrorVentana(Ventana.getStrSWMensajesErrorVentana() + "((((" + strUltimoParametroSaldoDeLaCuenta + "))))");

    	System.out.println("strCtastrCtastrCtastrCta strCta: " + strUltimoParametroSaldoDeLaCuenta);

    	Ventana.setSwDoubleSaldoHistorico(0.0);

		System.out.println("6antesDespuesFor -size: " + arrayListSWResultadoBiDimens.size());

    	//----Si encontró el saldo anterior -------   entonces se hacen los cálculos
    	if (!(strUltimoParametroSaldoDeLaCuenta.equals(""))){
        	String[] elems = strUltimoParametroSaldoDeLaCuenta.split("saldo:");
        	Ventana.setSwDoubleSaldoHistorico(Double.valueOf(elems[1].toString().toLowerCase().toString()));

    		Ventana.setBooleanBtnCrearExcelOmnexEnabled(true);
    		Ventana.setBooleanBtnComprobacionesEnabled(false);
    	}else{
    		Ventana.setBooleanBtnCrearExcelOmnexEnabled(false);
    		Ventana.setBooleanBtnComprobacionesEnabled(true);
        	Ventana.acumularStrTextoResultadoComprobaciones("\n" + " *** ¡¡ AVISO !! : No se ha podido consultar en el archivo histórico el saldo de esa cuenta.");
    		Ventana.setStrSWMensajesErrorVentana("\n" + " *** ¡¡ AVISO !! : No se ha podido consultar en el archivo histórico el saldo de esa cuenta.");        	
    	}
    	System.out.println("8 -size for end: 6 " + arrayListSWResultadoBiDimens.size());
		System.out.println("7antesDespuesFor -size: " + arrayListSWResultadoBiDimens.size());

    	System.out.println("05_setIntSwMovimientosSobran --- arrayListSWResultadoBiDimens.size(): " + arrayListSWResultadoBiDimens.size());
		for(int conEle=0;conEle<arrayListSWResultadoBiDimens.size();conEle++){
//        	System.out.println("055_setIntSwMovimientosSobran conEle: " + conEle + "-" + arrayListSWResultadoBiDimens.get(conEle));
    	}
    	
    	//Devuelve un número, posición del elemento que es continuación del Saldo Historial.
    	//  si no encuentra continuación:	devuelve -1
    	int intMovimientoDeContinuaciónAlCorteHistorico = -1;
    	
//		Ventana.setStrSWMensajesErrorVentana(Ventana.getStrSWMensajesErrorVentana() + "***");

    	intMovimientoDeContinuaciónAlCorteHistorico = calcularSiHayElementoDeArrayContinuacion();

//    	Ventana.setStrSWMensajesErrorVentana(Ventana.getStrSWMensajesErrorVentana() + "---(" + intMovimientoDeContinuaciónAlCorteHistorico + ")");

//    	Ventana.setStrSWMensajesErrorVentana(Ventana.getStrSWMensajesErrorVentana() + "intMovimientoDeContinuaciónAlCorteHistorico" + intMovimientoDeContinuaciónAlCorteHistorico);
    	
		System.out.println("intMovimientoDeContinuaciónAlCorteHistorico: " + intMovimientoDeContinuaciónAlCorteHistorico);
    	
		Ventana.setIntSwMovimientosSobran(0);
		Ventana.setIntSwMovimientosNoSobran(0);
		
		//*** Si ha encontrado en el corte un movimiento continuación del Historico.

		//ficheroLog.InfoLog("intMovimientoDeContinuaciónAlCorteHistorico: " + intMovimientoDeContinuaciónAlCorteHistorico, "__3333333333___Ajuste");
		
    	if (!(intMovimientoDeContinuaciónAlCorteHistorico<0)){
			System.out.println("02_setIntSwMovimientosSobran   arrayListSWResultadoBiDimens.size(): " + arrayListSWResultadoBiDimens.size());
			System.out.println("03_setIntSwMovimientosSobran   intMovimientoDeContinuaciónAlCorteHistorico: " + intMovimientoDeContinuaciónAlCorteHistorico);
			
    		Ventana.setIntSwMovimientosSobran(Integer.valueOf(arrayListSWResultadoBiDimens.size()) - intMovimientoDeContinuaciónAlCorteHistorico - 1);
    		Ventana.setIntSwMovimientosNoSobran(Integer.valueOf(arrayListSWResultadoBiDimens.size()) - Ventana.getIntSwMovimientosSobran());
    	}else{
        	//*****************************************************************************************************************************
        	//**** Si en el corte del archivo Excel no hay ningún movimiento de continuación al saldo anterior. 
    		//Si la cuenta tiene saldo anterior:
    		//	si el nuevo corte tiene continuidad:        mostraremos msg en Ventana. ----> y se habilita crear XLS
    		//	si el nuevo corte NO tiene continuidad:        mostraremos msg en Ventana. ----> y NO habilita crear XLS *******
    		//Si la cuenta NO tiene saldo anterior:
    		//		Mostraremos ese msg en Ventana. ----> y se habilita crear XLS

//        	Ventana.acumularStrTextoResultadoComprobaciones("\n" + " *** ¡¡ AVISO !! : El archivo no tiene ningún movimiento que sea continuación al saldo histórico.");
       		arrayListSWResultadoBiDimens.clear();

    		Ventana.setStrSWMensajesErrorVentana("ERROR ***** : ¡¡ AVISO !! : El archivo no tiene ningún movimiento que sea continuación al saldo histórico.");
    	}
        //*************************** mostrar *****************************
    	System.out.println("1cotrebla  : ------------------  " );
    	for(int contElea = 0;contElea<arrayListSWResultadoBiDimens.size();contElea++){
//    		System.out.println("20171002 Antes de remove:   " + arrayListSWResultadoBiDimens.get(contElea));
        }
        //*************************** mostrado ****************************

    	//******************************************************
    	//	Copiamos el array que contiene TODOS los movimientos del excel
        //******************************************************
		//ficheroLog.InfoLog("TODOS los movs. : arrayListSWResultadoBiDimens: " + arrayListSWResultadoBiDimens.size(), "__3333333333___Ajuste");

    	ArrayList<ArrayList<String>> arrayListSWResultadoBiDimensTODOS = new ArrayList<ArrayList<String>>();
    	arrayListSWResultadoBiDimensTODOS.clear();
    	arrayListSWResultadoBiDimensTODOS.addAll(arrayListSWResultadoBiDimens);
    	
    	
    	
    	//**************************************************************************************************************************************
    	//**** Si hay movimientos que sobran:		Grabamos los movimientos sobrantes en array de Ventana.
    	//Ventanaarr
    	
		
    	
    	
    	ArrayList<ArrayList<String>> ArrayDatosMovs = new ArrayList<ArrayList<String>>();
    	if (Ventana.getIntSwMovimientosSobran()>0){
    		ArrayDatosMovs = generarArrayConMovimientosSobrantes(Ventana.getIntSwMovimientosSobran(), arrayListSWResultadoBiDimens);
    		Ventana.addAllArrayListSWMovimientosSobrantes(ArrayDatosMovs);
    	}

    	//**************************************************************************************************************************************
    	//**** Si hay movimientos que sobran:		eliminamos esos elementos del array.
		//ficheroLog.InfoLog("Movimientos que sobran: " + Ventana.getIntSwMovimientosSobran(), "__3333333333___Ajuste");

    	if (Ventana.getIntSwMovimientosSobran()>0){
    		//--se eliminan los ultimos elementos ----------------
    		int intPosicionUltimoElemento =0;
    		for(int contEle=0 ; contEle<Ventana.getIntSwMovimientosSobran() ; contEle++) {
    			intPosicionUltimoElemento = arrayListSWResultadoBiDimens.size() - 1;
    			arrayListSWResultadoBiDimens.remove(intPosicionUltimoElemento);
    		}
    		
    		Ventana.setSwDoubleSumatorioImportes(Double.valueOf(0));
    		for(int contEle=0;contEle<arrayListSWResultadoBiDimens.size();contEle++) {
    			String strImp = arrayListSWResultadoBiDimens.get(contEle).get(1).toString();
				Ventana.setSwDoubleSumatorioImportes(Ventana.getSwDoubleSumatorioImportes()+Double.valueOf(strImp));
    		}
    	}
    		
		if (incidenciasLog){
			//--------------------------------------------- Grabar en el fichero Logs.
//			LogsFile fileLog = new LogsFile();
//			fileLog.InfoLog(contRowsDepositos, arrayTextoLog, "DetalleCC");
			//------------------------------------------------------------------------			
		}

		//-----Escribir en Logs
		for(int contEle = 0; contEle<arrayResultadoBiDimens.size();contEle++){
//			System.out.println("1111 azaz contEle: " + contEle + " valor: " + arrayResultadoBiDimens.get(contEle));
		}		
		//---------------------------------------------------
		//ficheroLog.InfoLog("Empieza el for ------>    de arrayListSWResultadoBiDimens " + arrayListSWResultadoBiDimens.size(), "__3333333333___Ajuste");
		for(int contEle = 0; contEle<arrayListSWResultadoBiDimens.size();contEle++){
//			System.out.println("20171002 contEle: " + contEle + " valor: " + arrayListSWResultadoBiDimens.get(contEle));
//			ficheroLog.InfoLog(arrayListSWResultadoBiDimens.get(contEle).toString(), "__3333333333___Ajuste");
		}		

		//cotrebla: Actualmente este no tiene este formato, pero debería ser:
		//		CodAgente   Importe		Concepto	Observaciones	Importe		Saldo.

/*		
    	ficheroLog.InfoLog("20171025 m(ProcesaHoja) arrayListSWResultadoBiDimens.size(): " + arrayListSWResultadoBiDimens.size(), "20171025_GeneraArrayDesdeXLS");        	
        for(int contEle = 0;contEle<arrayListSWResultadoBiDimens.size();contEle++){
        	ficheroLog.InfoLog("20171025 m(ProcesaHoja) arrayListSWResultadoBiDimens.toString(): " + arrayListSWResultadoBiDimens.get(contEle).toString(), "20171025_GeneraArrayDesdeXLS");        	
        }
*/
//		return arrayResultadoBiDimens;
//		for (int iProcesarHoja = 0; iProcesarHoja < arrayListSWResultadoBiDimens.size(); iProcesarHoja++) {
//			ficheroLog.InfoLog("arrayListSWResultadoBiDimens: " + arrayListSWResultadoBiDimens.get(iProcesarHoja).toString(), "0004_Procesar_Hoja");
//		}
		
		return arrayListSWResultadoBiDimens;	
    } 
  
	public static String swObtenerImporteSaldo(HSSFSheet sheet1) throws IOException{
		String strImporteSaldo = "";
		int intElementosEnArrayDelCorte = arrayListSWResultadoBiDimens.size();

		String[] elems = Ventana.getStrOpcionPosicionSaldoSelected().split(":");
    	String strColumna = elems[1].toString().toLowerCase();
    	int intPosicionColumna = -1;
  
    	intPosicionColumna = Ventana.obtenerPosicionColumnaDesdeLetra(strColumna);
    	
		if(Ventana.getStrOpcionPosicionSaldoSelected().startsWith("todas:")){	
			int intRowDondeSeEncuentraElSaldoNuevDesdeCero = -1;

			if(Ventana.getStrOpcionOrdenSelected().equals(strCreciente)){
				intRowDondeSeEncuentraElSaldoNuevDesdeCero = Integer.valueOf(Ventana.getStrOpcionFilaPrimerMovimientoSelected()) - 1 + intElementosEnArrayDelCorte - 1 ;
//				strImporteSaldo = sheet1.getRow(19).getCell(intPosicionColumna).getStringCellValue().toString();
				strImporteSaldo = String.valueOf(sheet1.getRow(intRowDondeSeEncuentraElSaldoNuevDesdeCero).getCell(intPosicionColumna).getNumericCellValue());
							System.out.println("1 998877   strImporteSaldo: " + strImporteSaldo);
				
			}
			if(Ventana.getStrOpcionOrdenSelected().equals(strDecreciente)){
				intRowDondeSeEncuentraElSaldoNuevDesdeCero = Integer.valueOf(Ventana.getStrOpcionFilaPrimerMovimientoSelected()) - 1;
				strImporteSaldo = String.valueOf(sheet1.getRow(intRowDondeSeEncuentraElSaldoNuevDesdeCero).getCell(intPosicionColumna).getNumericCellValue());
				System.out.println("2 998877   strImporteSaldo: " + strImporteSaldo);

			}
			System.out.println("3 998877   strImporteSaldo: " + strImporteSaldo);
		}
		//hay que controlar   como viene el saldo (caixa,  bbva,   posta,   etc)
		if(Ventana.getStrOpcionPosicionSaldoSelected().startsWith("final:")){		
		
		}
		if(Ventana.getStrOpcionPosicionSaldoSelected().startsWith("cabecera:")){
		
		}
		
		
		

		
		
		
		
    	for(int contEle=0;contEle<arrayListSWResultadoBiDimens.size();contEle++){
//			System.out.println("181818 * * * * * * * * * * * * : " + arrayListSWResultadoBiDimens.get(contEle).toString());
		}
		System.out.println("181818   getStrOpcionPosicionSaldoSelected 			: " + Ventana.getStrOpcionPosicionSaldoSelected());
		System.out.println("181818   getStrOpcionDistanciaSaldoAdatosSelected 	: " + Ventana.getStrOpcionDistanciaSaldoAdatosSelected());
		System.out.println("181818   getStrOpcionFilaPrimerMovimientoSelected 	: " + Ventana.getStrOpcionFilaPrimerMovimientoSelected());
		System.out.println("181818   getStrOpcionOrdenSelected					: " + Ventana.getStrOpcionOrdenSelected());
		System.out.println("181818   getStrOpcionPosicionSaldoSelected 			: " + Ventana.getStrOpcionPosicionSaldoSelected());

		
		//Con Ventana.getIntIndiceCB()   conocemos qué linea de saldos_properties tenemos que leer
		int intElementoArraySaldosProperties = 0;
		intElementoArraySaldosProperties = Ventana.getIntIndiceCB() - 1;
/*		
		valorColumna
		valorFila
		
		Desde las coordenadas de saldo_properties
			------------------------------------------------------------------------------------------------------------------------------------------------
			si en coordenadas está 'todos:'
				Para obtener la columna:
					desde el primer caracter siguiente a 'todos:'	---> convertimos a número ese carácter, para que represente la columna ( A=0, B=1, etc )
				Para obtener la fila:
					y la fila es la que tenga en ese momento.
				
				Entonces leemos sheet.row etc  y lo retornamos.
			------------------------------------------------------------------------------------------------------------------------------------------------
			si en coordenadas está 'cabecera:'
				Para obtener la columna:
					desde el primer caracter siguiente a 'cabecera'	---> convertimos a número ese carácter, para que represente la columna ( A=0, B=1, etc )
				Para obtener la fila:
					obtenemos los siguientes caracteres siguientes	---> así obtenemos la fila (para el programa la primera fila en Excel es la cero).
				
				Entonces leemos sheet.row etc y lo retornamos.
			------------------------------------------------------------------------------------------------------------------------------------------------
			si en coordenadas está 'final:''
				Para obtener la columna:
					desde el primer caracter siguiente a 'final:'	---> convertimos a número ese carácter, para que represente la columna ( A=0, B=1, etc )
				Para obtener la fila:
					consultamos en Ventana.getRowsExcel				---> así obtenemos la fila (para el programa la primera fila en Excel es la cero).
					
				Entonces leemos sheet.row etc y lo retornamos.
			------------------------------------------------------------------------------------------------------------------------------------------------
*/
//		if (strTipoPosicion.equals("cabecera")){
//			En esta opción el importe del saldo aparece en la cabecera.			
//			strImporte = Ventana.getStrSaldoCuenta();
//		}

		//ficheroLog.InfoLog("20171002 ***** strImporteSaldo: " + strImporteSaldo, "__3333333333___Ajuste");
		System.out.println("20171002 ***** strImporteSaldo: " + strImporteSaldo);
		return strImporteSaldo;
	}
	
	public static void setDoubleTotalImportes(Double impTot){
		doubleTotalImportes = impTot;
	}
	
	private static int calcularSiHayElementoDeArrayContinuacion(){

		int intValor = -1;
		System.out.println("calcularSiHayElementoDeArrayContinuacion: " + Ventana.getStrNuevoSaldoCuenta());
		
		Double doubleSaldoOperacion = 0.0;
		Double doubleImporteDelElemento = 0.0;
		
		doubleSaldoOperacion = Double.valueOf(Ventana.getStrNuevoSaldoCuenta());
		
		
//		try {ficheroLog.InfoLog("**** doubleSaldoOperacion: " + doubleSaldoOperacion, "__3333333333___calcularSiHayElementoDeArrayContinuacion");} catch (IOException e) {e.printStackTrace();}
//		try {ficheroLog.InfoLog("**** Ventana.getDoubleTotalSumatorioAjustes(): " + Ventana.getDoubleTotalSumatorioAjustes(), "__3333333333___calcularSiHayElementoDeArrayContinuacion");} catch (IOException e) {e.printStackTrace();}
		//****************************************************************************************************************
		//	Por si ha habido algún ajuste del saldo por parte del operador,   se aplicamos el ajuste al SaldoOperacion qur tuviera.
		// Se resta el ajuste porque si el saldo historico   debia ser mayor por algún abono,  habrá quese había visto
		doubleSaldoOperacion = doubleSaldoOperacion - Ventana.getDoubleTotalSumatorioAjustes();
		//****************************************************************************************************************
//		try {ficheroLog.InfoLog("**** doubleSaldoOperacion: " + doubleSaldoOperacion, "__3333333333___calcularSiHayElementoDeArrayContinuacion");} catch (IOException e) {e.printStackTrace();}
		
		
		System.out.println("resultados: Saldo Nuevo: " + Ventana.getStrNuevoSaldoCuenta());
    	System.out.println("resultados: SaldoHistorico: " +  Ventana.getSwDoubleSumatorioImportes());
    	
//    	for(int conEleFor=0;conEleFor<arrayListSWResultadoBiDimens.size();conEleFor++){
////        	System.out.println("elementos1616 arrayListSWResultadoBiDimens (" + conEleFor + ")" +  arrayListSWResultadoBiDimens.get(conEleFor));   		
//    	}

		System.out.println("222   Ventana.getSwDoubleSaldoHistorico(): " + Ventana.getSwDoubleSaldoHistorico());
		
//		try {ficheroLog.InfoLog("arrayListSWResultadoBiDimens.size(): " + arrayListSWResultadoBiDimens.size(), "__3333333333___calcularSiHayElementoDeArrayContinuacion");} catch (IOException e) {e.printStackTrace();}
//		try {ficheroLog.InfoLog("arrayListSWResultadoBiDimens.size()    FOR", "__3333333333___calcularSiHayElementoDeArrayContinuacion");} catch (IOException e) {e.printStackTrace();}

    	for(int conEle=0;conEle<arrayListSWResultadoBiDimens.size();conEle++){
//    		System.out.println("1999 conEle: " + conEle + "--------------------------------------------");
//    		System.out.println("1999 Si al saldo del Archivo EXCEL: " + doubleSaldoOperacion);
    		doubleImporteDelElemento = Double.valueOf(arrayListSWResultadoBiDimens.get(conEle).get(1));
//    		System.out.println("1999 apartamos importe del movimiento: " + doubleImporteDelElemento);
//    		System.out.print("1999 " + doubleSaldoOperacion + " - " + doubleImporteDelElemento + " = ");
    		
//    		System.out.println("111   				 doubleSaldoOperacion - doubleImporteDelElemento = " + doubleSaldoOperacion + " - " + doubleImporteDelElemento);

//			System.out.println("1999 --!!!!----con contEle (" + conEle + ")doubleSaldoOperacion: (" + doubleSaldoOperacion + ") Importe: (" + doubleImporteDelElemento + ")");
			
    		doubleSaldoOperacion = doubleSaldoOperacion - doubleImporteDelElemento;

    		//Redondeo con 2 decimales.    		
    		doubleSaldoOperacion = redondearDecimales(doubleSaldoOperacion, 2);

//			System.out.println("1999 --!!!!--------------------> resultado de esa SUMA   doubleSaldoOperacion: " + doubleSaldoOperacion + ")doubleSaldoOperacion: " + doubleSaldoOperacion);
//			System.out.println("1999 --versus Ventana.getSwDoubleSaldoHistorico()	: " + Ventana.getSwDoubleSaldoHistorico());
    		
//			System.out.println("1999 --!!!!->>>>--- doubleSaldoOperacion: " + doubleSaldoOperacion);
//			System.out.println("1999 --!!!!----     getSwDoubleSaldoHistorico: " + Ventana.getSwDoubleSaldoHistorico());
			
//    		try {ficheroLog.InfoLog("Dentro del for. conEle(" + conEle + ")arrayListSWResultadoBiDimens(" + arrayListSWResultadoBiDimens.get(conEle).get(1) + ")","__3333333333___calcularSiHayElementoDeArrayContinuacion");} catch (IOException e) {e.printStackTrace();}
//    		try {ficheroLog.InfoLog("------------------------------------------ doubleSaldoOperacion(" + doubleSaldoOperacion + ")(Ventana.getSwDoubleSaldoHistorico(" + Ventana.getSwDoubleSaldoHistorico() + ")","__3333333333___calcularSiHayElementoDeArrayContinuacion");} catch (IOException e) {e.printStackTrace();}

    		//noviembrecotrebla
//    		System.out.println(conEle + "-->conEle. nov 6 valor: "	+ intValor + "*** nov 6 doubleSaldoOperacion:(" + doubleSaldoOperacion + ") Ventana.getSwDoubleSaldoHistorico: (" + Ventana.getSwDoubleSaldoHistorico() + ")");
					
    		if (doubleSaldoOperacion.equals(Ventana.getSwDoubleSaldoHistorico())){
    			intValor = conEle;
    		}
    	}
		System.out.println("1999 -- calcularSiHayElementoDeArrayContinuacion -------- devuelve valor: "	+ intValor);

		return intValor;
	}
    public static double redondearDecimales(double valorInicial, int intNumeroDecimales) {
    	double parteEntera, resultado;
        resultado = valorInicial;
        parteEntera = Math.floor(resultado);
        
        resultado=(resultado-parteEntera)*Math.pow(10, intNumeroDecimales);
        resultado=Math.round(resultado);
        resultado=(resultado/Math.pow(10, intNumeroDecimales))+parteEntera;
        return resultado;
    }
    
	private static ArrayList<ArrayList<String>> generarArrayConMovimientosSobrantes(int intMovsSobrantes, ArrayList<ArrayList<String>> arrayListDatos) throws IOException{	

		ArrayList<ArrayList<String>> arrayListBidimen = new ArrayList<ArrayList<String>>();
		//---El array que se recibe por argumento lo leemos desde el primer movimiento sobrante.
		int intSizeArrayListDatos = arrayListDatos.size();
		int intPrimerElementoSobrante = intSizeArrayListDatos - intMovsSobrantes;

		System.out.println("**0906 size: " + intSizeArrayListDatos);
		System.out.println("**0906 intMovsSobrantes: " + intMovsSobrantes);
		System.out.println("**0906 intPrimerElementoSobrante: " + intPrimerElementoSobrante);

		for(int contEle = intPrimerElementoSobrante; contEle<intSizeArrayListDatos; contEle++ ){
			arrayListBidimen.add(arrayListDatos.get(contEle));
		}
		//---mostrar
		for(int cont = 0; cont<arrayListDatos.size() ; cont++){
//			System.out.println(cont + "/No sobrantes: " + arrayListDatos.get(cont));			
		}
		for(int cont = 0; cont<arrayListBidimen.size() ; cont++){
//			System.out.println(cont + "/sobrantes: " + arrayListBidimen.get(cont));			
		}
		return arrayListBidimen;
	}

	private static String obtenerFormatoDeFechaParaOmnex(String strFechaDeCelda) throws IOException{
//		System.out.println("1111. 7nov***strFechaDeCelda--->: " + strFechaDeCelda);

//		ficheroLog.InfoLog("strFechaDeCelda: " + strFechaDeCelda ,"__fecha___");
		
		//---------funcion para obtener fecha con formato para Omnex:
		String strFechaFormateada = "";
		//------------------------------------------------------------------------------------------------------	
		//1. Si el formato de la fecha en la celda es el del tipo:		Tue Sep 19 00:00:00 CEST 2017 
		//------------------------------------------------------------------------------------------------------
		String strMonthNumero = "00";
		String[] elemsFechaEspacios = strFechaDeCelda.split(" ");
		String[] elemsFechaBarra = strFechaDeCelda.split("//");
		String[] elemsFechaBarraSimple = strFechaDeCelda.split("/");
		
//		ficheroLog.InfoLog("elemsFechaEspacios.length: " + elemsFechaEspacios.length ,"__fecha___");
//		ficheroLog.InfoLog("elemsFechaBarra.length: " + elemsFechaBarra.length ,"__fecha___");
//		ficheroLog.InfoLog("elemsFechaBarraSimple.length: " + elemsFechaBarraSimple.length ,"__fecha___");

		if (elemsFechaEspacios.length==6){//6 espacios son los que se encuentra en ese formato:		Tue Sep 19 00:00:00 CEST 2017
			//Jan,	Feb,	Mar,	Apr,	May,	Jun,	Jul,	Aug,	Sep,	Oct,	Nov,	Dec
			//Convertir mes a número.
			switch(elemsFechaEspacios[1].toString()){
				case "Jan": strMonthNumero = "01"; break;
				case "Feb": strMonthNumero = "02"; break;
				case "Mar": strMonthNumero = "03"; break;
				case "Apr": strMonthNumero = "04"; break;
				case "May": strMonthNumero = "05"; break;
				case "Jun": strMonthNumero = "06"; break;
				case "Jul": strMonthNumero = "07"; break;
				case "Aug": strMonthNumero = "08"; break;
				case "Sep": strMonthNumero = "09"; break;
				case "Oct": strMonthNumero = "10"; break;
				case "Nov": strMonthNumero = "11"; break;
				case "Dec": strMonthNumero = "12"; break;
				default:	
					strMonthNumero = "00";
		    		Ventana.setStrSWMensajesErrorVentana("Error *** No se ha podido leer la fecha de un depósito.");
					break;
			}
			strFechaFormateada = elemsFechaEspacios[2] + "/" + strMonthNumero + "/" + elemsFechaEspacios[5];			
		}
		//------------------------------------------------------------------------------------------------------	
		//2. Si el formato de la fecha en la celda es el del tipo:		19/06/2017	con doble separador /
		//------------------------------------------------------------------------------------------------------
		if (elemsFechaBarra.length==3){
			strFechaFormateada = strFechaDeCelda;
		}
		//------------------------------------------------------------------------------------------------------	
		//3. Si el formato de la fecha en la celda es el del tipo:		19/06/2017	con separador simple /
		//------------------------------------------------------------------------------------------------------
		if (elemsFechaBarraSimple.length==3){
			strFechaFormateada = strFechaDeCelda;
		}
		//------------------------------------------------------------------------------------------------------	
		//****** El formato de la fecha que necesita Omnex para importar el archivo es:		mm/dd/aaaa
		//------------------------------------------------------------------------------------------------------
//		ficheroLog.InfoLog("strFechaFormateada: " + strFechaFormateada ,"__fecha___");

//		System.out.println("7nov***fecha--->: " + strFechaFormateada);
		String[] elemsFecha = strFechaFormateada.split("/");
		strFechaFormateada = elemsFecha[1] + "/" + elemsFecha[0] + "/" + elemsFecha[2];			
		
//		ficheroLog.InfoLog("elemsFecha[1]" + elemsFecha[1] ,"__fecha___");
//		ficheroLog.InfoLog("elemsFecha[0]" + elemsFecha[0] ,"__fecha___");
//		ficheroLog.InfoLog("elemsFecha[2]" + elemsFecha[2] ,"__fecha___");

		//------------------------------------------------------------------------------------------------------
//		System.out.println("***fecha--->: " + strFechaFormateada);
		return strFechaFormateada;
	}
}
