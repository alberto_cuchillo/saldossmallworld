import java.lang.Runtime;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
 
import java.io.IOException;

import javax.swing.ComboBoxEditor;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;

public class Ventana extends javax.swing.JFrame {
	//0904 cambios
    private static javax.swing.JButton btnSeleccionarDetalleCC;
    
//  private javax.swing.JButton btnAceptarFicheros;
    private static javax.swing.JButton btnInicio;
    private static javax.swing.JButton btnCrearExcelOmnex;
    private static javax.swing.JButton btnComprobaciones;
    private static javax.swing.JButton btnAceptarCorteOmnex;
    private static javax.swing.JButton btnAbrirExcelGenerado;
    private static javax.swing.JButton btnAjustarSaldoCuenta;    
    
    private static javax.swing.JTextArea txtAreaRutasArchivos;
    private static javax.swing.JTextArea txtAreaResultadoComprobaciones;
    private static javax.swing.JTextArea txtAreaResultadoComprobacionesAjustes;
    private static javax.swing.JTextArea txtAreaAjustesAlmacenados;
    private static javax.swing.JTextField txtFieldSaldo;

//  private javax.swing.JComboBox<String> cbOpcionesLayout;
    private static javax.swing.JComboBox<String> cbOpcionesLayout;

//  Formulario para ajustar el saldo en una cuenta.
    private static javax.swing.JTextField txtFieldFormAjustarSaldoCuentaAgente;
    private static javax.swing.JTextField txtFieldFormAjustarSaldoCuentaImporte;
    private static javax.swing.JTextField txtFieldFormAjustarSaldoCuentaDatos;
    private static javax.swing.JButton btnComprobarAjuste;
//  private static javax.swing.JButton btnVolverAinsertarAjuste;
    private static javax.swing.JButton btnCrearExcelOmnexConAjustes;
//    private static javax.swing.JComboBox<String> cbFormAjustarSaldoCuentaOpcionesCuenta;   
    
    private static javax.swing.JLabel jLabelMensaje = new javax.swing.JLabel();

    private static javax.swing.JLabel jLabelFieldSaldo;

    private static javax.swing.JLabel jLabelCabecera;
    private static javax.swing.JLabel jLabelResultadoOmnex;
    
    private static javax.swing.JLabel jLabelPedirFormatoLayout;
    private static javax.swing.JLabel jLabelCabecera0AjustarSaldo;
    private static javax.swing.JLabel jLabelCabecera1AjustarSaldo;
    private static javax.swing.JLabel jLabelCabecera2AjustarSaldo;
    private static javax.swing.JLabel jLabelAjustesMsjError;
//    private static javax.swing.JLabel jLabelAjustesAlmacenados;
//    private static javax.swing.JLabel jLabelPedirCuentaParaAjustarSaldo;
    private static javax.swing.JLabel jLabelSaldoDelCorte;
    private static javax.swing.JLabel jLabelPedirDocumentoDetalleCC;
    private static javax.swing.JLabel jLabelFormAjustarAgente;
    private static javax.swing.JLabel jLabelFormAjustarImporte;
    private static javax.swing.JLabel jLabelFormAjustarDatos;
    private static javax.swing.JLabel jLabelFormAjustarCuentaCabeceraValor;    

    private static javax.swing.JTextField txtRutaDetalleCC;

	private static ArrayList<String> arrayTest = new ArrayList<String>();    

	private static String strCarpetaArchivosLayout = "Saldos_Omnex";
	private static String strCarpetaArchivosLog = "Saldos_Log";
	private static String strCarpetaHistorialOmnex = "Historial_Omnex";

	private static String strOpcionBancoNoSelected = "Seleccionar Banco...";
//	private static String strOpcionBancoBBVA = "BBVA";
//	private static String strOpcionBancoLaCaixa = "La Caixa";
//	private static String strOpcionBancoPosta = "POSTA";

	private static String strOpcionBancoSelected = "Seleccionar Banco...";
	private static String strOpcionCuentaSelected = "";
	private static String strOpcionDescripcionSelected = "";
	private static String strOpcionDatosIdAgenteSelected = "";
	private static String strOpcionImporteSelected  = "";        	
	private static String strOpcionIdentificadorSelected = "";
	private static String strOpcionFilaPrimerMovimientoSelected = "";
	private static String strOpcionPosicionSaldoSelected = "";
	private static String strOpcionDistanciaSaldoAdatosSelected = "";
	private static String strOpcionOrdenSelected = "";	
	private static String strOpcionPosicionMovimientoSelected = "";	
	private static String strOpcionPosicionFechaSelected = "";

	private static String strRutaDestinoArchivos = "";
	private static String strRutaArchivoXlsBanco = "";
	private static String strDateFileName = "";
	private static String strCuentaBanco = "";
	private static String strRutaJAR = "";

	private static int intRowPosicionInicialMovimiento = 0;
	private static int intCellPosicionInicialMovimiento = 0;
	private static int intRowPosicionInicialObservaciones = 0;
	private static int intCellPosicionInicialObservaciones = 0;
	private static int intRowPosicionInicialImporte = 0;
	private static int intCellPosicionInicialImporte = 0;
	private static int intRowPosicionInicialSaldo = 0;
	private static int intCellPosicionInicialSaldo = 0;
	private static int intRowPosicionInicialFecha = 0;
	private static int intCellPosicionInicialFecha = 0;
	private static SaldosProperties datosSaldosProperties = new SaldosProperties();
	
	private static ArrayList<ArrayList<String>> arrayListBidimSaldosProperties = new ArrayList<ArrayList<String>>();
	private static ArrayList<ArrayList<String>> arrayListAjustesAlmacenados = new ArrayList<ArrayList<String>>();
	private static Double doubleTotalSumatorioAjustes = 0.0;

	//Array para escribir en el HistorialOmnex
	private static ArrayList<String> arrayCortePreparadoParaHistorialOmnex = new ArrayList<String>();
	
	private static LogsFile ficheroLog = new LogsFile();

	private static ArrayList<String> arrayMovimientoSaldo = new ArrayList<String>();    
	
	private static String strNuevoSaldoCuenta = "";
	
	private static int intIndiceCB = 0;

	private static double swDoubleSumatorioImportes = 0;

//	private static String strTextoResultadoComprobaciones = "\n" + "Resultado de las comprobaciones:" + "\n";
	private static String strTextoResultadoComprobaciones = "";

	private static boolean booleanComprobacionCorrecta = true;
	
	private static String strNombreDelArchivoXLS = "";

	private static boolean booleanBtnComprobacionesEnabled = true;
	private static boolean booleanBtnCrearExcelOmnexEnabled = true;

//	private static String strFileExcelAgentes = "C:\\Work\\20170406_Saldos_LaCaixa_BBVA_etc\\agentes.xls";
	private static String strFileExcelAgentes = "";

	private static String strSWMensajesErrorVentana = "";
	private static String strPruebaCodigo = "";
	
	private static ArrayList<String> arrayListSWAgentesValidos = new ArrayList<String>();
	private static ArrayList<ArrayList<String>> arrayListSWBidimSaldosPropertiesCuentas = new ArrayList<ArrayList<String>>();
	private static String strSWrutaDestinoFicherosGenerados = "";
	private static String strSWrutaAgentesActivos = "";
	private static String strTipoConfigInicioErrorSaldosPropertiesFormato = "TipoConfigInicioErrorSaldosPropertiesFormato";
	private static String strTipoConfigComprobacionesCorrectas = "TipoConfigComprobacionesCorrectas";
	private static String strTipoConfigCrearExcelCorrecto = "TipoConfigCrearExcelCorrecto";
	private static String strTipoConfigBotonInicio = "TipoConfigBotonInicio";
	
	private static String strSWletras = "abcdefghijklmn";
	private static String strMsgTxtFieldSaldoSinValor = "No se ha introducido el valor del nuevo saldo final.";
	
	//ArrayList  desde archivo saldos_properteis Excepciones:	banco, cuenta, agente, nuevocodigo.
	private static ArrayList<ArrayList<String>> arrayListSWBidimExcepcionesAgentesActivados = new ArrayList<ArrayList<String>>();
	//ArrayList  desde lectura del excel seleccionado.
	private static ArrayList<ArrayList<String>> swArrayBiDimensDelExcel = new ArrayList<ArrayList<String>>();

	private static ArrayList<ArrayList<String>> swArrayDatosParaOmnex = new ArrayList<ArrayList<String>>();

	private static int intSwMovimientos = 0;

	private static Double swDoubleSaldoHistorico = 0.0;

	private static int intSwMovimientosSobran = 0;
	private static int intSwMovimientosNoSobran = 0;
    
	private static String strTextoAreaRutasArchivos = "";

	private static ArrayList<String> arrayStrRutasFicherosGenerados = new ArrayList<>();

	//ArrayList con los movimientos sobrantes del excel seleccionado.
	private static ArrayList<ArrayList<String>> arrayListSWMovimientosSobrantes = new ArrayList<ArrayList<String>>();

	//ArrayList con los movimientos sobrantes del excel seleccionado.
	private static String strRutaFileNameExcelGenerado = "";
    private static String strDate = "";
    private static Calendar cal;
    private static String strRespuestaComprobacionFormatoSaldosProperties = "";
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
    private static String strDateExcelOmnex = "";
    private static String strVersion = "v1.4.1";
    private static String strAjusteSW = "ajusteSW";

    private static ArrayList<ArrayList<String>> arrayListStr10historicos = new ArrayList<ArrayList<String>>();
    private static String strBancoCuenta_Caixa_2200076885 = "La Caixa - Uno Agentes2200076885";
    private static String strBancoCuenta_BBVA_0208509436 = "Bbva - Agentes0208509436";
    private static String strBancoCuenta_BBVA_1111111111 = "BBVA1111111111";

//    private static String strBancoEnSaldosProperties = "";

    @SuppressWarnings("deprecation")

	public static void main(String args[]) throws IOException{

		System.out.println("***** Main.");
		
		strRespuestaComprobacionFormatoSaldosProperties = "";
		cal = Calendar.getInstance();
		sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
	    strDate = sdf.format(cal.getTime());
	    setStrDateFileName(strDate);
	    
	    String strFechayyyyMMdd_HHmmss = "";
	    strFechayyyyMMdd_HHmmss = obtenerFechayyyyMMdd_HHmmss();
	    strDate = strFechayyyyMMdd_HHmmss;
	    setStrDateFileName(strDate);
		
	    //Fecha del dia, con formato para grabar en el excel que importar� Omnex.
	    SimpleDateFormat sdfOmnex = new SimpleDateFormat("MM/dd/yyyy");
	    strDateExcelOmnex = sdfOmnex.format(cal.getTime());
	    setStrDateExcelOmnex(strDateExcelOmnex);
	    strSWMensajesErrorVentana = "";
    	strRutaJAR = Ventana.class.getProtectionDomain().getCodeSource().getLocation().toString();
/*    	
    	strRespuestaComprobacionFormatoSaldosProperties = datosSaldosProperties.comprobarFormatoValido(strRutaJAR);
    	//Si el formato es incorrecto ---> no se contin�a con los procesos y mostramos en Ventana un mensaje con la incidencia ocurrida.
    	if (!(strRespuestaComprobacionFormatoSaldosProperties.equals(""))){
    		System.out.println("***** Ventana formato incorrecto");
    		Ventana.acumularStrTextoResultadoComprobaciones("\n" + strRespuestaComprobacionFormatoSaldosProperties);
//			strTextoResultadoComprobaciones = "";
//    		Ventana.txtAreaResultadoComprobaciones.setText("aaabbbcccstrTextoResultadoComprobaciones");    		
    		setBooleanComprobacionCorrecta(false);
    	}
    	
		System.out.println("***** Ventana: strRutaJAR: " + strRutaJAR);
		System.out.println("***** Ventana: strRespuestaComprobacionFormtatoSaldosProperties: " + strRespuestaComprobacionFormatoSaldosProperties);
		System.out.println("***** Ventana ***** formato***** formato***** formato***** formato***** formato***** formato***** formato***** formato");
    	
    	arrayListBidimSaldosProperties = datosSaldosProperties.getBancos(strRutaJAR);
*/    	
//    	Ventana.acumularStrTextoResultadoComprobaciones("\n" + " SiZE: arrayListBidimSaldosProperties.size: " + arrayListBidimSaldosProperties.size());    		

		//-- Preparar desplegable opciones de cuentas ---------------------------------------------------------------------

/*   	  	
    	//Array para asignar valores y utilizar este array en la b�squeda posterior del banco y la cuenta.
    	String[] arrayComboBancosCuentas = new String[arrayListBidimSaldosProperties.size() + 1];
		arrayComboBancosCuentas[0] =	"<>";
		System.out.println("**************************** 20170714 arrayListBidimSaldosProperties.get");
		for(int contEle=0;contEle<arrayListBidimSaldosProperties.size();contEle++){
//			System.out.println("111   20170714 " + arrayListBidimSaldosProperties.get(contEle).toString());
			
			arrayComboBancosCuentas[contEle + 1] =	arrayListBidimSaldosProperties.get(contEle).get(0) + "<>" +
    								arrayListBidimSaldosProperties.get(contEle).get(1);
    	}
		System.out.println("**************************** 20170714 arrayListBidimSaldosProperties.get");
*/	

    	
    	//ENVIADO A PROCESOS INICIALES ---------------- INICIO

/*    	
		//-- Preparar desplegable opciones de cuentas ---------------------------------------------------------------------
		//Array para asignar opciones en el desplegable
    	String[] arrayCombo = new String[arrayListBidimSaldosProperties.size() + 1];
		arrayCombo[0] =	"Seleccione una cuenta:";
		
		String strAbreParenteris = " ";
		String strCierraParenteris = " ";

		for(int contEle=0;contEle<arrayListBidimSaldosProperties.size();contEle++){
			strAbreParenteris = " ";
			strCierraParenteris = " ";
			if (arrayListBidimSaldosProperties.get(contEle).get(2).length()>0){
				strAbreParenteris = " (";
				strCierraParenteris = ") ";
			}
    		arrayCombo[contEle + 1] =	arrayListBidimSaldosProperties.get(contEle).get(0) + strAbreParenteris +
									arrayListBidimSaldosProperties.get(contEle).get(2) + strCierraParenteris +
    								arrayListBidimSaldosProperties.get(contEle).get(1);
    	}
		//-- fin ---------------- Preparar desplegable opciones de cuentas ---------------------------------------------------------------------

		//-- mostrar ---------------------------------------------------------------------
		System.out.println("**************************** 20170714 arrayCombo.get");
		for(int contEle=0;contEle<arrayCombo.length;contEle++){
//			System.out.println("222   20170714 " + arrayCombo[contEle].toString());
    	}
		System.out.println("**************************** 20170714 arrayCombo.get");		
		//-- mostrado ---------------------------------------------------------------------

*/    	
    	
    	//*************************************** Cargamos datos en arrayListBidimSaldosProperties ******************************************
	   	if (strSWMensajesErrorVentana.equals("")){	    	
	    	try {
	    		arrayListBidimSaldosProperties.clear();
				arrayListBidimSaldosProperties = datosSaldosProperties.getBancos(strRutaJAR);
			} catch (IOException e) {
				// TODO Auto-generated catch block
    			strSWMensajesErrorVentana = "ERROR: * No se pudo cargar datos de cuentas configuradas en saldos_properties";
    			Ventana.acumularStrTextoResultadoComprobaciones("\n" + " (i) ERROR: * No se pudo cargar datos de cuentas configuradas en saldos_properties");
				e.printStackTrace();
			}
//	    	Ventana.acumularStrTextoResultadoComprobaciones("\n" + " SiZE: arrayListBidimSaldosProperties.size: " + arrayListBidimSaldosProperties.size());    		
	   	}

	   	//ENVIADO A PROCESOS INICIALES ----------------

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
 
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Ventana().setVisible(true);
            }
        });
    }

    public Ventana() {
    	initComponents();
    }
 
    @SuppressWarnings("unchecked")
    private void initComponents() {
 
        txtRutaDetalleCC = new javax.swing.JTextField();
        btnSeleccionarDetalleCC = new javax.swing.JButton();

        btnComprobarAjuste = new javax.swing.JButton();
        //btnVolverAinsertarAjuste = new javax.swing.JButton();
        btnCrearExcelOmnexConAjustes = new javax.swing.JButton();
        
        btnInicio = new javax.swing.JButton();
        btnCrearExcelOmnex = new javax.swing.JButton();
        btnAjustarSaldoCuenta = new javax.swing.JButton();
        btnComprobaciones = new javax.swing.JButton();
        btnAceptarCorteOmnex = new javax.swing.JButton();
        btnAbrirExcelGenerado = new javax.swing.JButton();
        
        txtAreaRutasArchivos = new javax.swing.JTextArea();      
        txtAreaResultadoComprobaciones = new javax.swing.JTextArea();
        txtAreaResultadoComprobacionesAjustes = new javax.swing.JTextArea();
        txtAreaAjustesAlmacenados = new javax.swing.JTextArea();

        txtFieldSaldo = new javax.swing.JTextField();
        txtFieldFormAjustarSaldoCuentaAgente = new javax.swing.JTextField();
        txtFieldFormAjustarSaldoCuentaImporte = new javax.swing.JTextField();
        txtFieldFormAjustarSaldoCuentaDatos = new javax.swing.JTextField();
        
//      cbOpcionesLayout = new javax.swing.JComboBox(arrayListStrOpcionesBancoComboBox);
    	
    	try {
   			//cotrebla111
			cbOpcionesLayout = new javax.swing.JComboBox(cargarDatosEncb(arrayListBidimSaldosProperties));
//			cbFormAjustarSaldoCuentaOpcionesCuenta = new javax.swing.JComboBox(cargarDatosEncb(arrayListBidimSaldosProperties));
		} catch (IOException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}

        jLabelSaldoDelCorte = new javax.swing.JLabel();
        jLabelFormAjustarAgente = new javax.swing.JLabel();
        jLabelFormAjustarImporte = new javax.swing.JLabel();
        jLabelFormAjustarDatos = new javax.swing.JLabel();
        jLabelFormAjustarCuentaCabeceraValor = new javax.swing.JLabel();

        jLabelPedirFormatoLayout = new javax.swing.JLabel();
//        jLabelPedirCuentaParaAjustarSaldo = new javax.swing.JLabel();
        jLabelCabecera0AjustarSaldo = new javax.swing.JLabel();
        jLabelCabecera1AjustarSaldo = new javax.swing.JLabel();
        jLabelCabecera2AjustarSaldo = new javax.swing.JLabel();
        jLabelAjustesMsjError = new javax.swing.JLabel();

        jLabelPedirDocumentoDetalleCC = new javax.swing.JLabel();

//        jLabelMensaje = new javax.swing.JLabel();
        jLabelFieldSaldo = new javax.swing.JLabel();
        
        jLabelCabecera = new javax.swing.JLabel();
        jLabelResultadoOmnex = new javax.swing.JLabel();
 
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("PRUEBAS " + strVersion + " - Saldos. Seleccionar ruta del archivo / Procesar.");
 
        txtRutaDetalleCC.setEditable(false);
 
        btnSeleccionarDetalleCC.setText("Seleccionar...");

        btnComprobarAjuste.setText("   Comprobaci�n del ajuste con los datos del historial.   ");
        //btnVolverAinsertarAjuste.setText("   Volver a insertar un nuevo ajuste y acumularlo.   ");
        btnCrearExcelOmnexConAjustes.setText("   Crear excel para Omnex con los ajustes.   ");

        btnInicio.setText("Inicio");
        btnCrearExcelOmnex.setText("2�: Crear Excel para Omnex.");
        btnAjustarSaldoCuenta.setText("Ajustar saldo de la cuenta.");
        btnComprobaciones.setText("1�: Comprobaciones de los dep�sitos del archivo.");
        btnAceptarCorteOmnex.setText("Aprobar el archivo generado y a�adirlo al hist�rico.");
        btnAbrirExcelGenerado.setText("Abrir archivo Excel generado.");
        
//        txtAreaRutasArchivos.setText("2Puede <br>ncons/nu\nltar el resultado en: ");
        txtAreaRutasArchivos.setText("");
        txtAreaRutasArchivos.setEditable(false);
        txtAreaResultadoComprobaciones.setEditable(false);
        txtAreaResultadoComprobacionesAjustes.setEditable(false);
        txtAreaAjustesAlmacenados.setEditable(false);
        
        btnAceptarCorteOmnex.setVisible(false);
        btnAbrirExcelGenerado.setVisible(false);
        jLabelResultadoOmnex.setVisible(false);
        txtAreaRutasArchivos.setVisible(false);

        txtAreaResultadoComprobaciones.setVisible(false);
        txtAreaResultadoComprobacionesAjustes.setVisible(false);
        txtAreaAjustesAlmacenados.setVisible(false);
        
        //cotrebla
//        txtFieldSaldo.setEditable(false);
  //      txtFieldSaldo.setVisible(false);
        txtFieldSaldo.setEditable(true);
        txtFieldSaldo.setVisible(false);
        txtFieldFormAjustarSaldoCuentaAgente.setEditable(true);
        txtFieldFormAjustarSaldoCuentaAgente.setVisible(false);
        txtFieldFormAjustarSaldoCuentaImporte.setEditable(true);
        txtFieldFormAjustarSaldoCuentaImporte.setVisible(false);
        txtFieldFormAjustarSaldoCuentaDatos.setEditable(true);
        txtFieldFormAjustarSaldoCuentaDatos.setVisible(false);
        
        jLabelSaldoDelCorte.setVisible(false);
        jLabelFieldSaldo.setText("Saldo en el corte: ");
        jLabelFieldSaldo.setVisible(false);

        jLabelFormAjustarAgente.setEnabled(false);
        jLabelFormAjustarAgente.setVisible(false);
        jLabelFormAjustarImporte.setEnabled(false);
        jLabelFormAjustarImporte.setVisible(false);
        jLabelFormAjustarDatos.setEnabled(false);
        jLabelFormAjustarDatos.setVisible(false);
        jLabelFormAjustarCuentaCabeceraValor.setEnabled(false);
        jLabelFormAjustarCuentaCabeceraValor.setVisible(false);
        btnComprobarAjuste.setEnabled(false);
        btnComprobarAjuste.setVisible(false);
        btnCrearExcelOmnexConAjustes.setEnabled(false);
        btnCrearExcelOmnexConAjustes.setVisible(false);
        txtAreaAjustesAlmacenados.setVisible(false);
        txtAreaResultadoComprobacionesAjustes.setVisible(false);
        
        cbOpcionesLayout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	cbOpcionesLayoutActionPerformed(evt);
            }
        });


        btnSeleccionarDetalleCC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {

            	LogsFile ficheroLog = new LogsFile();
            	
            	btnSeleccionarDetalleCCActionPerformed(evt);
            }
        });
        
        btnAceptarCorteOmnex.addActionListener(new java.awt.event.ActionListener() {
//       
        	
//        	String fileLocal = new String("src/archivos/ArchivoPrueba.xlsx"); 
//        	String fileLocal = "srcarchivosArchivoPrueba.xlsx"; 

            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	
            	btnAceptarCorteOmnexActionPerformed(evt);
            }
        });
        
        btnAbrirExcelGenerado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {

            	try {
//     				File path = new File (strFileExcelAgentes);
     				File path = new File (getStrRutaFileNameExcelGenerado());
     				Desktop.getDesktop().open(path);
            	} catch (IOException e) {
            		// TODO Auto-generated catch block
            		e.printStackTrace();
            	}            	
            }
        });

        btnAjustarSaldoCuenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {

            	txtAreaResultadoComprobacionesAjustes.setEnabled(false);
        		txtAreaResultadoComprobacionesAjustes.setVisible(false);
        		txtAreaAjustesAlmacenados.setEnabled(false);
        		txtAreaAjustesAlmacenados.setVisible(false);

//        		arrayListBidimSaldosProperties.clear();
        		arrayListAjustesAlmacenados.clear();

            	if (btnAjustarSaldoCuentaActionPerformed(evt)){
            	}else{
            		
            	}
//            	txtAreaResultadoComprobaciones.setEnabled(false);
//        		txtAreaResultadoComprobaciones.setVisible(false);
            }
        });

        btnComprobarAjuste.addActionListener(new java.awt.event.ActionListener() {

        	public void actionPerformed(java.awt.event.ActionEvent evt) {
        		System.out.println("*** Comprobacion ajuste.**********************************************");
        		btnComprobarAjusteActionPerformed(evt);
            }
        });
/*
        btnVolverAinsertarAjuste.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	btnVolverAinsertarAjusteActionPerformed(evt);
            }
        });
*/        
        btnCrearExcelOmnexConAjustes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	if (btnCrearExcelOmnexConAjustesActionPerformed(evt)){
            		
            	}else{
            		
            	}
//            	setVisibleTxtAreaResultadoComprobacionesAjustes();
//            	setNoVisibleTxtAreaResultadoComprobacionesAjustes();
//cotrebla     	setVisibleTxtAreaAjustesAlmacenados();
            	
            	setNoVisibleTxtAreaAjustesAlmacenados();
            	setNoVisibleTxtAreaResultadoComprobacionesAjustes();
            	txtAreaRutasArchivos.setEnabled(true);
            	txtAreaRutasArchivos.setVisible(true);
            	btnAbrirExcelGenerado.setEnabled(true);
            	btnAbrirExcelGenerado.setVisible(true);
            	btnAceptarCorteOmnex.setEnabled(true);
            	btnAceptarCorteOmnex.setVisible(true);
            	btnComprobarAjuste.setEnabled(false);
            	btnComprobarAjuste.setVisible(false);
            	btnCrearExcelOmnexConAjustes.setEnabled(false);
            	btnCrearExcelOmnexConAjustes.setVisible(false);
            	jLabelFormAjustarAgente.setVisible(false);
            	jLabelFormAjustarImporte.setVisible(false);
            	jLabelFormAjustarDatos.setVisible(false);
            	txtFieldFormAjustarSaldoCuentaAgente.setVisible(false);
            	txtFieldFormAjustarSaldoCuentaImporte.setVisible(false);
            	txtFieldFormAjustarSaldoCuentaDatos.setVisible(false);
            }
        });

        btnCrearExcelOmnex.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	if (btnCrearExcelOmnexActionPerformed(evt)){
            		
            	}else{
            		
            	}
            }
        });
        
        btnInicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {

            	btnInicioActionPerformed(evt);
            	
            	try {
        			verValoresVariablesInicioEjecucion("Boton inicio");
        		} catch (IOException e) {
        			// TODO Auto-generated catch block
        			e.printStackTrace();
        		}

            }
        });

        btnComprobaciones.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	if (!(procesosDeComprobaciones().equals(""))){
            		
			        btnAjustarSaldoCuenta.setText("Ajustar saldo de la cuenta " + getStrOpcionCuentaSelected());
            		btnAjustarSaldoCuenta.setEnabled(true);
            		
            		//--- Si el operador no ha seleccionado todav�a una opcion del combo y el archivo ---> 
            		//--- 			---> no se muestra el bot�n de ajustes. 
            		System.out.println("strOpcionCuentaSelected====> " + strOpcionCuentaSelected);
            		System.out.println("strNombreDelArchivoXLS====> " + strNombreDelArchivoXLS);

            		if(strOpcionCuentaSelected.equals("") || strNombreDelArchivoXLS.equals("")){
                		btnAjustarSaldoCuenta.setVisible(false);            			
            		}else{
                		btnAjustarSaldoCuenta.setVisible(true);
            		}
            	}else{
					btnAjustarSaldoCuenta.setEnabled(false);
            		btnAjustarSaldoCuenta.setVisible(false);            		
            	}
    		}
        });        
        jLabelMensaje.setText(" ");
        
        Font fontjLabelVerdanaBold12 = new Font("Verdana", Font.BOLD,12);
        Font fontjLabelVerdanaBold14 = new Font("Verdana", Font.BOLD,14);
        Font fontjLabelVerdanaBold15 = new Font("Verdana", Font.BOLD,15);
        
        jLabelPedirFormatoLayout.setFont(fontjLabelVerdanaBold12);
        jLabelFieldSaldo.setFont(fontjLabelVerdanaBold12);
        jLabelSaldoDelCorte.setFont(fontjLabelVerdanaBold12);
        jLabelCabecera0AjustarSaldo.setFont(fontjLabelVerdanaBold15);
        jLabelCabecera1AjustarSaldo.setFont(fontjLabelVerdanaBold14);
        jLabelCabecera2AjustarSaldo.setFont(fontjLabelVerdanaBold14);
        
        txtAreaAjustesAlmacenados.setFont(fontjLabelVerdanaBold12);
        txtAreaAjustesAlmacenados.setForeground(Color.RED);      
        txtAreaResultadoComprobacionesAjustes.setFont(fontjLabelVerdanaBold12);
        txtAreaResultadoComprobacionesAjustes.setForeground(Color.RED);      
//      jLabelAjustesMsjError.setFont(fontjLabelVerdanaBold12);
//      jLabelAjustesMsjError.setForeground(Color.blue);      
        jLabelAjustesMsjError.setFont(fontjLabelVerdanaBold12);
        jLabelAjustesMsjError.setBackground(Color.RED);
        
//      jLabelPedirFormatoLayout.setText("Seleccione el banco origen del archivo descargado: ");  
        jLabelPedirFormatoLayout.setText("Seleccione la cuenta vinculada a su archivo XLS: ");  
        jLabelCabecera0AjustarSaldo.setText("                  AJUSTES:");
        jLabelCabecera1AjustarSaldo.setText("Introducir nuevo movimiento para");
        jLabelCabecera2AjustarSaldo.setText("ajustar saldo de la cuenta: ");
        txtAreaAjustesAlmacenados.setText("");
        txtAreaResultadoComprobacionesAjustes.setText("");
        jLabelAjustesMsjError.setText("");
//        jLabelPedirCuentaParaAjustarSaldo.setText("Seleccione la cuenta que requiere ajustar el saldo hist�rico: ");
//        jLabelPedirCuentaParaAjustarSaldo.setVisible(false);
        jLabelCabecera0AjustarSaldo.setVisible(false);
        jLabelCabecera1AjustarSaldo.setVisible(false);
        jLabelCabecera2AjustarSaldo.setVisible(false);
        jLabelAjustesMsjError.setVisible(false);
        jLabelSaldoDelCorte.setText("Saldo del corte del archivo: ");  
        jLabelFormAjustarAgente.setText("Indicar el c�digo del agente:");  
        jLabelFormAjustarImporte.setText("Indicar el importe:");
        jLabelFormAjustarDatos.setText("Indicar datos:");
        jLabelFormAjustarCuentaCabeceraValor.setText("");
        jLabelFormAjustarAgente.setFont(fontjLabelVerdanaBold12);  
        jLabelFormAjustarImporte.setFont(fontjLabelVerdanaBold12);  
        jLabelFormAjustarDatos.setFont(fontjLabelVerdanaBold12);
        jLabelFormAjustarCuentaCabeceraValor.setFont(fontjLabelVerdanaBold14);

        Font fontjLabelCabecera = new Font("Verdana", Font.BOLD,12);
        jLabelCabecera.setFont(fontjLabelCabecera);
    	jLabelCabecera.setText("Seleccionar el archivo descargado del banco:");
    	jLabelResultadoOmnex.setFont(fontjLabelVerdanaBold14);
    	
        jLabelPedirDocumentoDetalleCC.setText("Archivo xls: ");
 
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(43, 43, 43)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnInicio)
                    .addComponent(btnAbrirExcelGenerado)
                    .addComponent(btnAceptarCorteOmnex)
                    .addComponent(jLabelMensaje)
                   	.addComponent(jLabelPedirFormatoLayout)
					.addGap(18, 18, 18)
					.addComponent(jLabelCabecera0AjustarSaldo)
					.addComponent(jLabelCabecera1AjustarSaldo)
					.addComponent(jLabelCabecera2AjustarSaldo)
//                   	.addComponent(jLabelPedirCuentaParaAjustarSaldo)
                    .addComponent(cbOpcionesLayout)
                    .addComponent(jLabelFieldSaldo)                
                    .addComponent(jLabelFormAjustarCuentaCabeceraValor)               
                    .addGap(18, 18, 18)
                    .addComponent(jLabelFormAjustarAgente)
                    .addComponent(txtFieldFormAjustarSaldoCuentaAgente)
                    .addComponent(jLabelFormAjustarImporte)
                    .addComponent(txtFieldFormAjustarSaldoCuentaImporte)
                    .addComponent(jLabelFormAjustarDatos)
                    .addComponent(txtFieldFormAjustarSaldoCuentaDatos)
                    .addComponent(btnComprobarAjuste)
//                    .addComponent(btnVolverAinsertarAjuste)
                    .addComponent(btnCrearExcelOmnexConAjustes)
					.addComponent(jLabelAjustesMsjError)                    
					.addComponent(txtAreaAjustesAlmacenados)  
                    .addComponent(txtAreaResultadoComprobacionesAjustes)
                    .addGap(18, 18, 18)
                    .addComponent(jLabelCabecera)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabelPedirDocumentoDetalleCC)
                        .addComponent(txtRutaDetalleCC, javax.swing.GroupLayout.PREFERRED_SIZE, 306, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnSeleccionarDetalleCC))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jLabelSaldoDelCorte)
                        .addComponent(txtFieldSaldo))                        
//                        .addComponent(cbFormAjustarSaldoCuentaOpcionesCuenta))                        
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
//                      .addComponent(btnAceptarFicheros))
                        .addComponent(btnComprobaciones)
                        .addComponent(btnCrearExcelOmnex))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(txtAreaRutasArchivos))
//                        .addComponent(btnAbrirExcelGenerado)
//                        .addComponent(btnAceptarCorteOmnex))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(txtAreaResultadoComprobaciones)                        
                        .addComponent(jLabelResultadoOmnex))                        
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(btnAjustarSaldoCuenta)))                        
                .addContainerGap(31, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(btnInicio)
                .addComponent(btnAbrirExcelGenerado)
                .addComponent(btnAceptarCorteOmnex)
                .addComponent(jLabelMensaje)
                .addComponent(jLabelPedirFormatoLayout)
                .addGap(18, 18, 18)
               	.addComponent(jLabelCabecera0AjustarSaldo)
               	.addComponent(jLabelCabecera1AjustarSaldo)
               	.addComponent(jLabelCabecera2AjustarSaldo)
//               	.addComponent(jLabelPedirCuentaParaAjustarSaldo)
                .addComponent(cbOpcionesLayout)
                .addComponent(jLabelFieldSaldo)
                .addComponent(jLabelFormAjustarCuentaCabeceraValor)
                .addGap(18, 18, 18)             
                .addComponent(jLabelFormAjustarAgente)                
                .addComponent(txtFieldFormAjustarSaldoCuentaAgente)
                .addComponent(jLabelFormAjustarImporte)                
                .addComponent(txtFieldFormAjustarSaldoCuentaImporte)
                .addComponent(jLabelFormAjustarDatos)
                .addComponent(txtFieldFormAjustarSaldoCuentaDatos)
                .addComponent(btnComprobarAjuste)
//                .addComponent(btnVolverAinsertarAjuste)
                .addComponent(btnCrearExcelOmnexConAjustes)
                .addComponent(jLabelAjustesMsjError)
				.addComponent(txtAreaAjustesAlmacenados)
                .addComponent(txtAreaResultadoComprobacionesAjustes)
                .addGap(18, 18, 18)
                .addComponent(jLabelCabecera)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelPedirDocumentoDetalleCC)                  
                    .addComponent(txtRutaDetalleCC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSeleccionarDetalleCC))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelSaldoDelCorte)
                    .addComponent(txtFieldSaldo))                                          
//                    .addComponent(cbFormAjustarSaldoCuentaOpcionesCuenta))                      
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
//                  .addComponent(btnAceptarFicheros))
                    .addComponent(btnComprobaciones)                		
                    .addComponent(btnCrearExcelOmnex))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtAreaRutasArchivos))
//                    .addComponent(btnAbrirExcelGenerado)
//                    .addComponent(btnAceptarCorteOmnex))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtAreaResultadoComprobaciones)                		
                    .addComponent(jLabelResultadoOmnex))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAjustarSaldoCuenta))
                .addContainerGap(44, Short.MAX_VALUE))
        );
        btnCrearExcelOmnex.setEnabled(false);
        btnAjustarSaldoCuenta.setEnabled(false);
      	btnAjustarSaldoCuenta.setVisible(false);
        
        //Para mostrar siempre los mensajes,     sin condicionarlo a  que la comprobaci�n sea correcta o no:
//        if (!(getBooleanComprobacionCorrecta())){
        	txtAreaResultadoComprobaciones.setVisible(true);
        	txtAreaResultadoComprobaciones.setText(getStrTextoResultadoComprobaciones());
        	txtAreaResultadoComprobacionesAjustes.setText(getStrTextoResultadoComprobaciones());
//        }

    	System.out.println("---FinDeInicio---");
 	
    	swProcesosIniciales();
    	
    	try {
			verValoresVariablesInicioEjecucion("Comienzo de ejecucion");
			System.out.println("banco********************************************************************************");
			System.out.println("banco			" + getStrOpcionBancoSelected());
			System.out.println("banco********************************************************************************");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
        pack();
    }// </editor-fold>

    private void cbOpcionesLayoutActionPerformed(java.awt.event.ActionEvent evt) {
        	int indiceCB = cbOpcionesLayout.getSelectedIndex();
    	strOpcionBancoSelected = "";
    	strOpcionCuentaSelected = "";
    	strOpcionDescripcionSelected = "";
    	intIndiceCB = indiceCB;

    	System.out.println("----------------click en el combo.   intIndiceCB = " + intIndiceCB);
    	System.out.println("-- strOpcionBancoSelected: " + strOpcionBancoSelected);
    	System.out.println("-- strOpcionCuentaSelected: " + strOpcionCuentaSelected);
    	System.out.println("-- strOpcionDescripcionSelected: " + strOpcionDescripcionSelected);
    	System.out.println("---------fin----click en el combo.   intIndiceCB = " + intIndiceCB);
    
    	//indiceCB = 0     es la opci�n 'Seleccione una cuenta'.
    	if (indiceCB>0){
        	//Obtenemos el texto de la opci�n elegida del ComboBox.
        	strOpcionBancoSelected = arrayListBidimSaldosProperties.get(indiceCB-1).get(0).toString();
        	strOpcionBancoSelected = strOpcionBancoSelected.trim();
        	strOpcionCuentaSelected = arrayListBidimSaldosProperties.get(indiceCB-1).get(1).toString();
        	strOpcionCuentaSelected = strOpcionCuentaSelected.trim();        	
        	strOpcionDescripcionSelected = arrayListBidimSaldosProperties.get(indiceCB-1).get(2).toString();
        	strOpcionDescripcionSelected = strOpcionDescripcionSelected.trim();      
        	strOpcionDatosIdAgenteSelected = arrayListBidimSaldosProperties.get(indiceCB-1).get(3).toString();
        	strOpcionDatosIdAgenteSelected = strOpcionDatosIdAgenteSelected.trim();      
        	strOpcionImporteSelected = arrayListBidimSaldosProperties.get(indiceCB-1).get(4).toString();
        	strOpcionImporteSelected = strOpcionImporteSelected.trim();        	
        	strOpcionIdentificadorSelected = arrayListBidimSaldosProperties.get(indiceCB-1).get(5).toString();
        	strOpcionIdentificadorSelected = strOpcionIdentificadorSelected.trim();
        	strOpcionFilaPrimerMovimientoSelected = arrayListBidimSaldosProperties.get(indiceCB-1).get(6).toString();
        	strOpcionFilaPrimerMovimientoSelected = strOpcionFilaPrimerMovimientoSelected.trim();
        	strOpcionPosicionSaldoSelected = arrayListBidimSaldosProperties.get(indiceCB-1).get(7).toString();
        	strOpcionPosicionSaldoSelected = strOpcionPosicionSaldoSelected.trim();
        	strOpcionDistanciaSaldoAdatosSelected = arrayListBidimSaldosProperties.get(indiceCB-1).get(8).toString();
        	strOpcionDistanciaSaldoAdatosSelected = strOpcionDistanciaSaldoAdatosSelected.trim();
        	strOpcionOrdenSelected = arrayListBidimSaldosProperties.get(indiceCB-1).get(9).toString();
        	strOpcionOrdenSelected = strOpcionOrdenSelected.trim();
        	strOpcionPosicionMovimientoSelected = arrayListBidimSaldosProperties.get(indiceCB-1).get(10).toString();
        	strOpcionPosicionMovimientoSelected = strOpcionPosicionMovimientoSelected.trim();
        	strOpcionPosicionFechaSelected = arrayListBidimSaldosProperties.get(indiceCB-1).get(11).toString();
        	strOpcionPosicionFechaSelected = strOpcionPosicionFechaSelected.trim();

        	System.out.println("778899 arrayListBidimSaldosProperties: " + arrayListBidimSaldosProperties.get(indiceCB-1));
        	System.out.println("778899 cabecera(" + arrayListBidimSaldosProperties.get(indiceCB-1).get(7).toString() + ")");

    		txtFieldSaldo.setText("");
    		jLabelFormAjustarCuentaCabeceraValor.setText("");
    		txtFieldFormAjustarSaldoCuentaAgente.setText("");
    		txtFieldFormAjustarSaldoCuentaImporte.setText("");
            txtFieldFormAjustarSaldoCuentaDatos.setText("");

    		//Si la cuenta esta configurada con:		posicionSaldo:(cabecera:)
    		//		entonces el operado introducira desde la Ventana del programa cual es el importe del Saldo resultante del corte.
        	if(arrayListBidimSaldosProperties.get(indiceCB-1).get(7).toString().equals("cabecera:")){
        		jLabelFieldSaldo.setVisible(true);
        		txtFieldSaldo.setVisible(true);
        		jLabelSaldoDelCorte.setVisible(true);        		
        		txtFieldSaldo.setEditable(true);
        	}else{
        		jLabelFieldSaldo.setVisible(false);
        		jLabelSaldoDelCorte.setVisible(false);        		
        		txtFieldSaldo.setVisible(false);
        	}

    	}

    	//Desde la funci�n:	asignarPosicionesValoresXLS():	asignamos en qu� posiciones (qu� columnas) se encuentran los valores que requiere el programa.
		try {
			asignarPosicionesValoresXLS(strOpcionBancoSelected);
		} catch (IOException e) {
			// TODO Auto-generated catch block
//			System.out.println("*********************************************************************************************************************** catch IO");
			//e.printStackTrace();
		}    	
    }

    private void txtFieldSaldoActionPerformed(java.awt.event.ActionEvent evt) {
    	
    }
    
    private void btnSeleccionarDetalleCCActionPerformed(java.awt.event.ActionEvent evt) {
        
        //Creamos una instancia de JFileChooser
        JFileChooser fc=new JFileChooser();
         
        //Escribimos el nombre del titulo
        fc.setDialogTitle("Elige un fichero");
         
        //Indicamos que solo se puedan elegir ficheros
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
         
        //Creamos un filtro para JFileChooser
        FileNameExtensionFilter filtro = new FileNameExtensionFilter("*.xls", "xls");
        fc.setFileFilter(filtro);
         
        int eleccion=fc.showSaveDialog(this);

        if(eleccion==JFileChooser.APPROVE_OPTION){
            txtRutaDetalleCC.setText(fc.getSelectedFile().getPath());
            strRutaArchivoXlsBanco = txtRutaDetalleCC.getText();
            int intPosicionUltimoSeparadorFichero = strRutaArchivoXlsBanco.lastIndexOf(File.separator); 
            strNombreDelArchivoXLS =            	strRutaArchivoXlsBanco.substring(intPosicionUltimoSeparadorFichero);
                        
      		System.out.println("NombreDelArchivoXLS         Ruta completa     : " + strRutaArchivoXlsBanco);
      		System.out.println("NombreDelArchivoXLS         Nombre archivo    : " + strNombreDelArchivoXLS);
        //cotrebla
        }

        if (intIndiceCB>0){
        	//cotrebla
        }        
    }                                         

    private void btnAceptarCorteOmnexActionPerformed(java.awt.event.ActionEvent evt) {
    	
    	//En intGrabadosEnElCorte iremos sumando los dep�sitos de un corte que se han llegado a escribir en su correspondiente archivo.
    	//y utilizaremos esta variable para saber si debemos acumular en el excel mensual de esa cuenta.
    	int intGrabadosEnElCorte = 0;
    	
/*    	
		//-- mostrar ---------------------------------------------------------------------
		for(int contEle=0;contEle<arrayCortePreparadoParaHistorialOmnex.size();contEle++){
			try {
				ficheroLog.InfoLog("20171011: " + arrayCortePreparadoParaHistorialOmnex.get(contEle).toString() ,  "_____________datosCorte");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("20171011: " + arrayCortePreparadoParaHistorialOmnex.get(contEle).toString());
    	}
		//-- mostrado ---------------------------------------------------------------------
*/
		
		
		
		
		String strSaldoCorteAnterior = "";
    	HistorialOmnex ficheroHistorialOmnex = new HistorialOmnex();
//		System.out.println("-----1234321 size: " + arrayCortePreparadoParaHistorialOmnex.size());
    	
    	
    	//******************** Se graban en un archivo todos los depositos del corte. **************************************************
		for(int contEle = 0;contEle<arrayCortePreparadoParaHistorialOmnex.size();contEle++){
			int intContEle = arrayCortePreparadoParaHistorialOmnex.size() - 1 - contEle;
    		try {
//    			System.out.println("-----1234321: " + arrayCortePreparadoParaHistorialOmnex.get(intContEle).toString());
    		    //Preparar la fecha
    		    SimpleDateFormat sdFormatFileName = new SimpleDateFormat("yyyyMMdd");
    		    String strDateInfoFileName = sdFormatFileName.format(cal.getTime());

    			intGrabadosEnElCorte = intGrabadosEnElCorte + ficheroHistorialOmnex.grabarCorte(arrayCortePreparadoParaHistorialOmnex.get(intContEle).toString(), strDateInfoFileName);
    			
    			//--Si el FOR se encuentra en el ultimo elemento del array ... se consultar� el saldo.
    			if (contEle==arrayCortePreparadoParaHistorialOmnex.size()-1){
        			ficheroHistorialOmnex.escribirSaldo(arrayCortePreparadoParaHistorialOmnex.get(intContEle).toString());
    			}				
    		} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}    		
    	}
		//******************* Si ha habido dep�sitos del corte que se han grabado en el historial de cortes  **************************
		//	>>>>>>>> entonces los acumulamos en el excel mensual de esa cuenta ******
		boolean booleanAcumuladoCorrecto = true;
		String strArchivoAcumuladoParaPrimerDeposito = "";
		String strArchivoAcumuladoParaUltimoDeposito = "";

		int intSizeArrayCortePreparadoParaHistorialOmnex = 0;
		intSizeArrayCortePreparadoParaHistorialOmnex = arrayCortePreparadoParaHistorialOmnex.size();
		
		System.out.println("___>>>>: " + intGrabadosEnElCorte);
		System.out.println("___>>>>: " + arrayCortePreparadoParaHistorialOmnex.size());
		System.out.println("___>>>>: " + strOpcionBancoSelected);
		System.out.println("___>>>>: " + strOpcionCuentaSelected);
/*
 //*********************** A continuaci�n la parte de escritura en el acumulado de cortes por mes/cuenta.
 
		if (intGrabadosEnElCorte>0){
			//----------------------- Pasamos al constructor el array de dep�sitos, el banco y la cuenta. *****************************
			ExcelAcumuladoMensual fileAcumulado = new ExcelAcumuladoMensual(arrayCortePreparadoParaHistorialOmnex, strOpcionBancoSelected, strOpcionCuentaSelected);

			//----------------------- Obtenemos los nombres de los archivos XLS que har�n falta para acumular los dep�sitos.
			strArchivoAcumuladoParaPrimerDeposito = fileAcumulado.obtenerNombreArchivoAcumulado(arrayCortePreparadoParaHistorialOmnex.get(0));
			strArchivoAcumuladoParaUltimoDeposito = fileAcumulado.obtenerNombreArchivoAcumulado(arrayCortePreparadoParaHistorialOmnex.get(intSizeArrayCortePreparadoParaHistorialOmnex-1));
			
			System.out.println("000: " + arrayCortePreparadoParaHistorialOmnex.get(0));
			System.out.println("000: " + arrayCortePreparadoParaHistorialOmnex.get(intSizeArrayCortePreparadoParaHistorialOmnex-1));

			System.out.println("0>>>>arrayCortePreparadoParaHistorialOmnex   size: " + arrayCortePreparadoParaHistorialOmnex.size());
			for (int ii = 0; ii < arrayCortePreparadoParaHistorialOmnex.size(); ii++) {
//				System.out.println("0>>>> datos: " + arrayCortePreparadoParaHistorialOmnex.get(ii));
			}
			
			System.out.println("1>>>>strArchivoAcumuladoParaPrimerDeposito: " + strArchivoAcumuladoParaPrimerDeposito);
			System.out.println("2>>>>strArchivoAcumuladoParaUltimoDeposito: " + strArchivoAcumuladoParaUltimoDeposito);
			
			//------- Se comprueba si ya existe el archivo 'acumulado del mes' que necesita el primer dep�sito.
			// Si no existe se crea.
			if (fileAcumulado.ConsultarSiExisteArchivoParaDeposito(strArchivoAcumuladoParaPrimerDeposito).equals("")){
				System.out.println("3>>>> no existe ArchivoAcumuladoParaPrimerDeposito");				
//				fileAcumulado.crearArchivoExcel(strArchivoAcumuladoParaPrimerDeposito);
				
				//cotrebla
				fileAcumulado.crearArchivoExcelMensual(strArchivoAcumuladoParaPrimerDeposito);
				fileAcumulado.acumularDepositosDelArrayEnArchivoXls(arrayCortePreparadoParaHistorialOmnex, strArchivoAcumuladoParaPrimerDeposito);
			}
			//------- Se comprueba si ya existe el archivo 'acumulado del mes' que necesita el �ltimo dep�sito.
			// Si no existe se crea.
			if (fileAcumulado.ConsultarSiExisteArchivoParaDeposito(strArchivoAcumuladoParaUltimoDeposito).equals("")){
				System.out.println("4>>>> no existe ArchivoAcumuladoParaUltimoDeposito");				
//				fileAcumulado.crearArchivoExcel(strArchivoAcumuladoParaUltimoDeposito);

				//cotrebla
				fileAcumulado.crearArchivoExcelMensual(strArchivoAcumuladoParaUltimoDeposito);
								
			}
			
//			fileAcumulado.acumularDepositosDelArrayEnArchivoXls(arrayCortePreparadoParaHistorialOmnex, strArchivoAcumuladoParaPrimerDeposito);
			
			//Si el ultimo deposito tiene fecha de mes distinta a la del primer dep�sito, se dever� acumular el array en el otro archivo xls.
			if(!(strArchivoAcumuladoParaPrimerDeposito.equals(strArchivoAcumuladoParaUltimoDeposito))){
				System.out.println("5>>>> primer y ultimo depositos son DISTINTOS");

				fileAcumulado.acumularDepositosDelArrayEnArchivoXls(arrayCortePreparadoParaHistorialOmnex, strArchivoAcumuladoParaUltimoDeposito);
			}
		}
		
		//*****************************************************************************************************************************
*/
//		System.out.println("obtenerUltimoSaldoDeCuenta-----1234321 strCuentaBanco: " + getStrOpcionCuentaSelected());
//		System.out.println("obtenerUltimoSaldoDeCuenta-----1234321 strSaldoCorteAnterior: " + strSaldoCorteAnterior);
		try {
			strSaldoCorteAnterior = ficheroHistorialOmnex.obtenerUltimoSaldoDeCuenta(getStrOpcionCuentaSelected());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		System.out.println("obtenerUltimoSaldoDeCuenta-----1234321 strSaldoCorteAnterior: " + strSaldoCorteAnterior);
		
		if (strSaldoCorteAnterior.equals("")){
			jLabelResultadoOmnex.setText("*** ERROR: no se pudo a�adir el corte al Historial Omnex.");
			btnCrearExcelOmnex.setEnabled(false);
		}else{
			jLabelResultadoOmnex.setText("Se ha a�adido el corte al Historial Omnex.");
			btnAceptarCorteOmnex.setVisible(false);
			btnAbrirExcelGenerado.setVisible(false);			
			jLabelResultadoOmnex.setEnabled(true);		
			jLabelResultadoOmnex.setVisible(true);
		}	
    }
    	
    private void btnInicioActionPerformed(java.awt.event.ActionEvent evt) {
//    	txtAreaResultadoComprobaciones.setText("");
    	
    	//**Asignar nuevo valor a la fecha hora	******************************
	    String strFechayyyyMMdd_HHmmss = "";
	    strFechayyyyMMdd_HHmmss = obtenerFechayyyyMMdd_HHmmss();
	    strDate = strFechayyyyMMdd_HHmmss;
	    setStrDateFileName(strDate);
	    //********************************************************************
	    
	    strSWMensajesErrorVentana = "";

    	configurarVentana(strTipoConfigBotonInicio);
    	swProcesosIniciales();    	
    }
    
    /**
     * @param evt
     */
    private void btnComprobarAjusteActionPerformed(java.awt.event.ActionEvent evt) {
		doubleTotalSumatorioAjustes = 0.0;
    	Ajuste objAjuste = new Ajuste();
    	String strCodAgente = txtFieldFormAjustarSaldoCuentaAgente.getText().toString();
    	String strImporte = txtFieldFormAjustarSaldoCuentaImporte.getText().toString();
    	String strDatos = txtFieldFormAjustarSaldoCuentaDatos.getText().toString();
    	ArrayList<String> arrayListAjuste = new ArrayList<>();
    	String strMsjProceso = "";
    	arrayListAjuste.add(0, "");
    	arrayListAjuste.add(1, "");
    	arrayListAjuste.add(2, "");
    	//1.- ----------------------  validarTiposDeDatosDelAjuste  ------		
		if (strMsjProceso.equals("")){			
	    	System.out.println("*** Comprobacion validarTiposDeDatosDelAjuste." + strMsjProceso);
	    	strMsjProceso = objAjuste.validarTiposDeDatosDelFormularioDeAjuste(strCodAgente, strImporte, strDatos);			
		}
		//2.- ----------------------  acumularArrayListAjustesAlmacenados  ------
		System.out.println("*** Comprobacion acumularArrayListAjustesAlmacenados." + strMsjProceso);
		if (strMsjProceso.equals("")){
    		try {
    			System.out.println("*** Acumular ajuste: " + strCodAgente + " - " + strImporte + " - " + strDatos);	
				acumularArrayListAjustesAlmacenados(strCodAgente, strImporte, strDatos);
			} catch (Exception e) {
	    		strMsjProceso = "Error *** al acumular el ajuste.";
				// TODO Auto-generated catch block
				e.printStackTrace();
			}  		
    	}
		objAjuste.mostrarAjustesAcumulados();
		//3.- -------------------------  Calcular sumatorio de importes de los ajustes.  -------------------------
		if (strMsjProceso.equals("")){
			if (arrayListAjustesAlmacenados.size()>0){
//				sumar todos los elementos (todos los importes)
				double doubleImporteAjuste = 0.0;
				for (int i = 0; i < arrayListAjustesAlmacenados.size(); i++) {
					try {
						doubleImporteAjuste = Double.parseDouble(arrayListAjustesAlmacenados.get(i).get(1).toString());
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						strMsjProceso = "Error *** no se ha podido leer el importe de un ajuste: " + arrayListAjustesAlmacenados.get(i).get(1).toString();
						e.printStackTrace();
						break;						
					}
					doubleTotalSumatorioAjustes = doubleTotalSumatorioAjustes + doubleImporteAjuste;
				}
			}
		}
		//4.- -------------------------  Comprobar continuidad con Historial  ------------------------------------
		System.out.println("20171002 4.- Comprobar continuidad con Historial");
		System.out.println("20171002 4.- Comprobar continuidad con Historial : " + strMsjProceso);

		if (strMsjProceso.equals("")){
			//Calculamos si con el sumatorio de los importes a�adidos   se encuentra continuidad con el saldo del historial
			//    recorriendo los depositos del excel seleccionado.
			System.out.println("doubleTotalSumatorioAjustes: " + doubleTotalSumatorioAjustes);
			//for (int i = 0; i < array.length; i++) {
//			try {
//				ficheroLog.InfoLog("datos", "objAjuste.comprobarAjustesSeleccionadosConDatosHistorial");
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				strMsjProceso = "ERROR **** excepci�n IO. - btnComprobarAjusteActionPerformed";
//				e.printStackTrace();
//			}
			if (strMsjProceso.equals("")){
				strMsjProceso = objAjuste.comprobarAjustesSeleccionadosConDatosHistorial(); 
				//**********************************************************************************************************************
				//*********** Aqu� se va a preguntar si encontr� continuidad con el historial ******************************************
				//**********************************************************************************************************************

				
				
				System.out.println("doubleTotalSumatorioAjustes: " + doubleTotalSumatorioAjustes);

            	if (!(strMsjProceso.equals(""))){
//   					try {ficheroLog.InfoLog("Comprobaciones no correctas", "9 procesosDeComprobaciones_btnComprobaciones");} catch (IOException e) {e.printStackTrace();}
   					Ventana.acumularStrTextoResultadoComprobaciones("\n *** � AVISO !   Los ajustes NO son complementarios con el historico.");
                		
               		btnAjustarSaldoCuenta.setEnabled(true);
               		btnAjustarSaldoCuenta.setVisible(true);
               	}else{
//   					try {ficheroLog.InfoLog("Comprobaciones CORRECTAS", "9 procesosDeComprobaciones_btnComprobaciones");} catch (IOException e) {e.printStackTrace();}
   					Ventana.acumularStrTextoResultadoComprobaciones("\n (i) *** CORRECTO *** Los ajustes son complementarios con el historico.");

   					btnAjustarSaldoCuenta.setEnabled(false);
               		btnAjustarSaldoCuenta.setVisible(false);
               		btnComprobarAjuste.setEnabled(false);
               		btnComprobarAjuste.setVisible(true);
               		btnCrearExcelOmnexConAjustes.setEnabled(true);
               		btnCrearExcelOmnexConAjustes.setVisible(true);               		
               	}
			}
			//}
		}
		txtAreaResultadoComprobaciones.setVisible(false);
		//txtAreaResultadoComprobaciones.setText("4333");
		
		if (getArrayListAjustesAlmacenados().size()>0) {
			txtAreaAjustesAlmacenados.setEnabled(true);
			txtAreaAjustesAlmacenados.setVisible(true);
		}else{
			txtAreaAjustesAlmacenados.setEnabled(false);
			txtAreaAjustesAlmacenados.setVisible(false);
		}
		txtAreaResultadoComprobacionesAjustes.setEnabled(true);
		txtAreaResultadoComprobacionesAjustes.setVisible(true);

		//txtAreaResultadoComprobacionesAjustes.setText("666666");
		//-------------------------------------------------------------------------
		if (!strMsjProceso.equals("")){
			jLabelAjustesMsjError.setVisible(true);
			jLabelAjustesMsjError.setText(strMsjProceso);
			acumularStrTextoResultadoComprobaciones("\n" + strMsjProceso);
		}else{
			jLabelAjustesMsjError.setVisible(false);
			jLabelAjustesMsjError.setText("");
			System.out.println("*** Comprobacion validarTiposDeDatosDelAjuste." + strMsjProceso);			
		}
    }
/*    
    private void btnVolverAinsertarAjusteActionPerformed(java.awt.event.ActionEvent evt) {

    }
*/    
    private boolean btnAjustarSaldoCuentaActionPerformed(java.awt.event.ActionEvent evt) {
    	btnInicio.setEnabled(true);
    	btnInicio.setVisible(true);
    	btnAbrirExcelGenerado.setEnabled(false);
    	btnAbrirExcelGenerado.setVisible(false);
    	btnAceptarCorteOmnex.setEnabled(false);
    	btnAceptarCorteOmnex.setVisible(false);
    	jLabelMensaje.setEnabled(false);
    	jLabelMensaje.setVisible(false);
    	jLabelPedirFormatoLayout.setEnabled(false);
    	jLabelPedirFormatoLayout.setVisible(false);
//    	jLabelPedirCuentaParaAjustarSaldo.setEnabled(true);
//    	jLabelPedirCuentaParaAjustarSaldo.setVisible(true);
    	jLabelCabecera0AjustarSaldo.setEnabled(true);
    	jLabelCabecera0AjustarSaldo.setVisible(true);
    	jLabelCabecera1AjustarSaldo.setEnabled(true);
    	jLabelCabecera1AjustarSaldo.setVisible(true);
    	jLabelCabecera2AjustarSaldo.setEnabled(true);
    	jLabelCabecera2AjustarSaldo.setVisible(true);
//    	jLabelAjustesMsjError.setEnabled(false);
    	jLabelAjustesMsjError.setVisible(true);
    	jLabelFieldSaldo.setEnabled(false);
    	jLabelFieldSaldo.setVisible(false);
    	jLabelCabecera.setEnabled(false);
    	jLabelCabecera.setVisible(false);
      	jLabelPedirDocumentoDetalleCC.setEnabled(false);
      	jLabelPedirDocumentoDetalleCC.setVisible(false);
    	txtRutaDetalleCC.setEnabled(false);
    	txtRutaDetalleCC.setVisible(false);
    	btnSeleccionarDetalleCC.setEnabled(false);
    	btnSeleccionarDetalleCC.setVisible(false);
    	jLabelSaldoDelCorte.setEnabled(false);
    	jLabelSaldoDelCorte.setVisible(false);
    	txtFieldSaldo.setEnabled(false);
    	txtFieldSaldo.setVisible(false);
    	btnComprobaciones.setEnabled(false);
    	btnComprobaciones.setVisible(false);
    	btnCrearExcelOmnex.setEnabled(false);
    	btnCrearExcelOmnex.setVisible(false);
    	txtAreaRutasArchivos.setEnabled(false);
    	txtAreaRutasArchivos.setVisible(false);
    	txtAreaResultadoComprobaciones.setEnabled(false);
    	txtAreaResultadoComprobaciones.setVisible(false);
    	jLabelResultadoOmnex.setEnabled(false);
    	jLabelResultadoOmnex.setVisible(false);
    	btnAjustarSaldoCuenta.setEnabled(false);
    	btnAjustarSaldoCuenta.setVisible(false);
//    	cbOpcionesLayout.setEnabled(true);
    	cbOpcionesLayout.setVisible(false);
        jLabelFormAjustarCuentaCabeceraValor.setEnabled(true);
        jLabelFormAjustarCuentaCabeceraValor.setVisible(true);
    	jLabelFormAjustarAgente.setEnabled(true);
    	jLabelFormAjustarAgente.setVisible(true);
    	jLabelFormAjustarImporte.setEnabled(true);
    	jLabelFormAjustarImporte.setVisible(true);
        jLabelFormAjustarDatos.setEnabled(true);
        jLabelFormAjustarDatos.setVisible(true);
        
        btnComprobarAjuste.setEnabled(true);
        btnComprobarAjuste.setVisible(true);
        //btnVolverAinsertarAjuste.setEnabled(false);
        //btnVolverAinsertarAjuste.setVisible(true);
        btnCrearExcelOmnexConAjustes.setEnabled(false);
        btnCrearExcelOmnexConAjustes.setVisible(true);
        
        txtAreaAjustesAlmacenados.setEnabled(false);
        txtAreaAjustesAlmacenados.setVisible(false);
    	txtAreaResultadoComprobacionesAjustes.setEnabled(false);
    	txtAreaResultadoComprobacionesAjustes.setVisible(false);
        
        jLabelResultadoOmnex.setEnabled(false);
    	jLabelResultadoOmnex.setVisible(false);
    	jLabelFormAjustarCuentaCabeceraValor.setVisible(true);
    	jLabelFormAjustarCuentaCabeceraValor.setEnabled(true);
    	jLabelFormAjustarCuentaCabeceraValor.setText(strOpcionBancoSelected + " " + strOpcionCuentaSelected + " " + strOpcionDescripcionSelected);

    	txtFieldFormAjustarSaldoCuentaAgente.setVisible(true);
    	txtFieldFormAjustarSaldoCuentaAgente.setEnabled(true);
    	txtFieldFormAjustarSaldoCuentaImporte.setVisible(true);
    	txtFieldFormAjustarSaldoCuentaImporte.setEnabled(true);
    	txtFieldFormAjustarSaldoCuentaDatos.setVisible(true);
    	txtFieldFormAjustarSaldoCuentaDatos.setEnabled(true);
    	
    	boolean booleanRetorno = false;
    	return booleanRetorno;    	
    }
    	
    private boolean btnCrearExcelOmnexActionPerformed(java.awt.event.ActionEvent evt) {
    	boolean booleanRetorno = false;

//    	for (int i = 0; i < swArrayDatosParaOmnex.size(); i++) {
//        	try {ficheroLog.InfoLog("datos: " + swArrayDatosParaOmnex.get(i).toString(),"procesosDeCrearExcel_swArrayDatosParaOmnex_Principal");} catch (IOException e) {e.printStackTrace();}
//		}

    	booleanRetorno = procesosDeCrearExcel();
//    	try {ficheroLog.InfoLog("booleanRetorno: " + booleanRetorno,"procesosDeCrearExcel_booleanRetorno_Principal");} catch (IOException e) {e.printStackTrace();}

    	return booleanRetorno;	
    }

    private boolean btnCrearExcelOmnexConAjustesActionPerformed(java.awt.event.ActionEvent evt) {
    	boolean booleanRetorno = false;

//    	for (int i = 0; i < swArrayDatosParaOmnex.size(); i++) {
//        	try {ficheroLog.InfoLog("datos: " + swArrayDatosParaOmnex.get(i).toString(),"procesosDeCrearExcel_swArrayDatosParaOmnex_Ajustes");} catch (IOException e) {e.printStackTrace();}
//		}

    	booleanRetorno = procesosDeCrearExcel();
//    	try {ficheroLog.InfoLog("booleanRetorno: " + booleanRetorno,"procesosDeCrearExcel_booleanRetorno_Ajustes");} catch (IOException e) {e.printStackTrace();}
    	
    	return booleanRetorno;
    }

    private static boolean procesosDeCrearExcel() {

      	boolean booleanRetorno = false;
       	String strRuta = null;

//    	try {ficheroLog.InfoLog("Inicio: " + booleanRetorno,"-------procesosDeCrearExcel-------");} catch (IOException e) {e.printStackTrace();}

		//Aqu� abajo teniamos el codigo de 
    	GeneraExcelDesdeArray generaExcel = new GeneraExcelDesdeArray();
    	
    	try {
			booleanRetorno = generaExcel.generaArchivo(swArrayDatosParaOmnex);
		} catch (Exception e) {
			booleanRetorno = false;
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
		System.out.println("***datos de getArrayVentanaDatosOmnex strSaldo: " + Ventana.getStrNuevoSaldoCuenta());
			
        //Consultamos si se han generado archivos LayOut.-------------------------------------
        strRuta = getStrRutaDestinoArchivos() + getStrCarpetaArchivosLog();
		System.out.println("***strRuta Archivos generados: " + strRuta);

        strTextoAreaRutasArchivos = "";
        arrayStrRutasFicherosGenerados = consultarFilesRutaPorFecha(strRuta);

    	System.out.println("***strRuta SIZE arrayStrRutasFicherosGenerados: " + arrayStrRutasFicherosGenerados.size());
        
        if(arrayStrRutasFicherosGenerados.size()>0){
			strTextoAreaRutasArchivos = strTextoAreaRutasArchivos + "\n" + "Archivos Log generados:" + "\n";
        }
        for(int contEle=0;contEle<arrayStrRutasFicherosGenerados.size();contEle++){
			strTextoAreaRutasArchivos = strTextoAreaRutasArchivos + arrayStrRutasFicherosGenerados.get(contEle) + "\n";
		}
        //Consultamos si se han generado archivos Log.-------------------------------------
        strRuta = getStrRutaDestinoArchivos();
		System.out.println("***strRuta Archivos generados: " + strRuta);

        arrayStrRutasFicherosGenerados = consultarFilesRutaPorFecha(strRuta);
        System.out.println("***strRuta SIZE arrayStrRutasFicherosGenerados: " + arrayStrRutasFicherosGenerados.size());

        if(arrayStrRutasFicherosGenerados.size()>0){
			strTextoAreaRutasArchivos = strTextoAreaRutasArchivos + "\n" + "Archivos Excel generados:" + "\n";
        }else{
			strTextoAreaRutasArchivos = strTextoAreaRutasArchivos + "\n" + "No se ha generado el archivo xls para importarlo en Omnex" + "\n";        	
        }

        for(int contEle=0;contEle<arrayStrRutasFicherosGenerados.size();contEle++){
			strTextoAreaRutasArchivos = strTextoAreaRutasArchivos + arrayStrRutasFicherosGenerados.get(contEle) + "\n";
		}
        //------------------------------------------------------------------------------------
        
		configurarVentana(strTipoConfigCrearExcelCorrecto);
		
		return booleanRetorno;
    }

//    private void btnComprobacionesActionPerformed(java.awt.event.ActionEvent evt) {
    private static void btnComprobacionesActionPerformed() {
    	
		configurarVentana(strTipoConfigComprobacionesCorrectas);
    }

    public static String getStrAjusteSW() {
		return strAjusteSW;
	}	

    public static String getStrCarpetaArchivosLayout() {
		return strCarpetaArchivosLayout;
	}	

	public static String getStrCarpetaArchivosLog() {
		return strCarpetaArchivosLog;
	}

	public static String getStrCarpetaHistorialOmnex() {
		return strCarpetaHistorialOmnex;
	}

	public static ArrayList<String> consultarFilesRutaPorFecha(String strRuta) {

		ArrayList<String> arrayFiles = new ArrayList<String>();
		// Aqu� la carpeta donde queremos buscar
		//String path = "C:/";
		String path = strRuta;

		String files;
		File folder = new File(path);
		File[] listOfFiles = folder.listFiles();
				
		for (int i = 0; i < listOfFiles.length; i++){
				if (listOfFiles[i].isFile())         
				{
					files = listOfFiles[i].getName();
					//Si el fichero (files) que ha encontrado en el directorio contiene la cadena   
					//		getStrDateFileName (fechaHora actual para los nombres de los archivos) -> lo obtenemos, si no, lo ignoraremos.
					if (!(files.lastIndexOf(getStrDateFileName())<0)){
						arrayFiles.add(strRuta + "/" + files);
					}
				}				
		}
		return arrayFiles;
	}

	public static ArrayList<String> consultarAllFilesRuta(String strRuta) {

		ArrayList<String> arrayFiles = new ArrayList<String>();
		// Aqu� la carpeta donde queremos buscar
		//String path = "C:/";
		String path = strRuta;

		String files;
		File folder = new File(path);
		File[] listOfFiles = folder.listFiles();
				
		for (int i = 0; i < listOfFiles.length; i++){
				if (listOfFiles[i].isFile())         
				{
					files = listOfFiles[i].getName();
					arrayFiles.add(strRuta + "/" + files);
				}				
		}
		return arrayFiles;
	}

	public static String getStrRutaArchivoXlsBanco() {
		return strRutaArchivoXlsBanco;
	}	

	public static String getStrRutaDestinoArchivos() {
		return strRutaDestinoArchivos;
	}	

	public static String getStrSWrutaAgentesActivos() {
		return strSWrutaAgentesActivos;
	}	

	public static String getStrDateFileName() {
		return strDateFileName;
	}

	public static String getStrDateExcelOmnex() {
		return strDateExcelOmnex;
	}

	public static void setStrSWrutaAgentesActivos() {
		SaldosProperties propertiesSaldos = new SaldosProperties();
		String strDireccion = "";

		try {
			strDireccion = propertiesSaldos.getDirRutaAgentesActivos();
		} catch (IOException e) {
			// Mostramos Ventana informando de que no se ha podido leer
			// properties.
//			System.out.println("*********************************************************************************************************************** catch IO");
			//e.printStackTrace();
		}

		// strRutaDestinoArchivos = "C:\\Concur\\Concur1\\Concur2222\\";

		strSWrutaAgentesActivos = strDireccion;
	}
	
	public static void setStrRutaDestinoArchivos() {
		SaldosProperties propertiesSaldos = new SaldosProperties();
		String strDireccion = "";

		try {
			strDireccion = propertiesSaldos.getDirRutaOutput();
		} catch (IOException e) {
			// Mostramos Ventana informando de que no se ha podido leer
			// properties.
//			System.out.println("*********************************************************************************************************************** catch IO");
			//e.printStackTrace();
		}

		// strRutaDestinoArchivos = "C:\\Concur\\Concur1\\Concur2222\\";

		strRutaDestinoArchivos = strDireccion;
	}
	
	public static void setStrDateExcelOmnex(String strDate) {
		strDateExcelOmnex = strDate;
	}	
	
	public static void setStrDateFileName(String strDate) {
		strDateFileName = strDate;
	}	
	
	public static void setIntRowPosicionInicialMovimiento(int valor) {
		intRowPosicionInicialMovimiento = valor;
	}	

	public static void setIntCellPosicionInicialMovimiento(int valor) {
		intCellPosicionInicialMovimiento = valor;
	}	
	
	public static void setIntRowPosicionInicialObservaciones(int valor) {
		intRowPosicionInicialObservaciones = valor;
	}	

	public static void setIntCellPosicionInicialObservaciones(int valor) {
		intCellPosicionInicialObservaciones = valor;
	}	

	public static void setIntRowPosicionInicialImporte(int valor) {
		intRowPosicionInicialImporte = valor;
	}	

	public static void setIntCellPosicionInicialImporte(int valor) {
		intCellPosicionInicialImporte = valor;
	}	

	public static void setIntRowPosicionInicialSaldo(int valor) {
		intRowPosicionInicialSaldo = valor;
	}	

	public static void setIntCellPosicionInicialSaldo(int valor) {
		intCellPosicionInicialSaldo = valor;
	}	

	public static void setIntRowPosicionInicialFecha(int valor) {
		intRowPosicionInicialFecha = valor;
	}	

	public static void setIntCellPosicionInicialFecha(int valor) {
		intCellPosicionInicialFecha = valor;
	}	

	public static int getIntRowPosicionInicialMovimiento() {
		return intRowPosicionInicialMovimiento;
	}	

	public static int getIntCellPosicionInicialMovimiento() {
		return intCellPosicionInicialMovimiento;
	}	

	public static int getIntRowPosicionInicialObservaciones() {
		return intRowPosicionInicialObservaciones;
	}	

	public static int getIntCellPosicionInicialObservaciones() {
		return intCellPosicionInicialObservaciones;
	}	

	public static int getIntRowPosicionInicialImporte() {
		return intRowPosicionInicialImporte;
	}	

	public static int getIntCellPosicionInicialImporte() {
		return intCellPosicionInicialImporte;
	}	

	public static int getIntRowPosicionInicialSaldo() {
		return intRowPosicionInicialSaldo;
	}	

	public static int getIntCellPosicionInicialSaldo() {
		return intCellPosicionInicialSaldo;
	}	

	public static int getIntRowPosicionInicialFecha() {
		return intRowPosicionInicialFecha;
	}	

	public static int getIntCellPosicionInicialFecha() {
		return intCellPosicionInicialFecha;
	}	

	public static String getStrOpcionBancoSelected() {
		return strOpcionBancoSelected;
	}	

	public static String getStrOpcionCuentaSelected() {
		return strOpcionCuentaSelected;
	}	

	public static String getStrOpcionDescripcionSelected() {
		return strOpcionDescripcionSelected;
	}

	public static String getStrOpcionDatosIdAgenteSelected() {
		return strOpcionDatosIdAgenteSelected;	
	}

	public static String getStrOpcionImporteSelected() {
		return strOpcionImporteSelected;	
	}

	public static String getStrOpcionIdentificadorSelected() {
		return strOpcionIdentificadorSelected;	
	}
	
	public static String getStrOpcionFilaPrimerMovimientoSelected() {
		return strOpcionFilaPrimerMovimientoSelected;	
	}
	
	public static String getStrOpcionPosicionSaldoSelected() {
		return strOpcionPosicionSaldoSelected;	
	}

	public static String getStrOpcionDistanciaSaldoAdatosSelected() {
		return strOpcionDistanciaSaldoAdatosSelected;	
	}

	public static String getStrOpcionOrdenSelected() {
		return strOpcionOrdenSelected;	
	}

	public static String getStrOpcionMovimientoSelected() {
		return strOpcionPosicionMovimientoSelected;	
	}

	public static String getStrOpcionFechaSelected() {
		return strOpcionPosicionFechaSelected;	
	}

	public static String getStrRutaJAR() {
		return strRutaJAR;
	}	
	
	public static String getStrVersion() {
		return strVersion;
	}	
	
	public static String getStrNuevoSaldoCuenta() {
		return strNuevoSaldoCuenta;
	}	
	
	public static void setStrNuevoSaldoCuenta(String strImporte) {
		strNuevoSaldoCuenta = strImporte;
	}	

//	
//	public static String getStrBancoEnSaldosProperties() {
//		return strBancoEnSaldosProperties;
//	}	
//	
//	public static void setStrBancoEnSaldosProperties(String strBanco) {
//		strBancoEnSaldosProperties = strBanco;
//	}	

	public static void acumularArrayListAjustesAlmacenados(String str1, String str2, String str3) {
		ArrayList<String> arrayListAjuste = new ArrayList<>();
		arrayListAjuste.add(str1);
		arrayListAjuste.add(str2);
		arrayListAjuste.add(str3);
		arrayListAjustesAlmacenados.add(arrayListAjuste);
	}	

	public static ArrayList<ArrayList<String>> getArrayListAjustesAlmacenados() {
		return arrayListAjustesAlmacenados;
	}	

	public static ArrayList<String> getArrayCortePreparadoParaHistorialOmnex() {
		return arrayCortePreparadoParaHistorialOmnex;
	}	

	public static void clearArrayCortePreparadoParaHistorialOmnex() {
		arrayCortePreparadoParaHistorialOmnex.clear();
	}
	
	public static void copiarDatosEnArrayCortePreparadoParaHistorialOmnex(ArrayList<String> array) {
		arrayCortePreparadoParaHistorialOmnex.addAll(array);
	}

	public static ArrayList<ArrayList<String>> getArrayListBidimSaldosProperties() {
		return arrayListBidimSaldosProperties;
	}	

	public static int getIntIndiceCB() {
		return intIndiceCB;
	}	

	public static double getSwDoubleSumatorioImportes() {
		return swDoubleSumatorioImportes;
	}	
	
	public static void setSwDoubleSumatorioImportes(Double valor) {
		swDoubleSumatorioImportes = valor;
	}	
	
	public static ArrayList<String> getArrayListSWAgentesValidos(){
		return arrayListSWAgentesValidos;
	}

	public static ArrayList<ArrayList<String>> getArrayListSWBidimExcepcionesAgentesActivados(){
		return arrayListSWBidimExcepcionesAgentesActivados;
	}

	public static void addAllArrayListSWMovimientosSobrantes(ArrayList<ArrayList<String>> arrayListDatos) {
		arrayListSWMovimientosSobrantes.clear();
		arrayListSWMovimientosSobrantes.addAll(arrayListDatos);
	}

	public static ArrayList<ArrayList<String>> getArrayListSWMovimientosSobrantes(){
		return arrayListSWMovimientosSobrantes;
	}

	public static ArrayList<ArrayList<String>> getSwArrayBiDimensDelExcel(){
		return swArrayBiDimensDelExcel;
	}

	public static void acumularStrTextoResultadoComprobaciones(String msg){
		strTextoResultadoComprobaciones = strTextoResultadoComprobaciones + msg;
//		System.out.println("->->: " + strTextoResultadoComprobaciones);
		txtAreaResultadoComprobaciones.setText(getStrTextoResultadoComprobaciones());
		txtAreaResultadoComprobacionesAjustes.setText(getStrTextoResultadoComprobaciones());
	}

	public static String getTxtAreaResultadoComprobaciones(){
		return txtAreaResultadoComprobaciones.getText().toString();
	}
	
//	public static void borrarStrTextoResultadoComprobaciones(){
//		strTextoResultadoComprobaciones = "\n" + "Resultado de las comprobaciones:" + "\n";
//		Ventana.txtAreaResultadoComprobaciones.setText(strTextoResultadoComprobaciones);
//	}
	
	public static String getStrTextoResultadoComprobaciones(){
		return strTextoResultadoComprobaciones;
	}
	
	public static void setBooleanComprobacionCorrecta(boolean bValor){
		booleanComprobacionCorrecta = bValor;
	}
	
	public static boolean getBooleanComprobacionCorrecta(){
		return booleanComprobacionCorrecta;
	}
	
	public static boolean getBooleanBtnCrearExcelOmnexEnabled(){
		return booleanBtnCrearExcelOmnexEnabled;
	}

	public static void setBooleanBtnCrearExcelOmnexEnabled(boolean b){
		booleanBtnCrearExcelOmnexEnabled = b;
	}
	
	public static boolean getBooleanBtnComprobacionesEnabled(){
		return booleanBtnComprobacionesEnabled;
	}

	public static void setBooleanBtnComprobacionesEnabled(boolean b){
		booleanBtnComprobacionesEnabled = b;
	}
	
	public static String getStrNombreDelArchivoXLS(){
		return strNombreDelArchivoXLS;
	}

	public static int getIntSwMovimientos(){
		return intSwMovimientos;
	}

	public static void setIntSwMovimientos(int intValor){
		intSwMovimientos = intValor;
	}
	
	public static String getStrRutaFileNameExcelGenerado(){
		return strRutaFileNameExcelGenerado;
	}

	public static void setStrRutaFileNameExcelGenerado(String strValor){
		strRutaFileNameExcelGenerado = strValor;
	}
	
	public static String getStrFileExcelAgentes(){
		return strFileExcelAgentes;
	}

	public static void setStrFileExcelAgentes(String str){
		strFileExcelAgentes = str;
	}
	
	public static int getIntSwMovimientosSobran(){
		return intSwMovimientosSobran;
	}

	public static void setIntSwMovimientosSobran(int intValor){
		intSwMovimientosSobran = intValor;
	}
	
	public static int getIntSwMovimientosNoSobran(){
		return intSwMovimientosNoSobran;
	}

	public static void setIntSwMovimientosNoSobran(int intValor){
		intSwMovimientosNoSobran = intValor;
	}

	public static Double getSwDoubleSaldoHistorico(){
		return swDoubleSaldoHistorico;
	}

	public static void setSwDoubleSaldoHistorico(Double doubleValor){
		swDoubleSaldoHistorico = doubleValor;
	}

	public static void setStrSWMensajesErrorVentana(String str){
		strSWMensajesErrorVentana = str;
	}

	public static String getStrSWMensajesErrorVentana(){
		return strSWMensajesErrorVentana;
	}
	
	public static void setStrPruebaCodigo(String str){
		strPruebaCodigo = str;
	}

	public static String getStrPruebaCodigo(){
		return strPruebaCodigo;
	}

	public static String getTxtAreaAjustesAlmacenados(){
		return txtAreaAjustesAlmacenados.getText().toString();
	}

	public static void setTxtAreaAjustesAlmacenados(String str){
		txtAreaAjustesAlmacenados.setText(str);
	}

	public static void acumularTxtAreaAjustesAlmacenados(String str){
		txtAreaAjustesAlmacenados.setText(getTxtAreaAjustesAlmacenados() + str);
	}

	public static void setVisibleTxtAreaAjustesAlmacenados(){
		txtAreaAjustesAlmacenados.setVisible(true);
	}

	public static void setNoVisibleTxtAreaAjustesAlmacenados(){
		txtAreaAjustesAlmacenados.setVisible(false);
	}

	public static void setVisibleTxtAreaResultadoComprobacionesAjustes(){
		txtAreaResultadoComprobacionesAjustes.setVisible(true);
	}

	public static void setNoVisibleTxtAreaResultadoComprobacionesAjustes(){
		txtAreaResultadoComprobacionesAjustes.setVisible(false);
	}

	public static double getDoubleTotalSumatorioAjustes(){
		return doubleTotalSumatorioAjustes;
	}

	public static void inicializarTxtAreaResultadoComprobaciones(){
//    	strTextoResultadoComprobaciones = "\n" + "Resultado de las comprobaciones:" + "\n";
    	strTextoResultadoComprobaciones = "";
    	txtAreaResultadoComprobaciones.setText("");
		txtAreaResultadoComprobaciones.setText(strTextoResultadoComprobaciones);
    	txtAreaResultadoComprobacionesAjustes.setText("");
		txtAreaResultadoComprobacionesAjustes.setText(strTextoResultadoComprobaciones);
	}

	public static void asignarPosicionesValoresXLS(String strOpcionComboSelected) throws IOException{

		LogsFile ficheroLog = new LogsFile();
		int intPosicionFilaPrimerMovimientoEmpezandoPorCero = 0;
		int intPosicionColumnaMovimientoEmpezandoPorCero = 0;
		int intPosicionColumnaObservacionesEmpezandoPorCero = 0;
		int intPosicionColumnaImporteEmpezandoPorCero = 0;
		int intPosicionColumnaSaldoEmpezandoPorCero = 0;
		int intPosicionColumnaFechaEmpezandoPorCero = 0;

    	for (int i = 0; i < arrayListBidimSaldosProperties.size(); i++) {

        	if (arrayListBidimSaldosProperties.get(i).get(0).equals(getStrOpcionBancoSelected()) 
        			&& arrayListBidimSaldosProperties.get(i).get(1).equals(getStrOpcionCuentaSelected())){
        		//posicion 6 del array de saldosProperties es:   la fila con el primer movimiento.
        		intPosicionFilaPrimerMovimientoEmpezandoPorCero = Integer.valueOf(arrayListBidimSaldosProperties.get(i).get(6)) - 1;
        		//posicion 10 del array de saldosProperties es:   movimiento (tipo de mov.)
        		intPosicionColumnaMovimientoEmpezandoPorCero = Ventana.obtenerPosicionColumnaDesdeLetra(arrayListBidimSaldosProperties.get(i).get(10));
        		//posicion 3 del array de saldosProperties es:   datos
        		intPosicionColumnaObservacionesEmpezandoPorCero = Ventana.obtenerPosicionColumnaDesdeLetra(arrayListBidimSaldosProperties.get(i).get(3));
        		//posicion 4 del array de saldosProperties es:   importe
        		intPosicionColumnaImporteEmpezandoPorCero = Ventana.obtenerPosicionColumnaDesdeLetra(arrayListBidimSaldosProperties.get(i).get(4));
        		//posicion 7 del array de saldosProperties es:   saldo
        		intPosicionColumnaSaldoEmpezandoPorCero = Ventana.obtenerPosicionColumnaDesdeLetra(arrayListBidimSaldosProperties.get(i).get(7));
        		//posicion 11 del array de saldosProperties es:   fecha
        		intPosicionColumnaFechaEmpezandoPorCero = Ventana.obtenerPosicionColumnaDesdeLetra(arrayListBidimSaldosProperties.get(i).get(11));

        		System.out.println("<20171107><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        		System.out.println("<20171107>valor de arrayListBidimSaldosProperties:  >>>>>>>>>>>>>>>>>>>>> : " + arrayListBidimSaldosProperties.get(i).toString());
        		
        		System.out.println("<20171107><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< intPosicionFilaPrimerMovimientoEmpezandoPorCero: " + intPosicionFilaPrimerMovimientoEmpezandoPorCero);
        		System.out.println("<20171107><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< intPosicionColumnaMovimientoEmpezandoPorCero: " + intPosicionColumnaMovimientoEmpezandoPorCero);
        		System.out.println("<20171107><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< intPosicionColumnaObservacionesEmpezandoPorCero: " + intPosicionColumnaObservacionesEmpezandoPorCero);
        		System.out.println("<20171107><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< intPosicionColumnaImporteEmpezandoPorCero: " + intPosicionColumnaImporteEmpezandoPorCero);
        		System.out.println("<20171107><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< intPosicionColumnaSaldoEmpezandoPorCero: " + intPosicionColumnaSaldoEmpezandoPorCero);
        		System.out.println("<20171107><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< intPosicionColumnaFechaEmpezandoPorCero: " + intPosicionColumnaFechaEmpezandoPorCero);
        		
        		//la caixa, bbva
        		//---Aqu� se pregunta de qu� forma se consulta el saldo en ese corte:
        		//		si el saldo llega en cada l�nea de cada movimiento, si llega solo una vez en el reporte,    o si el operador lo introduce por Ventana.
            	if (arrayListBidimSaldosProperties.get(i).get(7).startsWith("todas:")){
        			String[] elemsPosicionSaldo = arrayListBidimSaldosProperties.get(i).get(7).toString().split("todas:");
        			String strLetraSaldo = "";
        			if (elemsPosicionSaldo.length>1){
            			strLetraSaldo = elemsPosicionSaldo[1].toString();
            			intPosicionColumnaSaldoEmpezandoPorCero = Ventana.obtenerPosicionColumnaDesdeLetra(strLetraSaldo);
            			//try {ficheroLog.InfoLog("Dentro de buscarValor999. swArrayDatosParaOmnex.get: " + swArrayDatosParaOmnex.get(i4).get(0), "buscarValor999");} catch (IOException e1) {e1.printStackTrace();}
//            			ficheroLog.InfoLog("CREAR LOG", "buscarValor999");
                    	ficheroLog.InfoLog("OK  cabecera:	valor de intPosicionColumnaImporteEmpezandoPorCero: " + intPosicionColumnaImporteEmpezandoPorCero, "22222_bancos");
        			}else{
        				intPosicionColumnaSaldoEmpezandoPorCero = -1;
        			}
        		}else{
        			//Este else habr� que quitarlo cuando se codifique si es cabecera:	o	final:
    				intPosicionColumnaSaldoEmpezandoPorCero = -1;
        		}
/*
            	if (arrayListBidimSaldosProperties.get(i).get(7).startsWith("cabecera:")){
            		.........
            	}
            	if (arrayListBidimSaldosProperties.get(i).get(7).startsWith("final:")){
            		.........
            	}
*/            	
        	}
		}
		setIntRowPosicionInicialMovimiento(intPosicionFilaPrimerMovimientoEmpezandoPorCero);
		setIntCellPosicionInicialMovimiento(intPosicionColumnaMovimientoEmpezandoPorCero);
		setIntRowPosicionInicialObservaciones(intPosicionFilaPrimerMovimientoEmpezandoPorCero);
		setIntCellPosicionInicialObservaciones(intPosicionColumnaObservacionesEmpezandoPorCero);
		setIntRowPosicionInicialImporte(intPosicionFilaPrimerMovimientoEmpezandoPorCero);
		setIntCellPosicionInicialImporte(intPosicionColumnaImporteEmpezandoPorCero);
		setIntRowPosicionInicialSaldo(intPosicionFilaPrimerMovimientoEmpezandoPorCero);
		setIntCellPosicionInicialSaldo(intPosicionColumnaSaldoEmpezandoPorCero);
		setIntRowPosicionInicialFecha(intPosicionFilaPrimerMovimientoEmpezandoPorCero);
		setIntCellPosicionInicialFecha(intPosicionColumnaFechaEmpezandoPorCero);

		System.out.println("<20171024><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<: " + intPosicionFilaPrimerMovimientoEmpezandoPorCero);
		System.out.println("<20171024><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<: " + intPosicionColumnaObservacionesEmpezandoPorCero);
		System.out.println("<20171024><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<: " + intPosicionFilaPrimerMovimientoEmpezandoPorCero);
		System.out.println("<20171024><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<: " + intPosicionColumnaImporteEmpezandoPorCero);
		System.out.println("<20171024><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<: " + intPosicionFilaPrimerMovimientoEmpezandoPorCero);
		System.out.println("<20171024><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<: " + intPosicionColumnaSaldoEmpezandoPorCero);
		System.out.println("<20171024><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<: " + intPosicionFilaPrimerMovimientoEmpezandoPorCero);
		System.out.println("<20171024><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<: " + intPosicionColumnaFechaEmpezandoPorCero);

		
    	//String[] elemsOpcionSeleccionada = strOpcionBancoSelected.split("<>");
    	
//    	String strOpcionBancoSelected = elemsOpcionSeleccionada[0].trim();
    	strOpcionBancoSelected = strOpcionComboSelected;
    	    	
    	//*********************************************************************************************
    	//Banco BBVA.
    	//Posiciones de valores
    	//Observaciones:			F16 (Fila:16 Celda:06)
    	//Importe:					G16 (Fila:16 Celda:07)
    	//*********************************************************************************************
    	//Banco La Caixa.
    	//Posiciones de valores
    	//Mas Datos					D4 (Fila:04 Celda:04)
    	//Importe:					E4 (Fila:04 Celda:05)
    	//*********************************************************************************************
    	//Banco POSTA.
    	//Posiciones de valores
    	//DESCRIZIONE OPERAZIONE:	E14 (Fila:14 Celda:05)
    	//Accrediti:				D14 (Fila:14 Celda:04)
    	//*********************************************************************************************
/*
    	if (strOpcionBancoSelected.equals("BBVA*****")){
    		setIntRowPosicionInicialObservaciones((int)15);
        	setIntCellPosicionInicialObservaciones((int)5);    		
        	setIntRowPosicionInicialImporte((int)15);
        	setIntCellPosicionInicialImporte((int)6);
    		setIntRowPosicionInicialSaldo((int)3);
    		setIntCellPosicionInicialSaldo((int)5);    		        	
    	}
*/    	
//    	if (strOpcionBancoSelected.equals("La Caixa")){
//      if (strOpcionBancoSelected.equals(getStrBancoEnSaldosProperties())){
/*
    	if (strOpcionBancoSelected.equals("La Caixaa")){
    		setIntRowPosicionInicialObservaciones((int)3);
    		setIntCellPosicionInicialObservaciones((int)3);    		
    		setIntRowPosicionInicialImporte((int)3);
    		setIntCellPosicionInicialImporte((int)4);    		
    		setIntRowPosicionInicialSaldo((int)3);
    		setIntCellPosicionInicialSaldo((int)5);    		
    	}
*/    	
    	if (strOpcionBancoSelected.equals("POSTA****")){
    		setIntRowPosicionInicialObservaciones((int)13);
    		setIntCellPosicionInicialObservaciones((int)4);    		
    		setIntRowPosicionInicialImporte((int)13);
    		setIntCellPosicionInicialImporte((int)3);    	
    		setIntRowPosicionInicialSaldo((int)3);
    		setIntCellPosicionInicialSaldo((int)5);    		
    	}

/*    	
    	ficheroLog.InfoLog("----setIntRowPosicionInicialObservaciones: " + getIntRowPosicionInicialObservaciones(), "________________bancos");
    	ficheroLog.InfoLog("----setIntCellPosicionInicialObservaciones: " + getIntCellPosicionInicialObservaciones(), "________________bancos");    		
    	ficheroLog.InfoLog("----setIntRowPosicionInicialImporte: " + getIntRowPosicionInicialImporte(), "________________bancos");
    	ficheroLog.InfoLog("----setIntCellPosicionInicialImporte: " + getIntCellPosicionInicialImporte(), "________________bancos");
    	ficheroLog.InfoLog("----setIntRowPosicionInicialSaldo: " + getIntRowPosicionInicialSaldo(), "________________bancos");
    	ficheroLog.InfoLog("----setIntCellPosicionInicialSaldo: " + getIntCellPosicionInicialSaldo(), "________________bancos");
*/    	
    	
    	//****************************************
    	//****************************************
    	//****************************************
    	//****************************************
    	//***Para hacer pruebas   	vuelvo a dejar los valores fijos.
    	
    	//****************************************
		System.out.println("1<20171030><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		System.out.println("1 fila->" + getIntRowPosicionInicialImporte());
		System.out.println("1 fila->" + getIntRowPosicionInicialObservaciones());
		System.out.println("1 fila->" + getIntRowPosicionInicialSaldo());
		System.out.println("1 Celda->" + getIntCellPosicionInicialImporte());
		System.out.println("1 Celda->" + getIntCellPosicionInicialObservaciones());
		System.out.println("1 Celda->" + getIntCellPosicionInicialSaldo());
		System.out.println("----------------------------------------------------------------");		
/*
		setIntRowPosicionInicialImporte((int)3);
		setIntRowPosicionInicialObservaciones((int)3);
		setIntRowPosicionInicialSaldo((int)3);
		setIntCellPosicionInicialImporte((int)4);    		
		setIntCellPosicionInicialObservaciones((int)3);    		
		setIntCellPosicionInicialSaldo((int)5);  
		
		System.out.println("2<20171030><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		System.out.println("2 --- fila->" + getIntRowPosicionInicialImporte());
		System.out.println("2 --- fila->" + getIntRowPosicionInicialObservaciones());
		System.out.println("2 --- fila->" + getIntRowPosicionInicialSaldo());
		System.out.println("2 --- Celda->" + getIntCellPosicionInicialImporte());
		System.out.println("2 --- Celda->" + getIntCellPosicionInicialObservaciones());
		System.out.println("2 --- Celda->" + getIntCellPosicionInicialSaldo());
		System.out.println("----------------------------------------------------------------");
*/
    }
    public static void escribirHistorialCortes(ArrayList<ArrayList<String>> arrayDatosParaOmnex) throws IOException{
    	
//    	for (int conEle=0;conEle<arrayDatosParaOmnex.size(); conEle++){
//    		ficheroLog.InfoLog(arrayDatosParaOmnex.get(conEle).toString(), "Historial_cortes");
//    	}
    }
    public static void swProcesosIniciales(){
    	
    	inicializarTxtAreaResultadoComprobaciones();
    	txtAreaResultadoComprobaciones.setVisible(true);

		Ventana.acumularStrTextoResultadoComprobaciones("\n Procesos Iniciales.");

    	String strInfoProcesosIniciales = "";
    	
		System.out.println("swProcesosIniciales: ");
//    	strSWMensajesErrorVentana = "";
    	//*****************************************************************************************************************************
		strInfoProcesosIniciales = strInfoProcesosIniciales + "\n" + " " + "Informaci�n - Procesos iniciales: ";
//		Ventana.acumularStrTextoResultadoComprobaciones("\n" + " " + "Informaci�n:");
		
		//***************************************  Si el formato es incorrecto  *******************************************************
    	//**********************************  ---> no se contin�a con los procesos y mostramos en Ventana un mensaje con la incidencia ocurrida
		strInfoProcesosIniciales = strInfoProcesosIniciales + "\n" + " " + ".....Formato Saldos Properties.";
//    	Ventana.acumularStrTextoResultadoComprobaciones("\n" + " " + ".....Formato Saldos Properties.");
    	
		if (strSWMensajesErrorVentana.equals("")){
			try {
				strRespuestaComprobacionFormatoSaldosProperties = datosSaldosProperties.comprobarFormatoValido(strRutaJAR);
			} catch (IOException e2) {
    			strSWMensajesErrorVentana = "ERROR: * No se pudo comprobar el formato del archivo: " + strRutaJAR;
        		acumularStrTextoResultadoComprobaciones("\n" + " (i) ERROR: * No se pudo comprobar el formato del archivo: " + strRutaJAR);				
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
	    	//Si el formato es incorrecto ---> no se contin�a con los procesos y mostramos en Ventana un mensaje con la incidencia ocurrida.
	    	if (!(strRespuestaComprobacionFormatoSaldosProperties.equals(""))){
	    		System.out.println("***** Ventana formato incorrecto");
	    		Ventana.acumularStrTextoResultadoComprobaciones("\n" + "Error **** : " + strRespuestaComprobacionFormatoSaldosProperties);  		
	    		setBooleanComprobacionCorrecta(false);
	    		
	    		strSWMensajesErrorVentana = strRespuestaComprobacionFormatoSaldosProperties;
	    	}
	    	
			if (strSWMensajesErrorVentana.equals("")){
				Ventana.acumularStrTextoResultadoComprobaciones("\n (i) SaldosProperties: Formato correcto.");				
			}
	   	}	
    	//***************************************  Preparar desplegable opciones de cuentas -------------------------------------------
/*	   	
	   	if (strSWMencbOpcionesLayoutsajesErrorVentana.equals("")){
    		try {  			
    			 = new javax.swing.JComboBox(cargarDatosEncb(arrayListBidimSaldosProperties));
    		} catch (IOException e1) {
    			strSWMensajesErrorVentana = "ERROR: ************ No se pudo asignar valores al desplegable de cuentas.";
        		acumularStrTextoResultadoComprobaciones("\n" + " (i) ERROR: ************ No se pudo asignar valores al desplegable de cuentas.");
    			// TODO Auto-generated catch block
    			e1.printStackTrace();
    		}
    	}
*/
    	//*****************************************************************************************************************************
    	//***************************** Desde archivo: saldos_properties.txt   obtener string con la ruta de localizaci�n de agentes.xls
		strInfoProcesosIniciales = strInfoProcesosIniciales + "\n" + " " + ".....Obtener ruta Agentes.";		
//		Ventana.acumularStrTextoResultadoComprobaciones("\n" + " " + ".....Obtener ruta Agentes.");
    	
		strSWrutaAgentesActivos = "";    	
    	if (strSWMensajesErrorVentana.equals("")){
    		setStrSWrutaAgentesActivos();
    		strInfoProcesosIniciales = strInfoProcesosIniciales + "\n" + " (i) getStrRutaAgentesActivos: " + getStrSWrutaAgentesActivos();    		
//    		acumularStrTextoResultadoComprobaciones("\n" + " (i) getStrRutaAgentesActivos: " + getStrSWrutaAgentesActivos());
    		if (getStrSWrutaAgentesActivos().toString().equals("")){
            	strSWMensajesErrorVentana = "ERROR ***** : No se ha podido consultar correctamente en saldos_properties.txt la ruta donde se encuentra agentes.xls";
    		}
			if (strSWMensajesErrorVentana.equals("")){
				Ventana.acumularStrTextoResultadoComprobaciones("\n (i) Localizado archivo: agentes.");				
			}
    	}
	   	//*************************************** Archivo XLS Agentes Activos *************************************************************
		strInfoProcesosIniciales = strInfoProcesosIniciales + "\n" + " " + ".....Obtener infor desde Agentes.";		
//    	Ventana.acumularStrTextoResultadoComprobaciones("\n" + " " + ".....Obtener infor desde Agentes.");
    	
    	if (strSWMensajesErrorVentana.equals("")){
    		arrayListSWAgentesValidos.clear();
			System.out.println("Numero de agentes activos: " + arrayListSWAgentesValidos.size());
    		try {
        		//A�adir agentes activos en el arrayList. Leer los 'Activo' con moneda EUR.   El archivo siempre estar� en la misma ubicaci�n.
    			//Clase:	GeneraArrayDesdeXlsAgentes		M�todo:		generar()	

    			GeneraArrayDesdeXlsAgentes generaArray = new GeneraArrayDesdeXlsAgentes();
    			arrayListSWAgentesValidos.addAll(generaArray.generar());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
    		
    		if (arrayListSWAgentesValidos.size()==0){
    			strInfoProcesosIniciales = strInfoProcesosIniciales + "\n" + " (i) No se ha encontrado ning�n c�digo de agente 'Activo' con moneda: EUR.(" + strPruebaCodigo + ")";
//        		acumularStrTextoResultadoComprobaciones("\n" + " (i) No se ha encontrado ning�n c�digo de agente 'Activo' con moneda: EUR.(" + strPruebaCodigo + ")");
    			strSWMensajesErrorVentana = "INFO: ************ No se ha encontrado ning�n c�digo de agente 'Activo' con moneda: EUR.";
//    	    	txtAreaResultadoComprobaciones.setText(getStrTextoResultadoComprobaciones());
    		}
			if (strSWMensajesErrorVentana.equals("")){
				Ventana.acumularStrTextoResultadoComprobaciones("\n (i) Datos recibidos desde archivo agentes.");				
			}
			//-- mostrar ---------------------------------------------------------------------
			for(int contEle=0;contEle<arrayListSWAgentesValidos.size();contEle++){
//				System.out.println("-1-1-1-1-1-    (" + contEle + ") " + arrayListSWAgentesValidos.get(contEle));	    	
			}//-- mostrado ---------------------------------------------------------------------			
    	}
    	//*****************************************************************************************************************************   	
    	//*************************************** Archivo saldos_properties.txt *** Formato v�lido ************************************
   		//------------------------------------------------Desde el archivo de �saldos_properties.txt� comprobar el formato del archivo.
		strInfoProcesosIniciales = strInfoProcesosIniciales + "\n" + " " + ".....Formato saldos properties.";
//    	Ventana.acumularStrTextoResultadoComprobaciones("\n" + " " + ".....Formato saldos properties.");
    	
    	if (strSWMensajesErrorVentana.equals("")){
    		System.out.println("1 formatoformato: " + strRutaJAR);
        	
        	try {
        		strSWMensajesErrorVentana = datosSaldosProperties.comprobarFormatoValido(strRutaJAR);
			} catch (IOException e) {
				strInfoProcesosIniciales = strInfoProcesosIniciales + "\n" + " (i) ERROR: No se ha podido acceder al contenido de saldos_properties.";				
//        		acumularStrTextoResultadoComprobaciones("\n" + " (i) ERROR: No se ha podido acceder al contenido de saldos_properties.");				
				strSWMensajesErrorVentana = "ERROR *********** No se ha podido acceder al contenido de saldos_properties.";
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		System.out.println("2 formatoformato: " + strSWMensajesErrorVentana);
        	//Si el formato es incorrecto ---> no se contin�a con los procesos y mostramos en Ventana un mensaje con la incidencia ocurrida.
        	if (!(strSWMensajesErrorVentana.equals(""))){
        		//--Ha habido ERROR y configuramos Ventana con los botones que corresponden estar Enabled.
        		System.out.println("3 formatoformato: " + strSWMensajesErrorVentana);
        		configurarVentana(strTipoConfigInicioErrorSaldosPropertiesFormato);
        	}
			if (strSWMensajesErrorVentana.equals("")){
				Ventana.acumularStrTextoResultadoComprobaciones("\n (i) Saldos properties: Formato correcto.");				
			}

    		//Debe tener correcto formato en las l�neas de configuraci�n de cuenta.
    		//Debe tener correcto formato en la l�nea de ruta para archivos resultantes.
    		//Debe tener correcto formato en la l�nea de excepciones.
    	}
    	//*****************************************************************************************************************************
    	//*************************************** Desde archivo: saldos_properties.txt   generar array con excepciones ACTIVADAS ******

    	//ArrayList  desde archivo saldos_properties Excepciones:	banco, cuenta, agente, nuevocodigo.
    	//private static ArrayList<ArrayList<String>> arrayListSWBidimExcepcionesAgentesActivados = new ArrayList<ArrayList<String>>();
		strInfoProcesosIniciales = strInfoProcesosIniciales + "\n" + " " + ".....Generar excepciones activadas.";    	
//		Ventana.acumularStrTextoResultadoComprobaciones("\n" + " " + ".....Generar excepciones activadas.");

    	if (strSWMensajesErrorVentana.equals("")){
    		SaldosProperties propertiesSaldos = new SaldosProperties();
    		arrayListSWBidimExcepcionesAgentesActivados.clear();
    		try {
    			arrayListSWBidimExcepcionesAgentesActivados.addAll(propertiesSaldos.getExcepcionesActivadas(strRutaJAR));
    		} catch (IOException e) {
        		strSWMensajesErrorVentana = "***** ERROR: " + "No se ha podido consultar las excepciones ACTIVADAS desde saldos_properties.txt ";
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    	}
		if (strSWMensajesErrorVentana.equals("")){
			Ventana.acumularStrTextoResultadoComprobaciones("\n (i) Excepciones generadas activadas.");				
		}
    	//*****************************************************************************************************************************
    	//***************************** Desde archivo: saldos_properties.txt   obtener string con la ruta de destinos de ficheros que se generan.
		strInfoProcesosIniciales = strInfoProcesosIniciales + "\n" + " " + ".....Obtener ruta de destinos de ficheros: " + getStrRutaDestinoArchivos();
//		Ventana.acumularStrTextoResultadoComprobaciones("\n" + " " + ".....Obtener ruta de destinos de ficheros: " + getStrRutaDestinoArchivos());

    	strSWrutaDestinoFicherosGenerados = "";    	
    	if (strSWMensajesErrorVentana.equals("")){
    		setStrRutaDestinoArchivos();
    		strInfoProcesosIniciales = strInfoProcesosIniciales + "\n" + " (i) getStrRutaDestinoArchivos: " + getStrRutaDestinoArchivos();    		
//    		acumularStrTextoResultadoComprobaciones("\n" + " (i) getStrRutaDestinoArchivos: " + getStrRutaDestinoArchivos());
    		if (getStrRutaDestinoArchivos().toString().equals("")){
            	strSWMensajesErrorVentana = "ERROR ***** : No se ha podido consultar correctamente en saldos_properties.txt una ruta de destino para los ficheros que se generan";
    		}
    	}
		if (strSWMensajesErrorVentana.equals("")){
			Ventana.acumularStrTextoResultadoComprobaciones("\n (i) Ficheros que se generan: ruta obtenida.");				
		}
    	//*****************************************************************************************************************************
    	//***************************** Generar desplegable desde la informaci�n de las cuentas. **************************************
    	if (strSWMensajesErrorVentana.equals("")){
    		//swGenerarDesplegableConInfoDeCuentas();
    	}
    	//*****************************************************************************************************************************

    	//Mostrar en Ventana los mensajes que contenga
    	if (!(strSWMensajesErrorVentana.equals(""))){
    		strSWMensajesErrorVentana = strSWMensajesErrorVentana + "\n" + strInfoProcesosIniciales;
    		Ventana.acumularStrTextoResultadoComprobaciones("\n" + strSWMensajesErrorVentana);
    	}
    	//*****************************************************************************************************************************
    	acumularStrTextoResultadoComprobaciones("\n" + "Procesos iniciales finalizados.");
    }
	private static void configurarVentana(String strTipoDeConfiguracion){
		if (strTipoDeConfiguracion.equals(strTipoConfigInicioErrorSaldosPropertiesFormato)){
			btnComprobaciones.setEnabled(false);
			btnCrearExcelOmnex.setEnabled(false);			
		}
		if (strTipoDeConfiguracion.equals(strTipoConfigBotonInicio)){
//	    	strTextoResultadoComprobaciones = "\n" + "Resultado de las comprobaciones:" + "\n";
//	    	strTextoResultadoComprobaciones = "";
//	    	inicializarTxtAreaResultadoComprobaciones();
	    	arrayStrRutasFicherosGenerados.clear();		
	    	
	    	cbOpcionesLayout.setSelectedIndex(0);
	    	txtFieldSaldo.setText("");
	    	txtRutaDetalleCC.setText("");
	    	jLabelMensaje.setText("");	
//	        txtAreaRutasArchivos.setText("2Puede <br>ncons/nu\nltar el resultado en: ");
	        txtAreaRutasArchivos.setText("");
	        txtFieldSaldo.setEditable(true);
	        jLabelFieldSaldo.setText("Saldo en el corte: ");
	      //*******************************************************************************************************
            btnInicio.setVisible(true);
            btnInicio.setEnabled(true);
            btnAbrirExcelGenerado.setVisible(false);
            btnAbrirExcelGenerado.setEnabled(false);
            btnAceptarCorteOmnex.setVisible(false);
            btnAceptarCorteOmnex.setEnabled(false);
            jLabelMensaje.setVisible(true);
            jLabelMensaje.setEnabled(true);
            jLabelFormAjustarCuentaCabeceraValor.setVisible(true);
            jLabelFormAjustarCuentaCabeceraValor.setEnabled(true);
            jLabelFormAjustarImporte.setVisible(true);
            jLabelFormAjustarImporte.setEnabled(true);
            jLabelFormAjustarAgente.setVisible(true);
            jLabelFormAjustarAgente.setEnabled(true);
            jLabelFormAjustarDatos.setVisible(true);
            jLabelFormAjustarDatos.setEnabled(true);
            btnComprobarAjuste.setVisible(true);
            btnComprobarAjuste.setEnabled(true);
            btnCrearExcelOmnexConAjustes.setVisible(true);
            btnCrearExcelOmnexConAjustes.setEnabled(true);
            //btnVolverAinsertarAjuste.setVisible(true);
            //btnVolverAinsertarAjuste.setEnabled(true);
            txtAreaAjustesAlmacenados.setEnabled(false);
            txtAreaAjustesAlmacenados.setVisible(false);
            txtAreaResultadoComprobacionesAjustes.setEnabled(false);
            txtAreaResultadoComprobacionesAjustes.setVisible(false);
            jLabelPedirFormatoLayout.setVisible(true);
            jLabelPedirFormatoLayout.setEnabled(true);
//            jLabelPedirCuentaParaAjustarSaldo.setVisible(false);
//            jLabelPedirCuentaParaAjustarSaldo.setEnabled(false);
            jLabelCabecera0AjustarSaldo.setVisible(false);
            jLabelCabecera0AjustarSaldo.setEnabled(false);
            jLabelCabecera1AjustarSaldo.setVisible(false);
            jLabelCabecera1AjustarSaldo.setEnabled(false);
            jLabelCabecera2AjustarSaldo.setVisible(false);
            jLabelCabecera2AjustarSaldo.setEnabled(false);
            jLabelAjustesMsjError.setVisible(false);
//          jLabelAjustesMsjError.setEnabled(false);
            jLabelAjustesMsjError.setEnabled(true);
            cbOpcionesLayout.setVisible(true);
            cbOpcionesLayout.setEnabled(true);
            jLabelFieldSaldo.setVisible(false);
            jLabelFieldSaldo.setEnabled(false);
            jLabelCabecera.setVisible(true);
            jLabelCabecera.setEnabled(true);
            jLabelPedirDocumentoDetalleCC.setVisible(true);
            jLabelPedirDocumentoDetalleCC.setEnabled(true);
            txtRutaDetalleCC.setVisible(true);
            txtRutaDetalleCC.setEnabled(true);
            btnSeleccionarDetalleCC.setVisible(true);
            btnSeleccionarDetalleCC.setEnabled(true);
            jLabelSaldoDelCorte.setVisible(false);
            jLabelSaldoDelCorte.setEnabled(false);
            txtFieldSaldo.setVisible(false);
            txtFieldSaldo.setEnabled(false);
            btnComprobaciones.setVisible(true);
            btnComprobaciones.setEnabled(true);
            btnCrearExcelOmnex.setVisible(true);
            btnCrearExcelOmnex.setEnabled(false);
            txtAreaRutasArchivos.setVisible(false);
            txtAreaRutasArchivos.setEnabled(false);
            txtAreaResultadoComprobaciones.setVisible(false);
            txtAreaResultadoComprobaciones.setEnabled(false);
            jLabelResultadoOmnex.setVisible(false);
            jLabelResultadoOmnex.setEnabled(false);
            jLabelFormAjustarCuentaCabeceraValor.setVisible(false);
            txtFieldFormAjustarSaldoCuentaAgente.setVisible(false);
            txtFieldFormAjustarSaldoCuentaImporte.setVisible(false);
            txtFieldFormAjustarSaldoCuentaDatos.setVisible(false);
//-------------------------------------------------------------------------------
			jLabelFormAjustarAgente.setEnabled(false);
			jLabelFormAjustarAgente.setVisible(false);
			jLabelFormAjustarImporte.setEnabled(false);
			jLabelFormAjustarImporte.setVisible(false);
			jLabelFormAjustarDatos.setEnabled(false);
			jLabelFormAjustarDatos.setVisible(false);

			btnComprobarAjuste.setEnabled(false);
			btnComprobarAjuste.setVisible(false);
			btnCrearExcelOmnexConAjustes.setEnabled(false);
			btnCrearExcelOmnexConAjustes.setVisible(false);
		
			arrayTest.clear();    
			strOpcionCuentaSelected = "";
			strOpcionDescripcionSelected = "";
			strOpcionDatosIdAgenteSelected = "";
			strOpcionImporteSelected  = "";        	
			strOpcionIdentificadorSelected = "";
			strOpcionFilaPrimerMovimientoSelected = "";
			strOpcionPosicionSaldoSelected = "";
			strOpcionDistanciaSaldoAdatosSelected = "";
			strOpcionOrdenSelected = "";	
			strOpcionPosicionMovimientoSelected  = "";        	
			strOpcionPosicionFechaSelected = "";        	
			
			strRutaDestinoArchivos = "";
			setStrRutaDestinoArchivos();
			
			strRutaArchivoXlsBanco = "";
			//strDateFileName = "";
			//strDateExcelOmnex = "";
			strCuentaBanco = "";
			//strRutaJAR = "";
			
			intRowPosicionInicialObservaciones = 0;
			intCellPosicionInicialObservaciones = 0;
			intRowPosicionInicialImporte = 0;
			intCellPosicionInicialImporte = 0;
			intRowPosicionInicialSaldo = 0;
			intCellPosicionInicialSaldo = 0;
			//arrayListBidimSaldosProperties.clear();
			arrayCortePreparadoParaHistorialOmnex.clear();
			arrayMovimientoSaldo.clear();    
			strNuevoSaldoCuenta = "";
			intIndiceCB = 0;
			swDoubleSumatorioImportes = 0;
			//strTextoResultadoComprobaciones = "\n" + "Resultado de las comprobaciones:" + "\n";
			//strTextoResultadoComprobaciones = "";
			booleanComprobacionCorrecta = true;
			strNombreDelArchivoXLS = "";
			booleanBtnComprobacionesEnabled = true;
			booleanBtnCrearExcelOmnexEnabled = true;
			strFileExcelAgentes = "";
			strSWMensajesErrorVentana = "";
			//arrayListSWAgentesValidos.clear();
			arrayListSWBidimSaldosPropertiesCuentas.clear();
			strSWrutaDestinoFicherosGenerados = "";
			strMsgTxtFieldSaldoSinValor = "No se ha introducido el valor del nuevo saldo final.";
			//arrayListSWBidimExcepcionesAgentesActivados.clear();
			swArrayBiDimensDelExcel.clear();
			swArrayDatosParaOmnex.clear();
			intSwMovimientos = 0;
			swDoubleSaldoHistorico = 0.0;
			intSwMovimientosSobran = 0;
			intSwMovimientosNoSobran = 0;
			strTextoAreaRutasArchivos = "";
			arrayStrRutasFicherosGenerados.clear();
			arrayListSWMovimientosSobrantes.clear();
			txtAreaResultadoComprobaciones.setEnabled(true);
			txtAreaResultadoComprobaciones.setVisible(true);
			txtAreaResultadoComprobaciones.setText("");
			
			arrayListAjustesAlmacenados.clear();
			doubleTotalSumatorioAjustes = 0.0;
    		btnAjustarSaldoCuenta.setVisible(false);            			
//*******************************************************************************************************            
		}
		if (strTipoDeConfiguracion.equals(strTipoConfigComprobacionesCorrectas)){  			
			btnComprobaciones.setEnabled(false);
			btnSeleccionarDetalleCC.setEnabled(false);			
			btnCrearExcelOmnex.setEnabled(true);
	    	btnAceptarCorteOmnex.setVisible(false);
	    	btnAbrirExcelGenerado.setVisible(false);
	    	
            txtAreaResultadoComprobaciones.setVisible(true);
            txtAreaResultadoComprobaciones.setEnabled(true);
		}
		if (strTipoDeConfiguracion.equals(strTipoConfigCrearExcelCorrecto)){
//			strTextoAreaRutasArchivos= "uno dos tres";

			txtAreaRutasArchivos.setText(strTextoAreaRutasArchivos);
	//********************************************************************************************************************************
//	        jLabelPedirCuentaParaAjustarSaldo.setVisible(false);
	        jLabelCabecera0AjustarSaldo.setVisible(false);
	        jLabelCabecera1AjustarSaldo.setVisible(false);
	        jLabelCabecera2AjustarSaldo.setVisible(false);
	        jLabelAjustesMsjError.setVisible(false);

	        jLabelPedirFormatoLayout.setText("");
	        jLabelSaldoDelCorte.setText("");
	        jLabelPedirDocumentoDetalleCC.setText("");
	        
	        cbOpcionesLayout.setVisible(false);
	        txtRutaDetalleCC.setVisible(false);
	        btnSeleccionarDetalleCC.setVisible(false);

	        btnCrearExcelOmnex.setVisible(false);
	        btnComprobaciones.setVisible(false);
	        btnAceptarCorteOmnex.setEnabled(true);        
	        btnAceptarCorteOmnex.setVisible(true);
	        btnAbrirExcelGenerado.setEnabled(true);
	        btnAbrirExcelGenerado.setVisible(true);
	        txtFieldSaldo.setVisible(false);
	        jLabelSaldoDelCorte.setVisible(false);
	        jLabelFieldSaldo.setVisible(false);
	        txtAreaRutasArchivos.setEnabled(true);			
	        txtAreaRutasArchivos.setVisible(true);			
		}
	}
	
	public static int obtenerPosicionColumnaDesdeLetra(String strLetra){
		// Devuelve -1   si  la letra que recibe por argumento no es una de las siguientes:		abcdefghijklmn   (strSWLetras).
		int intPosicion = -1;
		strLetra = strLetra.toLowerCase();
		
		for(int contEle=0;contEle<14;contEle++){
			if (strLetra.equals(strSWletras.substring(contEle, contEle+1))){
				intPosicion = contEle;
			}
		}
		return intPosicion;
	}

	public static String swValidaSeleccionCuentaArchivoSaldoEnVentana(){
		String stringMensaje = "";
		
		if(strOpcionBancoSelected.equals(strOpcionBancoNoSelected.toString()) || strOpcionBancoSelected.equals("")){
			stringMensaje = " Debe seleccionar la cuenta vinculada a su archivo XLS.";			
		}else{
			if(txtRutaDetalleCC.getText().length()<1){
				stringMensaje = " Debe seleccionar el archivo xls del banco.";
			}else{
				if(txtFieldSaldo.isVisible()){
					String strValorSaldoActual = txtFieldSaldo.getText().toString();
	    			System.out.println("Valor Saldo Actual string: " + strValorSaldoActual);
					double doubleValorSaldoActual;
					try {
						doubleValorSaldoActual = Double.valueOf(strValorSaldoActual);
    	    			System.out.println("2 Valor Saldo Actual double: " + doubleValorSaldoActual);
    	    			txtFieldSaldo.setEditable(false);
    	    			setStrNuevoSaldoCuenta(strValorSaldoActual);
    	    			System.out.println("22 Valor Saldo Actual str: " + getStrNuevoSaldoCuenta());
					} catch (NumberFormatException e) {
    					stringMensaje = strMsgTxtFieldSaldoSinValor;
    	    			System.out.println("3 Valor Saldo Actual double: " + strValorSaldoActual);
						// TODO Auto-generated catch block
						e.printStackTrace();
					}        					
				}
			}
		}	
		return stringMensaje;
	}

	public static String swValidarNombreArchivoConIdentificador(){
		String stringMensaje = "";
		return stringMensaje;
	}
	
	public static double redondearDecimales(double valorInicial, int intNumeroDecimales) {
    	double parteEntera, resultado;
        resultado = valorInicial;
        parteEntera = Math.floor(resultado);
        
        resultado=(resultado-parteEntera)*Math.pow(10, intNumeroDecimales);
        resultado=Math.round(resultado);
        resultado=(resultado/Math.pow(10, intNumeroDecimales))+parteEntera;
        return resultado;
    }
	
	public static boolean comprobarExisteCarpetaDeLaCuenta(String strCuenta) {
    	boolean booleanRetorno = false;
	    String FileName = strRutaDestinoArchivos + "Historial_Omnex" +"/Cuenta" + strCuenta;  

		if (new File(FileName).exists()==true){
			booleanRetorno = true;
		}
		System.out.println("FileName: " + FileName);
		System.out.println("booleanRetorno: " + booleanRetorno);
        return booleanRetorno;
    }
	
	public static boolean comprobarDirectorioDeCuentaContieneArchivos(String strCuenta) {
    	boolean booleanRetorno = false;
    	String strRuta = strRutaDestinoArchivos + "Historial_Omnex" +"/Cuenta" + strCuenta;
    	
        arrayStrRutasFicherosGenerados = consultarAllFilesRuta(strRuta);
		System.out.println("***SIZE archivos directorio cuenta: " + arrayStrRutasFicherosGenerados.size());
        
        if(arrayStrRutasFicherosGenerados.size()>0){
			booleanRetorno = true;
        }
        return booleanRetorno;
    }
	
	public static boolean comprobarRutaArchivoExiste(String strRuta) {
		boolean booleanRetorno = false;
		String FileName = strRuta;

		if (new File(FileName).exists()==true){
			booleanRetorno = true;
		}
		return booleanRetorno;
	}
	
	public static ArrayList<ArrayList<String>> generarArrayDesdeArchivoHistorialDeMovimientos(String strArchivo) throws IOException{
		
		ArrayList<ArrayList<String>> arrayListBidimenMovimientos = new ArrayList<ArrayList<String>>();
		ArrayList<String> arrayListMovimiento = new ArrayList<String>();
		
		arrayListMovimiento.add(0, "");
		arrayListMovimiento.add(1, "");
		arrayListMovimiento.add(2, "");
		arrayListMovimiento.add(3, "");

		String strIdAgente = "";
		String strDatos = "";
		String strImporte = "";
		String strSaldo = "";

		int intPosicionInicioIdAgente = 0;
		int intPosicionFinalIdAgente =  0;
		int intPosicionInicioDatos = 0;
		int intPosicionFinalDatos =  0;
		int intPosicionInicioImporte = 0;
		int intPosicionFinalImporte =  0;
		int intPosicionInicioSaldo = 0;
		int intPosicionFinalSaldo =  0;
		
        BufferedReader br = null;

        String line = "";
		System.out.println("11properties-***1 --dato-strRutaProperties (" + strArchivo + ")");
        try {
            br = new BufferedReader(new FileReader(strArchivo));
            int intContMovimientos = 0;
            int contArrayBancoCuentaDesc = 0;
            
            while((line = br.readLine()) != null){           	

        		intPosicionInicioIdAgente = line.indexOf("celda3(") + 7;
        		intPosicionFinalIdAgente =  line.indexOf(")_celda4") - 1;
        		intPosicionInicioDatos = line.indexOf("celda4(") + 7;
        		intPosicionFinalDatos =  line.indexOf(")_celda5") - 1;
        		intPosicionInicioImporte = line.indexOf("celda6(") + 7;
        		intPosicionFinalImporte =  line.indexOf(")_celda7") - 1;
        		intPosicionInicioSaldo = line.indexOf("saldo:") + 6;
        		intPosicionFinalSaldo =  line.length() - 1;
/*        		
        		System.out.println("0906 posiciones:  " + intPosicionInicioIdAgente);
        		System.out.println("0906 posiciones:  " + intPosicionFinalIdAgente);
        		System.out.println("0906 posiciones:  " + intPosicionInicioDatos);
        		System.out.println("0906 posiciones:  " + intPosicionFinalDatos);
        		System.out.println("0906 posiciones:  " + intPosicionInicioImporte);
        		System.out.println("0906 posiciones:  " + intPosicionFinalImporte);
*/            	
        		strIdAgente = line.substring(intPosicionInicioIdAgente, intPosicionFinalIdAgente + 1);
        		strDatos = line.substring(intPosicionInicioDatos, intPosicionFinalDatos + 1);
        		strImporte = line.substring(intPosicionInicioImporte, intPosicionFinalImporte + 1);
        		strSaldo = line.substring(intPosicionInicioSaldo, intPosicionFinalSaldo + 1);
        		
            	arrayListMovimiento.set(0, strIdAgente);
            	arrayListMovimiento.set(1, strDatos);
            	arrayListMovimiento.set(2, strImporte);
            	arrayListMovimiento.set(3, strSaldo);
	    		arrayListBidimenMovimientos.add(new ArrayList<>(arrayListMovimiento));
            }
        } catch (StringIndexOutOfBoundsException e) {
    		System.out.println("StringIndexOutOfBoundsException *** catch.");
    		Ventana.setStrSWMensajesErrorVentana("ERROR ***** : Archivo historial: formato incorrecto.");            	
        } catch (FileNotFoundException e) {
    		System.out.println("properties-*** catch.");
    		Ventana.setStrSWMensajesErrorVentana("ERROR ***** : No se ha podido encontrar el archivo historial.");
        } catch (IOException e) {
    		Ventana.setStrSWMensajesErrorVentana("ERROR ***** : Archivo historial.   Se ha producico un error IO.");
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                	br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        //*************************** mostrar *****************************
/*
        for(int contEle = 0;contEle<arrayListBidimenMovimientos.size();contEle++){
        	System.out.println("0927 arrayListBidimenMovimientos: " + arrayListBidimenMovimientos.get(contEle));        	
        }
*/        
        //*************************** mostrado ****************************
       return arrayListBidimenMovimientos;
	}

	private static boolean compararMovHistorialConMovExcel(ArrayList<String> arrayMovDeHistorial , ArrayList<String> arraySobrante) throws IOException{

//		try {ficheroLog.InfoLog("----------------------------------------------------", "___compararMovHistorialConMovExcel_____________________");} catch (IOException e) {e.printStackTrace();}
//		try {ficheroLog.InfoLog("arrayMovDeHistorial----> " + arrayMovDeHistorial.toString(), "___compararMovHistorialConMovExcel_____________________");} catch (IOException e) {e.printStackTrace();}
//		try {ficheroLog.InfoLog("arraySobrante----------> " + arraySobrante.toString(), "___compararMovHistorialConMovExcel_____________________");} catch (IOException e) {e.printStackTrace();}
		
		String strPosicionSaldoCuenta = "todas";
		
		System.out.println("antes   0920 arrayMovDeHistorial: (" + arrayMovDeHistorial + ")");
    	System.out.println("antes   0920 arraySobrante: (" + arraySobrante + ")");
    	//Punto 1.-
		//1.- Se da el caso de que se reciba desde arrayMovDeHistorial	un importe como:	-307.375,00
		//1.-					 y se reciba desde arraySobrante		un importe como:	-307375.00
		//1.-En la pr�ctica es el mismo valor, pero primero deberemos modificar el importe que recibimos de Historial.
    	//1-   As� que:
    	//1-	para poder quitar el punto:
    	//1-		Si tiene punto:
    	//1-			-si no tiene coma:
    	//1-				-si despues del punto no tiene mas de 2 caracteres.
    	//1-					No se quita el punto.
    	//1-				-si despues del punto tiene mas de 2 caracteres.
    	//1-					Se quita el punto.
    	//1-			-si tiene coma:
    	//1-				se quita el punto directamente. 

    						
    	//Punto 2.-
		//2.- Y posteriormente,   si tiene separador decimal coma (,) lo convertimos a separador decimal punto (.)

    	//Punto 3.-
		//3.- Si historial tiene dos decimales, y el seguno es cero, entonces dejamos s�lo un decimal
    	//				para que coincida con los decimales del movimiento sobrante con el que compara.

    	//Punto 1.- *************************************************************************************************************
    	String[] elemsMovHistorial = arrayMovDeHistorial.get(2).toString().split("\\.");
    	String strNuevoValorImporteHistorial = "";

    	System.out.println("1 -----------Nuevo punto 1");
    
		//--- Tiene punto
    	if (elemsMovHistorial.length>1){
        	System.out.println("1 -----------Nuevo punto 1 - Tiene punto");
    		//--- Tiene punto----------------------------------------------------------------    		
        	String[] elemsMovHistorialComa = elemsMovHistorial[1].toString().split(",");    		
    		if (elemsMovHistorialComa.length<2){
            	System.out.println("1 -----------Nuevo punto 1 - no tiene coma");    			
        		//--- NO Tiene coma----------------------------------------------------------------    		
    	    	if (elemsMovHistorial[1].length() < 3){
                	System.out.println("1 -----------Nuevo punto 1 - Despues del punto NO tiene m�s de 2 caracteres");
    	    		//Despues del punto NO tiene m�s de 2 caracteres.
    	    		strNuevoValorImporteHistorial = arrayMovDeHistorial.get(2).toString();
    	    	}else{
                	System.out.println("1 -----------Nuevo punto 1 - Despues del punto NO tiene m�s de 2 caracteres");    	    		
    	    		strNuevoValorImporteHistorial = elemsMovHistorial[0] + elemsMovHistorial[1];
    	    	}    			
    		}else{
            	System.out.println("1 -----------Nuevo punto 1 - Tiene coma");    			    	    		    			
        		//--- Tiene coma----------------------------------------------------------------    		
	    		strNuevoValorImporteHistorial = elemsMovHistorial[0] + elemsMovHistorial[1];
    		}
    	}else{
        	System.out.println("1 -----------Nuevo punto 1 - No Tiene punto");    			    	    		    			    		
    		//---No tiene punto
    		strNuevoValorImporteHistorial = arrayMovDeHistorial.get(2).toString();
    	}
		arrayMovDeHistorial.set(2, strNuevoValorImporteHistorial);
    	System.out.println("1 -----------Nuevo punto 1 - VALOR arrayMovDeHistorial.get(2): " + arrayMovDeHistorial.get(2).toString());    			    	    		    			    		
		
/*    	
    	//Punto 1.- *************************************************************************************************************
		System.out.println("1 ----------------------------------arrayMovDeHistorial.get(2).toString(): " + arrayMovDeHistorial.get(2).toString());

    	for(int intEleCont = 0;intEleCont<elemsMovHistorial.length ; intEleCont++){
        	//** Los decimales ser�n menores de 3. Solo se procesa en ese caso.
    		if (elemsMovHistorial.length<3){
        		System.out.println("1 despues 0920 decimales < 3");
        		strNuevoValorImporteHistorial = strNuevoValorImporteHistorial + elemsMovHistorial[intEleCont].toString();      	    			
    		}else{
    			//---En este caso no hacemos nada y dejamos el valor que tenia importe.
        		strNuevoValorImporteHistorial = arrayMovDeHistorial.get(2).toString();      	    			
        		System.out.println("1 despues 0920 decimales > 2");
    		}
    	}
    	arrayMovDeHistorial.set(2, strNuevoValorImporteHistorial);

    	System.out.println("1 despues 0920 arrayMovDeHistorial: (" + arrayMovDeHistorial + ")");
    	System.out.println("1 despues 0920 arraySobrante: (" + arraySobrante + ")");
    	System.out.println("1    despues 0920 - arrayMovDeHistorial.get(2): " + arrayMovDeHistorial.get(2));    	
    	//**************************************************************************************************
*/
    	
    	
    	//Punto 2.- ************* Convertir separador decimal coma ---> por punto **************************
    	String[] elemsMovHistorialConvertir = arrayMovDeHistorial.get(2).toString().split(",");
    	String strNuevoValorImporteHistorialConvertir = "";
    	
    	if (elemsMovHistorialConvertir.length>1){
    		strNuevoValorImporteHistorialConvertir = elemsMovHistorialConvertir[0] + "." + elemsMovHistorialConvertir[1];
    	}else{
    		strNuevoValorImporteHistorialConvertir = arrayMovDeHistorial.get(2).toString();    		
    	}
    	arrayMovDeHistorial.set(2, strNuevoValorImporteHistorialConvertir);
    	System.out.println("2 despues 0920 ************************************************************");		
    	System.out.println("2 despues 0920 arrayMovDeHistorial: (" + arrayMovDeHistorial + ")");
    	System.out.println("2 despues 0920 arraySobrante: (" + arraySobrante + ")");		
    	System.out.println("2 despues 0920 ************************************************************");
    	System.out.println("2    despues 0920 - arrayMovDeHistorial.get(2): " + arrayMovDeHistorial.get(2));    	
    	//**************************************************************************************************
    	//Punto 3.- *************************************************************************************************************
    	String[] elemsMovHistorialDecimales = arrayMovDeHistorial.get(2).toString().split("\\.");
    	String strNuevoValorImpHistorial = "";		
    	
		System.out.println("3<<<<<           (" + arrayMovDeHistorial.get(2).toString() +")");
		
		//---Si  tiene parte decimal separada por punto.
    	if (elemsMovHistorialConvertir.length>1){
        	System.out.println("3<<<<<despues 0920 Segundo decimal *** OK, tiene decimales.");		
    		//---si no tiene dos decimales.
    		if (elemsMovHistorialConvertir[1].length()<2){
    	    	System.out.println("3<<<<<despues 0920 Segundo decimal *** no tiene 2 decimales");	    			
				strNuevoValorImporteHistorialConvertir = arrayMovDeHistorial.get(2).toString();
    		}else{
    			// si tiene mas de un decimal. Preguntamos por el ultimo, si es cero lo quitamos.
    			System.out.println("3<<<<<despues 0920 Segundo decimal *** Tiene mas de un decimal");
    			int intLengthImporte = arrayMovDeHistorial.get(2).length();
    			System.out.println("3<<<<<despues primer CARACTER: " + arrayMovDeHistorial.get(2).toString().substring(0,1));
    			System.out.println("3<<<<<despues cuarto CARACTER: " + arrayMovDeHistorial.get(2).toString().substring(3,4));
    			System.out.println("3<<<<<despues ULTIMO CARACTER: " + arrayMovDeHistorial.get(2).toString().substring(intLengthImporte-1,intLengthImporte));

    			if (arrayMovDeHistorial.get(2).toString().substring(intLengthImporte-1,intLengthImporte).equals("0")){
    				//Al ser un cero el ultimo caracter,   quitamos ese caracter final.
    				String strImporteQuitandoUltimoCero = arrayMovDeHistorial.get(2).substring(0, intLengthImporte-1).toString();
    				strNuevoValorImporteHistorialConvertir = strImporteQuitandoUltimoCero;
        	    	System.out.println("3<<<<<despues 0920 - strImporteQuitandoUltimoCero: " + strImporteQuitandoUltimoCero);	    			    				
    			}else{
    				strNuevoValorImporteHistorialConvertir = arrayMovDeHistorial.get(2).toString();
    			}    		
    		}
    	}else{
	    	System.out.println("3<<<<<despues 0920 Segundo decimal *** no tiene parte decimal");
    		strNuevoValorImporteHistorialConvertir = arrayMovDeHistorial.get(2).toString();
    	}
    	arrayMovDeHistorial.set(2, strNuevoValorImporteHistorialConvertir);
    	System.out.println("3<<<<<despues 0920 - arrayMovDeHistorial.get(2): " + arrayMovDeHistorial.get(2));	    			    				
    	//**************************************************************************************************
    	//**************************************************************************************************
		boolean booleanRetorno = false;
		
		//----- Pueden tener valores iguales pero diferenciandose los importes con punto decimal.
		//  Comprobar si hay punto decimal en el importe y sin decimales. Por si fueran valores iguales y alguno sin punto decimal.
		
		String stringImporteSobrante = "";
		String stringImporteHistorial = "";
		String stringDatosSobrante = "";
		String stringDatosHistorial = "";
		String stringSaldoSobrante = "";
		String stringSaldoHistorial = "";
    	
		String[] elemsImporteSobrante = arraySobrante.get(1).toString().split("\\.");
		if (elemsImporteSobrante.length>1){
			if(elemsImporteSobrante[1].toString().equals("0")){		stringImporteSobrante = elemsImporteSobrante[0].toString();
			}else{													stringImporteSobrante = arraySobrante.get(1).toString();				
			}
		}else{
			stringImporteSobrante = arraySobrante.get(1).toString();
		}
		
		String[] elemsDatosSobrante = arraySobrante.get(0).toString().split("\\.");
		if (elemsDatosSobrante.length>1){
			if(elemsDatosSobrante[1].toString().equals("0")){		stringDatosSobrante = elemsDatosSobrante[0].toString();
			}else{													stringDatosSobrante = arraySobrante.get(0).toString();				
			}
		}else{
			stringDatosSobrante = arraySobrante.get(0).toString();	
		}
		
		String[] elemsImporteHistorial = arrayMovDeHistorial.get(2).toString().split("\\.");
		if (elemsImporteHistorial.length>1){
//	    	System.out.println("0906 final aaa 1 DENTRO importe Historial" + stringImporteHistorial);			
			if(elemsImporteHistorial[1].toString().equals("0")){	stringImporteHistorial = elemsImporteHistorial[0].toString();
			}else{													stringImporteHistorial = arrayMovDeHistorial.get(2).toString();
			}
		}else{
			stringImporteHistorial = arrayMovDeHistorial.get(2).toString();
		}    	
		
		String[] elemsDatosHistorial = arrayMovDeHistorial.get(0).toString().split("\\.");
		if (elemsDatosHistorial.length>1){
			if(elemsDatosHistorial[1].toString().equals("0")){		stringDatosHistorial = elemsDatosHistorial[0].toString();
			}else{													stringDatosHistorial = arrayMovDeHistorial.get(1).toString();				
			}
		}else{
			stringDatosHistorial = arrayMovDeHistorial.get(1).toString();			
		}
		//------------------------------------------------------------------------------------------------------------------------------
		
//		try {ficheroLog.InfoLog("111 stringDatosHistorial.toString().equals(stringDatosSobrante.toString()" + stringDatosHistorial.toString() + "<--->" + stringDatosSobrante.toString(), "___Compara_____________________");} catch (IOException e) {e.printStackTrace();}
//		try {ficheroLog.InfoLog("&& 222 stringImporteHistorial.toString().equals(stringImporteSobrante.toString()" + stringImporteHistorial.toString() + "<--->" + stringImporteSobrante.toString(), "___Compara_____________________");} catch (IOException e) {e.printStackTrace();}
//		try {ficheroLog.InfoLog("---- stringDatosHistorial: " + stringDatosHistorial, "___compararMovHistorialConMovExcel_____________________");} catch (IOException e) {e.printStackTrace();}
//		try {ficheroLog.InfoLog("---- stringDatosSobrante: " + stringDatosSobrante, "___compararMovHistorialConMovExcel_____________________");} catch (IOException e) {e.printStackTrace();}
//		try {ficheroLog.InfoLog("---- stringImporteHistorial: " + stringImporteHistorial, "___compararMovHistorialConMovExcel_____________________");} catch (IOException e) {e.printStackTrace();}
//		try {ficheroLog.InfoLog("---- stringImporteSobrante: " + stringImporteSobrante, "___compararMovHistorialConMovExcel_____________________");} catch (IOException e) {e.printStackTrace();}

		if (stringDatosHistorial.toString().equals(stringDatosSobrante.toString())
		 && stringImporteHistorial.toString().equals(stringImporteSobrante.toString())
				){
//			try {ficheroLog.InfoLog("333 booleanRetorno = true", "___Compara_____________________");} catch (IOException e) {e.printStackTrace();}
			booleanRetorno = true;
		}
    	System.out.println("0906 DENTRO Historial Datos/Importe/Saldo: (" + arrayMovDeHistorial.get(1).toString() + ")(" + arrayMovDeHistorial.get(2).toString() + ")(" + arrayMovDeHistorial.get(3).toString() + ")");
    	System.out.println("0906 DENTRO Sobrantes Datos/Importe:       (" + arraySobrante.get(0).toString() + ")(" + arraySobrante.get(1).toString() + ")(" + arraySobrante.get(2).toString() + ")");
    	
    	//*********************************************************************************************************************************caixa******lacaixa bbva posta
    	// En cuentas como LaCaixa,  que obtenemos el saldo que tiene cada movimiento:
    	//		comprobar si el saldo que aparece en el corte en cada movimiento es el saldo que se grab� en el historial.
    	// Aqu� fuerzo a que lo compruebe siempre pero habr� que condicionarlo a como indique saldos_properties (que tenga saldo cada linea).
    	//***************************************************************************************************************************************
    	stringSaldoHistorial = arrayMovDeHistorial.get(3).toString();
    	stringSaldoSobrante = arraySobrante.get(2).toString();

    	
    	
//		try {ficheroLog.InfoLog("555 booleanRetorno && strPosicionSaldoCuenta.equals(todas) -------->("+booleanRetorno+")("+strPosicionSaldoCuenta+")", "___Compara_____________________");} catch (IOException e) {e.printStackTrace();}

    	
		//<<<<<<<<<<	Si hubiera importes de ajustes,   habr�a que aplicarlos.
		Double doubleSaldoCalculadoConImporteDeAjustes = 0.0;
		String strSaldoCalculadoConImporteDeAjustes = "";
		try {
			doubleSaldoCalculadoConImporteDeAjustes = Double.valueOf(stringSaldoSobrante) - Ventana.getDoubleTotalSumatorioAjustes();
			strSaldoCalculadoConImporteDeAjustes = String.valueOf(doubleSaldoCalculadoConImporteDeAjustes);
			
//			try {ficheroLog.InfoLog("666 111 Double.valueOf(stringSaldoSobrante): " + Double.valueOf(stringSaldoSobrante), "___Compara_____________________");} catch (IOException e) {e.printStackTrace();}
//			try {ficheroLog.InfoLog("666 111 Ventana.getDoubleTotalSumatorioAjustes(): " + Ventana.getDoubleTotalSumatorioAjustes(), "___Compara_____________________");} catch (IOException e) {e.printStackTrace();}
//			try {ficheroLog.InfoLog("666 111 strSaldoCalculadoConImporteDeAjustes: " + strSaldoCalculadoConImporteDeAjustes, "___Compara_____________________");} catch (IOException e) {e.printStackTrace();}
		} catch (NumberFormatException e1) {
			strSaldoCalculadoConImporteDeAjustes = stringSaldoSobrante;
//			try {ficheroLog.InfoLog("666 222 strSaldoCalculadoConImporteDeAjustes: " + strSaldoCalculadoConImporteDeAjustes, "___Compara_____________________");} catch (IOException e) {e.printStackTrace();}

			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//<<<<<<<<<<--------------------------------------------------------
//		try {ficheroLog.InfoLog("222222---- booleanRetorno: " + booleanRetorno, "___compararMovHistorialConMovExcel_____________________");} catch (IOException e) {e.printStackTrace();}
//		try {ficheroLog.InfoLog("222222---- strPosicionSaldoCuenta: " + strPosicionSaldoCuenta, "___compararMovHistorialConMovExcel_____________________");} catch (IOException e) {e.printStackTrace();}

		//Si booleanRetorno es true,   porque Datos e Importe son iguales,   entonces consultamos el saldo (si la cuenta tuviera saldo en cada linea).	
		if (booleanRetorno && strPosicionSaldoCuenta.equals("todas")){
//    		try {ficheroLog.InfoLog("666 ok", "___Compara_____________________");} catch (IOException e) {e.printStackTrace();}

    		System.out.println("0927 stringSaldoHistorial: " + stringSaldoHistorial);
	    	System.out.println("0927 stringSaldoSobrante:  " + strSaldoCalculadoConImporteDeAjustes);
/*
	    	try {ficheroLog.InfoLog("24 stringSaldoHistorial: " + stringSaldoHistorial, "24_compara_hist_sobrantes");} catch (IOException e) {e.printStackTrace();}
	    	try {ficheroLog.InfoLog("24 strSaldoCalculadoConImporteDeAjustes: " + strSaldoCalculadoConImporteDeAjustes, "24_compara_hist_sobrantes");} catch (IOException e) {e.printStackTrace();}
*/
//    		try {ficheroLog.InfoLog("777 stringSaldoHistorial.toString(" + stringSaldoHistorial.toString() + ")stringSaldoSobrante.toString(" + stringSaldoSobrante.toString() + ")", "___Compara_____________________");} catch (IOException e) {e.printStackTrace();}

	    	if (!(stringSaldoHistorial.toString().equals(strSaldoCalculadoConImporteDeAjustes.toString()))){
//	    		try {ficheroLog.InfoLog("888", "___Compara_____________________");} catch (IOException e) {e.printStackTrace();}

	    		booleanRetorno = false;
			}
    	}
    	//***************************************************************************************************************************************
//		try {ficheroLog.InfoLog("-----booleanRetorno: " + booleanRetorno, "___compararMovHistorialConMovExcel_____________________");} catch (IOException e) {e.printStackTrace();}

		return booleanRetorno;
	}

	private static void verValoresVariablesInicioEjecucion(String strOrigen) throws IOException{
		//cotrebla20171005
		System.out.println("** (Desde: " + strOrigen + ") *****	verValoresVariablesInicioEjecucion  *************************************************");
		System.out.println("********************************************************");
		System.out.println("Valor de 	btnSeleccionarDetalleCC	: " + 	btnSeleccionarDetalleCC.getText());
		System.out.println("********************************************************");
		System.out.println("Valor de 	btnInicio	: " + 	btnInicio.getText());
		System.out.println("********************************************************");
				System.out.println("Valor de 	btnCrearExcelOmnex	: " + 	btnCrearExcelOmnex.getText());
				System.out.println("********************************************************");
				System.out.println("Valor de 	btnComprobaciones	: " + 	btnComprobaciones.getText());
				System.out.println("********************************************************");
				System.out.println("Valor de 	btnAceptarCorteOmnex	: " + 	btnAceptarCorteOmnex.getText());
				System.out.println("********************************************************");
				System.out.println("Valor de 	btnAbrirExcelGenerado	: " + 	btnAbrirExcelGenerado.getText());
				System.out.println("********************************************************");
				System.out.println("Valor de 	btnAjustarSaldoCuenta     	: " + 	btnAjustarSaldoCuenta.getText());
				System.out.println("********************************************************");
				System.out.println("Valor de 	txtAreaRutasArchivos	: " + 	txtAreaRutasArchivos.getText());
				System.out.println("********************************************************");
				System.out.println("Valor de 	txtAreaResultadoComprobaciones	: " + 	txtAreaResultadoComprobaciones.getText());
				System.out.println("********************************************************");
				System.out.println("Valor de 	txtAreaResultadoComprobacionesAjustes	: " + 	txtAreaResultadoComprobacionesAjustes.getText());
				System.out.println("********************************************************");
				System.out.println("Valor de 	txtAreaAjustesAlmacenados	: " + 	txtAreaAjustesAlmacenados.getText());
				System.out.println("********************************************************");
				System.out.println("Valor de 	txtFieldSaldo	: " + 	txtFieldSaldo.getText());
				System.out.println("********************************************************");
				System.out.println("Valor de 	cbOpcionesLayout	: " + 	cbOpcionesLayout.getSize().toString());
				System.out.println("********************************************************");
				System.out.println("Valor de 	txtFieldFormAjustarSaldoCuentaAgente	: " + 	txtFieldFormAjustarSaldoCuentaAgente.getText());
				System.out.println("********************************************************");
				System.out.println("Valor de 	txtFieldFormAjustarSaldoCuentaImporte	: " + 	txtFieldFormAjustarSaldoCuentaImporte.getText());
				System.out.println("********************************************************");
				System.out.println("Valor de 	txtFieldFormAjustarSaldoCuentaDatos	: " + 	txtFieldFormAjustarSaldoCuentaDatos.getText());
				System.out.println("********************************************************");
				System.out.println("Valor de 	btnComprobarAjuste	: " + 	btnComprobarAjuste.getText());
				System.out.println("********************************************************");
				System.out.println("Valor de 	btnCrearExcelOmnexConAjustes  	: " + 	btnCrearExcelOmnexConAjustes.getText());
				System.out.println("********************************************************");
				System.out.println("Valor de 	jLabelMensaje	: " + 	jLabelMensaje.getText());
				System.out.println("********************************************************");
				System.out.println("Valor de 	jLabelFieldSaldo	: " + 	jLabelFieldSaldo.getText());
				System.out.println("********************************************************");
				System.out.println("Valor de 	jLabelCabecera	: " + 	jLabelCabecera.getText());
				System.out.println("********************************************************");
				System.out.println("Valor de 	jLabelResultadoOmnex	: " + 	jLabelResultadoOmnex.getText());
				System.out.println("********************************************************");
				System.out.println("Valor de 	jLabelPedirFormatoLayout	: " + 	jLabelPedirFormatoLayout.getText());
				System.out.println("********************************************************");
				System.out.println("Valor de 	jLabelCabecera0AjustarSaldo	: " + 	jLabelCabecera0AjustarSaldo.getText());
				System.out.println("********************************************************");
				System.out.println("Valor de 	jLabelCabecera1AjustarSaldo	: " + 	jLabelCabecera1AjustarSaldo.getText());
				System.out.println("********************************************************");
				System.out.println("Valor de 	jLabelCabecera2AjustarSaldo	: " + 	jLabelCabecera2AjustarSaldo.getText());
				System.out.println("********************************************************");
				System.out.println("Valor de 	jLabelAjustesMsjError	: " + 	jLabelAjustesMsjError.getText());
				System.out.println("********************************************************");
				System.out.println("Valor de 	jLabelSaldoDelCorte	: " + 	jLabelSaldoDelCorte.getText());
				System.out.println("********************************************************");
				System.out.println("Valor de 	jLabelPedirDocumentoDetalleCC	: " + 	jLabelPedirDocumentoDetalleCC.getText());
				System.out.println("********************************************************");
				System.out.println("Valor de 	jLabelFormAjustarAgente	: " + 	jLabelFormAjustarAgente.getText());
				System.out.println("********************************************************");
				System.out.println("Valor de 	jLabelFormAjustarImporte	: " + 	jLabelFormAjustarImporte.getText());
				System.out.println("********************************************************");
				System.out.println("Valor de 	jLabelFormAjustarDatos	: " + 	jLabelFormAjustarDatos.getText());
				System.out.println("********************************************************");
				System.out.println("Valor de 	jLabelFormAjustarCuentaCabeceraValor    	: " + 	jLabelFormAjustarCuentaCabeceraValor.getText());
				System.out.println("********************************************************");
				System.out.println("Valor de 	txtRutaDetalleCC	: " + 	txtRutaDetalleCC.getText());
				System.out.println("********************************************************");
				System.out.println("Valor de 	arrayTest    	: " + 	arrayTest.size());
				System.out.println("********************************************************");
				System.out.println("Valor de 	strCarpetaArchivosLayout	: " + 	strCarpetaArchivosLayout	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	strCarpetaArchivosLog	: " + 	strCarpetaArchivosLog	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	strCarpetaHistorialOmnex	: " + 	strCarpetaHistorialOmnex	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	strOpcionBancoNoSelected	: " + 	strOpcionBancoNoSelected	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	strOpcionBancoSelected	: " + 	strOpcionBancoSelected	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	strOpcionCuentaSelected	: " + 	strOpcionCuentaSelected	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	strOpcionDescripcionSelected	: " + 	strOpcionDescripcionSelected	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	strOpcionDatosIdAgenteSelected	: " + 	strOpcionDatosIdAgenteSelected	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	strOpcionImporteSelected         	: " + 	strOpcionImporteSelected         	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	strOpcionIdentificadorSelected	: " + 	strOpcionIdentificadorSelected	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	strOpcionFilaPrimerMovimientoSelected	: " + 	strOpcionFilaPrimerMovimientoSelected	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	strOpcionPosicionSaldoSelected	: " + 	strOpcionPosicionSaldoSelected	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	strOpcionDistanciaSaldoAdatosSelected	: " + 	strOpcionDistanciaSaldoAdatosSelected	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	strOpcionOrdenSelected	: " + 	strOpcionOrdenSelected	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	strOpcionPosicionMovimientoSelected	: " + 	strOpcionPosicionMovimientoSelected	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	strOpcionPosicionFechaSelected	: " + 	strOpcionPosicionFechaSelected	);
				System.out.println("********************************************************");				
				System.out.println("Valor de 	strRutaDestinoArchivos	: " + 	strRutaDestinoArchivos	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	strRutaArchivoXlsBanco	: " + 	strRutaArchivoXlsBanco	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	strDateFileName	: " + 	strDateFileName	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	strCuentaBanco	: " + 	strCuentaBanco	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	strRutaJAR	: " + 	strRutaJAR	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	intRowPosicionInicialObservaciones	: " + 	intRowPosicionInicialObservaciones	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	intCellPosicionInicialObservaciones	: " + 	intCellPosicionInicialObservaciones	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	intRowPosicionInicialImporte	: " + 	intRowPosicionInicialImporte	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	intCellPosicionInicialImporte	: " + 	intCellPosicionInicialImporte	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	intRowPosicionInicialSaldo	: " + 	intRowPosicionInicialSaldo	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	intCellPosicionInicialSaldo	: " + 	intCellPosicionInicialSaldo	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	datosSaldosProperties	: " + 	"*****"	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	arrayListBidimSaldosProperties	: " + 	arrayListBidimSaldosProperties.size());
				System.out.println("********************************************************");
				System.out.println("Valor de 	arrayListAjustesAlmacenados	: " + 	arrayListAjustesAlmacenados.size());
				System.out.println("********************************************************");
				System.out.println("Valor de 	doubleTotalSumatorioAjustes	: " + 	doubleTotalSumatorioAjustes	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	arrayCortePreparadoParaHistorialOmnex	: " + 	arrayCortePreparadoParaHistorialOmnex.size());
				System.out.println("********************************************************");
				System.out.println("Valor de 	ficheroLog	: " + 	"*****"	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	arrayMovimientoSaldo    	: " + 	arrayMovimientoSaldo.size());
				System.out.println("********************************************************");
				System.out.println("Valor de 	strNuevoSaldoCuenta	: " + 	strNuevoSaldoCuenta	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	intIndiceCB	: " + 	intIndiceCB	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	swDoubleSumatorioImportes	: " + 	swDoubleSumatorioImportes	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	strTextoResultadoComprobaciones	: " + 	strTextoResultadoComprobaciones	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	booleanComprobacionCorrecta	: " + 	booleanComprobacionCorrecta	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	strNombreDelArchivoXLS	: " + 	strNombreDelArchivoXLS	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	booleanBtnComprobacionesEnabled	: " + 	booleanBtnComprobacionesEnabled	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	booleanBtnCrearExcelOmnexEnabled	: " + 	booleanBtnCrearExcelOmnexEnabled	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	strFileExcelAgentes	: " + 	strFileExcelAgentes	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	strSWMensajesErrorVentana	: " + 	strSWMensajesErrorVentana	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	strPruebaCodigo	: " + 	strPruebaCodigo	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	arrayListSWAgentesValidos	: " + 	arrayListSWAgentesValidos.size());
				System.out.println("********************************************************");
				System.out.println("Valor de 	arrayListSWBidimSaldosPropertiesCuentas	: " + 	arrayListSWBidimSaldosPropertiesCuentas.size());
				System.out.println("********************************************************");
				System.out.println("Valor de 	strSWrutaDestinoFicherosGenerados	: " + 	strSWrutaDestinoFicherosGenerados	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	strSWrutaAgentesActivos	: " + 	strSWrutaAgentesActivos	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	strTipoConfigInicioErrorSaldosPropertiesFormato	: " + 	strTipoConfigInicioErrorSaldosPropertiesFormato	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	strTipoConfigComprobacionesCorrectas	: " + 	strTipoConfigComprobacionesCorrectas	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	strTipoConfigCrearExcelCorrecto	: " + 	strTipoConfigCrearExcelCorrecto	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	strTipoConfigBotonInicio	: " + 	strTipoConfigBotonInicio	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	strSWletras	: " + 	strSWletras	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	strMsgTxtFieldSaldoSinValor	: " + 	strMsgTxtFieldSaldoSinValor	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	arrayListSWBidimExcepcionesAgentesActivados	: " + 	arrayListSWBidimExcepcionesAgentesActivados.size());
				System.out.println("********************************************************");
				System.out.println("Valor de 	swArrayBiDimensDelExcel	: " + 	swArrayBiDimensDelExcel.size());
				System.out.println("********************************************************");
				System.out.println("Valor de 	swArrayDatosParaOmnex	: " + 	swArrayDatosParaOmnex.size());
				System.out.println("********************************************************");
				System.out.println("Valor de 	intSwMovimientos	: " + 	intSwMovimientos	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	swDoubleSaldoHistorico	: " + 	swDoubleSaldoHistorico	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	intSwMovimientosSobran	: " + 	intSwMovimientosSobran	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	intSwMovimientosNoSobran	: " + 	intSwMovimientosNoSobran	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	strTextoAreaRutasArchivos	: " + 	strTextoAreaRutasArchivos	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	arrayStrRutasFicherosGenerados	: " + 	arrayStrRutasFicherosGenerados.size());
				System.out.println("********************************************************");
				System.out.println("Valor de 	arrayListSWMovimientosSobrantes	: " + 	arrayListSWMovimientosSobrantes.size());
				System.out.println("********************************************************");
				System.out.println("Valor de 	strRutaFileNameExcelGenerado	: " + 	strRutaFileNameExcelGenerado	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	strDate	: " + 	strDate	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	cal	: " + 	cal.LONG	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	strRespuestaComprobacionFormatoSaldosProperties	: " + 	strRespuestaComprobacionFormatoSaldosProperties	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	sdf	: " + 	"*****"	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	strDateExcelOmnex	: " + 	strDateExcelOmnex	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	strVersion	: " + 	strVersion	);
				System.out.println("********************************************************");
				System.out.println("Valor de 	strAjusteSW	: " + 	strAjusteSW	);
				System.out.println("********************************************************");
				System.out.println("Valores ----------------------------------------------------------------------------------------");
//				cbOpcionesLayout.setSize(500, 500);
//				.getdDsetd	: java.awt.Dimension[width=500,height=26]
	}
	
	private static String[] cargarDatosEncb(ArrayList<ArrayList<String>> arrayListValores) throws IOException{
    	String[] arrayCombo = new String[arrayListValores.size() + 1];
		arrayCombo[0] =	"Seleccione una cuenta:";
		
		String strAbreParenteris = " ";
		String strCierraParenteris = " ";

		for(int contEle=0;contEle<arrayListValores.size();contEle++){
			strAbreParenteris = " ";
			strCierraParenteris = " ";
			if (arrayListValores.get(contEle).get(2).length()>0){
				strAbreParenteris = " (";
				strCierraParenteris = ") ";
			}
    		arrayCombo[contEle + 1] =	arrayListValores.get(contEle).get(0) + strAbreParenteris +
									arrayListValores.get(contEle).get(2) + strCierraParenteris +
    								arrayListValores.get(contEle).get(1);
    	}
		return arrayCombo;
	}
	
//	private static void procesosDeComprobaciones(){
	public static String procesosDeComprobaciones(){

//		try {ficheroLog.InfoLog("CREAR LOG_1", "buscarValor999");} catch (IOException e2) {e2.printStackTrace();} 
		
		arrayListStr10historicos.clear();

    	String strInfoBotonComprobaciones = "";
//    	strInfoBotonComprobaciones = "\n";
//    	strInfoBotonComprobaciones = "\n" + "Comprobaciones: ";
    	LogsFile ficheroLog = new LogsFile();
    	
//    	strTextoResultadoComprobaciones = "\n" + "Resultado de las comprobaciones:" + "\n";
//    	txtAreaResultadoComprobaciones.setText("");
    	inicializarTxtAreaResultadoComprobaciones();

    	System.out.println("DATOS 1 EN txtAreaResultadoComprobaciones : " + txtAreaResultadoComprobaciones.getText());
    	System.out.println("111************************************************************************************");

    	System.out.println("DATOS 2 EN txtAreaResultadoComprobaciones : " + txtAreaResultadoComprobaciones.getText());
    	System.out.println("222************************************************************************************");
    			    	
    	strSWMensajesErrorVentana = "";
    	//*****************************************************************************************************************************
    	//************* Validar seleccion de cuenta/ seleccion xls / valor del saldo (si estuviera visible) ***************************            	
    	if (strSWMensajesErrorVentana.equals("")){
    		strSWMensajesErrorVentana = swValidaSeleccionCuentaArchivoSaldoEnVentana();
    	}
    	
		System.out.println("20171002 i size: " + getArrayListBidimSaldosProperties().size());    	
    	for (int i = 0; i < Ventana.getArrayListBidimSaldosProperties().size(); i++) {
//    		try {
//				ficheroLog.InfoLog(getArrayListBidimSaldosProperties().get(i).toString(), "20171002_properties");
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
    		System.out.println("20171002 i: " + i + "---getArrayListBidimSaldosProperties: " + getArrayListBidimSaldosProperties().get(i).toString());
		}

//		try {ficheroLog.InfoLog("2", "___procesosDeComprobaciones");} catch (IOException e) {e.printStackTrace();}
//
//    	
    	
    	//************ Valida nombre del archivo con el identificador de saldos properties.
    	if (strSWMensajesErrorVentana.equals("")){
    		String strIdentificadorBancoCuenta = Ventana.getArrayListBidimSaldosProperties().get(getIntIndiceCB()-1).get(5).toString();
    		
			if(strNombreDelArchivoXLS.toLowerCase().lastIndexOf(strIdentificadorBancoCuenta.toLowerCase())<0){
        		strSWMensajesErrorVentana = "Error *** El identificador '" + strIdentificadorBancoCuenta + "' no se ha encontrado en el nombre del archivo. Debe corregirse.";
    		}

    	}
    	//************ Generar array desde excel **************************************************************************************

    	if (strSWMensajesErrorVentana.equals("")){

//			strSWMensajesErrorVentana = strSWMensajesErrorVentana + "+2";
			try {
				String strTextoAjustes = "";
				String strTextoImporteAjustes = "";
				int intMovsParaOmnex = 0;
				
				GeneraArrayDesdeXls generaArrayMovimientos = new GeneraArrayDesdeXls();
//				strSWMensajesErrorVentana = strSWMensajesErrorVentana + "1";
				swArrayBiDimensDelExcel = generaArrayMovimientos.generar();
				
//				for (int iSW = 0; iSW < swArrayBiDimensDelExcel.size(); iSW++) {
//					try {ficheroLog.InfoLog("swArrayBiDimensDelExcel: " + swArrayBiDimensDelExcel.get(iSW).toString(), "0003");} catch (IOException e) {e.printStackTrace();}
//				}
				
            	if (strSWMensajesErrorVentana.equals("")){

	        		Ventana.acumularStrTextoResultadoComprobaciones("\n" + " Banco: " + Ventana.getStrOpcionBancoSelected());
	        		Ventana.acumularStrTextoResultadoComprobaciones("\n" + " Cuenta: " + Ventana.getStrOpcionCuentaSelected() + " " + Ventana.getStrOpcionDescripcionSelected());
	        		Ventana.acumularStrTextoResultadoComprobaciones("\n");
	        		int intSWmovimientosDelArchivoInicial = getIntSwMovimientosNoSobran() + getIntSwMovimientosSobran();
	        		Ventana.acumularStrTextoResultadoComprobaciones("\n" + " Info: N�mero de Filas de movimientos en el archivo: " + intSWmovimientosDelArchivoInicial);						
	        		Ventana.acumularStrTextoResultadoComprobaciones("\n");
	        		Ventana.acumularStrTextoResultadoComprobaciones("\n" + " Info: Saldo final obtenido del nuevo archivo: " + getStrNuevoSaldoCuenta().toString());
	        		Ventana.acumularStrTextoResultadoComprobaciones("\n" + " Info: Saldo anterior grabado en el historial: " + redondearDecimales(swDoubleSaldoHistorico, 2));
	        		Ventana.acumularStrTextoResultadoComprobaciones("\n");
	        		Ventana.acumularStrTextoResultadoComprobaciones("\n" + " Info: " + 
	        		"N�mero de movimientos que se van a procesar del archivo Excel: " + Ventana.getIntSwMovimientosNoSobran() + ".");
	        		
	    			Ventana.acumularStrTextoResultadoComprobaciones("\n" + " Info: Valor del sumatorio de los " + Ventana.getIntSwMovimientosNoSobran() + " movimientos: " + redondearDecimales(Ventana.getSwDoubleSumatorioImportes(), 2));
	    			
	    			strTextoAjustes = "";
	    			strTextoImporteAjustes = "";
	    			intMovsParaOmnex = 0;
	    			//cotrebla
	    			if (Ventana.getArrayListAjustesAlmacenados().size()>0){
	    				strTextoAjustes = ", lo importes de los ajustes";
	    				Double doubleTotImportesAjustes = 0.0;
		    			for (int j = 0; j < Ventana.getArrayListAjustesAlmacenados().size(); j++) {
//		    				ficheroLog.InfoLog(Ventana.getArrayListAjustesAlmacenados().get(j).toString(),"______Ahustes");
		    				doubleTotImportesAjustes = doubleTotImportesAjustes + Double.valueOf(Ventana.getArrayListAjustesAlmacenados().get(j).get(1));
		    				doubleTotImportesAjustes = redondearDecimales(doubleTotImportesAjustes, 2);
						}
		    			strTextoImporteAjustes = " + " + doubleTotImportesAjustes.toString();
		    			Ventana.acumularStrTextoResultadoComprobaciones("\n" + " Info: Valor del sumatorio de los " + Ventana.getArrayListAjustesAlmacenados().size() + " ajustes: " + doubleTotImportesAjustes);
		    			
		    			intMovsParaOmnex = Ventana.getIntSwMovimientosNoSobran() + Ventana.getArrayListAjustesAlmacenados().size();
		    			Ventana.acumularStrTextoResultadoComprobaciones("\n" + " Info: " + Ventana.getIntSwMovimientosNoSobran() + " (mov.s del excel) + " + Ventana.getArrayListAjustesAlmacenados().size() + " (mov.s de ajustes) = " + intMovsParaOmnex + " movimientos para Omnex.");
	    			}
	    			
	    			Double swDoubleSaldoCalculado = 0.0;
	    			swDoubleSumatorioImportes = redondearDecimales(swDoubleSumatorioImportes, 2);
	    			swDoubleSaldoCalculado = swDoubleSaldoHistorico + swDoubleSumatorioImportes;
	    			
	    			
	    			
	    			//Adem�s hay que sumar el importe de los ajuste,   si hubiera
	    			swDoubleSaldoCalculado = swDoubleSaldoCalculado + Ventana.getDoubleTotalSumatorioAjustes();
	    			
	    			
	    			
	    			
//	    			try {ficheroLog.InfoLog("41 000 Ventana.getDoubleTotalSumatorioAjustes(" + Ventana.getDoubleTotalSumatorioAjustes() + ")", "___procesosDeComprobaciones");} catch (IOException e) {e.printStackTrace();}
//	    			try {ficheroLog.InfoLog("41 " + strSWMensajesErrorVentana, "___procesosDeComprobaciones");} catch (IOException e) {e.printStackTrace();}
//	    			try {ficheroLog.InfoLog("42 swDoubleSaldoCalculado swDoubleSaldoHistorico swDoubleSumatorioImportes(" + swDoubleSaldoCalculado + ")(" + swDoubleSaldoHistorico + ")(" + swDoubleSumatorioImportes + ")", "___procesosDeComprobaciones");} catch (IOException e) {e.printStackTrace();}

	        		Ventana.acumularStrTextoResultadoComprobaciones("\n" + " Info: C�lculo de la suma del anterior saldo" + strTextoAjustes + " y los movimientos del corte actual: ");
	        		Ventana.acumularStrTextoResultadoComprobaciones("\n" + "    " + swDoubleSaldoHistorico + strTextoImporteAjustes + " + " + swDoubleSumatorioImportes + " = " + redondearDecimales(swDoubleSaldoCalculado, 2));
//		        		Ventana.acumularStrTextoResultadoComprobaciones("\n" + " Info: " + "El archivo Excel que se ha seleccionado tiene: " + Ventana.getIntSwMovimientosSobran() + " movimientos que no se procesar�n");

//	        		try {ficheroLog.InfoLog("43 ", "___procesosDeComprobaciones");} catch (IOException e) {e.printStackTrace();}

	        		System.out.println("Comparacion: a: " + getStrNuevoSaldoCuenta().toString());
	        		swDoubleSaldoCalculado = redondearDecimales(swDoubleSaldoCalculado, 2);
	        		System.out.println("Comparacion: b: " + String.valueOf(swDoubleSaldoCalculado));

//	        		try {ficheroLog.InfoLog("44 ", "___procesosDeComprobaciones");} catch (IOException e) {e.printStackTrace();}
//	        		try {ficheroLog.InfoLog("compara 44 getStrNuevoSaldoCuenta().toString(" + getStrNuevoSaldoCuenta().toString() + ")String.valueOf(swDoubleSaldoCalculado)(" + String.valueOf(swDoubleSaldoCalculado) + ")", "___procesosDeComprobaciones");} catch (IOException e) {e.printStackTrace();}
	        		
	        		if(getStrNuevoSaldoCuenta().toString().equals(String.valueOf(swDoubleSaldoCalculado))){
		        		Ventana.acumularStrTextoResultadoComprobaciones("\n" + " Info: El anterior saldo y el saldo calculado son iguales - CORRECTO -");
						strPruebaCodigo = strPruebaCodigo + "d";
	        		}else{
						strPruebaCodigo = strPruebaCodigo + "e";
	                	Ventana.acumularStrTextoResultadoComprobaciones("\n" + " *** �� AVISO !! : " + "El anterior saldo y el saldo calculado no coinciden.");
						strSWMensajesErrorVentana = strSWMensajesErrorVentana + "f";
	                	strSWMensajesErrorVentana = " *** �� AVISO !! : " + "El anterior saldo y el saldo calculado no coinciden.";
						strPruebaCodigo = strPruebaCodigo + "f";
	        		}
//	        		try {ficheroLog.InfoLog("45 ", "___procesosDeComprobacione s");} catch (IOException e) {e.printStackTrace();}

            	}
        	//	Ventana.inicializarTxtAreaResultadoComprobaciones(Ventana.getStrTextoResultadoComprobaciones());
        		
			} catch (IOException e1) {
//        		try {ficheroLog.InfoLog("45b catch ", "___procesosDeComprobaciones");} catch (IOException e) {e.printStackTrace();}

            	strSWMensajesErrorVentana = "Se ha producido un error 'IO' en la lectura del archivo excel." + getStrSWMensajesErrorVentana();
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
    	}
		//-- mostrar ---------------------------------------------------------------------
    	strPruebaCodigo = strPruebaCodigo + "(a)";
    	strPruebaCodigo = strPruebaCodigo + "(" + swArrayBiDimensDelExcel.size() + ")";		    	

//    	try {ficheroLog.InfoLog("46 ", "___procesosDeComprobaciones");} catch (IOException e) {e.printStackTrace();}

//    	for(int contEle=0;contEle<swArrayBiDimensDelExcel.size();contEle++){
//    		System.out.println("20171002 swArrayBiDimensDelExcel: " + swArrayBiDimensDelExcel.get(contEle).toString());
//    	}
    	//***********************************************************************************************************************
    	//************ Validar si el archivo Excel seleccionado tiene adem�s del 'apunteContinuaci�n'
    	//************ otros 10 apuntes m�s para comprobar en el historial si es contiguo. 
		//				Si no tiene 10 m�s se avisar� por pantalla sin dejar continuar.
		//***********************************************************************************************************************
		System.out.println("123 01: intSwMovimientosSobran: " + intSwMovimientosSobran);
		System.out.println("123 01: intSwMovimientosNoSobran: " + intSwMovimientosNoSobran);
		
    	strPruebaCodigo = strPruebaCodigo + "(c)";
    	strPruebaCodigo = strPruebaCodigo + "(" + strSWMensajesErrorVentana + ")";

		if (strSWMensajesErrorVentana.equals("")){

			if (intSwMovimientosSobran>9){
        		Ventana.acumularStrTextoResultadoComprobaciones("\n" + " " +
        				"Info: El archivo excel tiene " + intSwMovimientosSobran + " movimientos sobrantes.");
			}else{
   				strSWMensajesErrorVentana = "El archivo excel no contiene al menos 10 movimientos m�s desde el movimientoContinuacion.";
        		Ventana.acumularStrTextoResultadoComprobaciones("\n" + " Aviso ***** : " + "El archivo excel no contiene al menos 10 movimientos m�s desde el movimientoContinuacion");
			}
    	}

		//***********************************************************************************************************************
    	//************ Localizar los ultimos 10 movimientos del historial que tiene la cuenta seleccionada.
    	//************  Grabarlos en un array.
		//***********************************************************************************************************************
		if (strSWMensajesErrorVentana.equals("")){
			if (intSwMovimientosSobran>9){
        		Ventana.acumularStrTextoResultadoComprobaciones("\n" + " " +
        				"Info: El archivo excel tiene " + intSwMovimientosSobran + " movimientos sobrantes.");
			}else{
   				strSWMensajesErrorVentana = "El archivo excel no contiene al menos 10 movimientos m�s desde el movimientoContinuacion.";
        		Ventana.acumularStrTextoResultadoComprobaciones("\n" + " Aviso ***** : " + "El archivo excel no contiene al menos 10 movimientos m�s desde el movimientoContinuacion");
			}
		}
		
		//***********************************************************************************************************************
    	//************ Validar si los ultimos 10 movimientos de esa cuenta en el historial
    	//************ corresponden a los   10 movimientos (del archivo excel seleccionado) 
		//************ que se han consultado a continuaci�n del 'movimiento de continuaci�n'.
		//***********************************************************************************************************************
		System.out.println("directorio: " + strCarpetaArchivosLayout);
		System.out.println("directorio: " + strCarpetaArchivosLog);
		System.out.println("directorio: " + strCarpetaHistorialOmnex);
		//---------------------------------------- Comprobar que para esa cuenta existe el directorio del Historial.        		
		if (strSWMensajesErrorVentana.equals("")){
//			try {ficheroLog.InfoLog("5 " + strSWMensajesErrorVentana, "___procesosDeComprobaciones");} catch (IOException e) {e.printStackTrace();}

	    	strPruebaCodigo = strPruebaCodigo + "(d)";
	    	strPruebaCodigo = strPruebaCodigo + "(" + strOpcionCuentaSelected + ")";
			if (!(comprobarExisteCarpetaDeLaCuenta(strOpcionCuentaSelected))){
    			System.out.println("mensaje 1 0");
				strSWMensajesErrorVentana = " *** Error: No existe el directorioHistorial de la cuenta: " + 
    					strRutaDestinoArchivos + "Historial_Omnex" +"/Cuenta" + strOpcionCuentaSelected;
			}
//			try {ficheroLog.InfoLog("5b " + strSWMensajesErrorVentana, "___procesosDeComprobaciones");} catch (IOException e) {e.printStackTrace();}
    	}
		//------------- Comprobar que el directorioHistorial de la cuenta contiene archivos hist�ricos.
		arrayStrRutasFicherosGenerados.clear();
		if (strSWMensajesErrorVentana.equals("")){
//			try {ficheroLog.InfoLog("6 " + strSWMensajesErrorVentana, "___procesosDeComprobaciones");} catch (IOException e) {e.printStackTrace();}
	    	strPruebaCodigo = strPruebaCodigo + "(e)";
			if (!(comprobarDirectorioDeCuentaContieneArchivos(strOpcionCuentaSelected))){
		    	strPruebaCodigo = strPruebaCodigo + "(f)";
				strSWMensajesErrorVentana = " *** Error: El directorioHistorial de la cuenta: " + strOpcionCuentaSelected+ 
    					" no contiene archivos (" + 
    					strRutaDestinoArchivos + "Historial_Omnex" +"/Cuenta" + strOpcionCuentaSelected;        				            	        
			}
//			try {ficheroLog.InfoLog("6b " + strSWMensajesErrorVentana, "___procesosDeComprobaciones");} catch (IOException e) {e.printStackTrace();}
		}	
		//--Ordenamos ascendentemente. As� que el archivo mas reciente lo encontramos en el elemento �ltimo del Array.
		Collections.sort(arrayStrRutasFicherosGenerados);
		//------------- Localizar en sus archivos sus �ltimos 10 movimientos.
    	strPruebaCodigo = strPruebaCodigo + "(g)";


    	if (strSWMensajesErrorVentana.equals("")){

	    	strPruebaCodigo = strPruebaCodigo + "(h)";        			
			int intContMovimientosEncontrados = 0;
			ArrayList<ArrayList<String>> arrayMovimientosArchivo = new ArrayList<ArrayList<String>>(); 
			
			//---For  recorre los archivos.---------------------------------
			int intNumeroDeArchivos = arrayStrRutasFicherosGenerados.size();
			int intElementoInicio = intNumeroDeArchivos - 1;
			String stringNombreFichero = "";
//			boolean booleanNoEncuentraContinuidad = false;
			//**************************************************************************************
			for(int contEle = intElementoInicio ; -1<contEle ; contEle--){
    			System.out.println("Numero de archivos - Dentro: contEle: " + contEle);
				System.out.println("vez archivo: " + contEle);

				stringNombreFichero = arrayStrRutasFicherosGenerados.get(contEle).toString();
    			String FileName = stringNombreFichero;
    			System.out.println("mensaje 82 ---->: " + FileName);

    			if (comprobarRutaArchivoExiste(FileName)){
//        			System.out.println("0906-IF --- si EXISTE ---->: " + FileName);
        			arrayMovimientosArchivo.clear();
        			
        			try {
						arrayMovimientosArchivo = generarArrayDesdeArchivoHistorialDeMovimientos(FileName);

						ArrayList<ArrayList<String>> arrayListTemp = new ArrayList<ArrayList<String>>();
						arrayListTemp.clear();
						//Guardamos en un arrayList		(arrayListStr10historicos)	los movimientos del historial.
						//		Guardamos los elementos en orden inverso al que est�n posicionados,   para que en el elemento CERO se encuentre el movimiento m�s reciente.
						//		Este arrayList,		arrayListStr10historicos,		lo utilizaremos si no hay equiparaci�n entre los 10 sobrantes y los ultimos 10 del historial, 
						//		y tuvi�ramos que comprobar y si un movimiento de los ultimos 10 del historial, no aparece en sobrantes  y lo represente un ajuste introducido por el operador.
						
						int intArrayMovimientosArchivoSize = arrayMovimientosArchivo.size();
						for (int iMovs = 0; iMovs < intArrayMovimientosArchivoSize; iMovs++) {
							arrayListTemp.add(arrayMovimientosArchivo.get(intArrayMovimientosArchivoSize - 1 - iMovs));
						}
						arrayListStr10historicos.addAll(arrayListTemp);
						
//        				try {ficheroLog.InfoLog("arrayListStr10historicos size: " + arrayListStr10historicos.size(), "procesosDeComprobaciones");} catch (IOException e) {e.printStackTrace();}
//        				for (int i = 0; i < arrayListStr10historicos.size(); i++) {
//            				try {ficheroLog.InfoLog("arrayListStr10historicos i (" + i + ") " + arrayListStr10historicos.get(i), "procesosDeComprobaciones");} catch (IOException e) {e.printStackTrace();}							
//						}
					} catch (IOException e) {
						strSWMensajesErrorVentana = " ERROR **** : lectura fallida del archivo historial de movimientos.";
						e.printStackTrace();
						break; //contEle = -1;
					}
        			//---recorrer los movimientos del archivo desde el ultimo elemento (ser� el movimiento mas reciente)-----
        			int intNumeroDeMovs = arrayMovimientosArchivo.size();
        			int intElementoMovInicio = intNumeroDeMovs - 1;
        			ArrayList<String> arrayListStrMovHistorial = new ArrayList<>();
        			ArrayList<String> arrayListStrMovSobranteDelExcel = new ArrayList<>();
        			
        			
        			for(int contEleMov = intElementoMovInicio ; -1<contEleMov ; contEleMov--){
            			//cotrebla
//        				System.out.println("0906-comparacion--datos elemento de array sobrantes: " + Ventana.getArrayListSWMovimientosSobrantes().get(intContMovimientosEncontrados).toString());
//        				System.out.println("0906-comparacion--datos elemento del arrayhistorial de movimientos: " + arrayMovimientosArchivo.get(contEleMov).toString());
        				
//        				strMovHistorial = Ventana.getArrayListSWMovimientosSobrantes().get(intContMovimientosEncontrados).toString();
//        				strMovSobranteDelExcel = arrayMovimientosArchivo.get(contEleMov).toString();
        				arrayListStrMovHistorial.clear();
        				arrayListStrMovSobranteDelExcel.clear();
        				arrayListStrMovHistorial.addAll(arrayMovimientosArchivo.get(contEleMov));
        				arrayListStrMovSobranteDelExcel.addAll(Ventana.getArrayListSWMovimientosSobrantes().get(intContMovimientosEncontrados));

        				try {
//        					try {ficheroLog.InfoLog("compararMovHistorialConMovExcel   arrayListStrMovHistorial--->" + arrayListStrMovHistorial.toString(),"SOBRANTES");} catch (IOException e){e.printStackTrace();}
//        					try {ficheroLog.InfoLog("compararMovHistorialConMovExcel   arrayListStrMovSobranteDelExcel--->" + arrayListStrMovSobranteDelExcel.toString(),"SOBRANTES");} catch (IOException e){e.printStackTrace();}
							if (compararMovHistorialConMovExcel(arrayListStrMovHistorial , arrayListStrMovSobranteDelExcel)){
								intContMovimientosEncontrados++;
							}else{
								contEle = -1;
//								booleanNoEncuentraContinuidad =  true;
								strSWMensajesErrorVentana = " ERROR ***** : No encuentra continuidad correcta de movimientos en el historial." + 
								    " Localizados: " + intContMovimientosEncontrados;
								break;//contEleMov = -1;
							}
						} catch (IOException e) {
                			strSWMensajesErrorVentana = " ERROR ***** : Excepci�n IO al comparar historial.";
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
            			if(intContMovimientosEncontrados==10){
            				contEle = -1;
            				break;//contEleMov = -1;
            			}
        			}//----End FOR --------------------
    			}else{
        			System.out.println("8282   IF --- NO EXISTE ---->: " + FileName);
        			contEle = -1;
        			strSWMensajesErrorVentana = " ERROR ***** : lectura fallida del archivo: " + stringNombreFichero;
    			}
    			//---Si en el historial ha encontrado que sus 10 movimientos m�s recientes son los del archivo excel 
    			if(intContMovimientosEncontrados==10){
    				contEle = -1;
    			}
			}//**********  End FOR ************************************************************************

			//****	Si no ha encontrado equivalencia en los �ltimos 10 movimientos del historial:	
			//****	se calcular� si alg�n ajuste debiera contemplarse como parte de los 10 sobrantes.
			//
			//****	Vamos a comprobar si el operador ha introducido ajustes, y si con ellos se pudiera 
			//****	realizar una comprobaci�n de continuidad de dep�sitos en el historial,   pues se dar�a como correcto.

			boolean booleanAjusteEncuentraEquivalencia = false;
			double doubleImporteDeAjuste = 0;
			double doubleImporteAjusteValidoEncontrado = 0;
	
			if(intContMovimientosEncontrados<10){			
				//******** Inicio del if (existen Ajustes) ***********************************
				//Si hay ajustes comprobaremos si alguno de ellos con 9 movimientos de sobrantes ----> pudiera completar la equivalencia con 10 movimientos del historial.	
				if(arrayListAjustesAlmacenados.size()>0){
					//Vamos a comprobar si se consigue equiparacion de los primeros 9 sobrantes con un ajuste y los 10 movimientos ultimos en el historial de esa cuenta.
					//Si hay m�s de un ajuste,  comprobaremos uno a uno   (si alguno de ellos con los 9 sobrantes consiguen esa equivalencia con el historial).

					double doubleSumatorio10movsHistorial = 0.0;
					double doubleSumatorio9movsSobrantes = 0.0;
					//  Sumamos importes de los 10 movimientos historicos.
					//y Sumamos importes de los 9 primeros sobrantes ------->Si a�adiendo el importe de alg�n ajuste    se equipara con el historial ---------------------> se da la comprobaci�n como v�lida.
				
					doubleSumatorio10movsHistorial = sumatorioDiezImportesDeHistorial(arrayMovimientosArchivo);
					doubleSumatorio9movsSobrantes = sumatorioDeImporteDeNueveSobrantes(Ventana.getArrayListSWMovimientosSobrantes());

					//Recorremos cada uno de los ajustes para ver si con alguno se consigue la equivalencia
					booleanAjusteEncuentraEquivalencia = false;

					Double doubleResultadoSumaAjusteYsobrantes = 0.0;
					for (int iAjustes = 0; iAjustes < arrayListAjustesAlmacenados.size(); iAjustes++) {
						doubleImporteDeAjuste = Double.valueOf(arrayListAjustesAlmacenados.get(iAjustes).get(1));
						doubleResultadoSumaAjusteYsobrantes = doubleSumatorio9movsSobrantes + (doubleImporteDeAjuste * -1);
						
						if (doubleSumatorio10movsHistorial == doubleResultadoSumaAjusteYsobrantes){
							booleanAjusteEncuentraEquivalencia = true;
							doubleImporteAjusteValidoEncontrado = doubleImporteDeAjuste;
						}
					}

					if (!booleanAjusteEncuentraEquivalencia){
						strSWMensajesErrorVentana = "ERROR *** � Ning�n ajuste ha complementado a los sobrantes !: No hay continuidad de movimientos.";
					}else{
						strSWMensajesErrorVentana = "";
	   					Ventana.acumularStrTextoResultadoComprobaciones("\n Info:	El ajuste con importe: " + doubleImporteAjusteValidoEncontrado + ", complementa a los primeros 9 sobrantes.");
					}
					//----------------------------------------------------------------------------------------------------------				
				}else{
					//si no tenia ajustes
					strSWMensajesErrorVentana = "ERROR *** � No hay ajuste que complementen con sobrantes, para dar continuidad con Historial !.";
				}
				//******** Fin del if (existen Ajustes) ***********************************
			}			
			//*********************************************************************************
			System.out.println("");
//			try {ficheroLog.InfoLog("7b " + strSWMensajesErrorVentana, "___procesosDeComprobaciones");} catch (IOException e) {e.printStackTrace();}
		}
		//***********************************************************************************************************************
    	//************ Generar Datos Omnex **************************************************************************************  	
    	if (strSWMensajesErrorVentana.equals("")){
    		
	    	strPruebaCodigo = strPruebaCodigo + "(j)";        			
			// Desde el arrayBiDimensDelExcel:	se debe leer su contenido.
			// Se encuentran datos del excel, las filas con contenido en 'observaciones' y en 'importe'.
			// Desde su contenido hay que obtener un array para generar el excel de Omnex.
			// Desde las observaciones hay que obtener el idAgente.
			//							Hay que obtener la fecha del dia con formato para Omnex (de calcula).
			//							Hay que obtener la cuenta bancaria (se consulta en un archivo properties.txt).
			//							Hay que obtener el nombre del campo (se consulta en un archivo properties.txt).
			//Generar array con datos correctos para Omnex.
	    	
    		GeneraArrayDatosOmnex generaDatosOmnex = new GeneraArrayDatosOmnex(swArrayBiDimensDelExcel);
    		
    		//*** Metodo	'generar'		para obtener los c�digos de agente desde el valor de 'observaciones'.
    		swArrayDatosParaOmnex = generaDatosOmnex.generar();
        	//***************************************************************************************************
        	String stringBancoCuenta = "";
        	stringBancoCuenta = getStrOpcionBancoSelected() + getStrOpcionCuentaSelected();
			//***	CONDICIONES DE LA CAIXA	******lacaixa********************************************************
        	//Esta comprobaci�n se hace cuando la cuenta sea la 2200076885 de la Caixa ------------------------------
//noviembrecotrebla
//    		try {ficheroLog.InfoLog("CREAR LOG 22 1", "buscarValor999");} catch (IOException e2) {e2.printStackTrace();} 
        	
        	if (stringBancoCuenta.equals(strBancoCuenta_Caixa_2200076885)){     		
        		for (int i4 = 0; i4 < swArrayDatosParaOmnex.size(); i4++) {
            		//--Aqu� vamos a preguntar por el valor de <movimiento>. Si su valor es: "INGRESO POR VALIDAR"
            		// --- ENTONCES tenemos que asignar el valor CERO al codigo de agente.
        			//***Significa que el banco tiene pendiente de comprobar un importe que ha recibido.
            		//Recorremos todo el array para ver qu� depositos tiene en <movimiento> el valor: "INGRESO POR VALIDAR"        			
        			if (swArrayDatosParaOmnex.get(i4).get(4).equals("INGRESO POR VALIDAR")){
        				swArrayDatosParaOmnex.get(i4).set(2, "0");		
    				}
        			//Si el programa ve en un corte un movimiento que tiene en 'Observaciones' la cifra: 999
        			//entonces se asignar� el valor cero al c�digo de agente.
        			if (buscarValor999(swArrayDatosParaOmnex.get(i4).get(0))){
        				swArrayDatosParaOmnex.get(i4).set(2, "0");        				
    				}else{
//            			try {ficheroLog.InfoLog("FIN: NO Ha encontrado 999", "buscarValor999");} catch (IOException e) {e.printStackTrace();}    					
    				}
        			//----------------------------------------------------------------------------------------------
    			} 
    		}
        	//ejemplo de swArrayDatosParaOmnex:
        	//swArrayDatosParaOmnex.get(i4)	[R.7659, 300.0, 7659, 661260.67, TRASPASO L.ABIERTA, 09/19/2017]
        	//***************************************************************************************************
			//***	CONDICIONES DE BBVA		*********************************************************************
    		//Recorremos todo el array
        	//
        	if (stringBancoCuenta.equals(strBancoCuenta_BBVA_0208509436)){
        		for (int iSW = 0; iSW < swArrayDatosParaOmnex.size(); iSW++) {
        			System.out.println("1 iSW 07nov_: swArrayDatosParaOmnex.get():" + swArrayDatosParaOmnex.get(iSW).toString());
            		//--Aqu� vamos a preguntar por el valor de <observaciones>. Si viene en blanco, asignamos el valor de <movimiento> a la columna <observaciones> (datos)
                	//	(para que el excel para Omnex tenga ese valor).
        			if (swArrayDatosParaOmnex.get(iSW).get(0).trim().equals("")){
        				swArrayDatosParaOmnex.get(iSW).set(0, swArrayDatosParaOmnex.get(iSW).get(4));
    				}
        			//Si el programa ve en un corte un movimiento que tiene 'Observaciones' con valor: NOTPROVIDED
        			//asignar� el valor 1111 en la columna del c�digo de agente.
        			if (swArrayDatosParaOmnex.get(iSW).get(0).toLowerCase().equals("notprovided")){
        				swArrayDatosParaOmnex.get(iSW).set(2, "1111");        				
    				}
        			//noviembrecotrebla
        			
        			//Si el programa ve en un corte un movimiento que tiene en 'Observaciones' la cifra: 999
        			//entonces se asignar� el valor cero al c�digo de agente.
        			if (buscarValor999(swArrayDatosParaOmnex.get(iSW).get(0))){
        				swArrayDatosParaOmnex.get(iSW).set(2, "0");
        				System.out.println("10nov ----swArrayDatosParaOmnex.get(iSW).get(0)::::::(" + swArrayDatosParaOmnex.get(iSW).get(0) + "):::: encuentra 999");
    				}
        		}
        	}
    		//***************************************************************************************************
				
			//Aqu� a�adimos los movimientos de los ajustes, si hubiera ************************************************
			for (int contAj = 0; contAj < getArrayListAjustesAlmacenados().size(); contAj++) {
				ArrayList<String> datos = new ArrayList<>();
				datos.add(0, getArrayListAjustesAlmacenados().get(contAj).get(2).toString());
				datos.add(1, getArrayListAjustesAlmacenados().get(contAj).get(1).toString());
				datos.add(2, getArrayListAjustesAlmacenados().get(contAj).get(0).toString());
				datos.add(3, "");
				datos.add(4, strAjusteSW);
				datos.add(5, "");
				swArrayDatosParaOmnex.add(new ArrayList(datos));				
			}
			//*********************************************************************************************************
    	}
    	
		String mensaje = "";
		mensaje = strSWMensajesErrorVentana;

//		cotrebla
//		mensaje = mensaje + strPruebaCodigo;

		System.out.println("mensaje 1 1: " + mensaje);
		
		jLabelMensaje.setForeground(Color.RED);
		jLabelMensaje.setText(mensaje);

    	txtAreaResultadoComprobaciones.setText(strTextoResultadoComprobaciones);
		txtAreaResultadoComprobaciones.setVisible(true);
    	txtAreaResultadoComprobacionesAjustes.setText(strTextoResultadoComprobaciones);
		//Si no ha habido mensajes:	continuamos el proceso.
		System.out.println("mensaje 1 2: " + mensaje);

    	strPruebaCodigo = strPruebaCodigo + "(m)";        			

//    	Ventana.acumularStrTextoResultadoComprobaciones("\n" + "Bot�n Comprobaciones: ");
    	Ventana.acumularStrTextoResultadoComprobaciones(strInfoBotonComprobaciones);

//		try {ficheroLog.InfoLog("fin-a- " + strSWMensajesErrorVentana, "___procesosDeComprobaciones");} catch (IOException e) {e.printStackTrace();}

		if (mensaje.equals("")){
			System.out.println("mensaje 1 3: " + mensaje);
			btnComprobacionesActionPerformed();
		}
		System.out.println("mensaje 1 4: " + mensaje);

//		try {ficheroLog.InfoLog("final", "___procesosDeComprobaciones");} catch (IOException e) {e.printStackTrace();}
		
		if (getTxtAreaResultadoComprobaciones().toString().equals("")) {
			txtAreaResultadoComprobaciones.setEnabled(false);
			txtAreaResultadoComprobaciones.setVisible(false);
		}

//		try {ficheroLog.InfoLog("mensaje: " + mensaje, "_mensaje_de_procesosDeComprobaciones");} catch (IOException e) {e.printStackTrace();}
//		try {ficheroLog.InfoLog("CREAR LOG_fin", "buscarValor999");} catch (IOException e2) {e2.printStackTrace();} 

		return mensaje;
	}

	private static double sumatorioDeImporteDeNueveSobrantes(ArrayList<ArrayList<String>> arrayListStrMovSobranteDelExcel){

		double doubleSumatorio = 0.0;
		//---Debemos sumar los primeros 9 importes de los sobrantes.
		//---Si no tiene 9 elementos entonces sumaremos los que tenga.
		int intFor = 0;
		intFor = arrayListStrMovSobranteDelExcel.size();
		if (arrayListStrMovSobranteDelExcel.size() > 9){
			intFor = 9;
		}
		
		for (int i = 0; i < intFor; i++) {
			doubleSumatorio = doubleSumatorio + Double.valueOf(arrayListStrMovSobranteDelExcel.get(i).get(1).toString());
		}
		return doubleSumatorio;
	}
	
	private static double sumatorioDiezImportesDeHistorial(ArrayList<ArrayList<String>> arrayListStrMovHistorial){

		double doubleSumatorio = 0.0;
		//---Debemos sumar los ultimos 10 importes de esa cuenta en el historial.
		//---Si no tiene 10 elementos entonces sumaremos los que tenga.
		int intFor = 0;
		int intSize = 0;
		intSize = arrayListStrMovHistorial.size();
		intFor = intSize;
		if (arrayListStrMovHistorial.size() > 10){
			intFor = 10;
		}
		
		double doubleImporte = 0.0;
		for (int i = 0; i < intFor; i++) {
//			doubleSumatorio = doubleSumatorio + Double.valueOf(arrayListStrMovHistorial.get(intSize-1-i).get(2).toString());

			//El punto de millas se quita y la coma decimal se sustituye por punto.
			doubleImporte = sustituirCaracterDecimalMillar(String.valueOf(arrayListStrMovHistorial.get(intSize-1-i).get(2)));
			
			doubleSumatorio = doubleSumatorio + doubleImporte;
		}
		return doubleSumatorio;
	}
	
	private static double sustituirCaracterDecimalMillar(String strImporte){
		//El punto de millar se quita y la coma decimal se sustituye por punto.
		double doubleImporte = 0.0;
		strImporte = strImporte.replace(".", "");
		strImporte = strImporte.replace(",", ".");
		doubleImporte = Double.valueOf(strImporte);
		
		return doubleImporte;
	}
	
	public static String obtenerFechayyyyMMdd_HHmmss(){
		String strDate = "";
		cal = Calendar.getInstance();
		sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
	    strDate = sdf.format(cal.getTime());
	    return strDate;	
	}

	public static boolean buscarValor999(String strValor){
//		try {ficheroLog.InfoLog("Dentro de buscarValor999. strValor: " + strValor, "buscarValor999");} catch (IOException e) {e.printStackTrace();}
			 
		//	 ficheroLog.InfoLog("OK  cabecera:	valor de intPosicionColumnaImporteEmpezandoPorCero: " + intPosicionColumnaImporteEmpezandoPorCero, "22222_bancos");
		System.out.println("10nov<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< (1) buscarValor999 strValor: " + strValor);

		//Recibe una cadena de texto
		//Devuelve true si encuentra la subcadena 999 sin tener otro caracter numerico ni a la izquierda ni a la derecha. Ej: a999b (true). 1999 (false). 89994 (false).
		boolean boolean_Anterior_No_Es_Numero = true;
		boolean boolean_Anterior_Es_Numero_No_Nueve = false;

		int intCuentaNueves = 0;
		String strCaracter = "";
		//strValor = "unodostres";

		boolean booleanRetorno = false;
		int intLongitudCadena = 0;
		intLongitudCadena = strValor.length();

		System.out.println("10nov 2 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< (1) buscarValor999 strValor: " + strValor + "- intLongitudCadena : " + intLongitudCadena);

//		try {ficheroLog.InfoLog("Dentro de buscarValor999. intLongitudCadena: " + intLongitudCadena, "buscarValor999");} catch (IOException e) {e.printStackTrace();}    					

		for (int i = 0; i < intLongitudCadena; i++) {
//			System.out.println("10nov  DENTRO DEL FOR <<<<<<<<<<<<<<<<<<<<<<<<<<< strCaracter: " + strCaracter + "<<<<<<<(" + strValor + ")<<<<<<<<<<<<<<<<<<<<<");			
			strCaracter = strValor.substring(i, i + 1);
//			try {ficheroLog.InfoLog("Dentro de buscarValor999. strCaracter: " + strCaracter, "buscarValor999");} catch (IOException e) {e.printStackTrace();}    					
			
//			System.out.println("8nov 4 ANTES intCuentaNueves: " + intCuentaNueves);
//			System.out.println("8nov 4 strCaracter: " + strCaracter);

			if (strCaracter.equals("9")){
//				try {ficheroLog.InfoLog("Dentro de buscarValor999. caracter es 9. intCuentaNueves: " + intCuentaNueves, "buscarValor999");} catch (IOException e) {e.printStackTrace();}    					
				intCuentaNueves ++;
			}else{
//				try {ficheroLog.InfoLog("Dentro de buscarValor999. caracter NO es 9. intCuentaNueves: " + intCuentaNueves, "buscarValor999");} catch (IOException e) {e.printStackTrace();}    					

				//no es 9
				if (intCuentaNueves > 3){
//					try {ficheroLog.InfoLog("Dentro de buscarValor999. caracter NO es 9. intCuentaNueves es > 3: " + intCuentaNueves, "buscarValor999");} catch (IOException e) {e.printStackTrace();}    					
					//Si llevaba m�s de tres 9 no consideramos 999 	
					intCuentaNueves = 0;
				}else{
//					try {ficheroLog.InfoLog("Dentro de buscarValor999. caracter NO es 9. intCuentaNueves NO es > 3: " + intCuentaNueves, "buscarValor999");} catch (IOException e) {e.printStackTrace();}    					

					if(intCuentaNueves == 3){
//						try {ficheroLog.InfoLog("Dentro de buscarValor999. caracter NO es 9. intCuentaNueves es = 3: " + intCuentaNueves, "buscarValor999");} catch (IOException e) {e.printStackTrace();}    					
						System.out.println("10nov HA contado tres NUEVES.(" + strValor + ")");

						//Llevaba 999
						//Hay que consultar si le preced�a un numero (de 0 a 8)
						//Al m�todo le pasamos 2 argumentos: la cadena de texto, y cu�l es la posici�n del for en la que estamos.
//						try {ficheroLog.InfoLog("Dentro de buscarValor999. caracter NO es 9. intCuentaNueves es = 3: " + intCuentaNueves + ". Metodo precedeNumeroDeCeroAocho.", "buscarValor999");} catch (IOException e) {e.printStackTrace();}    					

						if(!precedeNumeroDeCeroAocho(strValor, i)){
//							try {ficheroLog.InfoLog("Dentro de buscarValor999.    funcion precedeNumeroDeCeroAocho devuelve: FALSE.", "buscarValor999");} catch (IOException e) {e.printStackTrace();}    					

							//Cuando NO precede de un numero de 0 a 8, entonces consideramos correcta la cadena que ha encontrado: 999
							System.out.println("10nov --ok 999--- nO ES DE 0 A 8 <<< strValor: ((((" + strValor + "))))<<<<<<<<<<<<<<<<<< ha consultado en   precedeNumeroDeCeroAocho--resultado:  No precede con un 0 a 8 (valor 999 valido debe poner CERO) strValor: " + strValor);
							booleanRetorno = true;
						}else{
//							try {ficheroLog.InfoLog("Dentro de buscarValor999.    funcion precedeNumeroDeCeroAocho devuelve: TRUE.", "buscarValor999");} catch (IOException e) {e.printStackTrace();}    					
							
							System.out.println("10nov - no es 999 ----- ES DE 0 A 8 << strValor: ((((" + strValor + "))))<<<<<<<<<<<<<<<<<< ha consultado en   precedeNumeroDeCeroAocho--resultado:  No precede con un 0 a 8 (valor 999 valido debe poner CERO) strValor: " + strValor);							
						}
					}
				}
				intCuentaNueves = 0;
			}
//			System.out.println("10nov  FUERA DEL FOR <<<<<<<<<<<<<<<<<<<<<<<<<<< intCuentaNueves: " + intCuentaNueves + "<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		}
//		System.out.println("8nov <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< strValor: " + strValor);

//		try {ficheroLog.InfoLog("Dentro de buscarValor999.   Preguntamos si al final del texto,  intCuentaNueves es 3 (" + intCuentaNueves + ")", "buscarValor999");} catch (IOException e) {e.printStackTrace();}    					

		if (intCuentaNueves == 3) {
//			try {ficheroLog.InfoLog("Dentro de buscarValor999.   Preguntamos si al final del texto,  intCuentaNueves es 3 (" + intCuentaNueves + ")", "buscarValor999");} catch (IOException e) {e.printStackTrace();}    					

			System.out.println("10nov AL FINAL DEL TEXTO HA contado tres NUEVES.(" + strValor + ")");

			if(!precedeNumeroDeCeroAocho(strValor, intLongitudCadena)){
				//Cuando NO precede de un numero de 0 a 8, entonces consideramos correcta la cadena que ha encontrado: 999
				booleanRetorno = true;
//				System.out.println("8nov OK  no precede un CERO a ocho");
//				try {ficheroLog.InfoLog("Dentro de buscarValor999.   La funci�n    precedeNumeroDeCeroAocho devuelve que    NO      precede de 0 a 8", "buscarValor999");} catch (IOException e) {e.printStackTrace();}    					

			}else{
//				try {ficheroLog.InfoLog("Dentro de buscarValor999.   La funci�n    precedeNumeroDeCeroAocho devuelve que    SI      precede de 0 a 8", "buscarValor999");} catch (IOException e) {e.printStackTrace();}    					

//				System.out.println("8nov   NO OK   (precede un CERo a ocho)");
			}
		}

//		try {ficheroLog.InfoLog("Dentro de buscarValor999.   se finaliza con return booleanRetorno: " + booleanRetorno, "buscarValor999");} catch (IOException e) {e.printStackTrace();}    					

		System.out.println("10novControl <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< buscarValor999 booleanRetorno: " + booleanRetorno);		
	    return booleanRetorno;
	}

	public static boolean precedeNumeroDeCeroAocho(String strCadenaTexto, int intPosicion){
		boolean booleanRetorno = false;
		char charA = 'a';
		charA = strCadenaTexto.charAt(intPosicion - 4);
		System.out.println("precedeNumeroDeCeroAocho>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		System.out.println("precedeNumeroDeCeroAocho>>>>> strCadenaTexto: " + strCadenaTexto);
		System.out.println("precedeNumeroDeCeroAocho>>>>> La posicion: " + intPosicion);
		System.out.println("precedeNumeroDeCeroAocho>>>>> El caracter es: " + charA);
		System.out.println("precedeNumeroDeCeroAocho>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");		
		
		if (Character.isDigit(charA)){
			booleanRetorno = true;			
		}
		System.out.println("precedeNumeroDeCeroAocho>>>>> booleanRetorno: " + booleanRetorno);
		return booleanRetorno;
	}
}