//package com.chuidiang.ejemplos.poi_excel;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.format.CellFormatType;
import org.apache.poi.ss.format.CellTextFormatter;
import org.apache.poi.ss.format.CellNumberFormatter;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Color;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.util.Removal;

	/**
	 * Ejemplo sencillo de c�mo crear una hoja Excel con POI
	 * 
	 * @author chuidiang
	 * 
	 */
public class GeneraExcelDesdeArray {
	//Descripcion:	Crea una hoja Excel y la guarda.
	//Descripcion:	Los valor de la hoja excel provienen del archivo excel descargado de la web del banco.
	private static String strConstanteLaCaixa = "La Caixa";
	private static LogsFile ficheroLog = new LogsFile();

	private static String strMensajeLog = ""; 
	private static String strNombreLog = "";

	//Aqu� asignamos el valor del banco concatenado al valor de la cuenta tal cual lo tengamos en el archivo saldos_properties.txt
//	private static String strBancoCuenta_Caixa_2200076885 = "La Caixa - Uno Agentes2200076885";
//	private static String strBancoCuenta_BBVA_1111111111 = "BBVA1111111111";
	//Aqu� creamos los arrayList,   uno por cada banco/cuenta   al que queramos indicar qu� literales son los que condicionar�n que se asigne el valor 1111 al c�digo de agente.
	private static ArrayList<String> arrayList_Caixa_2200076885_cambianA_1111 = new ArrayList();
	private static ArrayList<String> arrayList_BBVA_1111111111_cambianA_1111 = new ArrayList();
	
	public boolean generaArchivo(ArrayList<ArrayList<String>> arrayListBid) throws Exception{
		
		arrayList_Caixa_2200076885_cambianA_1111.add(0, "SCF-TRASPASO FONDOS");arrayList_Caixa_2200076885_cambianA_1111.add(1, "MANTENIMIENTO");arrayList_Caixa_2200076885_cambianA_1111.add(2, "EXTRACTO");arrayList_Caixa_2200076885_cambianA_1111.add(3, "PRECIO ED. EXTRACTO");arrayList_Caixa_2200076885_cambianA_1111.add(4, "CUOTA T. PRIVADA");arrayList_Caixa_2200076885_cambianA_1111.add(5, "COND.CUOTA TARJETA");arrayList_Caixa_2200076885_cambianA_1111.add(6, "MANT. CERG FINANCE");
		arrayList_BBVA_1111111111_cambianA_1111.add(0, "SCF-TRASPASO FONDOS");arrayList_BBVA_1111111111_cambianA_1111.add(1, "MANTENIMIENTO");arrayList_BBVA_1111111111_cambianA_1111.add(2, "EXTRACTO");arrayList_BBVA_1111111111_cambianA_1111.add(3, "PRECIO ED. EXTRACTO");arrayList_BBVA_1111111111_cambianA_1111.add(4, "CUOTA T. PRIVADA");arrayList_BBVA_1111111111_cambianA_1111.add(5, "COND.CUOTA TARJETA");arrayList_BBVA_1111111111_cambianA_1111.add(6, "MANT. CERG FINANCE");

		//-----Para obtener la fecha con la que se deber�n grabar en el excel los ajustes,   obtenemos la fecha de otro movimiento.
		String strFechaParaAjustes = "";
		if(arrayListBid.size()>0){
			strFechaParaAjustes = arrayListBid.get(0).get(5).toString();
		}
		//--------------------------------------------------------------------------------------------------------------------------
		
		
		String strAA = "";
		String strAAResultante = "";

		strAA = "12345,321";
		strAAResultante = obtenerImporteFormatoOmnex(strAA);
		System.out.println("-aa------------------inicio de strAA (" + strAA + ") (" + strAAResultante + ")");

		String strImporte = "";
		String saldo = "";
		ArrayList<String> arrayCortePreparadoParaHistorialOmnex = new ArrayList<>();
		
		try {
        	// Se crea el libro
        	HSSFWorkbook libro = new HSSFWorkbook();
        	// Se crea una hoja dentro del libro
        	HSSFSheet hoja = libro.createSheet();

        	// Se crea un array de filas.
        	ArrayList<HSSFRow> arrayFilasExcel = new ArrayList<>();
        	// Array de celdas. Se crean celdas para una fila.
        	ArrayList<HSSFCell> arrayCeldasExcel = new ArrayList<>();

        	DataFormat format = libro.createDataFormat();
      	
            /* Get access to HSSFCellStyle */
            HSSFCellStyle style_origen = libro.createCellStyle();
        	style_origen.setDataFormat(format.getFormat("#,##0.0000"));
        	style_origen.setFillPattern(HSSFCellStyle.FINE_DOTS );
            style_origen.setFillForegroundColor(new HSSFColor.GREY_25_PERCENT().getIndex());
            style_origen.setFillBackgroundColor(new HSSFColor.BLACK().getIndex());
            
            /* Get access to HSSFCellStyle */
            HSSFCellStyle style_omnex = libro.createCellStyle();
        	style_omnex.setDataFormat(format.getFormat("#,##0.0000"));
        	style_omnex.setFillPattern(HSSFCellStyle.FINE_DOTS );
            style_omnex.setFillForegroundColor(new HSSFColor.LIGHT_GREEN().getIndex());
            style_origen.setFillBackgroundColor(new HSSFColor.BLACK().getIndex());
                        
        	//Escribimos la cabecera.
        	HSSFRow fila0 = hoja.createRow(0);
        	HSSFCell celda1Cabecera = fila0.createCell(0);
        	HSSFCell celda2Cabecera = fila0.createCell(1);
        	HSSFCell celda3Cabecera = fila0.createCell(2);
        	HSSFCell celda4Cabecera = fila0.createCell(3);
        	HSSFCell celda5Cabecera = fila0.createCell(4);
        	HSSFCell celda6Cabecera = fila0.createCell(5);
        	HSSFCell celda7Cabecera = fila0.createCell(6);
        	HSSFCell celda8Cabecera = fila0.createCell(7);
        	HSSFCell celda9Cabecera = fila0.createCell(8);
        	celda1Cabecera.setCellValue("BankNumber");
        	celda2Cabecera.setCellValue("Bank Name");
        	celda3Cabecera.setCellValue("Bank Account");
        	celda4Cabecera.setCellValue("Agent");
        	celda5Cabecera.setCellValue("Notes");
        	celda6Cabecera.setCellValue("Date");
        	celda7Cabecera.setCellValue("Amount");
        	celda8Cabecera.setCellValue("Currency");
        	celda9Cabecera.setCellValue("Type Code");

        	//Recorremos el arrayBidimen y generamos filas y celdas.
        	//contEle y contEle2  inicializados a CERO para recorrer el array de datos (arrayBidimen). 
        	//Para escribir en la fila	se indica: conEle+1.
        	//Para escribir en la celda	se indica: contEle.
        	
//    		System.out.println("292929 arrayListBid.size(): " + arrayListBid.size());
    		String stringMensajesExcepcionesAgentes = "";

    		
/*
    		for(int contEle=0;contEle<arrayListBid.size();contEle++){
        		try {ficheroLog.InfoLog(contEle + " arrayListBid: " + arrayListBid.get(contEle).toString(),"-------GeneraExcelDesdeArray-------");} catch (IOException e) {e.printStackTrace();}
    		}
*/
    		
    		
    		for(int contEle=0;contEle<arrayListBid.size();contEle++){

        		String stringCodigoAgente = arrayListBid.get(contEle).get(2).toString();
            	//**********************************************************************************************************
        		//cotrebla29
        		//En el arrayList:		arrayListSWAgentesValidos    tenemos los codigos de agente validos
        		ArrayList<String> arrayValidosAgentes = Ventana.getArrayListSWAgentesValidos(); 
        		int intValidosSize = arrayValidosAgentes.size();
        		boolean booleanCodigoAgenteActivo = false;
        		//hay que validar cada codigoAgente en ese array y pasarlo a CERO si no lo encuentra.
        		//y posteriormente,  si existe en saldos_properties asignar el codAgente que indique saldos_properties
        		//	y mostrar en Ventana mensaje de si no estaba en excelACTIVOS  y ademas tenia excepci�n en saldos_properties.

//        		System.out.println("292929 intValidosSize: " + intValidosSize);

        		for(int contEleValidos=0;contEleValidos<intValidosSize; contEleValidos++){
            		String stringValorArray = "";
            		stringValorArray = arrayValidosAgentes.get(contEleValidos);
            		if (arrayListBid.get(contEle).get(2).equals(stringValorArray)){
            			booleanCodigoAgenteActivo = true;
            			contEleValidos = intValidosSize;
            		}
            	}

    	    	//try {ficheroLog.InfoLog("*** FOR 2 *************** contEle: " + contEle,"-------GeneraExcelDesdeArray-------");} catch (IOException e) {e.printStackTrace();}

            	//**********************************************************************************************************
            	//cotrebla
            	//En Ventana.getArrayListSWBidimExcepcionesAgentesActivados();    tenemos las excepciones de saldos_properties
            	ArrayList<ArrayList<String>> arrayListExcepcionesSaldosProperties = Ventana.getArrayListSWBidimExcepcionesAgentesActivados();
            	ArrayList<String> arrayListExcepcionSaldosProperties = new ArrayList<>();
            	int intExcepcionesSize = arrayListExcepcionesSaldosProperties.size();
            	boolean booleanExcepcionActivada = false;
            	String stringNuevoCodigoAgente = "";

    	    	//try {ficheroLog.InfoLog("*** FOR 3 ***************** contEle: " + contEle,"-------GeneraExcelDesdeArray-------");} catch (IOException e) {e.printStackTrace();}

            	for(int contEleExcepciones=0;contEleExcepciones<intExcepcionesSize; contEleExcepciones++){
            		
//                	System.out.println("292929 contEleExcepciones: " + contEleExcepciones);
//                	System.out.println("292929 arrayListExcepcionesSaldosProperties size: " + arrayListExcepcionesSaldosProperties.size());
//                	System.out.println("292929 arrayListExcepcionSaldosProperties size: " + arrayListExcepcionSaldosProperties.size());

            		arrayListExcepcionSaldosProperties = arrayListExcepcionesSaldosProperties.get(contEleExcepciones);
//                	System.out.println("292929 1");
            		if (arrayListExcepcionSaldosProperties.get(0).equals("ACTIVADA") 
            				&& arrayListExcepcionSaldosProperties.get(1).equals(Ventana.getStrOpcionBancoSelected())
            				&& arrayListExcepcionSaldosProperties.get(2).equals(Ventana.getStrOpcionCuentaSelected())
            				&& arrayListExcepcionSaldosProperties.get(3).equals(arrayListBid.get(contEle).get(2).toString())){
//                    	System.out.println("292929 2");
            			booleanExcepcionActivada = true;
//                    	System.out.println("292929 3");            			
            			stringNuevoCodigoAgente = arrayListExcepcionSaldosProperties.get(4).toString();

//                    	System.out.println("292929 4");            			
            			contEleExcepciones = intExcepcionesSize;
//                    	System.out.println("292929 5");            			
            		}
            	}

            	//**** Si el agente tiene una excepcion activada.----------------------------------------------------------- 
            	if (booleanExcepcionActivada){
//                	System.out.println("el agente tiene una excepcion activada");
            		arrayListBid.get(contEle).set(2, stringNuevoCodigoAgente);
                	//**** Si el agente no esta activo.
            		if (!booleanCodigoAgenteActivo){
//                    	System.out.println("if el agente tiene una excepcion activada------ booleanCodigoAgenteActivo: " + booleanCodigoAgenteActivo);
                    	stringMensajesExcepcionesAgentes = stringMensajesExcepcionesAgentes + 
                    			" ***** Aviso: �" + stringCodigoAgente + " --> No est� activo el agente. !" +
                    			" Pero existe excepci�n. Valor del C�digo resultado: " + stringNuevoCodigoAgente;
                	}else{
//                    	System.out.println("else  el agente tiene una excepcion activada------ booleanCodigoAgenteActivo: " + booleanCodigoAgenteActivo);
                    	stringMensajesExcepcionesAgentes = stringMensajesExcepcionesAgentes +
                    			" Codigo de Agente: " + stringCodigoAgente + " --> " + 
                    			" Existe excepci�n. Valor del C�digo resultado: " + stringNuevoCodigoAgente;
                	}
            	}

            	//**** Si el agente NO tiene una excepcion activada.-----------------------------------------------------------            	
            	if (!booleanExcepcionActivada){
//                	System.out.println("el agente NO tiene una excepcion activada");
            		if (!booleanCodigoAgenteActivo){
//                    	System.out.println("el agente NO tiene una excepcion activada----booleanCodigoAgenteActivo:" + booleanCodigoAgenteActivo);
            			stringNuevoCodigoAgente = "0";
                		arrayListBid.get(contEle).set(2, stringNuevoCodigoAgente);            			
                    	stringMensajesExcepcionesAgentes = stringMensajesExcepcionesAgentes + 
                    			" ***** Aviso: �" + stringCodigoAgente + " --> No est� activo el agente. !" +
                    			" Valor del C�digo resultado: 0";
                	}
            	}   

    	    	//try {ficheroLog.InfoLog("*** FOR 6 *************** contEle: " + contEle,"-------GeneraExcelDesdeArray-------");} catch (IOException e) {e.printStackTrace();}

//            	System.out.println("valor  al final cod:     " + arrayListBid.get(contEle).get(2));
//            	System.out.println("------222222222");
            	
            	//******************************************************************************************************************
            	//**** se acaba de consultar el archivo agentes.xls
            	//		y las excepciones de saldos_properties.txt.
            	//			y se ha podido asignar un codigo de agente nuevo aun cuando puede que tuviermos un 1111 ya en el codigo de agente
            	//			por <movimiento>.
            	//		 
            	//		Aqu� volvemos a preguntar si el <movimiento>  tiene algun texto que conlleve codigo 1111 para el agente.
            	//		Tambi�n podemos quitar la validacion inicial  de los textos de <movimiento> porque aqu� queda controlado.
            	//******************************************************************************************************************
            	
            	//Se graba en un log el array final que tenemos para generar el excel.
//           		for(int cont = 0; cont<arrayCortePreparadoParaHistorialOmnex.size() ; cont++){
//           			ficheroLog.InfoLog(arrayCortePreparadoParaHistorialOmnex.get(cont).toString(), "2_arrayCortePreparadoParaHistorialOmnex");
//           		}
               	//Se graba en un log el array inicial de la lectura del excel original.
//           		for(int cont2 = 0; cont2<Ventana.getSwArrayBiDimensDelExcel().size() ; cont2++){
//           			ficheroLog.InfoLog(Ventana.getSwArrayBiDimensDelExcel().get(cont2).toString(), "2_arrayDatosExcelOriginal");
//           		}
               	//Se consulta en el array inicial:		Ventana.getSwArrayBiDimensDelExcel		si la fila correspondiente 
            	//		tuviera en <movimiento> alg�n texto que implique asignar 1111 al codigo de agente.
    	    	//Solo se consulta si no es un movimiento de ajuste,   que el operador haya introducido en el formulario de la Ventana.
    	    	//		ya que si hay ajustes,  el array inicial tendr� menos movimientos pues no tienen los ajustes.
    	    	
    	    	//try {ficheroLog.InfoLog("*** FOR 6b ************** contEle: " + contEle,"-------GeneraExcelDesdeArray-------");} catch (IOException e) {e.printStackTrace();}
    	    	
            	//*******************************************************************************************************************************************************
            	//
            	//	C�digo condicionado por banco/cuenta.
            	//	
            	//	Al c�digo de agente: arrayListBid.get(contEle).set(2, ...		se asignar� el valor: 1111		cuando el movimiento sea una operaci�n interna de SmallWorld,
            	//	para ello se consultar� el valor de la columna excel  (movimiento) , 	que en el array getSwArrayBiDimensDelExcel	lo tenemos en el elemento 4.
            	//
            	// La Caixa:
            	//			"SCF-TRASPASO FONDOS"	|| "MANTENIMIENTO"		|| "EXTRACTO") || "PRECIO ED. EXTRACTO" || 
            	//			"CUOTA T. PRIVADA"		|| "COND.CUOTA TARJETA"	|| "MANT. CERG FINANCE"
            	//
            	// BBVA ...
            	// Posta ...
            	// etc...
            	//
            	//*******************************************************************************************************************************************************
/*
            	if (!(arrayListBid.get(contEle).get(4).equals(Ventana.getStrAjusteSW()))) {
           			if (Ventana.getSwArrayBiDimensDelExcel().get(contEle).get(4).equals("SCF-TRASPASO FONDOS") || 
    						Ventana.getSwArrayBiDimensDelExcel().get(contEle).get(4).equals("MANTENIMIENTO") ||
    						Ventana.getSwArrayBiDimensDelExcel().get(contEle).get(4).equals("EXTRACTO") || 
    						Ventana.getSwArrayBiDimensDelExcel().get(contEle).get(4).equals("PRECIO ED. EXTRACTO") ||
    						Ventana.getSwArrayBiDimensDelExcel().get(contEle).get(4).equals("CUOTA T. PRIVADA") || 
    						Ventana.getSwArrayBiDimensDelExcel().get(contEle).get(4).equals("COND.CUOTA TARJETA") || 
    						Ventana.getSwArrayBiDimensDelExcel().get(contEle).get(4).equals("MANT. CERG FINANCE")){
    					//Asignamos 1111 al c�digo de agente.
           				//try {ficheroLog.InfoLog("*** FOR 6c ************** contEle: " + contEle,"-------GeneraExcelDesdeArray-------");} catch (IOException e) {e.printStackTrace();}

                		arrayListBid.get(contEle).set(2, "1111");
       				}					
				}else{
					//***** si el movimiento es de un ajuste (la celda <movimiento> es ajusteSW),  entonces le asignamos la fecha.
            		arrayListBid.get(contEle).set(5, strFechaParaAjustes);
				}
*/            	
            	//*******************************************************************************************************************************************************
            	ArrayList<String> arrayListTemp = new ArrayList<String>();
			
            	String stringBancoCuenta = "";
            	stringBancoCuenta = Ventana.getStrOpcionBancoSelected() + Ventana.getStrOpcionCuentaSelected();

            	//los case     son valores del banco concatenado al valor de la cuenta tal cual lo tengamos en el archivo saldos_properties.txt
            	switch(stringBancoCuenta){
            		case "La Caixa - Uno Agentes2200076885":
            										arrayListTemp.addAll(arrayList_Caixa_2200076885_cambianA_1111);
            										break;
            		case "BBVA1111111111":
            										arrayListTemp.addAll(arrayList_BBVA_1111111111_cambianA_1111);
            										break;
            		default:						break;            	
            	}

//            	try {ficheroLog.InfoLog("arrayListBid conEle: " + contEle + " ---> " + arrayListBid.get(contEle),"-------GeneraExcelDesdeArray-------");} catch (IOException e) {e.printStackTrace();}
//            	try {ficheroLog.InfoLog("stringBancoCuenta: " + stringBancoCuenta,"-------GeneraExcelDesdeArray-------");} catch (IOException e) {e.printStackTrace();}

            	if (!(arrayListBid.get(contEle).get(4).equals(Ventana.getStrAjusteSW()))) {
            		//for para recorrer todos los literales,  para esa cuenta, 	que hacer que se asigne el valor 1111     al c�digo de agente.
            		for (int i1111 = 0; i1111 < arrayListTemp.size(); i1111++) {
                		if (Ventana.getSwArrayBiDimensDelExcel().get(contEle).get(4).equals(arrayListTemp.get(i1111))){
        					//Asignamos 1111 al c�digo de agente.
                			arrayListBid.get(contEle).set(2, "1111");
                		}
					}
				}else{
					//***** si el movimiento es de un ajuste (la celda <movimiento> es ajusteSW),  entonces le asignamos la fecha.
            		arrayListBid.get(contEle).set(5, strFechaParaAjustes);
				}
            	//*******************************************************************************************************************************************************
            	try {ficheroLog.InfoLog("arrayListBid.get(contEle)get(2): " + arrayListBid.get(contEle).get(2),"11111_GeneraExcelDesdeArray-------");} catch (IOException e) {e.printStackTrace();}
            	
    	    	//try {ficheroLog.InfoLog("*** FOR 7 *************** contEle: " + contEle,"-------GeneraExcelDesdeArray-------");} catch (IOException e) {e.printStackTrace();}

/*
            	if (stringCodigoAgente.equals("1111")){
            		stringNuevoCodigoAgente = stringCodigoAgente;
            		arrayListBid.get(contEle).set(2, stringNuevoCodigoAgente);
        		}
*/
            	//**********************************************************************************************************

            	arrayFilasExcel.add(hoja.createRow(contEle+1));
        		arrayCeldasExcel.clear();
//            	for(int contEle2=0;contEle2<arrayListBid.get(contEle).size();contEle2++){
//                	System.out.println("****    Celda: " + contEle2 + " *** valor: " + arrayListBid.get(contEle).get(contEle2));
//            		arrayCeldasExcel.add(arrayFilasExcel.get(contEle).createCell(contEle2));
//            		arrayCeldasExcel.get(contEle2).setCellValue("c:" + contEle + " c2:" + contEle2 + " valor:" + arrayListBid.get(contEle).get(contEle2));
//            	}
        		arrayCeldasExcel.add(arrayFilasExcel.get(contEle).createCell(0));
        		arrayCeldasExcel.add(arrayFilasExcel.get(contEle).createCell(1));
        		arrayCeldasExcel.add(arrayFilasExcel.get(contEle).createCell(2));
        		arrayCeldasExcel.add(arrayFilasExcel.get(contEle).createCell(3));
        		arrayCeldasExcel.add(arrayFilasExcel.get(contEle).createCell(4));
        		arrayCeldasExcel.add(arrayFilasExcel.get(contEle).createCell(5));
        		arrayCeldasExcel.add(arrayFilasExcel.get(contEle).createCell(6));
        		arrayCeldasExcel.add(arrayFilasExcel.get(contEle).createCell(7));
        		arrayCeldasExcel.add(arrayFilasExcel.get(contEle).createCell(8));
//        		arrayCeldasExcel.add(arrayFilasExcel.get(contEle).createCell(9));
        		arrayCeldasExcel.get(0).setCellValue("");
//        		arrayCeldasExcel.get(1).setCellValue("qqqla caixa");
        		arrayCeldasExcel.get(1).setCellValue(Ventana.getStrOpcionBancoSelected());
//        		arrayCeldasExcel.get(2).setCellValue("2200076885");
        		arrayCeldasExcel.get(2).setCellValue(Ventana.getStrOpcionCuentaSelected());
            	//System.out.println("*************** Fila: " + contEle + " valor:" + arrayListBid.get(contEle).get(1));

        		arrayCeldasExcel.get(3).setCellValue(arrayListBid.get(contEle).get(2));        		
            	//System.out.println("*************** Fila: " + contEle + " valor:" + arrayListBid.get(contEle).get(0));

        	  //arrayCeldasExcel.get(4).setCellValue(arrayListBid.get(contEle).get(0));
          	  //Esta apareciendo en el excel el valor del idAgente con punto decimal seguido de CERO
          	  //Para evitarlo:	 buscamos el punto y nos quedamos con la parte entera.
        		
        		String stringValorIdAgente = "";
        		stringValorIdAgente = arrayListBid.get(contEle).get(0);
        		
        		String[] elemsIdAgente = stringValorIdAgente.split("\\.");
//        		ficheroLog.InfoLog("1.- " + stringValorIdAgente, "izDePunto__________");        		
        		if(elemsIdAgente[0].toString().equals("0")){
            		stringValorIdAgente = elemsIdAgente[0].toString();
        		}
//        		ficheroLog.InfoLog("2.- " + stringValorIdAgente, "izDePunto__________");        		
        		
    	    	//try {ficheroLog.InfoLog("*** FOR 8 *************** contEle: " + contEle,"-------GeneraExcelDesdeArray-------");} catch (IOException e) {e.printStackTrace();}

        		try {
					Double doubleValorGet4 = Double.valueOf(arrayListBid.get(contEle).get(0));
	        		arrayCeldasExcel.get(4).setCellValue(stringValorIdAgente);					
				} catch (Exception e) {
					// TODO Auto-generated catch block
	        		arrayCeldasExcel.get(4).setCellValue(arrayListBid.get(contEle).get(0).toString());
					//cotrebla
	        		//e.printStackTrace();
				}
        		
//        		arrayCeldasExcel.get(4).setCellValue(stringValorIdAgente);
        	  //-------------------------------------------------------------------------------------------
        		
        		//Fecha---------------------------------------------------------------------
//        		arrayCeldasExcel.get(5).setCellValue(Ventana.getStrDateExcelOmnex());
        		arrayCeldasExcel.get(5).setCellValue(arrayListBid.get(contEle).get(5));
        		strImporte = arrayListBid.get(contEle).get(1);

//        		strImporte = strImporte.replace(".", ",");
        		//************************************************** Antes de asignar a la celda el valor de strImporte *****
        		// Se analiza si tiene separador decimal    y si tiene, separador de millar.
        		String strImporteResultante = "";
        		
        		strImporte = arrayListBid.get(contEle).get(1);

    	    	//try {ficheroLog.InfoLog("*** arrayListBid.get(contEle).get(1)   strImporte: " + strImporte,"-------GeneraExcelDesdeArray-------");} catch (IOException e) {e.printStackTrace();}

        		strImporte = obtenerImporteFormatoOmnex(strImporte);

        		//***********************************************************************************************************
            	arrayCeldasExcel.get(6).setCellValue(strImporte);
        		arrayCeldasExcel.get(7).setCellValue("EUR");       		
        		arrayCeldasExcel.get(8).setCellValue("BANK_DEP");      		      		

        		strMensajeLog = "celda0(" + arrayCeldasExcel.get(0) + ")_" + "celda1(" + arrayCeldasExcel.get(1) + ")_" + 
        		"celda2(" + arrayCeldasExcel.get(2) + ")_" + "celda3(" + arrayCeldasExcel.get(3) + ")_" + 
        				"celda4(" + arrayCeldasExcel.get(4) + ")_" + "celda5(" + arrayCeldasExcel.get(5) + ")_" + 
        		"celda6(" + arrayCeldasExcel.get(6) + ")_" + "celda7(" + arrayCeldasExcel.get(7) + ")_" + 
				"celda8(" + arrayCeldasExcel.get(8) + ")_" + "celda9(" + 
				arrayListBid.get(contEle).get(4).toString() + ")_";
        		
        		strNombreLog = "Historial_cortes";
        		        		
        		arrayCortePreparadoParaHistorialOmnex.add(strMensajeLog + "saldo:" + arrayListBid.get(contEle).get(3));
        	}
        	//****** end for ****************************
        	
	    	//try {ficheroLog.InfoLog(" 00001 arrayCortePreparadoParaHistorialOmnex size: " + arrayCortePreparadoParaHistorialOmnex.size(),"-------GeneraExcelDesdeArray-------");} catch (IOException e) {e.printStackTrace();}
        	Ventana.clearArrayCortePreparadoParaHistorialOmnex();
	    	//try {ficheroLog.InfoLog(" 00002 arrayCortePreparadoParaHistorialOmnex size: " + arrayCortePreparadoParaHistorialOmnex.size(),"-------GeneraExcelDesdeArray-------");} catch (IOException e) {e.printStackTrace();}
        	Ventana.copiarDatosEnArrayCortePreparadoParaHistorialOmnex(arrayCortePreparadoParaHistorialOmnex);

        	Ventana.setStrRutaFileNameExcelGenerado(Ventana.getStrRutaDestinoArchivos() + "Omnex_" + Ventana.getStrOpcionBancoSelected() + "_" + Ventana.getStrOpcionDescripcionSelected() + "_" + Ventana.getStrDateFileName() + ".xls");
            FileOutputStream elFichero = new FileOutputStream(Ventana.getStrRutaDestinoArchivos() + "Omnex_" + Ventana.getStrOpcionBancoSelected() + "_" + Ventana.getStrOpcionDescripcionSelected() + "_" + Ventana.getStrDateFileName() + ".xls");

            //****Asignar en la variable    strFileExcelAgentes    la ruta donde se encuentra el archivo      agentes.xls **********
        	String strRutaAgentesXLS = Ventana.class.getProtectionDomain().getCodeSource().getLocation().toString();
    		System.out.println("agentesagentes (" + strRutaAgentesXLS + ")");
        	String[] elemsFileNameRutaTotalAgentes = strRutaAgentesXLS.split("file:/");
    		String strRutaTotalAgentes = elemsFileNameRutaTotalAgentes[1].toString();
    		int lengthCadenaRutaAgentes = strRutaTotalAgentes.length();
    		int intPosicionFileNameAgentes = strRutaTotalAgentes.toString().lastIndexOf("/");
    		String strLocalizacionAgentes = strRutaTotalAgentes.substring(0, intPosicionFileNameAgentes+1);   		
    		String strRutaCalculadaAgentes = strLocalizacionAgentes + "agentes.xls";
//cotrebla
    		System.out.println("Separador:"+File.separator);
    		System.out.println("Ruta calculada :"+strRutaCalculadaAgentes);
    		            
            //**********************************************************************************************************************
//          Ventana.setStrFileExcelAgentes(Ventana.getStrRutaDestinoArchivos() + "Omnex_" + Ventana.getStrOpcionBancoSelected() + "_" + Ventana.getStrOpcionDescripcionSelected() + "_" + Ventana.getStrDateFileName() + ".xls");
            Ventana.setStrFileExcelAgentes(strRutaCalculadaAgentes);
            //**********************************************************************************************************************
        	System.out.println("rutaExcel: (" + Ventana.getStrRutaDestinoArchivos() + "Omnex_" + Ventana.getStrOpcionBancoSelected() + "_" + Ventana.getStrOpcionDescripcionSelected() + "_" + Ventana.getStrDateFileName() + ".xls)");
        	libro.write(elFichero);
            
            elFichero.close();
        } catch (Exception e) {
            //e.printStackTrace();
//			System.out.println("*********************************************************************************************************************** catch e");
            return false;
        }
		return true;
    }
	
	public static double redondearDecimales(double valorInicial, int intNumeroDecimales) {
    	double parteEntera, resultado;
        resultado = valorInicial;
        parteEntera = Math.floor(resultado);
        
        resultado=(resultado-parteEntera)*Math.pow(10, intNumeroDecimales);
        resultado=Math.round(resultado);
        resultado=(resultado/Math.pow(10, intNumeroDecimales))+parteEntera;
        return resultado;
    }
	
	public static String obtenerImporteFormatoOmnex(String strImp){	

		System.out.println(">>>>>>> Comienza obtener..... strImp: " + strImp);

		String strImporteResultante = strImp;
    	int intPosicionPunto = -1;
    	int intPosicionComa = -1;
    	
		int intLongitudImporte = 0;
		int intDecimalesDespuesPunto = 0;
		int intDecimalesDespuesComa = 0;
  
		String strResultadoImporte = "";
		String strInicioImporte = "";
		String strFinal_tresCaracteres = "";
/*
1.-		PuntoNo		ComaNo
			el mismo
2.-		PuntoNo		Comasi
			si despues de la coma hay mas de 2 caracteres se sustituye por punto.
3.-		PuntoSi		ComaNo
   			si despues del punto hay menos de 3 caracteres se sustituye por coma.
4.-		PuntoSi		ComaSi
   			si primero se encuentra la coma entonces se sustituye uno por el otro.
*/		
    	intPosicionPunto = strImp.indexOf(".");
   		intPosicionComa = strImp.indexOf(",");
		System.out.println("**************************(" + intPosicionPunto + ")("+ intPosicionComa+")" + strImp);

//1.-		
//		if (intPosicionPunto<0 && intPosicionComa<0){
// 			strImporteResultante = strImp;   			
// 		}
//2.-
		if (intPosicionPunto<0 && intPosicionComa>-1){
			System.out.println("*** Entra por: (no) (si)");
			intLongitudImporte = strImp.length();
   			intDecimalesDespuesComa = intLongitudImporte - intPosicionComa - 1;//Se suma uno porque la posicion empieza en CERO y el CERO restaria uno.
   			System.out.println("*** Valores: ----- intDecimalesDespuesComa = intLongitudImporte - intPosicionComa (" + intDecimalesDespuesComa + " = " + intLongitudImporte + " - " + intPosicionComa + ")");
   			if (intDecimalesDespuesComa>2){
   				System.out.println("*** ademas ----------------------> decimales despues de coma MAYOR de 2");   				
   				strImporteResultante = strImp.replace(",", ".");
   			}
   		}
//3.-
		if (intPosicionPunto>-1 && intPosicionComa<0){
			System.out.println("*** Entra por: (si) (no)");
   			intLongitudImporte = strImp.length();
   			intDecimalesDespuesPunto = intLongitudImporte - intPosicionPunto - 1;//Se resta 1 porque la posicion empieza en CERO y el CERO restaria uno.
			System.out.println("*** Valores: ----- intDecimalesDespuesPunto = intLongitudImporte - intPosicionPunto (" + intDecimalesDespuesPunto + " = " + intLongitudImporte + " - " + intPosicionPunto + ")");
   			if (intDecimalesDespuesPunto<3){
   				System.out.println("*** ademas ----------------------> decimales despues de punto MENOR de 3");   				
   				strImporteResultante = strImp.replace(".", ",");
   			}		
		}
//4.-
		if (intPosicionPunto>-1 && intPosicionComa>-1){
			System.out.println("*** Entra por: (si) (si)");
			if(intPosicionComa<intPosicionPunto){
   				System.out.println("*** ademas ----------------------> Aparece coma antes que punto.");   								
				strImporteResultante = strImp.replace(".", ";");
				strImporteResultante = strImporteResultante.replace(",", ".");
				strImporteResultante = strImporteResultante.replace(";", ",");
			}
		}
		System.out.println("valor de importe **** y Resultado obtenido *********: (" + strImp + ") (" + strImporteResultante + ")");
		System.out.println("**********************************************************************************************************");
		//************************************* Ahora consultamos si es un importe con millares y no tiene el punto de millar*********************
		//		Contamos desde la izquierda cuantos caracteres hay hasta que encuentre fin de cadena, punto o coma.		
		//		Si hay mas de 3 caracteres:	  asignamos los ultimos caracteres a:				strFinal_tresCaracteres
		//									y asignamos los caracteres iniciales restantes a:	strInicioImporte
		//		En una variable 		strResultadoImporte		asignamos:				strInicioImporte + "." + strFinal_tresCaracteres; 
		strResultadoImporte = strImporteResultante;
		
		boolean booleanConsultarPuntosDeMillar = true;
		
		//int intContCaracteresIzquierdaAbsoluto 			es la parte de la izquierda sin el signo.
		int intContCaracteresIzquierdaAbsoluto = 0;
		int intContCaracteresIzquierdaTotal = 0;
		int intContCaracteresIzquierdaSigno = 0;
		
		int intLongitudTotalCadena = 0;
		
		System.out.println(">>>>>>> while " + strImp);
		System.out.println(">>>>>>> while 1" + booleanConsultarPuntosDeMillar);

		//Mientras que haya que consultar si hay que a�adir puntos de millar.-----------------------------------------------------
		while (booleanConsultarPuntosDeMillar) {
			intLongitudTotalCadena = strResultadoImporte.length();
			intContCaracteresIzquierdaAbsoluto = 0;
			intContCaracteresIzquierdaTotal = 0;
			intContCaracteresIzquierdaSigno = 0;
			
//			System.out.println(">>>>>>> while 2       strResultadoImporte (" + strResultadoImporte + ")");
//			
//			System.out.println(">>>>>>> while 3 " + intLongitudTotalCadena);

			//Contar caracteres por la izquierda para evaluar millares.
			for(int intC = 0 ; intC<intLongitudTotalCadena ; intC++){
//				System.out.println(">>>>>>> entra en el for (" + intC + ")(" + intLongitudTotalCadena + ")");
//				System.out.println(">>>>>>> entra en el for caracter es: (" + strResultadoImporte.substring(intC, intC+1) + ")");

				//Si el caracter consultado NO es punto NI coma,   contabilizamos.
				if ((!strResultadoImporte.substring(intC, intC+1).equals(".")) && (!strResultadoImporte.substring(intC, intC+1).equals(","))){
					//Si no es caracter signo puntuaci�n:	contabilizamos en intContCaracteresIzquierdaAbsoluto.
					if ((!strResultadoImporte.substring(intC, intC+1).equals("+")) && (!strResultadoImporte.substring(intC, intC+1).equals("-"))){
						intContCaracteresIzquierdaAbsoluto++;
					}else{
						intContCaracteresIzquierdaSigno++;
					}
//					System.out.println(">>>>>>> No ha encontrado punto o coma. Posicion: " + intC + " *** intContCaracteresIzquierdaAbsoluto: " + intContCaracteresIzquierdaAbsoluto);
				}else{
//					System.out.println(">>>>>>> Ha encontrado punto o coma. Posicion: " + intC);					
					break;
				}
//				System.out.println(">>>>>>>fin iteracion del for  strResultadoImporte: " + strResultadoImporte);
			}
			
			intContCaracteresIzquierdaTotal = intContCaracteresIzquierdaAbsoluto + intContCaracteresIzquierdaSigno;
			
			//Si ha contabilizado m�s de tres caracteres,   se a�ade el punto de millas donde corresponda.
//			System.out.println(">>>>>>> strResultadoImporte: " + strResultadoImporte);			
//			System.out.println(">>>>>>> intContCaracteresIzquierdaAbsoluto: " + intContCaracteresIzquierdaAbsoluto);			
//			System.out.println(">>>>>>> intContCaracteresIzquierdaSigno: " + intContCaracteresIzquierdaSigno);

			if (intContCaracteresIzquierdaAbsoluto>3){
//				System.out.println("A   >>>> 0 >>> intContCaracteresIzquierdaAbsoluto: " + intContCaracteresIzquierdaAbsoluto);
//				System.out.println("A   >>>> 0 >>> intContCaracteresIzquierdaSigno: " + intContCaracteresIzquierdaSigno);
				strInicioImporte = strResultadoImporte.substring(0, intContCaracteresIzquierdaTotal - 3);
				strFinal_tresCaracteres = strResultadoImporte.substring(intContCaracteresIzquierdaTotal - 3, intLongitudTotalCadena);
				strResultadoImporte = strInicioImporte + "." + strFinal_tresCaracteres;
//				System.out.println(">>>> 0 >>> strResultadoImporte: " + strResultadoImporte);
//				System.out.println(">>>> 1 >>> strInicioImporte: " + strInicioImporte);
//				System.out.println(">>>> 2 >>> strFinal_tresCaracteres: " + strFinal_tresCaracteres);
//				System.out.println(">>>> 3 >>> strResultadoImporte: " + strResultadoImporte);
			}
			//Si no ha contabilizado m�s de 6 caracteres,   no hay que a�adir m�s puntos de millar y salimos del while.
//			System.out.println(">>>>>>> intContCaracteresIzquierda: " + intContCaracteresIzquierdaAbsoluto);
			if(intContCaracteresIzquierdaAbsoluto<7){
				booleanConsultarPuntosDeMillar = false;
			}
//			System.out.println(">>>>>>> booleanConsultarPuntosDeMillar: " + booleanConsultarPuntosDeMillar);
//			System.out.println(">>>>>>> endwhile");
		}

//		System.out.println(">>>>>>> *** strImporteResultante (" + strImporteResultante +")");

		strImporteResultante = strResultadoImporte;
		return strImporteResultante;		
    }
}
