import java.io.IOException;
import java.util.ArrayList;


public class Ajuste {

	public String validarTiposDeDatosDelFormularioDeAjuste (String strCodAgente, String strImporte, String strDatos){
		String strRetorno = "";
		Integer intCodAgente = 0;		
		double doubleImporte = 0;
				
		if (strCodAgente.length()<4 || strCodAgente.length()>5){
			strRetorno = "Error *** El valor del c�digo de agente debe ser de 4 � 5 caracteres.";			
		}
		if (strRetorno.equals("")){
			try {
				intCodAgente = Integer.parseInt(strCodAgente);
			} catch (NumberFormatException e) {
				strRetorno = "Error *** El valor del c�digo de agente debe ser num�rico (" + strCodAgente + ")";			
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (strRetorno.equals("")){
			doubleImporte = 0;
			try {
				doubleImporte = Double.parseDouble(strImporte);
			} catch (NumberFormatException e) {
				strRetorno = "Error *** El valor del importe debe ser num�rico " + strImporte;			
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return strRetorno;
	}
	
//	public String comprobarAjustesSeleccionadosConDatosHistorial (String strCodAgente, int intImporte, String strDatos){
	public String comprobarAjustesSeleccionadosConDatosHistorial (){
		String strRetorno = "";
		System.out.println("20171002 procesosDeComprobaciones");			
    	
		LogsFile ficheroLog = new LogsFile();
//		try {
//			ficheroLog.InfoLog("comprobarAjustesSeleccionadosConDatosHistorial", "1procesosDeComprobaciones_comprobarAjustesSeleccionadosConDatosHistorial");
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
		strRetorno = Ventana.procesosDeComprobaciones();
		return strRetorno;
	}
	
	public void mostrarAjustesAcumulados (){
		Ventana.setNoVisibleTxtAreaAjustesAlmacenados();
		
		ArrayList<ArrayList<String>> arrayListAcumulados = Ventana.getArrayListAjustesAlmacenados();

		Ventana.setTxtAreaAjustesAlmacenados("");
		if (arrayListAcumulados.size()>0) {
			int intContAjustes = 0;

			Ventana.setVisibleTxtAreaAjustesAlmacenados();
			Ventana.acumularTxtAreaAjustesAlmacenados("\n" + " " + arrayListAcumulados.size() + " Movimientos de ajuste a�adidos:" + "\n");
			System.out.println("Antes ----for");
			for (int i = 0; i < arrayListAcumulados.size(); i++) {
				intContAjustes++;
				System.out.println(" ----for");
				Ventana.acumularTxtAreaAjustesAlmacenados("\n" + intContAjustes + "� Cod.Agente: " + arrayListAcumulados.get(i).get(0).toString() + " Importe: " + arrayListAcumulados.get(i).get(1).toString());
				if (!(arrayListAcumulados.get(i).get(2).toString().equals(""))){
					Ventana.acumularTxtAreaAjustesAlmacenados(" Datos: " + arrayListAcumulados.get(i).get(2).toString());	
				}
			}
			System.out.println("Despues ----for");			
		}else{
			Ventana.setNoVisibleTxtAreaAjustesAlmacenados();
		}
	}
}
