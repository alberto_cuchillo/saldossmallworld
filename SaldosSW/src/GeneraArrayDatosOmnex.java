import java.io.IOException;
import java.util.ArrayList;

public class GeneraArrayDatosOmnex {
	private ArrayList<ArrayList<String>> arrayBanco;
	private static LogsFile ficheroLog = new LogsFile();
	private static String strMensajeLog = "";
	private static String strNombreLog = "";
	
	public GeneraArrayDatosOmnex(ArrayList<ArrayList<String>> arrayExcelBanco) {
		this.arrayBanco = arrayExcelBanco;
	}
	
	public ArrayList<ArrayList<String>> generar() {
		
		LogsFile logsFileFichero = new LogsFile();
/*		
		for(int contEle=0;contEle<arrayBanco.size();contEle++){
//			System.out.println("223344arrayBanco:" + arrayBanco.get(contEle));
			try {
				logsFileFichero.InfoLog(arrayBanco.get(contEle).toString(), "Datos_Para_Omnex");
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} 
		}
*/			
			
//		String strAgenteLocalizado = "localizadoFalse";
		String strAgenteLocalizado = "0";
		
		ArrayList<ArrayList<String>> arrayDatosOmnex = new ArrayList<ArrayList<String>>();
		
		for(int cont=0;cont<arrayBanco.size();cont++){
			strAgenteLocalizado  = localizarIdAgente(arrayBanco.get(cont).get(0));
//			System.out.println("223344strAgenteLocalizado cont:" + cont);
//			System.out.println("223344strAgenteLocalizado:" + strAgenteLocalizado);
//			System.out.println("arrayBanco223344strAgenteLocalizado:" + arrayBanco.get(cont).toString());
			
//			System.out.println(" d0920 : " + arrayBanco.toString());
			
			ArrayList<String> datos = new ArrayList<>();
			datos.add(0, arrayBanco.get(cont).get(0));
			datos.add(1, arrayBanco.get(cont).get(1));
			datos.add(2, strAgenteLocalizado);
			datos.add(3, arrayBanco.get(cont).get(2));
			datos.add(4, arrayBanco.get(cont).get(4));
			datos.add(5, arrayBanco.get(cont).get(5));
			arrayDatosOmnex.add(cont, new ArrayList(datos));
		}	
		return arrayDatosOmnex;
	}

	public String localizarIdAgente(String valor) {
		
		int intLongitudValor = 0;
		intLongitudValor = valor.length();
		boolean booleanEsNumerico = true;
		String strAgente = "";
//		String strAgenteLocalizado = "localizadoFalse";
		String strAgenteLocalizado = "0";
		
		int intLongitudCapturada = 0;
		int contAgentesLocalizados = 0;
		for(int cont=0;cont<intLongitudValor;cont++){
			String charCaracter = valor.substring(cont, cont+1);

			booleanEsNumerico = true;
			try {
				Integer.valueOf(charCaracter);
			} catch (NumberFormatException e) {
				//e.printStackTrace();();
//				System.out.println("*********************************************************************************************************************** catch e");
				booleanEsNumerico = false;
			}			

			if(booleanEsNumerico){
				intLongitudCapturada++;
//peto			if (intLongitudCapturada>6){
//peto				strAgente = "";	
//peto				intLongitudCapturada = 0;
//peto			}else{					
					strAgente = strAgente + charCaracter;
					//**************** Escribir el log
//peto			}				
			}else{
				if (intLongitudCapturada == 4 || intLongitudCapturada == 5){
					strAgenteLocalizado = strAgente;
					contAgentesLocalizados++;
				}
				strAgente = "";
				intLongitudCapturada = 0;				
			}		
			
		}
		if (intLongitudCapturada == 4 || intLongitudCapturada == 5){
			strAgenteLocalizado = strAgente;
			contAgentesLocalizados++;			
		}		
		if (contAgentesLocalizados>1){
			strAgenteLocalizado = "0";
		}		
		return strAgenteLocalizado;
	}
}
