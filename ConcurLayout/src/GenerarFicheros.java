import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
 
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.omg.CORBA.StructMemberHelper;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class GenerarFicheros {

	//Este array contiene los mensajes que se mostrar�n en Ventana cuando finalice el proceso.
	private static ArrayList<String> arrayReturnVentana = new ArrayList<String>();

	private static String strRutaConcurModificada = "";

	private static String strEmailConcur = "";

	private static String strMonedaGBP = "gbp";
	private static String strMonedaEUR = "eur";

	private static ArrayList<String> arrayConcurCSV = new ArrayList<String>();
	private static ArrayList<String> arrayNuevoConcurCSV = new ArrayList<String>();
	private static ArrayList<String> arrayValidosCSV = new ArrayList<String>();
	
	private static ArrayList<String> arrayValidosDetalleXLSX = new ArrayList<String>();
	private static ArrayList<String> arrayLogsDetalleXLSX = new ArrayList<String>();
	
	private static ArrayList<String> arrayValidosCSVAgrupados = new ArrayList<String>();
	private static ArrayList<String> arrayValidosCSVAgrupadosRedondeado = new ArrayList<String>();
	private static ArrayList<String> arrayValidosCSVAgrupadosRedondeadoLayout = new ArrayList<String>();
	
	private static ArrayList<String> arrayLayOutGenerado = new ArrayList<String>();
	
	private static Logs_Exceptions excepciones = new Logs_Exceptions();
	private static String strExcepcion = "";
	
	private static LogsFile_Online fileLogOnline = new LogsFile_Online();
	
    @SuppressWarnings("deprecation")
    
    public GenerarFicheros(){

    }
    
    public static void GenerarSalida(String sRutaConcur, String sRutaDetalleCC) throws IOException{

		fileLogOnline.InfoLog("GenerarFicheros" + "_GenerarSalida_sRutaConcur(in:" + sRutaConcur + ")_sRutaDetalleCC(in:" + sRutaDetalleCC + ")", "00_CSVfile");		

		Calendar cal = Calendar.getInstance();
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
	    String strDate = sdf.format(cal.getTime());
	    
		//----------------------------Logs_Exceptions--------------grabar excepciones.
		ArrayList<String> arrayExcepciones = new ArrayList<String>();
		
		String sRutaConcurModificada = sRutaConcur.replace("\\", "\\\\");
		FileInputStream file = new FileInputStream(new File(sRutaConcurModificada));
		
		//------------------------------- Preparamos la ruta donde se deber� crear el archivo Layout ----- rutaLayOut----
		//-----------Desde esa ruta, posteriormente    se crear� otra carpeta   para guardar los layout------------------
		//-----------                                y se crear� otra carpeta   para guardar los logs  ------------------
	    int lengthCadenaRuta = sRutaConcurModificada.length();

	    int posLastCaracteres = sRutaConcurModificada.lastIndexOf( '\\' );
	    int numCaracteres = lengthCadenaRuta - posLastCaracteres;
		String rutaLayOut = sRutaConcurModificada.substring(0, posLastCaracteres+1);

	    System.out.println("1  setStrRutaLayOut rutaLayOut: " + rutaLayOut);
	    System.out.println("1  setStrRutaLayOut Ventana.getStrRutaLayOut(): " + Ventana.getStrRutaLayOut());
	    System.out.println("1  setStrRutaLayOut sRutaConcurModificada: " + sRutaConcurModificada);
	    System.out.println("1  setStrRutaLayOut");
	    System.out.println("1  setStrRutaLayOut");
	    System.out.println("1  setStrRutaLayOut");
	    System.out.println("1  setStrRutaLayOut");
	    System.out.println("1  setStrRutaLayOut");
	    
		//------------------------------- Lectura del CSV ---------------------------------------------------------------
		CsvGeneraArray arrayCSV = new CsvGeneraArray();
		arrayConcurCSV = arrayCSV.getArrayCSV(sRutaConcurModificada);
		
		//--- Excepciones -----------------
		strExcepcion = "1.- **************************************** arrayConcurCSV";		
		for(int contEle = 0;contEle<arrayConcurCSV.size();contEle++){
			strExcepcion = "1.- (" + contEle + ")" + arrayConcurCSV.get(contEle);
			excepciones.InfoLog(strExcepcion);
		}
		//---------------------------------	
		
		int sizeArrayConcurCSV = arrayConcurCSV.size();

		//----El metodo  generarArrayValidosCSV()   nos devuelve un array con las filas del CSV         sin incidencias detectadas de momento.
		//-- ese metodo  generarArrayValidosCSV() a su vez   escribir� en arrayLogsCSV   (array de la Clase) las incidencias que haya encontrado.

		arrayValidosCSV = generarArrayValidosCSV(arrayConcurCSV);
	
		//--- Excepciones -----------------
		strExcepcion = "2.- **************************************** arrayValidosCSV";
		for(int contEle = 0;contEle<arrayValidosCSV.size();contEle++){
			strExcepcion = "2.- (" + contEle + ")" + arrayValidosCSV.get(contEle);
//			excepciones.InfoLog(strExcepcion, rutaLayOut, strDate);
			excepciones.InfoLog(strExcepcion);			
		}
		//---------------------------------

		
		//----------------------------fin Lectura del CSV ---------------------------------------------------------------
		
		ArrayList<String> arrayResultadoDetalleCC = new ArrayList<String>();
		
		double importeTotal = 0;
		
		boolean aceptarFila;

		int contInicio = 0;

		String textoLog; 

		//Collections.sort(array1ValidosCSV);
		Collections.sort(arrayValidosCSV);

		//***** Desde el array arrayValidosCSV   se leer�   para agrupar por email/moneda     y se acumular� el importe para ese grupo (arrayValidosCSVAgrupados)
		arrayValidosCSVAgrupados = acumularEmailMoneda(arrayValidosCSV);
		
		//--- Excepciones -----------------
		strExcepcion = "3.- **************************************** arrayValidosCSVAgrupados";
		for(int contEle = 0;contEle<arrayValidosCSVAgrupados.size();contEle++){
			strExcepcion = "3.- (" + contEle + ")" + arrayValidosCSVAgrupados.get(contEle);
//			excepciones.InfoLog(strExcepcion, rutaLayOut, strDate);
			excepciones.InfoLog(strExcepcion);		
		}
		//---------------------------------	
		//**************** Aplicar redondeo desde arrayValidosCSVAgrupados en arrayValidosCSVAgrupadosRedondeado *****************
		arrayValidosCSVAgrupadosRedondeado = aplicarRedondeo(arrayValidosCSVAgrupados);
		System.out.println("*********************************************************** size arrayValidosCSVAgrupadosRedondeado: " + arrayValidosCSVAgrupadosRedondeado.size());

		//--- Excepciones -----------------
		strExcepcion = "4.- **************************************** arrayValidosCSVAgrupadosRedondeado";
		for(int contEle = 0;contEle<arrayValidosCSVAgrupadosRedondeado.size();contEle++){
			strExcepcion = "4.- (" + contEle + ")" + arrayValidosCSVAgrupadosRedondeado.get(contEle);
//			excepciones.InfoLog(strExcepcion, rutaLayOut, strDate);
			excepciones.InfoLog(strExcepcion);		
		}
		//---------------------------------	
		//---------Instancia de 'XlsxGeneraArray.	---------------------------------
		//---------     Se comprueban los valores:  Email, IBAN, Swift.
		//					Si la l�nea del archivo de DetalleCC es v�lida,   lo graba en un array   arrayValidosDetalleXLSX 
		//						Las l�neas donde encuentre incidencias las grabar� en un archivo log de DetalleCC.
		XlsxGeneraArray gDetalle2 = new XlsxGeneraArray(sRutaDetalleCC);
		arrayValidosDetalleXLSX = gDetalle2.generar();
		
		strExcepcion = "4_b ****************************** arrayValidosDetalleXLSX size: " + arrayValidosDetalleXLSX.size();
		excepciones.InfoLog(strExcepcion);		

		//--- Excepciones -----------------
		strExcepcion = "5.- **************************************** arrayValidosDetalleXLSX";
		for(int contEle = 0;contEle<arrayValidosDetalleXLSX.size();contEle++){
			strExcepcion = "5.- (" + contEle + ")" + arrayValidosDetalleXLSX.get(contEle);
//			excepciones.InfoLog(strExcepcion, rutaLayOut, strDate);
			excepciones.InfoLog(strExcepcion);		
		}

		strExcepcion = "5a.- arrayValidosCSVAgrupadosRedondeado size: " + arrayValidosCSVAgrupadosRedondeado.size();
		excepciones.InfoLog(strExcepcion);

		//---------------------------------				
		//**************** Leer el array arrayValidosCSVAgrupadosRedondeado
		// y comprobar si las cuentas de correo que contiene se encuentran en el array de Detalle:		arrayValidosDetalleXLSX
		//Las lineas de arrayValidosCSVAgrupadosRedondeado	que haya encontrado email, se escriben en el array	----->	arrayValidosCSVAgrupadosRedondeadoLayout
		//Las lineas de arrayValidosCSVAgrupadosRedondeado	que no haya encontrado email, se escriben en el array	----->	arrayLogsCSV
		
		arrayValidosCSVAgrupadosRedondeadoLayout = generarConcurEmailsValidos(arrayValidosCSVAgrupadosRedondeado, arrayValidosDetalleXLSX);
	
		//--- Excepciones -----------------
		strExcepcion = "6.- **************************************** arrayValidosCSVAgrupadosRedondeadoLayout";
//		excepciones.InfoLog(strExcepcion, rutaLayOut, strDate);
		excepciones.InfoLog(strExcepcion);
		
		for(int contEle = 0;contEle<arrayValidosCSVAgrupadosRedondeadoLayout.size();contEle++){
			strExcepcion = "6.- (" + contEle + ")" + arrayValidosCSVAgrupadosRedondeadoLayout.get(contEle);
//			excepciones.InfoLog(strExcepcion, rutaLayOut, strDate);
			excepciones.InfoLog(strExcepcion);		
		}
		//---------------------------------	
		//--------------------------------------------- Grabar plantilla con LayOut con el formato que haya seleccionado el usuario.
		GenerarArrayLayout gFormato = new GenerarArrayLayout();
		
		//***** Escribir log ------------------------
		for(int contEle = 0;contEle<arrayValidosCSVAgrupadosRedondeadoLayout.size();contEle++){
			fileLogOnline.InfoLog("GenerarFicheros" + "_GenerarSalida_arrayValidosCSVAgrupadosRedondeadoLayout_AntesDe(createArrayLayOut): " + arrayValidosCSVAgrupadosRedondeadoLayout.get(contEle).toString(), "00_CSVfile");
		}	
		//****
		
//		ArrayList<String> arrayFormatoSeleccionado = gFormato.createArrayLayOut(arrayValidosCSVAgrupadosRedondeadoLayout, strMonedaEUR);
		ArrayList<String> arrayFormatoSeleccionado = gFormato.createArrayLayOut(arrayValidosCSVAgrupadosRedondeadoLayout, Ventana.getStrMonedaSeleccionada());

		//***** Escribir log ------------------------
		for(int contEle = 0;contEle<arrayFormatoSeleccionado.size();contEle++){
			fileLogOnline.InfoLog("GenerarFicheros" + "_GenerarSalida_arrayFormatoSeleccionado_createArrayLayOut: " + arrayFormatoSeleccionado.get(contEle).toString(), "00_CSVfile");		
		}	
		//****
		
		if (arrayFormatoSeleccionado.size()>0){
			fileLogOnline.InfoLog("GenerarFicheros" + "_GenerarSalida_arrayFormatoSeleccionado_ es MAYOR que CERO", "00_CSVfile");		
			if (Ventana.getStrMonedaSeleccionada().toString()=="eur"){
				fileLogOnline.InfoLog("GenerarFicheros" + "_GenerarSalida_arrayFormatoSeleccionado_ es MAYOR que CERO y EUR", "00_CSVfile");		
				//--------------------------------------------- Grabar plantilla con LayOut para CaixaGrals.
				CaixaGeralLayoutcreator fileLayOutCaixaGral = new CaixaGeralLayoutcreator(); 
				fileLayOutCaixaGral.createLayOut(arrayFormatoSeleccionado, "CaixaGral", rutaLayOut);
			}
			if (Ventana.getStrMonedaSeleccionada().toString()=="gbp"){
				fileLogOnline.InfoLog("GenerarFicheros" + "_GenerarSalida_arrayFormatoSeleccionado_ es MAYOR que CERO y GBP", "00_CSVfile");		
				//--------------------------------------------- Grabar plantilla con LayOut para Barclays.
				BarclaysPegasusLayoutcreator fileLayOutBarclaysPegasus = new BarclaysPegasusLayoutcreator();
				fileLayOutBarclaysPegasus.createLayOut(arrayFormatoSeleccionado, "Barclays_Pegasus", rutaLayOut);
			}
		}
	}

	private static ArrayList<String> acumularEmailMoneda(ArrayList<String> arrayIn) {
		// TODO Auto-generated method stub		
		String valorGrupoEmailMoneda = "";
		String importeString = "";
		double importeNumerico = 0;
		double acumuladoImporte = 0;
			
		ArrayList<String> arrayResultadoAcumular = new ArrayList<String>();
			
		String AnteriorEmail = "";
		String AnteriorMoneda = "";
		String AnteriorGrupo = "";
		String AnteriorNombre = "";
		String AnteriorRowNumber = "";
	
		int sizearrayIn = arrayIn.size();
		for(int cont = 0;cont<sizearrayIn;cont++){
				
			String[] splitRow = arrayIn.get(cont).split("<>");
			valorGrupoEmailMoneda = splitRow[0].toString();
			importeString = splitRow[1].toString();
				
			String[] splitImporte = importeString.split("��");
			String valorImporteSplit = splitImporte[0].toString();
			importeNumerico = Double.parseDouble(valorImporteSplit);
	
			String valorNombreSplit = splitImporte[1].toString();
			String valorRowNumberSplit = splitImporte[2].toString();
			// valorRowNumberSplit:		es el valor de la fila en el CSV donde se encontraba ese gasto.
			// 					Se utilizar� para generar un identificador en cada registro de la plantilla.
			// 					(yyyyMMdd_HHmmss_       valorRowNumberSplit
	
			//Si el row leido es del mismo grupo que el anterior:   debemos acumular
			if (valorGrupoEmailMoneda.equals(AnteriorGrupo)){
				acumuladoImporte = acumuladoImporte + importeNumerico;
			}else{
			//Si el row leido        tiene un grupo diferente:
			//	insertamos en el array    si no es el primer row (el primero row AnteriorGrupo ser�a "")
				if(!AnteriorGrupo.equals("")){
					arrayResultadoAcumular.add(AnteriorEmail + "!!" + AnteriorMoneda + "<>" + acumuladoImporte + "��" + AnteriorNombre + "��" + AnteriorRowNumber);
				}

				String[] splitGrupo = valorGrupoEmailMoneda.split("!!");

				AnteriorGrupo = valorGrupoEmailMoneda;
				AnteriorEmail =  splitGrupo[0].toString();
				AnteriorMoneda =  splitGrupo[1].toString();
				AnteriorNombre =  valorNombreSplit;
				AnteriorRowNumber =  valorRowNumberSplit;

//			    double acumuladoImporteRedondeado = Math.round(acumuladoImporte * 100.0)/100.0;
				acumuladoImporte = importeNumerico;
			}
			//Si el row leido        es el ultimo: insertamos en el array.
			if((cont+1)==sizearrayIn){
//				arrayResultadoAcumular.add(AnteriorEmail + "!!" + AnteriorMoneda + "<>" + acumuladoImporte + "��" + valorNombreSplit);
				arrayResultadoAcumular.add(AnteriorEmail + "!!" + AnteriorMoneda + "<>" + acumuladoImporte + "��" + AnteriorNombre + "��" + AnteriorRowNumber);				
			}			
		}
			
		
		
		
		
		return arrayResultadoAcumular;
	}

	private static int controlarTipoMoneda (String tipo){

		tipo = tipo.toLowerCase();
		switch (tipo.toString()) {
			case "eur":
				return 1;
			case "gbp":
				return 2;
			default:
				return 0;
		}		
	}
	
	private static boolean consultarEmailenCC (String sConcur, String sCC){

		String[] splitConcur = sConcur.split("!!");
		String sEmailConcur = splitConcur[0].toString();

		String[] splitCC = sCC.split("<>");
		String sEmailCC = splitCC[0].toString();
		
		if (sEmailConcur.equals(sEmailCC)){
			try {
				fileLogOnline.InfoLog("GenerarFicheros" + "_consultarEmailenCC_sConcur:" + sConcur + "_sCC:" + sCC, "00_CSVfile");
				fileLogOnline.InfoLog("GenerarFicheros" + "_consultarEmailenCC_: TRUE Email igual", "00_CSVfile");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			return true;			
		}
		try {
			fileLogOnline.InfoLog("GenerarFicheros" + "_consultarEmailenCC_sConcur:" + sConcur + "_sCC:" + sCC, "00_CSVfile");
			fileLogOnline.InfoLog("GenerarFicheros" + "_consultarEmailenCC_: FALSE Email distinto", "00_CSVfile");			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		return false;
	}

	private static String consultarIbanEnCC (String sCC){
		
		String[] splitCC = sCC.split("<>");
		String sIbanCC = "";
		sIbanCC = splitCC[1].toString();
		
		if (sIbanCC.equals("")){
			return "";
		}
		return sIbanCC;
	}	

	private static String consultarSwiftEnCC (String sCC){
		
		String[] splitCC = sCC.split("<>");
		String sSwiftCC = splitCC[2].toString();

		if (sSwiftCC.equals("")){
			return "";
		}
		return sSwiftCC;
	}	
	
	private static ArrayList<String> generarArrayValidosCSV (ArrayList<String> arrayCSV){
	
//		LogsFile_Online fileLogOnline = new LogsFile_Online();

		ArrayList<String> array1 = new ArrayList<String>();
		
		double importeTotal = 0;
		
		boolean aceptarFila;

		int contInicio = 0;
//		int contRows = sheet.getLastRowNum();
		int contRows = arrayCSV.size();

		String textoLogCeldaCSV0; 
		String textoLogCeldaCSV7; 
		String textoLogCeldaCSV8; 
		String textoLogCeldaCSV10; 
		String textoLogCeldaCSV11; 
		String textoLogCeldaCSV12; 
		String textoLogCSV; 		

		//--------------- Inicio del for	-   para evaluar todos los rows del Excel ------------------
		for(contInicio=0;contInicio<contRows;contInicio++){

			String[] splitFilaCSV = arrayCSV.get(contInicio).split("<>");
			String sTipoGastoCSV = splitFilaCSV[0].toString();
			String sMonedaCSV = splitFilaCSV[1].toString();
			String sAmountCSV = splitFilaCSV[2].toString();
			String sApellidoCSV = splitFilaCSV[3].toString();
			String sNombreCSV = splitFilaCSV[4].toString();
			String sEmailCSV = splitFilaCSV[5].toString();
			String sFilaCSV = splitFilaCSV[6].toString();
			
			textoLogCeldaCSV0 = ""; 
			textoLogCeldaCSV7 = ""; 
			textoLogCeldaCSV8 = ""; 
			textoLogCeldaCSV10 = ""; 
			textoLogCeldaCSV11 = ""; 
			textoLogCeldaCSV12 = ""; 
			textoLogCSV = ""; 

			aceptarFila = true;
			
//			if (sheet.getRow(contInicio).getCell(0) == null){
			if (sTipoGastoCSV == null){
    			aceptarFila = false;    			
	    		//Escribir en un log.
    			textoLogCeldaCSV0 = "***** Valor de 'Report Entry Payment Type Name' es: null."; 
			}else{
//				if (sheet.getRow(contInicio).getCell(0).toString().equals("Cash/Personal Card")){
				if (sTipoGastoCSV.toString().equals("Cash/Personal Card")){
					//Si obtenemos los valores: Moneda, Importe, Email
					//		entonces aceptamos la fila para tratarla posteriormente.
//					if (sheet.getRow(contInicio).getCell(7) == null){
					if (sMonedaCSV == null){
						aceptarFila = false;
		    			textoLogCeldaCSV7 = "***** Valor de 'Report Entry Currency Alpha Code' es: null";
//						System.out.println("no se acepta el valor de fila: " + contInicio + "- celda 7");
						//Escribir en un log.
					}else{
						//--- El tipo de moneda debe ser alguno de los que est�n en el listado de monedas. ---------------------------------------
						int respTipoMoneda = 0;
//						respTipoMoneda = controlarTipoMoneda(sheet.getRow(contInicio).getCell(7).toString());							
						respTipoMoneda = controlarTipoMoneda(sMonedaCSV.toString());							
						if (respTipoMoneda==0){
			    			textoLogCeldaCSV7 = "***** (" + sMonedaCSV.toString() + ") Tipo de moneda no permitido.";
							aceptarFila = false;
						}
					}	
					if (sAmountCSV == null){
						aceptarFila = false;    			
		    			textoLogCeldaCSV8 = "***** Valor de 'Journal Amount' es: null";
//						System.out.println("no se acepta el valor de fila: " + contInicio + "- celda 8");
						//Escribir en un log.
					}
					if (sApellidoCSV == null){
						aceptarFila = false;
		    			textoLogCeldaCSV10 = "***** Valor de 'Employee Last Name' es: null";
					}else{
						if (sApellidoCSV.toString() == ""){
							aceptarFila = false;
			    			textoLogCeldaCSV10 = "***** 'Employee Last Name' no tiene valor";
						}	
					}
					if (sNombreCSV == null){
						aceptarFila = false;
		    			textoLogCeldaCSV11 = "***** Valor de 'Employee First Name' es: null";
					}else{
						if (sNombreCSV.toString() == ""){
							aceptarFila = false;
			    			textoLogCeldaCSV11 = "***** 'Employee First Name' no tiene valor";
						}	
					}
					if (sEmailCSV == null){
						aceptarFila = false;
		    			textoLogCeldaCSV12 = "***** Valor de 'Employee ID' es: null";
//						System.out.println("no se acepta el valor de fila: " + contInicio + "- celda 12");
						//Escribir en un log.
					}else{
						if (sEmailCSV.toString() == ""){
							aceptarFila = false;
			    			textoLogCeldaCSV12 = "***** 'Employee ID' no tiene valor";
						}	
					}
				}else{
					aceptarFila = false;
	    			textoLogCeldaCSV0 = "***** Valor de 'Report Entry Payment Type Name' es distinto de: 'Cash/Personal Card'."; 
					
					System.out.println("-----No se acepta el valor de fila ---------------------------------------- Valor distinto de: 'Cash/Personal Card'.");
					//No grabamos en el log   por valor distinto de 'Cash/Personal Card'   en la celda: '0'.
				}	
			}
    		
    		if (aceptarFila){   			
 		
    			String valorMoneda;
    			valorMoneda = sMonedaCSV.toString().toUpperCase();
    			
    			double valorImporte = 0;
    			valorImporte = Double.parseDouble(sAmountCSV.toString());

    			String valorEmail;
    			valorEmail = sEmailCSV.toString().toLowerCase();

    			String valorNombre;
    			valorNombre = sNombreCSV.toString() + " " + sApellidoCSV.toString();

    			String valorRowNumber;
    			valorRowNumber = sFilaCSV;

    			importeTotal = importeTotal + valorImporte;
    			
    			String valorRow;
    			valorRow = valorEmail + "!!" + valorMoneda + "<>" + valorImporte + "��" + valorNombre + "��" + valorRowNumber;

    			array1.add(valorRow);
    			
    		}else{
    			textoLogCSV = "Archivo CSV de Concur - Fila encontrada con incidencias: " + textoLogCeldaCSV0 + textoLogCeldaCSV7 + textoLogCeldaCSV8 + textoLogCeldaCSV10 + textoLogCeldaCSV11 + textoLogCeldaCSV12;
    			textoLogCSV = textoLogCSV + " ***** Datos de la fila: " + 
				 "(" + sTipoGastoCSV + ")" +
				 "(" + sMonedaCSV + ")" +
				 "(" + sAmountCSV + ")" +
				 "(" + sApellidoCSV + ")" +
				 "(" + sNombreCSV + ")" +
				 "(" + sEmailCSV + ")";

//    			arrayLogsCSV.add(textoLogCSV);
    			
    			strExcepcion = "";
				try {
					fileLogOnline.InfoLog(textoLogCSV, "CSVfile");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		}
		}
		//--------------- FIN del for	-   para evaluar todos los rows del Excel ------------------
		//--En el array: array1				---Tenemos todas las filas del CSV a las qe no se les ha encontrado incidencias.
		//--En el array: arrayLogsCSV	---Tenemos todos las filas del CSV en las que se han detectado incidencias,   e ir�n al fichero LOG.

		return array1;
	}
	
	private static ArrayList<String> aplicarRedondeo(ArrayList<String> arrayStr) {
		// TODO Auto-generated method stub

		int sizearrayStr = arrayStr.size();		
		
		//---------Aplicar redondeo al importe acumulado.
		//---------Desde el valor acumulado de los importes:		arrayStr
		//---------se va a realizar un redondeo.	Resultado en:	arrayResultadoParaLayoutRedondeado
	    double acumuladoImporteRedondeado = 0;

		ArrayList<String> arrayResultadoParaLayoutRedondeado = new ArrayList<String>();
		for(int cont = 0;cont<sizearrayStr;cont++){
			String valorEmail = "";
			String valorMoneda = "";
			String valorDatosImporteAcumuladoSplit = "";
			String valorDatosNombreSplit = "";
			String valorDatosRowNumberSplit = "";
			
			String[] splitDatos = arrayStr.get(cont).toString().split("<>");
			String[] splitDatosInicio = splitDatos[0].toString().split("!!");
			valorEmail = splitDatosInicio[0].toString();
			valorMoneda = splitDatosInicio[1].toString();
			
			String[] splitDatosImporteacumuladoNombre = splitDatos[1].toString().split("��");
			valorDatosImporteAcumuladoSplit = splitDatosImporteacumuladoNombre[0].toString();
			valorDatosNombreSplit = splitDatosImporteacumuladoNombre[1].toString();
			valorDatosRowNumberSplit = splitDatosImporteacumuladoNombre[2].toString();
			
			Double importeAcumulado =  Double.valueOf(valorDatosImporteAcumuladoSplit.toString());	
			acumuladoImporteRedondeado = Math.round(importeAcumulado * 100.0)/100.0;
			
			arrayResultadoParaLayoutRedondeado.add(valorEmail + "!!" + valorMoneda + "<>" + acumuladoImporteRedondeado + "��" + valorDatosNombreSplit + "��" + valorDatosRowNumberSplit);			
		}
		
		return arrayResultadoParaLayoutRedondeado;
	}
	
	private static ArrayList<String> generarConcurEmailsValidos(ArrayList<String> arrayStrConcur, ArrayList<String> arrayStrDetalle) {

//		LogsFile_Online fileLogOnline = new LogsFile_Online();

		System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! arrayStrConcur: " + arrayStrConcur.size());
		System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! arrayStrDetalle: " + arrayStrDetalle.size());
		
		ArrayList<String> arrayResultadoConcatenaConcurDetalle = new ArrayList<>();
		
		String msgLogIbanSwift = "";
		String cadenaElementoLayoutSatisfactorio = "";
		
		boolean bEmailNoEncontrado = true;
		boolean elementoLayoutSatisfactorio = false;
		int sizearrayStrConcur = arrayStrConcur.size();
		int sizearrayStrDetalle = arrayStrDetalle.size();
		
		try {
			strExcepcion = "5b.- (Metodo generarConcurEmailsValidos)";
			excepciones.InfoLog(strExcepcion);
			strExcepcion = "5b.- (Metodo generarConcurEmailsValidos) arrayStrConcur size: " + arrayStrConcur.size();
			excepciones.InfoLog(strExcepcion);
			strExcepcion = "5b.- (Metodo generarConcurEmailsValidos) arrayStrDetalle size: " + arrayStrDetalle.size();
			excepciones.InfoLog(strExcepcion);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		//---FOR----------------------------------------------------------------------------Recorremos el array de Concur:	
		for(int contEle = 0;contEle<sizearrayStrConcur;contEle++){

			try {
				strExcepcion = "5b9.- contEle: (" + contEle + ") arrayStrConcur.get(contEle):" + arrayStrConcur.get(contEle);
				excepciones.InfoLog(strExcepcion);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			String[] splitConcur = arrayStrConcur.get(contEle).split("!!");
			strEmailConcur = splitConcur[0].toString();
			
			elementoLayoutSatisfactorio = false;
			bEmailNoEncontrado = true;
	
			msgLogIbanSwift = "";
			
			cadenaElementoLayoutSatisfactorio = "";
			//---FOR----------------------------------------------------------------------------Recorremos el array de CC:	

			System.out.println("xxxxxxxxx----sizearrayStrDetalle:" + sizearrayStrDetalle);
			System.out.println("xxxxxxxxx----sizearrayStrDetalle:" + sizearrayStrDetalle);
			System.out.println("xxxxxxxxx----sizearrayStrDetalle:" + sizearrayStrDetalle);
			
			try {
				strExcepcion = "5b90.- contEle: (" + contEle + ") arrayStrConcur.get(contEle):" + arrayStrConcur.get(contEle);
				excepciones.InfoLog(strExcepcion);
				strExcepcion = "5b91.- sizearrayStrDetalle: " + sizearrayStrDetalle;
				excepciones.InfoLog(strExcepcion);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			for(int contEleCC = 0;contEleCC<sizearrayStrDetalle;contEleCC++){

				System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
				System.out.println("xxxxxxxxx----arrayStrConcur.get(contEle): " + arrayStrConcur.get(contEle));
				System.out.println("xxxxxxxxx----arrayStrDetalle.get(contEle): " + arrayStrDetalle.get(contEle));
				
				try {
					strExcepcion = "5c.- arrayStrConcur.get(contEle) : " + arrayStrConcur.get(contEle);
					excepciones.InfoLog(strExcepcion);
					strExcepcion = "5c.- arrayStrDetalle.get(contEleCC) : " + arrayStrDetalle.get(contEleCC);
					excepciones.InfoLog(strExcepcion);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				if (consultarEmailenCC(arrayStrConcur.get(contEle),arrayStrDetalle.get(contEleCC))){
				//---Ha encontrado el Email ------------------------------------------------------------------------------------
					bEmailNoEncontrado = false;
	
					String sReturnIban = "";
					sReturnIban = consultarIbanEnCC(arrayStrDetalle.get(contEleCC));
					
					if(sReturnIban.equals("")){
						//********** Valor IBAN no valido.
						msgLogIbanSwift = msgLogIbanSwift + " *** Email encontrado - IBAN no es valido.";
					}else{
						if(consultarSwiftEnCC(arrayStrDetalle.get(contEleCC)).equals("")){
							//********** Valor Swift no valido.
							msgLogIbanSwift = msgLogIbanSwift + " *** Email encontrado - Swift no es valido.";
						}else{
							elementoLayoutSatisfactorio = true;
							cadenaElementoLayoutSatisfactorio = arrayStrConcur.get(contEle)+"<>"+arrayStrDetalle.get(contEleCC);
						}
					}
				}
				if (!bEmailNoEncontrado){
					//-- Ha encontrado el Email. Salimos de este FOR-------------	
					contEleCC = 999999999;
				}
				System.out.println("xxxxxxxxx---- contEleCC = 999999999: " + contEleCC);
			}
			//---- END FOR, Ya ha consultado DetalleCC -----------------------------------------------------------------------------------------------------
	
			try {
				strExcepcion = "5d.- elementoLayoutSatisfactorio: " + elementoLayoutSatisfactorio;
				excepciones.InfoLog(strExcepcion);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}				
			
			if (elementoLayoutSatisfactorio){				
				//--- Comprobacion correcta.   Se escribre en el arrayConcurConcatenaCC. 	
				
				try {
					strExcepcion = "5e.- a�adimos en el array: " + cadenaElementoLayoutSatisfactorio;
					excepciones.InfoLog(strExcepcion);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}				

				arrayResultadoConcatenaConcurDetalle.add(cadenaElementoLayoutSatisfactorio);			
			}else{
				if (!bEmailNoEncontrado){				
				//-- Ha encontrado el email,    pero tiene incidencia por IBAN o Swift. --------------------------------------------------
//					arrayLogsCSV.add(msgLogIbanSwift);
					try {
						fileLogOnline.InfoLog(msgLogIbanSwift, "ConcurCSV");
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else{
//					arrayLogsCSV.add("*** ('Employee ID: '" + strEmailConcur + ") El archivo de Concur contiene una cuenta de correo no encontrada correctamente en el archivo de comprobaci�n de Small World.");
					try {
						fileLogOnline.InfoLog("*** ('Employee ID: '" + strEmailConcur + ") El archivo CSV de Concur contiene una cuenta de correo no encontrada correctamente en el archivo XLSX de DetalleCC Small World FS.", "CSVfile");
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}					
				}
			}			
			//-------------------------------------------------------------------
		}
		//---- END FOR, ya ha consultado gastos de Concur ---------------------------------------------------------------------------------------------		

		System.out.println("************!!!!!!!!!!!!!!!!  arrayResultadoConcatenaConcurDetalle  size: " + arrayResultadoConcatenaConcurDetalle.size());
		
		try {
			strExcepcion = "5f.- arrayResultadoConcatenaConcurDetalle size: " + arrayResultadoConcatenaConcurDetalle.size();
			excepciones.InfoLog(strExcepcion);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
		return arrayResultadoConcatenaConcurDetalle;	
	}

	public static void insertElementArrayReturnVentana(String strArhivoGenerado) {
		arrayReturnVentana.add(strArhivoGenerado);		
	}		
}