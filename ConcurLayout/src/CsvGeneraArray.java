
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.xssf.usermodel.XSSFSheet;

public class CsvGeneraArray {

	private static String strReportEntryPaymentTypeName = "Report Entry Payment Type Name";
	private static String strJournalAmount = "Journal Amount";
	private static String strEmployeeDefaultCurrencyAlphaCode = "Employee Default Currency Alpha Code";
	private static String strEmployeeLastName = "Employee Last Name";
	private static String strEmployeeFirstName = "Employee First Name";
	private static String strEmployeeID = "Employee ID";

	private static int intNumberOfLiteralReportEntryPaymentTypeName = 0;
	private static int intNumberOfLiteralJournalAmount = 0;
	private static int intNumberOfLiteralEmployeeDefaultCurrencyAlphaCode = 0;
	private static int intNumberOfLiteralEmployeeLastName = 0;
	private static int intNumberOfLiteralEmployeeFirstName = 0;
	private static int intNumberOfLiteralEmployeeID = 0;

	private static boolean booleanNumberOfLiteralReportEntryPaymentTypeName = false;
	private static boolean booleanNumberOfLiteralJournalAmount = false;
	private static boolean booleanNumberOfLiteralEmployeeDefaultCurrencyAlphaCode = false;
	private static boolean booleanNumberOfLiteralEmployeeLastName = false;
	private static boolean booleanNumberOfLiteralEmployeeFirstName = false;
	private static boolean booleanNumberOfLiteralEmployeeID = false;

	private static boolean booleanLiteralesCorrectos = true;
	
	private static LogsFile_Online fileLogOnline = new LogsFile_Online();
	
	// ----- Devuelve un array con todo el contenido del archivo CSV de Concur.
	public ArrayList<String> getArrayCSV(String strRuta) throws IOException{

    	int intPosicion1 = 0;
    	int intPosicion2 = 0;
    	int intPosicion3 = 0;
    	int intPosicion4 = 0;
    	int intPosicion5 = 0;
    	int intPosicion6 = 0;

		ArrayList<String> arrayCSV = new ArrayList<String>();

        System.out.println("111 DENTRO nuevo 1 2 3 4 5 ------------------------");
        
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        int contFila = 0;
        try {
            System.out.println("222 DENTRO nuevo 1 2 3 4 5 ------------------------");
            br = new BufferedReader(new FileReader(strRuta));
            
            //---------En primer lugar s�lo se lee la primera linea del CSV para consultar los literales de la cabecera.
            //---------En esa linea buscamos los literales que debemos localizar para consultar sus valores:
            /*				
            Report Entry Payment Type Name
            Journal Amount
            Employee Default Currency Alpha Code
            Employee Last Name
            Employee First Name
            Employee ID
            */
            //---------Si alguno de esos literales no los encuentra,  escribir� en el Log CSV.
            //---y no se generar� el archivo LayOut para el banco.
            
            if ((line = br.readLine()) != null) {
                // use comma as separator
                String[] strLineaCabecera = line.split(cvsSplitBy);

                //Si ha podido localizar los literales entonces conoce las posiciones
                if (asignarPosicionesLiteralesCabecera(line.toString())){

                	arrayCSV.add(strLineaCabecera[intPosicion1] + "<>" + strLineaCabecera[intPosicion2] + "<>" + strLineaCabecera[intPosicion3] + "<>" + strLineaCabecera[intPosicion4] + "<>" + strLineaCabecera[intPosicion5] + "<>" + strLineaCabecera[intPosicion6] + "<>" + contFila);
                	contFila++;
                	
                	intPosicion1 = intNumberOfLiteralReportEntryPaymentTypeName;
                	intPosicion2 = intNumberOfLiteralEmployeeDefaultCurrencyAlphaCode;
                	intPosicion3 = intNumberOfLiteralJournalAmount;
                	intPosicion4 = intNumberOfLiteralEmployeeLastName;
                	intPosicion5 = intNumberOfLiteralEmployeeFirstName;
                	intPosicion6 = intNumberOfLiteralEmployeeID;

	                System.out.println("1 DENTRO nuevo 1 2 3 4 5 -------------------------- : " + contFila);
	                System.out.println("1 DENTRO nuevo 1 2 3 4 5 -------------------------- : " + contFila);
	                System.out.println("1 DENTRO nuevo 1 2 3 4 5 -------------------------- : " + contFila);
	                System.out.println("1 DENTRO nuevo 1 2 3 4 5 -------------------------- : " + contFila);
	                System.out.println("1 DENTRO **************** ----while ---------------------- : " + contFila);

	                while ((line = br.readLine()) != null) {
		                // use comma as separator
	                	String[] country = line.split(cvsSplitBy);

		                System.out.println("1 DENTRO **** dentro del while --------------contfila: " + contFila);
		                System.out.println("1 DENTRO **** dentro del while posicion: " + intPosicion1);
			            System.out.println("1 DENTRO **** dentro del while    contenido: " + country[intPosicion1]);
			            System.out.println("1 DENTRO **** dentro del while" + country[0] + "<>" + country[7] + "<>" + country[8] + "<>" + country[10] + "<>" + country[11] + "<>" + country[12]);

//		        		arrayCSV.add(country[0] + "<>" + country[7] + "<>" + country[8] + "<>" + country[10] + "<>" + country[11] + "<>" + country[12] + "<>" + contFila);
//						arrayCSV.add(country[0] + "<>" + country[7] + "<>" + country[8] + "<>" + country[12] + "<>" + country[13] + "<>" + country[14] + "<>" + contFila);
		        		arrayCSV.add(country[intPosicion1] + "<>" + country[intPosicion2] + "<>" + country[intPosicion3] + "<>" + country[intPosicion4] + "<>" + country[intPosicion5] + "<>" + country[intPosicion6] + "<>" + contFila);
		//peto
		// Cuando el CSV de Concur tenga los dos nuevos campos que van a incluirle,
		// habr� que descomentar las siguientes dos lineas.
		// Dichos campos ir�an justo a conltinuaci�n de los campos actuales de moneda y cantidad.                
		//
		//        		arrayCSV.add(country[0] + "<>" + country[7] + "<>" + country[8] + "<>" + country[12] + "<>" + country[13] + "<>" + country[14] + "<>" + contFila);
		//              System.out.println("Country [code= " + country[0] + " , name=" + country[7] + " , name=" + country[8] + " , name=" + country[12] + " , name=" + country[13] + " , name=" + country[14] + "]");

		                contFila++;
		            }
		            if (arrayCSV.size()>0){
		        		arrayCSV.remove(0);
		            }
                }else{
                //No ha podido localizar los literales ---> Escribimos el LOG. "ERROR de lectura: El Archivo CSV no contiene literales correctos en la cabecera."
            		String textoLogCSV = "Archivo CSV de Concur - ERROR de lectura: no contiene literales correctos en la cabecera.";
        			
//                	LogsFile_Online fileLogOnline = new LogsFile_Online();
            		fileLogOnline.InfoLog(textoLogCSV, "CSVfile");
                }     
            }         
        } catch (FileNotFoundException e) {
        	Logs_Exceptions excepciones = new Logs_Exceptions();
        	String strExcepcion = "Exception-CsvGeneraArray_catch FileNotFoundException";
//			excepciones.InfoLog(strExcepcion, rutaLayOut, strDate);
			excepciones.InfoLog(strExcepcion);
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }	
        
		
		//***** Escribir log ------------------------
		for(int contEle = 0;contEle<arrayCSV.size();contEle++){
			fileLogOnline.InfoLog("ConcurProperties" + "_arrayCSV(return): " + arrayCSV.get(contEle).toString(), "00_CSVfile");		
		}	
		//*****	

		return arrayCSV;
	}
	
    public static boolean asignarPosicionesLiteralesCabecera(String strCabecera) throws IOException{

        System.out.println("2 DENTRO nuevo 1 2 3 4 5 --------------------------*******************************************************************");
        
		String strLiteral = "";
		int intNumeroLiterales = 0;
        String cvsSplitBy = ",";
        
        String[] strLiteralesTodos = strCabecera.split(cvsSplitBy);
		intNumeroLiterales = strLiteralesTodos.length;

		System.out.println("2 DENTRO --- antes del FOR ----------strCabecera: " + strCabecera);
		System.out.println("2 DENTRO --- antes del FOR ----------intNumeroLiterales: " + intNumeroLiterales);

		/*
        Report Entry Payment Type Name
        Journal Amount
        Employee Default Currency Alpha Code
        Employee Last Name
        Employee First Name
        Employee ID
        */

		booleanLiteralesCorrectos = false;
        System.out.println("2 DENTRO --- antes del FOR ------------------------booleanLiteralesCorrectos: " + booleanLiteralesCorrectos);

		for(int cont = 0; cont<intNumeroLiterales; cont++){
    		strLiteral = strLiteralesTodos[cont].toString();
    		//Quitamos espacios.
    		strLiteral = strLiteral.trim();
    		
    		//Si algun literal se encuentra,    asignamos TRUE a su variable booleana asignada. 
    		if (strReportEntryPaymentTypeName.toLowerCase().trim().equals(strLiteral.toLowerCase())){
    			booleanNumberOfLiteralReportEntryPaymentTypeName = true;
    			intNumberOfLiteralReportEntryPaymentTypeName = cont;
    		}
    		if (strJournalAmount.toLowerCase().trim().equals(strLiteral.toLowerCase())){
    			booleanNumberOfLiteralJournalAmount = true;
    			intNumberOfLiteralJournalAmount = cont;
    		}
    		if (strEmployeeDefaultCurrencyAlphaCode.toLowerCase().trim().equals(strLiteral.toLowerCase())){
    			booleanNumberOfLiteralEmployeeDefaultCurrencyAlphaCode = true;
    			intNumberOfLiteralEmployeeDefaultCurrencyAlphaCode = cont;
    		}
    		if (strEmployeeLastName.toLowerCase().trim().equals(strLiteral.toLowerCase())){
    			booleanNumberOfLiteralEmployeeLastName = true;
    			intNumberOfLiteralEmployeeLastName = cont;
    		}
    		if (strEmployeeFirstName.toLowerCase().trim().equals(strLiteral.toLowerCase())){
    			booleanNumberOfLiteralEmployeeFirstName = true;
    			intNumberOfLiteralEmployeeFirstName = cont;
    		}
    		if (strEmployeeID.toLowerCase().trim().equals(strLiteral.toLowerCase())){
    			booleanNumberOfLiteralEmployeeID = true;
    			intNumberOfLiteralEmployeeID = cont;
    		}
		}
		if (booleanNumberOfLiteralEmployeeDefaultCurrencyAlphaCode && booleanNumberOfLiteralEmployeeFirstName && booleanNumberOfLiteralEmployeeID && booleanNumberOfLiteralEmployeeLastName && booleanNumberOfLiteralJournalAmount && booleanNumberOfLiteralReportEntryPaymentTypeName){
			booleanLiteralesCorrectos = true;
		}

        System.out.println("2 DENTRO --- 1 booleanos: " + booleanNumberOfLiteralEmployeeDefaultCurrencyAlphaCode); 
        System.out.println("2 DENTRO --- 2 booleanos: " + booleanNumberOfLiteralEmployeeFirstName); 
        System.out.println("2 DENTRO --- 3 booleanos: " + booleanNumberOfLiteralEmployeeID);
        System.out.println("2 DENTRO --- 4 booleanos: " + booleanNumberOfLiteralEmployeeLastName); 
        System.out.println("2 DENTRO --- 5 booleanos: " + booleanNumberOfLiteralJournalAmount);
        System.out.println("2 DENTRO --- 6 booleanos: " + booleanNumberOfLiteralReportEntryPaymentTypeName);
        	
        System.out.println("2 DENTRO --- booleanLiteralesCorrectos: " + booleanLiteralesCorrectos);
		
		return booleanLiteralesCorrectos;
    }
}
