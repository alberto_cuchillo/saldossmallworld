import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class ConcurProperties {
	private static LogsFile_Online fileLogOnline = new LogsFile_Online();
	
	// ----- Devuelve un array con todo el contenido del archivo CSV de Concur.
	public String getDirOutput() throws IOException{

		//Obtendremos   la ruta que indica     el archivo   concur_properties.txt

//		Logs_Exceptions excepciones = new Logs_Exceptions();
//		String strExcepcion = null;
//		strExcepcion = "Exception" + "_123*****************************************************************" + this.getClass().getName().toString() + "_" + "catch";
//		excepciones.InfoLog(strExcepcion);        	
		
		String strLocalizacionJAR = "";
		
		//El usuario debe situar el     archivo    concur_properties.txt     donde haya ubicado		el archivo	.jar
		//Localizamos la ruta donde se encuentra    												el archivo	.jar
		String strFileNameRutaTotal = "";
    	
//		strFileNameRutaTotal = GenerarFicheros.class.getProtectionDomain().getCodeSource().getLocation().toString();
		strFileNameRutaTotal = Ventana.class.getProtectionDomain().getCodeSource().getLocation().toString();
		
    	String[] elemsFileNameRutaTotal = strFileNameRutaTotal.split("file:/");

		String strRutaTotal = elemsFileNameRutaTotal[1].toString();
		int lengthCadenaRuta = strRutaTotal.length();

		int intPosicionFileNameJar = strRutaTotal.toString().lastIndexOf("/");
	    int numCaracteres = lengthCadenaRuta - intPosicionFileNameJar;
		strLocalizacionJAR = strRutaTotal.substring(0, intPosicionFileNameJar+1);

		
		String strRutaProperties = strLocalizacionJAR + "concur_properties.txt";
//		String strRutaProperties = "C:/Work/Alberto/a/concur_properties.txt";

        BufferedReader br = null;
        String strRutaOutputProperties = "";
        String line = "";
//        String cvsSplitBy = ",";
        String strSplitBy = "output:";
		System.out.println("-***1 --dato-strRutaProperties (" + strRutaProperties + ")");
        try {
    		System.out.println("-***2 --dato-strRutaProperties (" + strRutaProperties + ")");
            br = new BufferedReader(new FileReader(strRutaProperties));
//            while ((line = br.readLine()) != null) {
//               String[] datosLinea = line.split(strSplitBy);
//            }
            if ((line = br.readLine()) != null) {
                if (!(line.indexOf(strSplitBy)<0)) {
                //--- Si la linea encuentra la referencia a output:    entonces leemos la ruta que venga indicada.
                	// use comma as separator
                	String[] datosLinea = line.split(strSplitBy);
                	strRutaOutputProperties = datosLinea[1];
                }else{
                	//Mostrar una ventana informando del error en la lectura del fichero concur_properties.txt
                }
            }else{
            	//Mostrar una ventana informando del error en la lectura del fichero concur_properties.txt            	
        		System.out.println("-***3 -- no ha leido el archivo properties.");
            }
        } catch (FileNotFoundException e) {
    		System.out.println("-*** catch.");        	
        	//Mostrar una ventana informando del error en la lectura del fichero concur_properties.txt
//			strExcepcion = "Exception-ConcurProperties_catch FileNotFoundException";
//			excepciones.InfoLog(strExcepcion, GenerarFicheros.getStrRutaLayOut(), GenerarFicheros.getStrDateFileName());        	
//          e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                	strRutaOutputProperties = strRutaOutputProperties.trim();
                	br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }	
        
		System.out.println("-----concur properties - strRutaOutputProperties: " + strRutaOutputProperties);
		System.out.println("-----concur properties - strRutaOutputProperties: " + strRutaOutputProperties);
		System.out.println("-----concur properties - strRutaOutputProperties: " + strRutaOutputProperties);
		System.out.println("-----concur properties - strRutaOutputProperties: " + strRutaOutputProperties);
		System.out.println("-----concur properties - strRutaOutputProperties: " + strRutaOutputProperties);
		System.out.println("-----concur properties - strRutaOutputProperties: " + strRutaOutputProperties);
		
		fileLogOnline.InfoLog("ConcurProperties" + "_strRutaOutputProperties: " + strRutaOutputProperties, "00_CSVfile");

		return strRutaOutputProperties;
	}
}