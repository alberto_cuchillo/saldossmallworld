import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
 

import java.io.IOException;

import javax.swing.ComboBoxEditor;
import javax.swing.JFileChooser;
import javax.swing.JTextArea;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Ventana extends javax.swing.JFrame {
//cambios
	private javax.swing.JButton btnSeleccionarConcur;
    private javax.swing.JButton btnSeleccionarDetalleCC;
    private javax.swing.JButton btnAceptarFicheros;
    private javax.swing.JTextArea txtAreaRutasArchivos;
    private javax.swing.JComboBox<String> cbOpcionesLayout;

    private javax.swing.JLabel jLabelMensaje;

    private javax.swing.JLabel jLabelCabecera;
    
    private javax.swing.JLabel jLabelPedirDocumentoConcur;
    private javax.swing.JLabel jLabelPedirFormatoLayout;
    private javax.swing.JLabel jLabelPedirDocumentoDetalleCC;

    private javax.swing.JTextField txtRutaConcur;
    private javax.swing.JTextField txtRutaDetalleCC;

	ArrayList<String> arrayTest = new ArrayList<String>();    
    // End of variables declaration   

	private static String strOpcionLayout = "Seleccionar LayOut...";
	private static String strOpcionComboBoxNoSelected = "Seleccionar LayOut...";
	private static String strOpcionComboBoxCaixaGral = "Formato CaixaGral";
	private static String strOpcionComboBoxBarclays = "Formato Barclays";
	private static String strCarpetaArchivosLayout = "Concur_Layout";
	private static String strCarpetaArchivosLog = "Concur_Log";

	private static String strRutaLayOut = "";
	private static String strDateFileName = "";

	private static String strMonedaSeleccionada = "";
	
	private static LogsFile_Online fileLogOnline = new LogsFile_Online();

    @SuppressWarnings("deprecation")

	public static void main(String args[]) throws IOException{
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
 
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Ventana().setVisible(true);
            }
        });
    }

    public Ventana() {
        initComponents();
    }
 
    @SuppressWarnings("unchecked")
    private void initComponents() {
    	
// Inicializamos la fecha para la nomenclatura de los archivos que se generen.
    	Calendar cal = Calendar.getInstance();
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
	    String strDate = sdf.format(cal.getTime());

	    setStrDateFileName(strDate);
// ---------------------------------------------------------------------------	    	
 
        txtRutaConcur = new javax.swing.JTextField();
        txtRutaDetalleCC = new javax.swing.JTextField();
        btnSeleccionarConcur = new javax.swing.JButton();
        btnSeleccionarDetalleCC = new javax.swing.JButton();

        btnAceptarFicheros = new javax.swing.JButton();
        txtAreaRutasArchivos = new javax.swing.JTextArea();
        
        String [] strOpcionesComboBox={getstrOpcionComboBoxnoSelected(),getstrOpcionComboBoxCaixaGral(),getStrOpcionComboBoxBarclays()};
        cbOpcionesLayout = new javax.swing.JComboBox(strOpcionesComboBox);
        
        jLabelPedirDocumentoConcur = new javax.swing.JLabel();
        jLabelPedirFormatoLayout = new javax.swing.JLabel();
        jLabelPedirDocumentoDetalleCC = new javax.swing.JLabel();

        jLabelMensaje = new javax.swing.JLabel();
        
        jLabelCabecera = new javax.swing.JLabel();
 
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("v1.2.15 - Concur. Seleccionar ruta de ficheros / Procesar.");
 
        txtRutaConcur.setEditable(false);
        txtRutaDetalleCC.setEditable(false);
 
        btnSeleccionarConcur.setText("Seleccionar...");
        btnSeleccionarDetalleCC.setText("Seleccionar...");

        btnAceptarFicheros.setText("Procesar ficheros");
        txtAreaRutasArchivos.setText("2Puede <br>ncons/nu\nltar el resultado en: ");
        txtAreaRutasArchivos.setEditable(false);
        txtAreaRutasArchivos.setVisible(false);

        cbOpcionesLayout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	cbOpcionesLayoutActionPerformed(evt);
            }
        });
        
        btnSeleccionarConcur.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSeleccionarConcurActionPerformed(evt);
            }
        });

        btnSeleccionarDetalleCC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSeleccionarDetalleCCActionPerformed(evt);
            }
        });

        btnAceptarFicheros.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {

				String mensaje = "";
				String mensajeConcur = "";
				String mensajeDetalleCC = "";

    			if(txtRutaConcur.getText().length()<1){
    				mensajeConcur = "  Concur.";
    			}
    			if(txtRutaDetalleCC.getText().length()<1){
    				mensajeDetalleCC = "  Detalle CC.";
    			}
    			
    			mensaje = mensajeConcur + mensajeDetalleCC;
    			
    			//Si no ha seleccionado un formato,     avisamos de que no es correcto.
    			System.out.println("--------->getStrOpcionLayout(): " + getStrOpcionLayout());
    			System.out.println("--------->getstrOpcionComboBoxnoSelected(): " + getstrOpcionComboBoxnoSelected());

		        jLabelMensaje.setForeground(Color.RED);
				try {
					fileLogOnline.InfoLog("Ventana" + "_Combo bancos. Opcion seleccionada: " + getstrOpcionComboBoxnoSelected().toString(), "00_CSVfile");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}    				

		        if (getStrOpcionLayout().equals(getstrOpcionComboBoxnoSelected().toString())){
    				mensaje = "� Debe seleccionar el formato del archivo que desea generar !.";
    				jLabelMensaje.setText(mensaje);    					
    			}else{
	    			if (mensaje.equals("")){
	    				jLabelMensaje.setText("");
	                    //------------------------------ Cuando ya ha seleccionado los 2 archivos :
	        			btnAceptarFicherosActionPerformed(evt);
	    			}else{
	    				mensaje = "� Debe seleccionar los ficheros: " + mensajeConcur + mensajeDetalleCC + " !.";
	    				jLabelMensaje.setText(mensaje);
	    			}
    			}	
            }
        });
        
        jLabelMensaje.setText("");
        
        //peto
//      jLabelCabecera.setText("Elige cada ruta pulsando en cada bot�n 'Seleccionar'.");

        jLabelCabecera.setText("Elige cada ruta pulsando en cada bot�n 'Seleccionar'." + GenerarFicheros.class.getProtectionDomain().getCodeSource().getLocation());
    	String strFileNameRutaTotal = "";
    	strFileNameRutaTotal = GenerarFicheros.class.getProtectionDomain().getCodeSource().getLocation().toString();

    	String[] elemsFileNameRutaTotal = strFileNameRutaTotal.split("file:/");

        Font fontjLabelPedirFormatoLayout = new Font("Verdana", Font.BOLD,12);
        jLabelPedirFormatoLayout.setFont(fontjLabelPedirFormatoLayout);
        jLabelPedirFormatoLayout.setText("Seleccione el formato del archivo que desea generar: ");  
        
        Font fontjLabelCabecera = new Font("Verdana", Font.BOLD,12);
        jLabelCabecera.setFont(fontjLabelCabecera);
    	jLabelCabecera.setText("Elija los archivos a consultar,  pulsando en cada bot�n 'Seleccionar':");
        
        jLabelPedirDocumentoConcur.setText("CSV - Concur: ");
        
        jLabelPedirDocumentoDetalleCC.setText("XLSX - Detalle ccc pagos: ");
 
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(43, 43, 43)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelMensaje)
                   	.addComponent(jLabelPedirFormatoLayout)
                    .addComponent(cbOpcionesLayout)                    
                    .addGap(18, 18, 18)
                    .addComponent(jLabelCabecera)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabelPedirDocumentoConcur)
                  		.addComponent(txtRutaConcur, javax.swing.GroupLayout.PREFERRED_SIZE, 306, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnSeleccionarConcur))                      
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabelPedirDocumentoDetalleCC)
                        .addComponent(txtRutaDetalleCC, javax.swing.GroupLayout.PREFERRED_SIZE, 306, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnSeleccionarDetalleCC))                        
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(btnAceptarFicheros))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(txtAreaRutasArchivos)))                        
                .addContainerGap(31, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(jLabelMensaje)
                .addComponent(jLabelPedirFormatoLayout)                		
                .addComponent(cbOpcionesLayout)
                .addGap(18, 18, 18)
                .addComponent(jLabelCabecera)                 
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelPedirDocumentoConcur)
               		.addComponent(txtRutaConcur, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSeleccionarConcur))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelPedirDocumentoDetalleCC)                  
                    .addComponent(txtRutaDetalleCC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSeleccionarDetalleCC))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAceptarFicheros))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtAreaRutasArchivos))                    
                .addContainerGap(44, Short.MAX_VALUE))
        );
  
        pack();
    }// </editor-fold>

    private void cbOpcionesLayoutActionPerformed(java.awt.event.ActionEvent evt) {                                          
    	int indiceCB = cbOpcionesLayout.getSelectedIndex();
    	String strOpcionSeleccionada = cbOpcionesLayout.getItemAt(indiceCB);
    	jLabelCabecera.setText(strOpcionSeleccionada);
    	
    	switch(indiceCB){
    		case 1:
    			setStrMonedaSeleccionada("eur");
				break;
			case 2: 
				setStrMonedaSeleccionada("gbp");
				break;
    	}
    	
    	//Obtenemos en strOpcionLayout		el texto de la opci�n elegida del ComboBox.
    	setStrOpcionLayout(strOpcionSeleccionada);
    }

    private void btnSeleccionarConcurActionPerformed(java.awt.event.ActionEvent evt) {                                          

        //Creamos una instancia de JFileChooser
        JFileChooser fc=new JFileChooser();
         
        //Escribimos el nombre del titulo
        fc.setDialogTitle("Elige un fichero");
         
        //Indicamos que solo se puedan elegir ficheros
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
         
        //Creamos un filtro para JFileChooser
        FileNameExtensionFilter filtro = new FileNameExtensionFilter("*.csv", "csv");
        fc.setFileFilter(filtro);
         
        int eleccion=fc.showSaveDialog(this);
        
        
        if(eleccion==JFileChooser.APPROVE_OPTION){
            txtRutaConcur.setText(fc.getSelectedFile().getPath());
        }
    }                                         

    private void btnSeleccionarDetalleCCActionPerformed(java.awt.event.ActionEvent evt) {                                          
        
        //Creamos una instancia de JFileChooser
        JFileChooser fc=new JFileChooser();
         
        //Escribimos el nombre del titulo
        fc.setDialogTitle("Elige un fichero");
         
        //Indicamos que solo se puedan elegir ficheros
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
         
        //Creamos un filtro para JFileChooser
        FileNameExtensionFilter filtro = new FileNameExtensionFilter("*.xlsx", "xlsx");
        fc.setFileFilter(filtro);
         
        int eleccion=fc.showSaveDialog(this);
        
        System.out.println("---------------------->------------------->------------------> detalle: " + txtRutaDetalleCC.getText());
        System.out.println("---------------------->------------------->------------------> detalle: " + txtRutaDetalleCC.getText());
        System.out.println("---------------------->------------------->------------------> detalle: " + txtRutaDetalleCC.getText());
        if(eleccion==JFileChooser.APPROVE_OPTION){
            txtRutaDetalleCC.setText(fc.getSelectedFile().getPath());
            System.out.println("---------------------->------------------->------------------> detalle: " + txtRutaDetalleCC.getText());
            System.out.println("---------------------->------------------->------------------> detalle: " + txtRutaDetalleCC.getText());
            System.out.println("---------------------->------------------->------------------> detalle: " + txtRutaDetalleCC.getText());
			try {
				fileLogOnline.InfoLog("Ventana" + "_FileChooser: " + fc.getSelectedFile().getPath(), "00_CSVfile");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}            
        }
        System.out.println("---------------------->------------------->------------------> detalle: " + txtRutaDetalleCC.getText());
        System.out.println("---------------------->------------------->------------------> detalle: " + txtRutaDetalleCC.getText());
        System.out.println("---------------------->------------------->------------------> detalle: " + txtRutaDetalleCC.getText());

        
    }                                         

    private void btnAceptarFicherosActionPerformed(java.awt.event.ActionEvent evt) {

    	String strRuta = null;
    	ArrayList<String> arrayStrRutasFicherosGenerados = new ArrayList<>();
		GenerarFicheros gSalida = new GenerarFicheros();
		
		String[] splitRutaArchivos = txtRutaConcur.getText().toString().split("\\\\");
		String rutaFinal = "";
    	int lengthSplitRutaArchivos = splitRutaArchivos.length;

//		Calendar cal = Calendar.getInstance();
//	    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
//	    String strDate = sdf.format(cal.getTime());

//	    setStrDateFileName(strDate);
	    
    	for(int cont=0;cont<lengthSplitRutaArchivos-1;cont++){
    		rutaFinal = rutaFinal + splitRutaArchivos[cont] + "\\";
    	}

		try {
//			gSalida.setStrRutaLayOut();
			setStrRutaLayOut();
			
			gSalida.GenerarSalida(txtRutaConcur.getText(), txtRutaDetalleCC.getText());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("<<<<<<<VENTANA");
		System.out.println("<<<<<<<VENTANA");
		System.out.println("<<<<<<<VENTANA");
		String strTextoAreaRutasArchivos = "";
		for(int contEle=0;contEle<arrayTest.size();contEle++){
			System.out.println("<<<<<<<VENTANA contEle: " + contEle + "-" + arrayTest.get(contEle));			
			strTextoAreaRutasArchivos = strTextoAreaRutasArchivos + arrayTest.get(contEle) + "\n";
		}
		txtAreaRutasArchivos.setText(strTextoAreaRutasArchivos);

        jLabelMensaje.setForeground(Color.BLACK);
        Font fontjLabelPedirFormatoLayout = new Font("Verdana", Font.BOLD,12);
        jLabelMensaje.setFont(fontjLabelPedirFormatoLayout);

        jLabelMensaje.setText("Proceso realizado. Se ha generado el fichero de logs y los ficheros con formato por moneda.");
        jLabelCabecera.setText("Puede consultar el resultado en: ");

        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<.getStrDateFileName: "+ getStrDateFileName());
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<.getStrRutaLayOut: "+ getStrRutaLayOut());
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<.getStrDateFileName: "+ getStrDateFileName());
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<.getStrRutaLayOut: "+ getStrRutaLayOut());
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<.getStrDateFileName: "+ getStrDateFileName());
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<.getStrRutaLayOut: "+ getStrRutaLayOut());
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<.getStrDateFileName: "+ getStrDateFileName());
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<.getStrRutaLayOut: "+ getStrRutaLayOut());
        
        //Consultamos si se han generado archivos LayOut.-------------------------------------
        strRuta = getStrRutaLayOut() + getStrCarpetaArchivosLayout();
        arrayStrRutasFicherosGenerados = consultarFilesRuta(strRuta);
        
        if(arrayStrRutasFicherosGenerados.size()>0){
			strTextoAreaRutasArchivos = strTextoAreaRutasArchivos + "\n" + "Archivos LayOut generados:" + "\n";
        }
        for(int contEle=0;contEle<arrayStrRutasFicherosGenerados.size();contEle++){
			System.out.println("<<<<<<<     rutas consultadas: contEle: " + contEle + "-" + arrayStrRutasFicherosGenerados.get(contEle));			
			strTextoAreaRutasArchivos = strTextoAreaRutasArchivos + arrayStrRutasFicherosGenerados.get(contEle) + "\n";
		}

        //Consultamos si se han generado archivos Log.-------------------------------------
        strRuta = getStrRutaLayOut() + getStrCarpetaArchivosLog();
        arrayStrRutasFicherosGenerados = consultarFilesRuta(strRuta);
        
        if(arrayStrRutasFicherosGenerados.size()>0){
			strTextoAreaRutasArchivos = strTextoAreaRutasArchivos + "\n" + "Archivos Log generados:" + "\n";
        }
        for(int contEle=0;contEle<arrayStrRutasFicherosGenerados.size();contEle++){
			System.out.println("<<<<<<<     rutas consultadas: contEle: " + contEle + "-" + arrayStrRutasFicherosGenerados.get(contEle));			
			strTextoAreaRutasArchivos = strTextoAreaRutasArchivos + arrayStrRutasFicherosGenerados.get(contEle) + "\n";
		}
        //------------------------------------------------------------------------------------
        
        System.out.println("----- *** ------------------------------------");
        System.out.println("----- *** ------------------------------------");
        System.out.println("----- *** ------------------------------------");
        System.out.println("----- *** ------------------------------------......." + strTextoAreaRutasArchivos);
        System.out.println("----- *** ------------------------------------");
        System.out.println("----- *** ------------------------------------");
        System.out.println("----- *** ------------------------------------");
           
		txtAreaRutasArchivos.setText(strTextoAreaRutasArchivos);
        
        jLabelPedirDocumentoConcur.setText("");
        jLabelPedirFormatoLayout.setText("");
        jLabelPedirDocumentoDetalleCC.setText("");
        
        cbOpcionesLayout.setVisible(false);
        txtRutaConcur.setVisible(false);
        txtRutaDetalleCC.setVisible(false);
        btnSeleccionarConcur.setVisible(false);
        btnSeleccionarDetalleCC.setVisible(false);
        btnAceptarFicheros.setVisible(false);
        txtAreaRutasArchivos.setVisible(true);
    }
    
	public static String getstrOpcionComboBoxnoSelected() {
		return strOpcionComboBoxNoSelected;
	}	


	public static String getstrOpcionComboBoxCaixaGral() {
		return strOpcionComboBoxCaixaGral;
	}	

	public static String getStrOpcionComboBoxBarclays() {
		return strOpcionComboBoxBarclays;
	}	

	public static String getStrOpcionLayout() {
		return strOpcionLayout;
	}	
    
	public static void setStrOpcionLayout(String strOpcion) {
		strOpcionLayout = strOpcion;
	}		

	public static String getStrMonedaSeleccionada() {
		return strMonedaSeleccionada;
	}	
    
	public static void setStrMonedaSeleccionada(String strMoneda) {
		strMonedaSeleccionada = strMoneda;
	}		

	public static String getStrCarpetaArchivosLayout() {
		return strCarpetaArchivosLayout;
	}	
    
	public static String getStrCarpetaArchivosLog() {
		return strCarpetaArchivosLog;
	}
	
	public static ArrayList<String> consultarFilesRuta(String strRuta) {

		ArrayList<String> arrayFiles = new ArrayList<String>();
		
		// Aqu� la carpeta donde queremos buscar

		//String path = "C:/";
		String path = strRuta;

		String files;
		File folder = new File(path);
		File[] listOfFiles = folder.listFiles();
		
		for (int i = 0; i < listOfFiles.length; i++)         {
			
			if (listOfFiles[i].isFile())             {
				files = listOfFiles[i].getName();
				//Si el fichero (files) que ha encontrado en el directorio contiene la cadena   
				//		getStrDateFileName (fechaHora actual para los nombres de los archivos) -> lo obtenemos, si no, lo ignaremos.
				if (!(files.lastIndexOf(getStrDateFileName())<0)){
					arrayFiles.add(strRuta + "/" + files);					
				}
			}
		}   
		return arrayFiles;
	}
	
	public static String getStrRutaLayOut() {
		return strRutaLayOut;
	}	

	public static String getStrDateFileName() {
		return strDateFileName;
	}	

	public static void setStrRutaLayOut() {
		Logs_Exceptions excepciones = new Logs_Exceptions();
		String strExcepcion = "";

		ConcurProperties propertiesConcur = new ConcurProperties();
		String strDireccion = "";
		try {
			//peto
			System.out.println(" ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ");
			System.out.println(" ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ");
			System.out.println(" ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ");
			System.out.println(" ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** strDireccion:" + strDireccion);
			System.out.println(" ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** se llama a propertiesConcur.getDirOutput()");
			System.out.println(" ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ");
			strDireccion = propertiesConcur.getDirOutput();
			System.out.println(" ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** strDireccion: " + strDireccion);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			strExcepcion = "Exception-GenerarFicheros_catch propertiesConcur.getDirOutput";
//			Mostramos Ventana informando de que no se ha podido leer properties.
			System.out.println(" ** ** ** catch ** ** ** ** ** ** ** ** ** ** ** ** ** ");
			e.printStackTrace();
		}

//		strRutaLayOut = "C:\\Concur\\Concur1\\Concur2222\\";
		strRutaLayOut = strDireccion;
	}	
	
	public static void setStrDateFileName(String strDate) {
		strDateFileName = strDate;
	}	
	
	
}
