
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class BarclaysPegasusLayoutcreator {

	public static final String saltoLinea = "\r\n";
	public static final String sMonedaGBP = "gbp";
	private static LogsFile_Online fileLogOnline = new LogsFile_Online();
	
	FileWriter archivo;
	//nuestro archivo log
	
	// Numero de filas que tenia el Excel
	// Array con los mensajes a escribir.
	// Ruta donde escribir el log.
	public void createLayOut(ArrayList<String> arrayConcatenado, String strNombre, String strRuta) throws IOException{
		
		int sizeArrayConcatenado = arrayConcatenado.size();

	    String strDate = Ventana.getStrDateFileName();
	    
	    //A la ruta que se ha recibido,  se concatena la carpeta: Concur_Layout
	    //	si no existe la crea.
//		strRuta = "C:\\Concur\\Concur1\\Concur2\\";
		strRuta = Ventana.getStrRutaLayOut();		    
	    strRuta = strRuta + Ventana.getStrCarpetaArchivosLayout() + "\\";
	    
		//Pregunta el archivo existe, caso contrario crea uno con el nombre log.txt
		String FileName = strRuta + "file_" + strNombre + "_" + strDate + ".txt";

		if (new File(FileName).exists()==false){
			File file = new File(strRuta + "file_" + strNombre + "_" + strDate + ".txt");
			file.getParentFile().mkdirs();
			archivo = new FileWriter(file, false);
		}
		archivo = new FileWriter(new File(FileName), true);
			
		Calendar fechaActual = Calendar.getInstance(); //Para poder utilizar el paquete calendar
		//Empieza a escribir en el archivo
		
		//***** Mostrar valores rows validas------------------------
		for(int contEle = 0;contEle<arrayConcatenado.size();contEle++){
			System.out.println("dentro caixaGeral*****" + 		arrayConcatenado.get(contEle));
			fileLogOnline.InfoLog("BarclaysPegasusLayoutcreator" + "_arrayConcatenado: " + arrayConcatenado.get(contEle).toString(), "00_CSVfile");			
		}	
		//***** Mostrado 		
				
		for(int cont = 0;cont<sizeArrayConcatenado;cont++){
			String sFila = arrayConcatenado.get(cont).toString();
			
			sFila = sFila.replace("!!", "<>");
			sFila = sFila.replace("��", "<>");
			sFila = sFila.replace("&&", "<>");
			String[] splitFila = sFila.split("<>");

			String sIbanConcur = splitFila[6].toString();
			String sNombreBenefConcur = splitFila[3].toString();
			String sSwiftConcur = splitFila[7].toString();			
			String sEmail = splitFila[0].toString();
			String sNombreSender = "Concur";
			String sMoneda = splitFila[1].toString();
			
			String sAmount = splitFila[2].toString();
			sAmount = sAmount.replace(",", ".");
			sAmount = AmountParaBarclaysPegasus(sAmount);
			
			String sRegisterID = strDate + "_" + splitFila[4].toString();

			sIbanConcur = sIbanConcur.replace(" ", "");
			sIbanConcur =  this.dropTabs(sIbanConcur);

			sNombreBenefConcur = this.cutString(sNombreBenefConcur, 27);
			sNombreSender = this.cutString(sNombreSender, 27);
			
			sSwiftConcur = sSwiftConcur.replace(" ","");
			sSwiftConcur =  this.dropTabs(sSwiftConcur);
			sNombreSender =  this.dropTabs(sNombreSender);

			sMoneda = sMoneda.toLowerCase();

			fileLogOnline.InfoLog("BarclaysPegasusLayoutcreator" + "_LineaGBP: " + "(" + sIbanConcur + ")(" + sNombreBenefConcur + ")(" + sSwiftConcur + ")(" + sAmount + ")", "00_CSVfile");
			//---Para Barclays solo preparamos los gastos que sucedieron en GBP.
			if (sMoneda.equals(sMonedaGBP.toString())){
				archivo.write("\"");
				archivo.write(sIbanConcur + ",");
				archivo.write(sNombreBenefConcur + ",");
				archivo.write(sSwiftConcur + ",");
				archivo.write(sAmount + ",");
				archivo.write(",");
				archivo.write("99" + "/\"");
				//-----Salto de Linea cuando no estamos escribiendo la �ltima l�nea.
				if((cont+1)<sizeArrayConcatenado){
					archivo.write(saltoLinea);					
				}
			}
			//----------------------------------------------------------------------
		}

		archivo.close(); //Se cierra el archivo
	}//Fin del metodo InfoLog

	private static String dropTabs(String text) {
		if (text != null) {
			text = text.replace('\t', ' ');
		}
	    return text;
	}
	  
	public static String cutString(String field, int len) {
		return field!=null?field.length()>len?field.substring(0, len):field:"";
	}
	
	public static String AmountParaBarclaysPegasus(String cantidad) {
		System.out.println("2222 AMOUNT ");
		System.out.println("2222 AMOUNT cantidad: " + cantidad);
		System.out.println("2222 AMOUNT ");
		//Ejemplos:
		//100	=> Pounds: 1.00
		//100.1	=> Pounds: 100.10
		//100.	=> Pounds: 100.00
		//100.11=> Pounds: 100.11
		//1		=> Pounds: 0.01
		//
		
		//Si no devolvemos el punto decimal en la cantidad que enviamos a Barclays, interpretar� que la cifra son centesimas.

		String strAmountBarclaysPegasus = "";
		String strParteEntera = "";
		String strParteDecimal = "";
		int intParteEntera = 0;
		int intParteDecimal = 0;
		int intCantidad = 0;

		if(cantidad.lastIndexOf(".")<0){
			System.out.println("AMOUNT--------sin decimal :" + cantidad);
			strAmountBarclaysPegasus = cantidad + ".";
		}else{
			strParteEntera = cantidad.substring(0, cantidad.lastIndexOf("."));
			System.out.println("AMOUNT--con decimal-------------str entera: " + strParteEntera);
			
			strParteDecimal = cantidad.substring(cantidad.lastIndexOf(".")+1,cantidad.length());
			System.out.println("AMOUNT---------------str decimal: " + strParteDecimal);

			intParteEntera = Integer.parseInt(strParteEntera);
			System.out.println("AMOUNT---------------int entera: " + intParteEntera);
			
			intParteDecimal = Integer.parseInt(strParteDecimal);
			System.out.println("AMOUNT---------------int decimal: " + intParteDecimal);

			if(intParteEntera<1){
				System.out.println("AMOUNT--------------- CERO con decimal :" + cantidad);
				strAmountBarclaysPegasus = String.valueOf(intParteDecimal);
			}else{
				System.out.println("AMOUNT---------------mayor que CERO con decimal :" + cantidad);				
				//Si la parte decimal es cero, quitamos los ceros y terminamos la cantidad en punto (sin indicar parte decimal).
				if (intParteDecimal==0){
					System.out.println("AMOUNT---------------mayor que CERO con decimal :" + cantidad + " - Parte decimal igual a 0.");					
					strAmountBarclaysPegasus = intParteEntera + ".";				
				}else{
					System.out.println("AMOUNT---------------mayor que CERO con decimal :" + cantidad + " - Parte decimal mayor que 0.");					
					strAmountBarclaysPegasus = cantidad;					
				}
			}			
		}
		System.out.println("AMOUNT-----------strAmountBarclaysPegasus: " + strAmountBarclaysPegasus + " - cantidad: " + cantidad);
		return strAmountBarclaysPegasus;		
	}	
}
