
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class CaixaGeralLayoutcreator {

	public static final String separator = "\t";
	public static final String saltoLinea = "\r\n";
	public static final String sMonedaEUR = "eur";
	private static LogsFile_Online fileLogOnline = new LogsFile_Online();
	
	FileWriter archivo;
	//nuestro archivo log
	
	// Numero de filas que tenia el Excel
	// Array con los mensajes a escribir.
	// Ruta donde escribir el log.
	public void createLayOut(ArrayList<String> arrayConcatenado, String strNombre, String strRuta) throws IOException{
		
		int sizeArrayConcatenado = arrayConcatenado.size();

	    String strDate = null;
	    strDate = Ventana.getStrDateFileName();
	    
	    //A la ruta que se ha recibido,  se concatena la carpeta: Concur_Layout
	    //	si no existe la crea.
//		strRuta = "C:\\Concur\\Concur1\\Concur2\\";
		strRuta = Ventana.getStrRutaLayOut();		    
	    strRuta = strRuta + Ventana.getStrCarpetaArchivosLayout() + "\\";
	    
		//Pregunta el archivo existe, caso contrario crea uno con el nombre log.txt
		String FileName = strRuta + "file_" + strNombre + "_" + strDate + ".txt";

		if (new File(FileName).exists()==false){
			File file = new File(strRuta + "file_" + strNombre + "_" + strDate + ".txt");
			file.getParentFile().mkdirs();
			archivo = new FileWriter(file, false);
		}
		archivo = new FileWriter(new File(FileName), true);
			
		Calendar fechaActual = Calendar.getInstance(); //Para poder utilizar el paquete calendar
		//Empieza a escribir en el archivo
		
		//***** escribir LOG
		for(int contEle = 0;contEle<arrayConcatenado.size();contEle++){
			fileLogOnline.InfoLog("CaixaGeralLayoutcreator" + "_createLayOut_arrayConcatenado: " + arrayConcatenado.get(contEle), "00_CSVfile");			
			System.out.println("dentro caixaGeral*****" + 		arrayConcatenado.get(contEle));
		}	
		//*****  		
				
		for(int cont = 0;cont<sizeArrayConcatenado;cont++){
			String sFila = arrayConcatenado.get(cont).toString();
			
			sFila = sFila.replace("!!", "<>");
			sFila = sFila.replace("��", "<>");
			sFila = sFila.replace("&&", "<>");
			String[] splitFila = sFila.split("<>");

			String sIbanConcur = splitFila[6].toString();
			String sNombreBenefConcur = splitFila[3].toString();
			String sSwiftConcur = splitFila[7].toString();			
			String sEmail = splitFila[0].toString();
			String sNombreSender = "Concur";
			String sMoneda = splitFila[1].toString();
			
			String sAmount = splitFila[2].toString();
			sAmount = sAmount.replace(".", ",");			
			
			String sRegisterID = strDate + "_" + splitFila[4].toString();

			sIbanConcur = sIbanConcur.replace(" ", "");
			sIbanConcur =  this.dropTabs(sIbanConcur);

			sNombreBenefConcur = this.cutString(sNombreBenefConcur, 27);
			sNombreSender = this.cutString(sNombreSender, 27);
			
			sSwiftConcur = sSwiftConcur.replace(" ","");
			sSwiftConcur =  this.dropTabs(sSwiftConcur);
			sNombreSender =  this.dropTabs(sNombreSender);

			sMoneda = sMoneda.toLowerCase();
			fileLogOnline.InfoLog("CaixaGeralLayoutcreator" + "_LineaEUR: " + "(" + sIbanConcur + ")(" + sNombreBenefConcur + ")(" + sSwiftConcur + ")(" + sAmount + ")(" + sRegisterID + ")", "00_CSVfile");
			fileLogOnline.InfoLog("CaixaGeralLayoutcreator" + "_LineaEUR - sMoneda: " + "(" + sMoneda + ")sMonedaEUR(" + sMonedaEUR+ ")", "00_CSVfile");
			//---Para CaixaGral solo preparamos los gastos que sucedieron en EUROS.
			if (sMoneda.equals(sMonedaEUR.toString())){
				fileLogOnline.InfoLog("CaixaGeralLayoutcreator" + "_write_LineaEUR - sMoneda: " + "(" + sMoneda + ")sMonedaEUR(" + sMonedaEUR+ ")", "00_CSVfile");				archivo.write(sIbanConcur + separator);
				archivo.write(sNombreBenefConcur + separator);
				archivo.write(sSwiftConcur + separator);
				archivo.write(sAmount + separator);
				archivo.write(sRegisterID + separator);
				archivo.write(cutString(sNombreSender + " - ", 140));
				archivo.write(saltoLinea);				
			}
			//---------------------------------------------------------------------- 			
		}
		
		archivo.close(); //Se cierra el archivo
	}//Fin del metodo InfoLog
	
	private static String dropTabs(String text) {
		if (text != null) {
			text = text.replace('\t', ' ');
		}
	    return text;
	}
		  
	public static String cutString(String field, int len) {
		return field!=null?field.length()>len?field.substring(0, len):field:"";
	}	  
}
