
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class Logs_Exceptions {

	FileWriter archivo;
	//nuestro archivo log
	
	// Array con los mensajes a escribir.
	// Ruta donde escribir el log.
	public void InfoLog(String logExcepcion) throws IOException{

	    //A la ruta que se ha recibido,  se concatena la carpeta: Concur_Log
	    //	si no existe la crea.

//		strRuta = "C:\\Concur\\Concur1\\Concur2\\";
		String strRuta = "";		
		strRuta = Ventana.getStrRutaLayOut();		
	    strRuta = strRuta + Ventana.getStrCarpetaArchivosLog() + "\\";
	    
		//Pregunta el archivo existe, caso contrario crea uno con el nombre log.txt
		String FileName = strRuta + "log_" + "Exceptions" + "_" + Ventana.getStrDateFileName() + ".txt";

		if (new File(FileName).exists()==false){
			File file = new File(strRuta + "log_" + "Exceptions" + "_" + Ventana.getStrDateFileName() + ".txt");
			file.getParentFile().mkdirs();
			archivo = new FileWriter(file, true);
		}
		archivo = new FileWriter(new File(FileName), true);

		Calendar fechaActual = Calendar.getInstance(); //Para poder utilizar el paquete calendar
		//Empieza a escribir en el archivo

		archivo.write("["+(String.valueOf(fechaActual.get(Calendar.YEAR))
					+"/"+String.valueOf(fechaActual.get(Calendar.MONTH)+1)
					+"/"+String.valueOf(fechaActual.get(Calendar.DAY_OF_MONTH))
					+" "+String.valueOf(fechaActual.get(Calendar.HOUR_OF_DAY))
					+":"+String.valueOf(fechaActual.get(Calendar.MINUTE))
					+":"+String.valueOf(fechaActual.get(Calendar.SECOND)))+"]"+"[INFO     ]"+logExcepcion+"\r\n");
		
		archivo.close(); //Se cierra el archivo
	}//Fin del metodo InfoLog
}	
