
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.hssf.record.ArrayRecord;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFHyperlink;
import org.apache.poi.xssf.usermodel.XSSFName;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class XlsxGeneraArray {

	private static String textoInfoEncabezamiento = "LOG - DetalleCC - Info:";
	private static boolean 	bHojaCorrecta = false;
	
	//	LowerCase
	private static String strSheetName_EUR = "iban";
	private static String strSheetName_GBP = "sheet1";
//	private static String strSheetName_GBP = "uk bank details";
	private static String strSheetName = "";
	
	private static int numberOfTheSheetSelected = 0;
	private static String ruta = "";
//	private static String rutaConcur = "";
	private ArrayList<String> arrayRows = new ArrayList<String>();
	private static ArrayList<String> arrayTextoLog = new ArrayList<String>();

	private static String strConstantEmail = "Email";
	private static String strConstantIban = "Iban";
	private static String strConstantSwift = "Swift";
	private static String strConstantSortCode = "Sort Code";
	private static String strConstantNumberAccount = "A/C No.";
//	private static String strConstantNumberAccount = "Account Number";

	private static int intNumberOfCellEmail = 0;
	private static int intNumberOfCellIBAN_AccountNumber = 0;
	private static int intNumberOfCellSwift_SortCode = 0;
	
	private static boolean booleanEncuentraLiteralEmail = false;
	private static boolean booleanEncuentraLiteralIban = false;
	private static boolean booleanEncuentraLiteralSwift = false;
	private static boolean booleanEncuentraLiteralNumberAccount = false;
	private static boolean booleanEncuentraLiteralSortCode = false;
	private static LogsFile_Online fileLogOnline = new LogsFile_Online();
	
	public XlsxGeneraArray(String rutaDetalle) throws IOException{
		
		//peto---Las siguientes lineas:   test-   grabo logs   en   log_CSVfile    para maximizar la info en las pruebas.
		//LogsFile_Online fileLogOnline = new LogsFile_Online();
		
		this.ruta = rutaDetalle;
    }

    public static ArrayList<String> generar() throws IOException{

		//LogsFile_Online fileLogOnline = new LogsFile_Online();

		ArrayList<String> arrayRowsValidas = new ArrayList<String>();
		String rutaCC = "";
		rutaCC = ruta;
		String sRutaModificada = rutaCC.replace("\\", "\\\\");
		FileInputStream file = new FileInputStream(new File(sRutaModificada));
		// Crear el objeto que tendra el libro de Excel
		XSSFWorkbook workbook = new XSSFWorkbook();
		try {
			workbook = new XSSFWorkbook(file);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

//		XSSFSheet sheet = workbook.getSheetAt(0);

		String strLowerName = "";
		int numberOfSheets = workbook.getNumberOfSheets();

		if(Ventana.getStrMonedaSeleccionada().equals("eur")){
			strSheetName = strSheetName_EUR;			
		}else{
			strSheetName = strSheetName_GBP;
		}
		
        System.out.println("22222223333333333333333333333333333333333333333333333333 numberOfSheets: " + numberOfSheets);		
        System.out.println("22222223333333333333333333333333333333333333333333333333 numberOfSheets: " + numberOfSheets);		
		for(int contEle = 0;contEle<numberOfSheets;contEle++){
	        System.out.println("22222223333333333333333333333333333333333333333333333333 workbook.getSheetName(contEle).toLowerCase(): " + workbook.getSheetName(contEle).toLowerCase());		

	        strLowerName = workbook.getSheetName(contEle).toLowerCase();
	        System.out.println("3333333333333333333333333333333333333333333333333 strLowerName: " + strLowerName);
	        System.out.println("3333333333333333333333333333333333333333333333333 strSheetName: " + strSheetName);
			if (strLowerName.indexOf(strSheetName)<0){
				//				no es la hoja correcta
				bHojaCorrecta = false;
		        System.out.println("3333333333333333333333333333333333333333333333333     hojCorrecta FALSE");
			}else{
				numberOfTheSheetSelected = contEle;
				contEle = numberOfSheets;
				bHojaCorrecta = true;
		        System.out.println("3333333333333333333333333333333333333333333333333     hojCorrecta TRUE");
			}
		}
		
//    	LogsFile_Online fileLogOnline = new LogsFile_Online();

		fileLogOnline.InfoLog("XlsxGeneraArray --- generar --- valor de bHojaCorrecta: " + bHojaCorrecta, "_log_locked_");	

		if (bHojaCorrecta){
			XSSFSheet sheet = workbook.getSheetAt(numberOfTheSheetSelected);
			arrayRowsValidas = procesarHoja(sheet);
		}else{
			fileLogOnline.InfoLog(textoInfoEncabezamiento +  "*** El archivo Excel no contiene la hoja requerida. ***", "Xlsxfile");					
			arrayTextoLog.add(textoInfoEncabezamiento + "*** El archivo Excel no contiene la hoja requerida. ***");
		}

        
        System.out.println("----------------------------------------------------");
        System.out.println("----------------------------------------------------");
        System.out.println("------------------intNumberOfCellEmail(" +intNumberOfCellEmail + ")------------------------------");
        System.out.println("------------------intNumberOfCellIBAN_AccountNumber(" + intNumberOfCellIBAN_AccountNumber + "----------------------------------");
        System.out.println("------------------intNumberOfCellSwift_SortCode(" + intNumberOfCellSwift_SortCode + "----------------------------------");
        System.out.println("----------------------------------------------------");
        System.out.println("----------------------------------------------------");
        System.out.println("----------------------------------------------------");
        System.out.println("----------------------------------------------------");
        System.out.println("----------------------------------------------------");
        System.out.println("----------------------------------------------------");
        System.out.println("----------------------------------------------------");
        System.out.println("----------------------------------------------------");
        System.out.println("----------------------------------------------------");
        System.out.println("----------------------------------------------------");
        System.out.println("----------------------------------------------------");
        System.out.println("----------------------------------------------------");
        System.out.println("----------------------------------------------------");
        System.out.println("----------------------------------------------------");
        
		workbook.close();

		//-----Escribir Log
		for(int contEle = 0; contEle<arrayRowsValidas.size();contEle++){
			fileLogOnline.InfoLog("XlsxGeneraArray" + "_generar_arrayRowsValidas(return): " + arrayRowsValidas.get(contEle).toString(), "00_CSVfile");
		}		
		//--------
		
		return	arrayRowsValidas;
	}
    
    public static ArrayList<String> procesarHoja(XSSFSheet sheet) throws IOException{
		
//		LogsFile_Online fileLog = new LogsFile_Online();
//    	LogsFile_Online fileLogOnline = new LogsFile_Online();

    	//----------------------------Logs_Exceptions--------------grabar excepciones.
		ArrayList<String> arrayExcepciones = new ArrayList<String>();
		Logs_Exceptions excepciones = new Logs_Exceptions();
		String strExcepcion = "";
    	
    	ArrayList<String> arrayResultado = new ArrayList<String>();
    	
		int contInicio = 0;
		
		int contRows = sheet.getLastRowNum();

		//--------------- Inicio del for	-   para evaluar todos los rows del Excel ------------------
		//String textoInfoEncabezamiento = "LOG - DetalleCC - Info:";
		String textoInfo = "";
		
    	//Desde la funci�n:	asignarPosicionesLiteralesCabecera()
    	//	asignamos en qu� posiciones (qu� columnas) se encuentran los valores que requiere el programa.
		asignarPosicionesLiteralesCabecera(sheet);

		fileLogOnline.InfoLog("XlsxGeneraArray --- procesarHoja --- valor Ventana.getStrMonedaSeleccionada: " + Ventana.getStrMonedaSeleccionada(), "_log_locked_");	

		textoInfo = "ERROR: Los literales de la cabecera del archivo, xlsx EUR, no son correctos.";
		//Si ha seleccionado LayOut CaixaGeral	y en el excel no encuentra las cabeceras correctas:  escribimos en el log y no escribimos en el array de salida.
		if (Ventana.getStrMonedaSeleccionada().equals("eur")){
			System.out.println("****** seleccionado del combo: " + Ventana.getstrOpcionComboBoxnoSelected());
			if (!(booleanEncuentraLiteralEmail && booleanEncuentraLiteralIban && booleanEncuentraLiteralSwift)){
				fileLogOnline.InfoLog(textoInfoEncabezamiento + textoInfo, "Xlsxfile");
				contRows = 0;
				fileLogOnline.InfoLog("XlsxGeneraArray --- procesarHoja --- !!!!!!!! !(booleanEncuentraLiteralEmail && booleanEncuentraLiteralIban && booleanEncuentraLiteralSwift)", "_log_locked_");
			}	
		}
		textoInfo = "ERROR: Los literales de la cabecera del archivo, xlsx GBP, no son correctos.";
		//Si ha seleccionado LayOut Barclays	y en el excel no encuentra las cabeceras correctas:  escribimos en el log y no escribimos en el array de salida.
		if (Ventana.getStrMonedaSeleccionada().equals("gbp")){
			System.out.println("****** getstrOpcionComboBoxnoSelected: " + Ventana.getstrOpcionComboBoxnoSelected());
			if (!(booleanEncuentraLiteralEmail && booleanEncuentraLiteralNumberAccount && booleanEncuentraLiteralSortCode)){
				fileLogOnline.InfoLog(textoInfoEncabezamiento + textoInfo, "Xlsxfile");
				contRows = 0;
				fileLogOnline.InfoLog("XlsxGeneraArray --- procesarHoja --- !!!!!!!! !(booleanEncuentraLiteralEmail && booleanEncuentraLiteralIban && booleanEncuentraLiteralSwift)", "_log_locked_");				
			}	
		}
		
		System.out.println("****** : " + Ventana.getStrCarpetaArchivosLayout());
		System.out.println("****** : " + Ventana.getStrCarpetaArchivosLog());
		System.out.println("****** : " + Ventana.getStrDateFileName());
		System.out.println("****** : " + Ventana.getStrMonedaSeleccionada());
		System.out.println("****** : " + Ventana.getStrOpcionComboBoxBarclays());
		System.out.println("****** : " + Ventana.getstrOpcionComboBoxCaixaGral());
		System.out.println("****** : " + Ventana.getstrOpcionComboBoxnoSelected());
		System.out.println("****** : " + Ventana.getStrOpcionLayout());
		System.out.println("****** : " + Ventana.getStrRutaLayOut());

		System.out.println("****** getstrOpcionComboBoxnoSelected: " + Ventana.getstrOpcionComboBoxnoSelected());
		System.out.println("****** getstrOpcionComboBoxCaixaGral: " + Ventana.getstrOpcionComboBoxCaixaGral());
		System.out.println("****** getStrOpcionComboBoxBarclays: " + Ventana.getStrOpcionComboBoxBarclays());
		System.out.println("****** 1: " + booleanEncuentraLiteralEmail + "-" + booleanEncuentraLiteralIban + "-" + booleanEncuentraLiteralSwift);
		System.out.println("****** 2: " + booleanEncuentraLiteralEmail + "-" + booleanEncuentraLiteralNumberAccount + "-" + booleanEncuentraLiteralSortCode);

		for(contInicio=1;contInicio<=contRows;contInicio++){
			fileLogOnline.InfoLog("XlsxGeneraArray --- procesarHoja --- Dentro del FOR <contRows. valor contRows:(" + contRows + ")contInicio:(" + contInicio +  ")", "_log_locked_");

			System.out.println(contInicio + "*******************************************************************************************");
			//-----Escribir en Logs
//			strExcepcion = "Exception-xlsx (" + contInicio + ")";
//			excepciones.InfoLog(strExcepcion, rutaLayOut, strDate);
			//---------------------------------------------------
			textoInfo = "";

			String strEmailValor = "";
			String strIBANValor = "";
			String strSwiftValor = "";	

//			excepciones.InfoLog("INFO-xlsx Linea: " + contInicio, rutaLayOut, strDate);
//------------------------------------------------------------------------------------------------------------------------------		
			System.out.println("email-------------- xlsx");
			fileLogOnline.InfoLog("XlsxGeneraArray --- procesarHoja --- Dentro del FOR  contInicio:(" + contInicio +  ")", "_log_locked_");
			fileLogOnline.InfoLog("XlsxGeneraArray --- procesarHoja --- Dentro del FOR  getCellType = "+ sheet.getRow(contInicio).getCell(intNumberOfCellEmail).getCellType(), "_log_locked_");
			fileLogOnline.InfoLog("nueva linea: strEmailValor: " + strEmailValor, "_formula_XLSX");						
			try {
					if (sheet.getRow(contInicio).getCell(intNumberOfCellEmail).getCellType()==1){
						strEmailValor = sheet.getRow(contInicio).getCell(intNumberOfCellEmail).getStringCellValue();					
						fileLogOnline.InfoLog("XlsxGeneraArray --- procesarHoja --- getHyperlink es NULL. strEmailValor:(" + strEmailValor +  ")", "_log_locked_");
						fileLogOnline.InfoLog("nueva linea(tipo 1): strEmailValor: " + strEmailValor, "_formula_XLSX");						
					}else{
						if (sheet.getRow(contInicio).getCell(intNumberOfCellEmail).getCellType()==2){
							strEmailValor = sheet.getRow(contInicio).getCell(intNumberOfCellEmail).getCellFormula();
							fileLogOnline.InfoLog("nueva linea(tipo 2): strEmailValor: " + strEmailValor, "_formula_XLSX");							
						}else{
							if(sheet.getRow(contInicio).getCell(intNumberOfCellEmail).getCellType()==0){
								strEmailValor = String.valueOf(sheet.getRow(contInicio).getCell(intNumberOfCellEmail).getNumericCellValue());
								fileLogOnline.InfoLog("nueva linea(tipo 0): strEmailValor: " + strEmailValor, "_formula_XLSX");								
							}else{
									strEmailValor = "";
									fileLogOnline.InfoLog("nueva linea(tipo ninguno): strEmailValor: " + strEmailValor, "_formula_XLSX");									
							}
						}					
					}
			} catch (Exception e) {
				fileLogOnline.InfoLog("XlsxGeneraArray --- procesarHoja --- catch", "_log_locked_");
				strEmailValor = "";
				strExcepcion = "Exception-xlsx_catch Email. Tipo1: getStringCellValue. Tipo2: getCellFormula. Linea Xlsx: " + contInicio;
				excepciones.InfoLog(strExcepcion);
				e.printStackTrace();
			}
			if(strEmailValor.equals(""))							textoInfo = textoInfo + " ***** " + sheet.getRow(0).getCell(intNumberOfCellEmail).getStringCellValue() + " : No tiene valor.";

			fileLogOnline.InfoLog("sheet.getRow(contInicio).getCell(intNumberOfCellEmail).getCellType()" + sheet.getRow(contInicio).getCell(intNumberOfCellEmail).getCellType(), "_formula_XLSX");
			fileLogOnline.InfoLog("strEmailValor" + strEmailValor, "_formula_XLSX");			
//------------------------------------------------------------------------------------------------------------------------------
			fileLogOnline.InfoLog("XlsxGeneraArray --- procesarHoja --- 1", "_log_locked_");
			
			System.out.println("1 IBAN-------------- xlsx contInicio: " + contInicio);
			System.out.println("2 IBAN-------------- xlsx");
			fileLogOnline.InfoLog("nueva linea: strIBANValor" + strIBANValor, "_formula_XLSX");			
			try {
				if (sheet.getRow(contInicio).getCell(intNumberOfCellIBAN_AccountNumber).getCellType()==1){
					strIBANValor = sheet.getRow(contInicio).getCell(intNumberOfCellIBAN_AccountNumber).getStringCellValue();
					fileLogOnline.InfoLog("nueva linea(tipo 1): strIBANValor" + strIBANValor, "_formula_XLSX");					
				}else{
					if (sheet.getRow(contInicio).getCell(intNumberOfCellIBAN_AccountNumber).getCellType()==2){
						strIBANValor = sheet.getRow(contInicio).getCell(intNumberOfCellIBAN_AccountNumber).getCellFormula();
						fileLogOnline.InfoLog("nueva linea(tipo 2): strIBANValor" + strIBANValor, "_formula_XLSX");						
					}else{
						if(sheet.getRow(contInicio).getCell(intNumberOfCellIBAN_AccountNumber).getCellType()==0){
//							strIBANValor = String.valueOf(sheet.getRow(contInicio).getCell(intNumberOfCellIBAN_AccountNumber).getNumericCellValue());
//							strIBANValor = Double.toString(sheet.getRow(contInicio).getCell(intNumberOfCellIBAN_AccountNumber).getNumericCellValue());
							int intValorIBAN =  (int) sheet.getRow(contInicio).getCell(intNumberOfCellIBAN_AccountNumber).getNumericCellValue();
							strIBANValor = String.valueOf(intValorIBAN);
							fileLogOnline.InfoLog("nueva linea(tipo 0): strIBANValor" + strIBANValor, "_formula_XLSX");							
						}else{
								strIBANValor = "";
								fileLogOnline.InfoLog("nueva linea(tipo ninguno): strIBANValor" + strIBANValor, "_formula_XLSX");								
						}						
					}					
				}
			} catch (Exception e) {
				strIBANValor = "";
				strExcepcion = "Exception-xlsx_catch IBAN. Linea Xlsx: " + contInicio;
				excepciones.InfoLog(strExcepcion);
				e.printStackTrace();				
			}

			System.out.println("3 IBAN-------------- xlsx");
			
			if(strIBANValor.equals(""))							textoInfo = textoInfo + " ***** " + sheet.getRow(0).getCell(intNumberOfCellIBAN_AccountNumber).getStringCellValue() + " : No tiene valor.";

			fileLogOnline.InfoLog("sheet.getRow(contInicio).getCell(intNumberOfCellIBAN_AccountNumber).getCellType()" + sheet.getRow(contInicio).getCell(intNumberOfCellIBAN_AccountNumber).getCellType(), "_formula_XLSX");
			fileLogOnline.InfoLog("strIBANValor" + strIBANValor, "_formula_XLSX");						
//------------------------------------------------------------------------------------------------------------------------------
			fileLogOnline.InfoLog("XlsxGeneraArray --- procesarHoja --- 2", "_log_locked_");
			System.out.println("Switch-------------- xlsx");
			fileLogOnline.InfoLog("nueva linea: strSwiftValor" + strSwiftValor, "_formula_XLSX");	
			try {
				if (sheet.getRow(contInicio).getCell(intNumberOfCellSwift_SortCode).getCellType()==1){
					strSwiftValor = sheet.getRow(contInicio).getCell(intNumberOfCellSwift_SortCode).getStringCellValue();	
					fileLogOnline.InfoLog("nueva linea(tipo 1): strSwiftValor" + strSwiftValor, "_formula_XLSX");	
				}else{
					if (sheet.getRow(contInicio).getCell(intNumberOfCellSwift_SortCode).getCellType()==2){
//						strSwiftValor = sheet.getRow(contInicio).getCell(intNumberOfCellSwift_SortCode).getCellFormula();
						strSwiftValor = sheet.getRow(contInicio).getCell(intNumberOfCellSwift_SortCode).getStringCellValue();
						fileLogOnline.InfoLog("nueva linea(tipo 2): strSwiftValor" + strSwiftValor, "_formula_XLSX");							
					}else{
						if(sheet.getRow(contInicio).getCell(intNumberOfCellSwift_SortCode).getCellType()==0){
//							strSwiftValor = String.valueOf(sheet.getRow(contInicio).getCell(intNumberOfCellSwift_SortCode).getNumericCellValue());
//							strSwiftValor = Double.toString(sheet.getRow(contInicio).getCell(intNumberOfCellSwift_SortCode).getNumericCellValue());
							int intValorSwitch = (int)sheet.getRow(contInicio).getCell(intNumberOfCellSwift_SortCode).getNumericCellValue();
							strSwiftValor = String.valueOf(intValorSwitch);
							fileLogOnline.InfoLog("nueva linea(tipo 0): strSwiftValor" + strSwiftValor, "_formula_XLSX");								
						}else{
							strSwiftValor = "";
							fileLogOnline.InfoLog("nueva linea(tipo ninguno): strSwiftValor" + strSwiftValor, "_formula_XLSX");								
						}												
					}					
				}
			} catch (Exception e) {
				strSwiftValor = "";
				strExcepcion = "Exception-xlsx_catch Swift. Linea Xlsx: " + contInicio;
				excepciones.InfoLog(strExcepcion);
				e.printStackTrace();
			}
//			if(strSwiftValor.equals(""))							textoInfo = textoInfo + " ***** Swift: No tiene valor.";
			if(strSwiftValor.equals(""))							textoInfo = textoInfo + " ***** " + sheet.getRow(0).getCell(intNumberOfCellSwift_SortCode).getStringCellValue() + " : No tiene valor.";

			fileLogOnline.InfoLog("sheet.getRow(contInicio).getCell(intNumberOfCellSwift_SortCode).getCellType()" + sheet.getRow(contInicio).getCell(intNumberOfCellSwift_SortCode).getCellType(), "_formula_XLSX");
			fileLogOnline.InfoLog("strSwiftValor" + strSwiftValor, "_formula_XLSX");			
//------------------------------------------------------------------------------------------------------------------------------
			fileLogOnline.InfoLog("XlsxGeneraArray --- procesarHoja --- 3", "_log_locked_");

			System.out.println("--- " + contInicio);
			System.out.println("---Email-------------- xlsx: " + strEmailValor);
			System.out.println("---IBAN-------------- xlsx: " + strIBANValor);
			System.out.println("---Swift-------------- xlsx: " + strSwiftValor);
			System.out.println("-------------textoInfo: " + textoInfo);
		
			//---- Si hay Info se debe escribir en el fichero LogDetalleCC.
			if (!textoInfo.equals("")){
				fileLogOnline.InfoLog(textoInfoEncabezamiento + textoInfo, "Xlsxfile");				
				arrayTextoLog.add(textoInfoEncabezamiento + textoInfo);			
			}else{
//				arrayResultado.add(sheet.getRow(contInicio).getCell(1).getStringCellValue().toString() + "<>" + 
//						sheet.getRow(contInicio).getCell(3).getStringCellValue()  + "<>"	+ 
//						sheet.getRow(contInicio).getCell(4).getStringCellValue());
				
				//'Limpiar' valor del email.
				//Por ejemplo, si es un hipervinculo de excel:  solo queremos la cuenta de correo.
				//HYPERLINK("mailto:ejemploDeEmail@smallworldfs.com","ejemploDeEmail@smallworldfs.com")----solo queremos-------> ejemploDeEmail@smallworldfs.com 

				strEmailValor = limpiarCuentaCorreo(strEmailValor);
				
				arrayResultado.add(strEmailValor + "<>" + 
						strIBANValor  + "<>"	+ 
						strSwiftValor);
			}
			
			System.out.println("fin " + contInicio + " *******************************************************************************************");			
		
			strEmailValor = strEmailValor.trim();
			strIBANValor = strIBANValor.trim();
			strSwiftValor = strSwiftValor.trim();
			if (strEmailValor.equals("") && strIBANValor.equals("") && strSwiftValor.equals("")){
				contRows=0;				
			}
		}

		fileLogOnline.InfoLog("XlsxGeneraArray --- procesarHoja --- arrayResultado size:(" + arrayResultado.size() + ")", "_log_locked_");

		//-----Mostrar arrayResultado
		for(int contEle = 0; contEle<arrayResultado.size();contEle++){
			fileLogOnline.InfoLog("XlsxGeneraArray --- procesarHoja --- arrayResultado contEle:(" + contEle + ")" + arrayResultado.get(contEle).toString(), "_log_locked_");
			System.out.println(" ******************************* arrayResultado: (" + contEle + ")" + arrayResultado.get(contEle).toString());
		}		
		//--------mostrado-------------------------------------------
		return arrayResultado;
    }

    public static String limpiarCuentaCorreo(String strEmail){
    	int intInicio=0;
    	int intFin=0;
    	
//    	strEmail="HYPERLINK(\"mailto:ejemploDeEmail@smallworldfs.com\",\"ejemploDeEmail1@smallworldfs.com\")";
    	
    	//PatternSyntaxException
    	//Si encuenta esa cadena continuamos 
    	System.out.println("->->->strEmail1: " + strEmail);

    	if (!(strEmail.lastIndexOf("\",\"")<0)){
    		intInicio = strEmail.lastIndexOf("\",\"") + 3;
    	
    		if (!(strEmail.lastIndexOf("\")")<0)){
        		intFin = strEmail.lastIndexOf("\")");
    			strEmail = strEmail.substring(intInicio, intFin);
    		}
    	}
    	System.out.println("->->->strEmail1: inicio(" + intInicio + ")fin(" + intFin + ")");
    	System.out.println("->->->strEmail1: " + strEmail);
   	
    	return strEmail;
    }


    public static void asignarPosicionesLiteralesCabecera(XSSFSheet sheet) throws IOException{
	//LogsFile_Online fileLogOnline = new LogsFile_Online();

		String strLiteral = "";
		int intNumeroColumnas = 50;

		try {
			if(sheet.getRow(0).getCell(0).getStringCellValue() == null){
				intNumeroColumnas = 0;			
			}
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			intNumeroColumnas = 0;			
			e.printStackTrace();
		}			
		
		//Escribir la cabecera.
		fileLogOnline.InfoLog("_head_", "Xlsxfile");					

		for(int cont = 0; cont<intNumeroColumnas; cont++){
			System.out.println("*********************************************************(" + sheet.getRow(0).getCell(cont).getStringCellValue() + ")*********************************************************************");
			System.out.println("1 >>>>>>>>>>>>>>>>>>>>>>> booleanos >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>cont: " + cont);
			System.out.println("2 >>22222>>>>>>>>>>>>>>>>>>>>> booleanos >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + sheet.getRow(0).getCell(cont).getStringCellValue().toString());

			//Escribir la cabecera.
			fileLogOnline.InfoLog("_head_" + cont + "_" + sheet.getRow(0).getCell(cont).getStringCellValue().toString() + "_", "Xlsxfile");
			fileLogOnline.InfoLog("_head_" + cont + "_tipo_" + sheet.getRow(1).getCell(cont).getCellType() + "_", "Xlsxfile");
			
    		strLiteral = sheet.getRow(0).getCell(cont).getStringCellValue().toString();
    		//Quitamos espacios.
			System.out.println("1a >>>>>>>>>>>>>>>>>>>>>>> booleanos >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    		strLiteral = strLiteral.trim();
			System.out.println("1b email: " + strConstantEmail + "-" + strLiteral);    		
			System.out.println("1b (" +strConstantEmail + ")(" + strLiteral + ")>>>>>>>>>>>>>>>>>>>>>>> booleanos >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");    		
    		if (strConstantEmail.toLowerCase().trim().equals(strLiteral.toLowerCase())){
    			booleanEncuentraLiteralEmail = true;
    			intNumberOfCellEmail = cont;
    		}
			System.out.println("1c (" +strConstantIban + ")(" + strLiteral + ")>>>>>>>>>>>>>>>>>>>>>>> booleanos >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");    		
    		if (strConstantIban.toLowerCase().trim().equals(strLiteral.toLowerCase())){
    			booleanEncuentraLiteralIban = true;
    			intNumberOfCellIBAN_AccountNumber = cont;
    		}
			System.out.println("1d (" +strConstantSwift + ")(" + strLiteral + ")>>>>>>>>>>>>>>>>>>>>>>> booleanos >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");  		
    		if (strConstantSwift.toLowerCase().trim().equals(strLiteral.toLowerCase())){
    			booleanEncuentraLiteralSwift = true;
    			intNumberOfCellSwift_SortCode = cont;
    		}
			System.out.println("1e (" +strConstantNumberAccount + ")(" + strLiteral + ")>>>>>>>>>>>>>>>>>>>>>>> booleanos >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");  		
    		if (strConstantNumberAccount.toLowerCase().trim().equals(strLiteral.toLowerCase())){
    			booleanEncuentraLiteralNumberAccount = true;
    			intNumberOfCellIBAN_AccountNumber = cont;
    		}
			System.out.println("1f (" +strConstantSortCode + ")(" + strLiteral + ")>>>>>>>>>>>>>>>>>>>>>>> booleanos >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");  		
    		if (strConstantSortCode.toLowerCase().trim().equals(strLiteral.toLowerCase())){
    			booleanEncuentraLiteralSortCode = true;
    			intNumberOfCellSwift_SortCode = cont;
    		}
			System.out.println(".");
			System.out.println("1g >>>>>>posiciones>>>>>>>>>>>>>>>>> booleanos >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			System.out.println("strLiteral:(" + strLiteral + ")intNumberOfCellEmail:(" +intNumberOfCellEmail + ")");
			System.out.println("strLiteral:(" + strLiteral + ")intNumberOfCellEmail:(" +intNumberOfCellIBAN_AccountNumber + ")");
			System.out.println("strLiteral:(" + strLiteral + ")intNumberOfCellEmail:(" +intNumberOfCellSwift_SortCode + ")");
			System.out.println("1g >>>>>>RESULTADO>>>>>>>>>>>>>>>>> booleanos >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");    	
			System.out.println(".");
			
			try {
				if(sheet.getRow(0).getCell(cont+1).getStringCellValue() == null){
					intNumeroColumnas = 0;			
				}
			} catch (NullPointerException e) {
				// TODO Auto-generated catch block
				intNumeroColumnas = 0;			
				e.printStackTrace();
			}
			System.out.println(booleanEncuentraLiteralEmail + "-"+  booleanEncuentraLiteralIban + "-" + booleanEncuentraLiteralSwift + "-" + booleanEncuentraLiteralNumberAccount + "-" + booleanEncuentraLiteralSortCode + "************************************************************************************************************************************************************************************");
			System.out.println("*************************************************************************************************************************************************");
    	}
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>> booleanos >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>> booleanos >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>> booleanos >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>> booleanos >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		if(booleanEncuentraLiteralEmail)			System.out.println(">>>>>>>>>>>>>>>>>>>>>>> booleanEncuentraLiteral email			----- TRUE");
		if(booleanEncuentraLiteralIban)				System.out.println(">>>>>>>>>>>>>>>>>>>>>>> booleanEncuentraLiteral iban			----- TRUE");
		if(booleanEncuentraLiteralSwift)			System.out.println(">>>>>>>>>>>>>>>>>>>>>>> booleanEncuentraLiteral Swift			----- TRUE");
		if(booleanEncuentraLiteralNumberAccount)	System.out.println(">>>>>>>>>>>>>>>>>>>>>>> booleanEncuentraLiteral NumberAccount	----- TRUE");
		if(booleanEncuentraLiteralSortCode)			System.out.println(">>>>>>>>>>>>>>>>>>>>>>> booleanEncuentraLiteral SortCode		----- TRUE");

		fileLogOnline.InfoLog("XlsxGeneraArray --- procesarHoja --- intNumberOfCellEmail:(" + intNumberOfCellEmail + ")", "_log_locked_");
		fileLogOnline.InfoLog("XlsxGeneraArray --- procesarHoja --- intNumberOfCellIBAN_AccountNumber:(" + intNumberOfCellIBAN_AccountNumber + ")", "_log_locked_");
		fileLogOnline.InfoLog("XlsxGeneraArray --- procesarHoja --- intNumberOfCellSwift_SortCode:(" + intNumberOfCellSwift_SortCode + ")", "_log_locked_");

    }
}
