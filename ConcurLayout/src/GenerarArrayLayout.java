
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class GenerarArrayLayout {

	public static final String separator = "\t";
	public static final String saltoLinea = "\r\n";
	public static final String sMonedaBGP = "GBP";
	public static final String sDebitAccountNumber = "GB07BARC20060540767670";
	private static LogsFile_Online fileLogOnline = new LogsFile_Online();
	
	public ArrayList<String> createArrayLayOut(ArrayList<String> arrayConcatenado, String strMoneda) throws IOException{

		//***** Escribir log ------------------------
		for(int contEle = 0;contEle<arrayConcatenado.size();contEle++){
			fileLogOnline.InfoLog("GenerarArrayLayout" + "_arrayConcatenado(in): " + arrayConcatenado.get(contEle).toString(), "00_CSVfile");		
		}	
		//*****	
		
		int sizeArrayConcatenado = arrayConcatenado.size();
		String sFila = "";
		String sMoneda = "";
		ArrayList<String> arrayLayout = new ArrayList<>();
				
		for(int cont = 0;cont<sizeArrayConcatenado;cont++){
			sFila = arrayConcatenado.get(cont).toString();
			
			sFila = sFila.replace("!!", "<>");
			sFila = sFila.replace("��", "<>");
			sFila = sFila.replace("&&", "<>");
			String[] splitFila = sFila.split("<>");

			sMoneda = splitFila[1].toString();
			sMoneda = sMoneda.toLowerCase();
			if (sMoneda.equals(strMoneda)){
				arrayLayout.add(arrayConcatenado.get(cont).toString());
			}
		}
		//---end For--------------------------------------------------------------		
		
		//***** Escribir log ------------------------
		for(int contEle = 0;contEle<arrayLayout.size();contEle++){
			fileLogOnline.InfoLog("GenerarArrayLayout" + "_arrayLayout(return): " + arrayLayout.get(contEle).toString(), "00_CSVfile");		
		}	
		//*****			
		return arrayLayout;
	}
}
