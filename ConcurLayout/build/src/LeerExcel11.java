import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
 
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

public class LeerExcel11 {
    // Variables declaration - do not modify                     
    private javax.swing.JButton btnElegir;
    private javax.swing.JButton btnAceptar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JTextField txtRuta;
	// End of variables declaration 

    @SuppressWarnings("deprecation")
    
    public LeerExcel11(){
    	
    }
    public static ArrayList<String> GenerarSalida(String sRuta) throws IOException{
		System.out.println(">>>>-------------- La ruta que llega es sRuta: " + sRuta);
		String sRutaModificada = sRuta.replace("\\", "\\\\");
		System.out.println(">>>>-------------- La ruta modificada. sRutaModificada: " + sRutaModificada);
//		FileInputStream file = new FileInputStream(new File("C:\\Work\\concur.xlsx"));
		FileInputStream file = new FileInputStream(new File(sRutaModificada));
		// Crear el objeto que tendra el libro de Excel
		XSSFWorkbook workbook = new XSSFWorkbook(file);
		/*
		 * Obtenemos la primera pesta�a a la que se quiera procesar indicando el indice.
		 */
		XSSFSheet sheet = workbook.getSheetAt(0);

		ArrayList<String> array1ValoresRow = new ArrayList<String>();

		double importeTotal = 0;
		
		boolean aceptarFila;

		int contInicio = 0;
		int contRows = sheet.getLastRowNum();

		String textoLogCelda0; 
		String textoLogCelda7; 
		String textoLogCelda8; 
		String textoLogCelda12; 
		String textoLog; 
		ArrayList<String> arrayTextoLog = new ArrayList<String>();
		
		System.out.println("************ Rows Excel: (" + contRows + ")*************************************************************");			

		//--------------- Inicio del for	-   para evaluar todos los rows del Excel ------------------
		for(contInicio=1;contInicio<=contRows;contInicio++){
			textoLogCelda0 = ""; 
			textoLogCelda7 = ""; 
			textoLogCelda8 = ""; 
			textoLogCelda12 = ""; 
			textoLog = ""; 

			aceptarFila = true;
		
			System.out.println("((((((((((((((contInicio(" + contInicio + ")))))))))))))))))))");
			
			if (sheet.getRow(contInicio).getCell(0) == null){
    			aceptarFila = false;    			
	    		//Escribir en un log.
    			textoLogCelda0 = "***** Valor de 'Report Entry Payment Type Name' es: null."; 
			}else{
				if (sheet.getRow(contInicio).getCell(0).toString().equals("Cash/Personal Card")){
					//Si obtenemos los valores: Moneda, Importe, Email
					//		entonces aceptamos la fila para tratarla posteriormente.
					if (sheet.getRow(contInicio).getCell(7) == null){
						aceptarFila = false;
		    			textoLogCelda7 = "***** Valor de 'Report Entry Currency Alpha Code' es: null";
						System.out.println("no se acepta el valor de fila: " + contInicio + "- celda 7");
						//Escribir en un log.
					}else{
						//--- El tipo de moneda debe ser alguno de los que est�n en el listado de monedas. ---------------------------------------
						int respTipoMoneda = 0;
						respTipoMoneda = controlarTipoMoneda(sheet.getRow(contInicio).getCell(7).toString());							
						if (respTipoMoneda==0){
			    			textoLogCelda7 = "***** (" + sheet.getRow(contInicio).getCell(7).toString() + ") Tipo de moneda no permitido.";
							aceptarFila = false;
						}
					}	
					if (sheet.getRow(contInicio).getCell(8) == null){
						aceptarFila = false;    			
		    			textoLogCelda8 = "***** Valor de 'Journal Amount' es: null";
						System.out.println("no se acepta el valor de fila: " + contInicio + "- celda 8");
						//Escribir en un log.
					}	
					if (sheet.getRow(contInicio).getCell(12) == null){
						aceptarFila = false;
		    			textoLogCelda12 = "***** Valor de 'Employee ID' es: null";
						System.out.println("**********************************************");
						System.out.println("**********************************************");
						System.out.println("**********************************************");
						System.out.println("**********************************************");
						System.out.println("**********************************************");
						System.out.println("no se acepta el valor de fila: " + contInicio + "- celda 12");
						//Escribir en un log.
					}else{
						if (sheet.getRow(contInicio).getCell(12).toString() == ""){
							aceptarFila = false;
			    			textoLogCelda12 = "***** 'Employee ID' no tiene valor";
							System.out.println("***************************else en blanco*******************");
							System.out.println("***************************else en blanco*******************");
							System.out.println("***************************else en blanco*******************");
							System.out.println("***************************else en blanco*******************");							
						}	
					}
				}else{
					aceptarFila = false;
	    			textoLogCelda0 = "***** Valor de 'Report Entry Payment Type Name' es distinto de: 'Cash/Personal Card'."; 
					
					System.out.println("-----No se acepta el valor de fila ---------------------------------------- Valor distinto de: 'Cash/Personal Card'.");
					//No grabamos en el log   por valor distinto de 'Cash/Personal Card'   en la celda: '0'.
				}	
			}
    		
    		System.out.println("************ CONDICION aceptarFilavalor************************************************************");			
    		
    		if (aceptarFila){   			
        		System.out.println("************ CONDICION aceptarFilavalor****por if***VALOR:(" + aceptarFila + ")*****************************************************");			

//	    		System.out.println("---WRITE LAYOUT------------ CORRECTO se ha aceptado la fila.");
//	    		System.out.println("--------------- CORRECTO se ha aceptado la fila.");
    			
    			String valorMoneda;
    			valorMoneda = sheet.getRow(contInicio).getCell(7).toString().toUpperCase();
    			
    			double valorImporte = 0;
    			valorImporte = Double.parseDouble(sheet.getRow(contInicio).getCell(8).toString());

    			String valorEmail;
    			valorEmail = sheet.getRow(contInicio).getCell(12).toString().toLowerCase();

    			importeTotal = importeTotal + valorImporte;
    			
    			String valorRow;
    			valorRow = valorEmail + "!!" + valorMoneda + "<>" + valorImporte;
//	    		System.out.println("------------------------------>><< VALOR del ROW correcto: " + valorRow);
    			array1ValoresRow.add(valorRow);
    			
    		}else{
        		System.out.println("************ CONDICION aceptarFilavalor****por else***VALOR:(" + aceptarFila + ")*****************************************************");			
    			
//	    		System.out.println("---WRITE ******* LOG ------------ CORRECTO se ha aceptado la fila.");
    			textoLog = textoLogCelda0 + textoLogCelda7 + textoLogCelda8 + textoLogCelda12;
    			arrayTextoLog.add(textoLog);
//	    		System.out.println("------------------------------>><< VALOR del ERROR ERROR ERROR : " + textoLog);
	    		//grabar textoLog
    			Logs wLog = new Logs(arrayTextoLog);
//    			wLog.write();

//    			LogsFile fileLog = new LogsFile();
//    			fileLog.InfoLog("operacionUno", "");
    			
//	    		System.out.println("--------------- No se ha aceptado la fila: ERROR:    " + textoLog);
    		}

			System.out.println("((((((((((((((textoLog(" + textoLog + ")))))))))))))))))))");
		}
		//--------------- FIN del for	-   para evaluar todos los rows del Excel ------------------
		//--En el array: array1ValoresRow  ---Tenemos todas las rows del Excel que deben grabarse en alg�n LayOut (el de EUR, GBP,...).
		//--En el array: arrayTextoLog    ---Tenemos todos los registros logs que se deben escribir en el fichero Log.
		
		
		
		
		
		
		int sizearray1ValoresRow = array1ValoresRow.size();
		//***** Mostrar valores rows validas------------------------
		System.out.println("********rows validas**** valor size: (" + sizearray1ValoresRow + ")*************************************************************");			
		for(int contEle = 0;contEle<sizearray1ValoresRow;contEle++){
			System.out.println("************ valor ele: (" + contEle + ")("+ array1ValoresRow.get(contEle) +")*************************************************************");			
		}	
		System.out.println("********fin rows validas**** valor size: (" + sizearray1ValoresRow + ")*************************************************************");
		//***** Mostrado       ------------------------
		//***** Mostrar registros log generados ------------------------
		System.out.println("********logs        **** valor size: (" + arrayTextoLog.size() + ")*************************************************************");			
		for(int contEle = 0;contEle<arrayTextoLog.size();contEle++){
			System.out.println("************ valor ele: (" + contEle + ")("+ arrayTextoLog.get(contEle) +")*************************************************************");			
		}	
		System.out.println("********fin logs        *****************************************************************");
		//***** Mostrado       ------------------------

		Collections.sort(array1ValoresRow);

		//***** Desde el array array1ValoresRow    se debe agrupar por email/moneda   y mostrar su importe acumulado.

		String valorGrupoEmailMoneda = "";
		String importeString = "";
		double importeNumerico = 0;
		double acumuladoImporte = 0;
		
		ArrayList<String> array2ResultadoParaLayout = new ArrayList<String>();
		
		String AnteriorEmail = "";
		String AnteriorMoneda = "";
		String AnteriorGrupo = "";

		for(int cont = 0;cont<sizearray1ValoresRow;cont++){
			
			String[] splitRow = array1ValoresRow.get(cont).split("<>");
			valorGrupoEmailMoneda = splitRow[0].toString();
			importeString = splitRow[1].toString();
			importeNumerico = Double.parseDouble(importeString);

			//Si el row leido es del mismo grupo que el anterior:   debemos acumular
			if (valorGrupoEmailMoneda.equals(AnteriorGrupo)){
				acumuladoImporte = acumuladoImporte + importeNumerico;
			}else{
			//Si el row leido        tiene un grupo diferente:
			//	se pregunta si ya no es el primer row.
				if(!AnteriorGrupo.equals("")){
					array2ResultadoParaLayout.add(AnteriorEmail + "!!" + AnteriorMoneda + "<>" + acumuladoImporte);
				}

				String[] splitGrupo = valorGrupoEmailMoneda.split("!!");

				AnteriorGrupo = valorGrupoEmailMoneda;
				AnteriorEmail =  splitGrupo[0].toString();
				AnteriorMoneda =  splitGrupo[1].toString();
				acumuladoImporte = importeNumerico;
			}
		}
		System.out.println("**************************************************************************** array2ResultadoParaLayout ********************<<<<<inicio>>>>>>");
		int sizearray2ResultadoParaLayout = array2ResultadoParaLayout.size();		
		
		for(int cont = 0;cont<sizearray2ResultadoParaLayout;cont++){
			System.out.println("*************** cont: " + cont + " (" + array2ResultadoParaLayout.get(cont) + ")");			
		}
		System.out.println("************************************************************************************************<<<<<fin>>>>>>");
		System.out.println("**************************************************************************** array LOGS ********************<<<<<inicio>>>>>>");
		for(int cont = 0;cont<arrayTextoLog.size();cont++){
			System.out.println("*************** cont LOGS: " + cont + " (" + arrayTextoLog.get(cont) + ")");			
		}
		System.out.println("************************************************************************************************<<<<<fin>>>>>>");

		//---- Grabar en el fichero Logs.
		LogsFile fileLog = new LogsFile();
		Logs wLog = new Logs(arrayTextoLog);

//		fileLog.InfoLog("operacionUno", "");
		fileLog.InfoLog(contRows, arrayTextoLog);
		
		// cerramos el libro excel
		workbook.close();
		
		return arrayTextoLog;
	}

	private static int controlarTipoMoneda (String tipo){

		tipo = tipo.toLowerCase();
		switch (tipo) {
			case "eur":
				System.out.println("Tipo de moneda: " + tipo);
				return 1;
			case "gbp":
				System.out.println("Tipo de moneda: " + tipo);
				return 2;
			default:
				return 0;
		}		
	}

}