
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class LogsFile {

	String dir;
	
	FileWriter archivo;
	//nuestro archivo log
	
	public void InfoLog(int numRows, ArrayList<String> logs) throws IOException{
	
   		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>************");
		System.out.println(">>>>>>>************************************************************************************************<<<<<inicio>>>>>>");

		int sizeLogs = logs.size();
		
		for(int cont = 0;cont<sizeLogs;cont++){
			System.out.println(">>>>>>>*************** cont LOGS: " + cont + " (" + logs.get(cont) + ")");
		}	
		System.out.println(">>>>>>>************************************************************************************************<<<<<fin>>>>>>");

		
	    Calendar cal = Calendar.getInstance();
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
	    String strDate = sdf.format(cal.getTime());
	    
		this.dir = dir;
		//Pregunta el archivo existe, caso contrario crea uno con el nombre log.txt
//		if (new File(dir+"//"+"logg21.txt").exists()==false){
		String FileName = "log_" + strDate + ".txt";
		if (new File(FileName).exists()==false){
	   		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>************ no existe fichero");
			archivo=new FileWriter(new File(FileName),false);
		}
		archivo = new FileWriter(new File(FileName), true);
		Calendar fechaActual = Calendar.getInstance(); //Para poder utilizar el paquete calendar
		//Empieza a escribir en el archivo
		archivo.write("["+(String.valueOf(fechaActual.get(Calendar.YEAR))
		+"/"+String.valueOf(fechaActual.get(Calendar.MONTH)+1)
		+"/"+String.valueOf(fechaActual.get(Calendar.DAY_OF_MONTH))
		+" "+String.valueOf(fechaActual.get(Calendar.HOUR_OF_DAY))
		+":"+String.valueOf(fechaActual.get(Calendar.MINUTE))
		+":"+String.valueOf(fechaActual.get(Calendar.SECOND)))+"]"+"[INFO]"+ "*********************** Total Filas en el Excel: " +numRows+ ". Total Filas no aceptadas: " +sizeLogs+"\r\n");

		for(int cont = 0;cont<sizeLogs;cont++){
			archivo.write("["+(String.valueOf(fechaActual.get(Calendar.YEAR))
					+"/"+String.valueOf(fechaActual.get(Calendar.MONTH)+1)
					+"/"+String.valueOf(fechaActual.get(Calendar.DAY_OF_MONTH))
					+" "+String.valueOf(fechaActual.get(Calendar.HOUR_OF_DAY))
					+":"+String.valueOf(fechaActual.get(Calendar.MINUTE))
					+":"+String.valueOf(fechaActual.get(Calendar.SECOND)))+"]"+"[INFO]"+ " [Fila no aceptada] Incidencias ocurridas: "+logs.get(cont)+"\r\n");
		}
		
		archivo.close(); //Se cierra el archivo
	}//Fin del metodo InfoLog
}	