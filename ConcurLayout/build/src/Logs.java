import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Logs {

	private ArrayList<String> regs = new ArrayList<String>();
	
	//	public static void main(String[] args) {  
	public Logs(ArrayList<String> logs) {
		this.setRegs(logs);
	}
		
	public void write() {  

	    Calendar cal = Calendar.getInstance();
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
	    String strDate = sdf.format(cal.getTime());
	    System.out.println("Fecha: "+strDate);
 
	    Logger logger = Logger.getLogger("MyLog");  
	    FileHandler fh;  
	        
	    try {  	
	        // This block configure the logger with handler and formatter  
	        fh = new FileHandler("C:/Work/ConcurLogFile_" + strDate + ".log");  
	        logger.addHandler(fh);
	        SimpleFormatter formatter = new SimpleFormatter();  
	        fh.setFormatter(formatter);  
	
	        // the following statement is used to log any messages  
		    Iterator itr = this.getRegs().iterator();
		      
		    while(itr.hasNext()) {
		    	Object element = itr.next();
			    logger.info("Log-Message---------------------------------------------------------------------- " + element.toString());
		        //System.out.print("*****-----(" + element + ")");
		    }	

	    } catch (SecurityException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }	    
	}
	
	public ArrayList<String> getRegs() {
		return regs;
	}

	public void setRegs(ArrayList<String> regs) {
		this.regs = regs;
	}
}