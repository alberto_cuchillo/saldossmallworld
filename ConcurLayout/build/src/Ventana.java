import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
 
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

//public class Ventana {
public class Ventana extends javax.swing.JFrame {
    // Variables declaration - do not modify                     
	private javax.swing.JButton btnSeleccionarConcur;
    private javax.swing.JButton btnSeleccionarDetalleCC;
    private javax.swing.JButton btnAceptarFicheros;

    private javax.swing.JLabel jLabelMensaje;

    private javax.swing.JLabel jLabelCabecera;
    
    private javax.swing.JLabel jLabelPedirDocumentoConcur;
    private javax.swing.JLabel jLabelPedirDocumentoDetalleCC;

    private javax.swing.JTextField txtRutaConcur;
    private javax.swing.JTextField txtRutaDetalleCC;

	ArrayList<String> arrayTest = new ArrayList<String>();    
    // End of variables declaration   
    
    @SuppressWarnings("deprecation")

//	public static void main(String args[]) throws IOException{
//		LeerExcel11.GenerarSalida("C:\\Work\\concur.xlsx");		
//    }

//***********************************************************************************************
	public static void main(String args[]) throws IOException{
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
 
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Ventana().setVisible(true);
            }
        });
    }

    public Ventana() {
        initComponents();
    }
 
    @SuppressWarnings("unchecked")
    private void initComponents() {
 
        txtRutaConcur = new javax.swing.JTextField();
        txtRutaDetalleCC = new javax.swing.JTextField();
        btnSeleccionarConcur = new javax.swing.JButton();
        btnSeleccionarDetalleCC = new javax.swing.JButton();

        btnAceptarFicheros = new javax.swing.JButton();

        jLabelPedirDocumentoConcur = new javax.swing.JLabel();
        jLabelPedirDocumentoDetalleCC = new javax.swing.JLabel();

        jLabelMensaje = new javax.swing.JLabel();
        jLabelCabecera = new javax.swing.JLabel();
 
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Concur. Seleccionar ruta de ficheros / Procesar.");
 
        txtRutaConcur.setEditable(false);
        txtRutaDetalleCC.setEditable(false);
 
        btnSeleccionarConcur.setText("Seleccionar...");
        btnSeleccionarDetalleCC.setText("Seleccionar...");

        btnAceptarFicheros.setText("Procesar ficheros");
        
        btnSeleccionarConcur.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSeleccionarConcurActionPerformed(evt);
            }
        });

        btnSeleccionarDetalleCC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSeleccionarDetalleCCActionPerformed(evt);
            }
        });

        btnAceptarFicheros.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {

				String mensaje = "";
				String mensajeConcur = "";
				String mensajeDetalleCC = "";

    			if(txtRutaConcur.getText().length()<1){
    				mensajeConcur = "  Concur.";
    			}
    			if(txtRutaDetalleCC.getText().length()<1){
    				mensajeDetalleCC = "  Detalle CC.";
    			}
    			
    			mensaje = mensajeConcur + mensajeDetalleCC;
    			
    			if (mensaje.equals("")){
    				jLabelMensaje.setText("");
                    //------------------------------------Ya ha seleccionado los 2 archivos.
        			btnAceptarFicherosActionPerformed(evt);
    			}else{
    				mensaje = "� Debe seleccionar los ficheros: " + mensajeConcur + mensajeDetalleCC + " !.";
    				jLabelMensaje.setText(mensaje);
    			}
            }
        });
        
        jLabelMensaje.setText("");
        jLabelCabecera.setText("Elige cada ruta pulsando en cada bot�n 'Seleccionar'.");
        jLabelPedirDocumentoConcur.setText("Excel - Concur: ");
        jLabelPedirDocumentoDetalleCC.setText("Excel - Detalle ccc pagos: ");
 
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(43, 43, 43)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelMensaje)
                    .addComponent(jLabelCabecera)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabelPedirDocumentoConcur)
                  		.addComponent(txtRutaConcur, javax.swing.GroupLayout.PREFERRED_SIZE, 306, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnSeleccionarConcur))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabelPedirDocumentoDetalleCC)
                        .addComponent(txtRutaDetalleCC, javax.swing.GroupLayout.PREFERRED_SIZE, 306, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnSeleccionarDetalleCC))                        
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(btnAceptarFicheros)))
                .addContainerGap(31, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(jLabelMensaje)
                .addComponent(jLabelCabecera) 
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelPedirDocumentoConcur)
               		.addComponent(txtRutaConcur, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSeleccionarConcur))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelPedirDocumentoDetalleCC)                  
                    .addComponent(txtRutaDetalleCC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSeleccionarDetalleCC))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAceptarFicheros))
                .addContainerGap(44, Short.MAX_VALUE))
        );
  
        pack();
    }// </editor-fold>                        

    private void btnSeleccionarConcurActionPerformed(java.awt.event.ActionEvent evt) {                                          
        
        //Creamos una instancia de JFileChooser
        JFileChooser fc=new JFileChooser();
         
        //Escribimos el nombre del titulo
        fc.setDialogTitle("Elige un fichero");
         
        //Indicamos que solo se puedan elegir ficheros
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
         
        //Creamos un filtro para JFileChooser
        FileNameExtensionFilter filtro = new FileNameExtensionFilter("*.xlsx", "xlsx");
        fc.setFileFilter(filtro);
         
        int eleccion=fc.showSaveDialog(this);
        
        
        if(eleccion==JFileChooser.APPROVE_OPTION){
            txtRutaConcur.setText(fc.getSelectedFile().getPath());
        }
    }                                         

    private void btnSeleccionarDetalleCCActionPerformed(java.awt.event.ActionEvent evt) {                                          
        
        //Creamos una instancia de JFileChooser
        JFileChooser fc=new JFileChooser();
         
        //Escribimos el nombre del titulo
        fc.setDialogTitle("Elige un fichero");
         
        //Indicamos que solo se puedan elegir ficheros
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
         
        //Creamos un filtro para JFileChooser
        FileNameExtensionFilter filtro = new FileNameExtensionFilter("*.xlsx", "xlsx");
        fc.setFileFilter(filtro);
         
        int eleccion=fc.showSaveDialog(this);
        
        
        if(eleccion==JFileChooser.APPROVE_OPTION){
            txtRutaDetalleCC.setText(fc.getSelectedFile().getPath());
        }
    }                                         

    private void btnAceptarFicherosActionPerformed(java.awt.event.ActionEvent evt) {

		String[] splitRutaArchivos = txtRutaConcur.getText().toString().split("\\\\");
		String rutaFinal = "";
    	int lengthSplitRutaArchivos = splitRutaArchivos.length;

    	for(int cont=0;cont<lengthSplitRutaArchivos-1;cont++){
    		rutaFinal = rutaFinal + splitRutaArchivos[cont] + "\\";
    	}

		try {
			arrayTest = LeerExcel11.GenerarSalida(txtRutaConcur.getText());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        jLabelMensaje.setText("Proceso realizado. Se ha generado el fichero de logs y los ficheros con formato por moneda.");
        jLabelCabecera.setText("Puede consultar el resultado en: " + rutaFinal);

        txtRutaConcur.setVisible(false);
        txtRutaDetalleCC.setVisible(false);
        btnSeleccionarConcur.setVisible(false);
        btnSeleccionarDetalleCC.setVisible(false);
        btnAceptarFicheros.setVisible(false);

    }
}

